rch = {
    "message": {
		"generalPurposeCard": {
			"content": {
				"media": {
					"height": "MEDIUM_HEIGHT",
					"mediaContentType": "image/jpg",
					"mediaUrl": "https://img-mm.manoramaonline.com/content/dam/mm/nri-news/europe/germany/images/2017/8/7/mosquito.jpg.image.784.410.jpg"
				    }
			    },
			"layout": {
				"cardOrientation": "HORIZONTAL",
				"imageAlignment": "LEFT"
				}
			}
		}
	}