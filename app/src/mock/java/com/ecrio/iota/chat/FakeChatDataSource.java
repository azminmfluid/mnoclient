package com.ecrio.iota.chat;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.chat.DataSource;
import com.ecrio.iota.db.tables.TableCompositionContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.enums.ConversationSender;
import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.enums.RichType;
import com.ecrio.iota.model.rich_response.ReplyData;
import com.ecrio.iota.model.rich_response.Reply;
import com.ecrio.iota.model.rich_response.Response;
import com.ecrio.iota.ui.chat.item.file_send.FileData;
import com.ecrio.iota.ui.chat.item.file_send.MFiles;
import com.ecrio.iota.ui.chat.model.CompositionMsg;
import com.ecrio.iota.ui.file.FileHelper;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.Utilities;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

public class FakeChatDataSource implements DataSource {

    private static FakeChatDataSource INSTANCE;

    public static FakeChatDataSource getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            INSTANCE = new FakeChatDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void startIMSession(String[] strings, int sessionType_oneToOne, int uriType_tel, int i, GetChatCallback callback) {
        // strings[0] = "+-7-  6556 00+00003";
        String peer = strings[0].replace("+", "").replaceAll("[^0-9]", "");
        Log.d2("SessionNumber", "no:" + peer);
        long contributionId = Calendar.getInstance().getTimeInMillis();


        Log.DebugApi("peer:-" + peer);
        Log.DebugApi("contribution ID:-" + contributionId);
        callback.onSuccess(String.valueOf(contributionId));
    }

    @Override
    public void saveMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {
        callback.onSuccess("" + System.currentTimeMillis());
    }

    @Override
    public void sendMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetChatCallback callback) {

        Log.d2("Notification_Data","{\"user_id\":"+conversationUserID+",\"user_cid\":"+conversationID+",\"user_status\":"+0+"}");
        callback.onSuccess("1");
//        ChatProvider.testNewUserMessageInsert(conversationPhNo);
        insertRichCarousalMessage(conversationPhNo);
        insertRichCardGeneralMessage(conversationPhNo);
        insertTextMessage(conversationPhNo);
        insertReplyMessage(conversationPhNo);
    }

    @Override
    public void saveReplyMessage(int conversationID, int conversationUserID, String newMessage, GetReplySaveCallback callback) {
        callback.onSuccess("" + System.currentTimeMillis());

    }

    @Override
    public void sendReplyMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetReplyCallback callback) {
        callback.onSuccess("1");
        ReplyData replyData = new ReplyData();
        Response response = new Response();
        response.setReply(new Gson().fromJson(newMessage, Reply.class));
        replyData.setResponse(response);
        String responseJson = new Gson().toJson(replyData);
        Log.DebugApi("Reply data " + responseJson);
        byte[] objAsBytes = responseJson.getBytes(StandardCharsets.UTF_8);
    }

    private void insertReplyMessage(String phone) {
        // The user number which send this message.
        String phoneNumber = phone;
        // The message that the user send.
        String Message = String.valueOf(Utilities.readString("response_rich_general.json"));
        // unique id of the this user.
        int conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(phoneNumber);
        // unique conversation id of the chat occurred with the user
        int ConversationID = AppBaseApplication.mDb.getConversationIDNew(conversationUserID);
        // Insert new conversation to "conversation reply" table

        CompositionMsg compositionMsg = new CompositionMsg();
        // setting userID.
        compositionMsg.setUserID(conversationUserID);
        // setting conversationID.
        compositionMsg.setConversationID(ConversationID);
        // setting status.
        compositionMsg.setCompositionState(Integer.parseInt("1"));
        // Insert new conversation to "conversation reply" table
//        YtsApplication.mDb.insertNewCompositionMessage(compositionMsg);

        final ContentResolver cr = AppBaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();

        values.put(TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID, compositionMsg.getConversationID());

        values.put(TableReplyContract.CompositionEntry.COLUMN_STATUS, compositionMsg.getCompositionState());

        values.put(TableReplyContract.CompositionEntry.COLUMN_USER_ID, compositionMsg.getUserID());

        values.put(TableReplyContract.CompositionEntry.COLUMN_MESSAGE, Message);

        Uri insert = cr.insert(TableReplyContract.CompositionEntry.CONTENT_URI_ALL_REPLIES, values);

    }

    private void insertTextMessage(String phone) {
        // The user number which send this message.
        String phoneNumber = phone;
        // The message that the user send.
        String Message = String.valueOf("Message~~" + Calendar.getInstance().getTimeInMillis());
        // unique id of the this user.
        int conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(phoneNumber);
        // unique conversation id of the chat occurred with the user
        int ConversationID = AppBaseApplication.mDb.getConversationIDNew(conversationUserID);
        // Insert new conversation to "conversation reply" table
        final ContentResolver cr = AppBaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, ConversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, Message);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.DELIVERED.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.IN.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID, 0);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "");
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "0");
        cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);
    }

    private void insertRichCarousalMessage(String phone) {
        // The user number which send this message.
        String phoneNumber = phone;
        // The message that the user send.
        String Message = String.valueOf("Message~~" + Calendar.getInstance().getTimeInMillis());
        try {
            String[] strings = new String[]{"response_rich_carousal.json", "rich3.json", "car_1.json"};
            int i = new Random().nextInt(2) + 1;
            JSONObject jsonObject = Utilities.readJson(strings[(i - 1)]);
            Message = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // unique id of the this user.
        int conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(phoneNumber);
        // unique conversation id of the chat occurred with the user
        int ConversationID = AppBaseApplication.mDb.getConversationIDNew(conversationUserID);
        // Insert new conversation to "conversation reply" table
        final ContentResolver cr = AppBaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, ConversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.RICH.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, Message);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.DELIVERED.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.IN.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID, 0);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "");
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "0");
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID, ConversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE, "" + RichType.CAROUSAL.ordinal());
        cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);
    }

    private void insertRichCardGeneralMessage(String phone) {
        // The user number which send this message.
        String phoneNumber = phone;
        // The message that the user send.

        String NotAJsonString = Utilities.readString("not_json.json");

        if (NotAJsonString.contains("}{")) {

            String splitText = "}\\{";

            String[] split = NotAJsonString.split(splitText);

            Log.d2("SPLITTED_JSOn", split[0]);
            Log.d2("SPLITTED_JSOn", split[1]);
        }
        String Message = String.valueOf("Message~~" + Calendar.getInstance().getTimeInMillis());
        try {
            String[] strings = new String[]{"rich5.json"};
            String[] stringsm = new String[]{"rich1.json",
                    "rich1.json",
                    "gen_1.json",
                    "gen_2.json",
                    "gen_3.json",
                    "rich2.json",
                    "response_rich_general_2.json"};
            int i = new Random().nextInt(strings.length) + 1;
            JSONObject jsonObject = Utilities.readJson(strings[i - 1]);
            Message = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // unique id of the this user.
        int conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(phoneNumber);
        // unique conversation id of the chat occurred with the user
        int ConversationID = AppBaseApplication.mDb.getConversationIDNew(conversationUserID);
        // Insert new conversation to "conversation reply" table
        final ContentResolver cr = AppBaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, ConversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.RICH.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, Message);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.DELIVERED.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.IN.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID, 0);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "");
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "0");
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID, ConversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE, "" + RichType.GENERAL.ordinal());
        cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);
    }

    @Override
    public void sendComposing(int conversationID, int type, GetChatCallback callback) {
        callback.onSuccess("1");
    }

    @Override
    public void saveMessageReadStatus(int conversationID, int conversationUserID, String msgID, GetChatCallback callback) {
        callback.onSuccess("1");
    }

    @Override
    public void sendMessageReadStatus(String conversationID, int status, int msgID, GetChatCallback callback) {
        callback.onSuccess("1");
    }

    @Override
    public void endIMSession(int conversationID, int conversationUserID, String conversationPhNo, GetChatCallback callback) {

    }
//
//    @Override
//    public void endIMSession(int conversationID, int conversationUserID, GetChatCallback callback) {
//        callback.onSuccess("1");
//    }

    @Override
    public void saveNewSession(String phoneNumber, int sessionID) {

    }

    @Override
    public void resetSession(String phoneNumber, int sessionID) {

    }

    @Override
    public void saveEstablishedSession(int sessionID) {

    }

    @Override
    public void deleteMessage(int conversationID, int conversationUserID, DeleteCallback callback) {
        callback.onSuccess();
    }

    @Override
    public void sendFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, SendFileCallback callback) {

        int h = Calendar.getInstance(Locale.UK).get(Calendar.HOUR);
        int s = Calendar.getInstance(Locale.UK).get(Calendar.SECOND);
        int ConversationFID = conversationID + h + s;

        Log.Debug("file Session ID is " + ConversationFID);
        FileData response = new FileData();
        response.setId(0);
        response.setFileName(FileHelper.getFileName(filePath));
        response.setFilePath(filePath);
        response.setSender(ConversationSender.OUT.ordinal());
        response.setSessionId("" + ConversationFID);
        response.setUserId(AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationID));
        response.setCompleted("0");

        Long internalFileSessionID = AppBaseApplication.mDb.insertFileSession(response);

        MFiles outgoingFileSessionDetailsNotCompleted = AppBaseApplication.mDb.getOutgoingFileSessionDetailsNotCompleted(ConversationFID);

        // do nothing if there is no file session saved.
        if (outgoingFileSessionDetailsNotCompleted == null) {

            Log.DebugFile("no record found in file table for conversationFileSessionID = " + ConversationFID);

        } else {
            Log.DebugFile("record found in file table for conversationFileSessionID = " + ConversationFID);
        }
        int value = internalFileSessionID.intValue();
        callback.onSuccess(value);
    }

    @Override
    public void sendQueuedFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {

        Log.DebugFile("sending queued file to server." + " fileID=" + fileID);

        callback.onFailed();
        if (true) return;
        int h = Calendar.getInstance(Locale.UK).get(Calendar.HOUR);
        int s = Calendar.getInstance(Locale.UK).get(Calendar.SECOND);
        int conversationFileSessionID = conversationID + h + (s * new Random().nextInt(9999));
        Log.DebugFile("new file session ID is " + conversationFileSessionID);
        MFiles outgoingFileSessionDetailsNotCompleted1 = AppBaseApplication.mDb.getOutgoingFileSessionDetailsNotCompleted(conversationFileSessionID);
        // checking if there is any records saved with the file session id.
        // do nothing for now if any records were found.
        if (outgoingFileSessionDetailsNotCompleted1 == null) {
            Log.DebugFile("no items found in file table for conversationFileSessionID = " + conversationFileSessionID);
        } else {
            Log.DebugFile("items found in file table for conversationFileSessionID = " + conversationFileSessionID);
        }
        int internalFileSessionID = AppBaseApplication.mDb.updateOutgoingFileProgress2(conversationUserID, "" + conversationFileSessionID, fileID);

        MFiles outgoingFileSessionDetailsNotCompleted = AppBaseApplication.mDb.getOutgoingFileSessionDetailsNotCompleted(conversationFileSessionID);
        // checking file record is updated with the new file session id.
        // do nothing if there is no file record saved.
        if (outgoingFileSessionDetailsNotCompleted == null) {
            Log.DebugFile("no items found in file table for conversationFileSessionID = " + conversationFileSessionID);
        } else {
            Log.DebugFile("items found in file table for conversationFileSessionID = " + conversationFileSessionID);
        }
        callback.onSuccess(internalFileSessionID);
    }

    @Override
    public void updateFailedFile(int conversationID, int conversationUserID, int msgID, UpdateQueueFileCallback callback) {

    }

    @Override
    public void updateInternalSession(String phoneNumber, String sessionID) {

    }

    @Override
    public void updateDeliveredMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback updateMsgCallback) {

    }

    @Override
    public void resetReply(int conversationID, int userID) {

    }

    @Override
    public void queueFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {

    }

    @Override
    public void queueFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {

    }

    @Override
    public void saveFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {

    }

    @Override
    public void queueMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

    }

    @Override
    public void queueReplyMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

    }

    @Override
    public void updateQueuedMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {

    }

    @Override
    public void updateQueuedReplyMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {

    }

    @Override
    public void updateFailedMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback callback) {

    }

    @Override
    public void updateQueuedFile(int conversationID, int conversationUserID, int fileID, int msgID, UpdateQueueFileCallback callback) {

    }

}