package com.ecrio.iota;

import android.content.Context;
import android.support.annotation.NonNull;

import com.ecrio.iota.chat.FakeChatDataSource;
import com.ecrio.iota.data.chat.ChatRepository;
import com.ecrio.iota.data.chat.local.ChatLocalDataSource;
import com.ecrio.iota.data.login.IotaRepository;
import com.ecrio.iota.data.login.local.IotaLocalDataSource;
import com.ecrio.iota.login.FakeLoginDataSource;
import com.ecrio.iota.data.main.MainRepository;
import com.ecrio.iota.data.main.local.MainLocalDataSource;
import com.ecrio.iota.data.main.remote.MainRemoteDataSource;

public class Injection {

    public static IotaRepository provideRepository(@NonNull Context context) {
//        checkNotNull(context);
        return IotaRepository.getInstance(FakeLoginDataSource.getInstance(context), IotaLocalDataSource.getInstance(context));
    }

    public static ChatRepository provideChatRepository(@NonNull Context context) {
//        checkNotNull(context);
        return ChatRepository.getInstance(FakeChatDataSource.getInstance(context), ChatLocalDataSource.getInstance(context));
    }

    public static MainRepository provideMainRepository(@NonNull Context context) {
//        checkNotNull(context);
        return MainRepository.getInstance(MainRemoteDataSource.getInstance(context), MainLocalDataSource.getInstance(context));
    }
}