package com.ecrio.iota.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.login.DataSource;
import com.ecrio.iota.model.LoginRequest;
import com.ecrio.iota.pref.Constants;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.utility.Log;

/**
 * Created by Mfluid on 5/16/2018.
 */

public class FakeLoginDataSource implements DataSource {

    private static FakeLoginDataSource INSTANCE;

    public static FakeLoginDataSource getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            INSTANCE = new FakeLoginDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void getCurrentUser(@NonNull GetUserCallback callback) {
        callback.onSuccess("1234", "password", "10.10.1.1", 7070);
    }

    @Override
    public void setRegistrationInvoked(boolean value) {

    }

    @Override
    public void quit() {

    }

    @Override
    public void register(LoginRequest request, @NonNull GetRegisterCallback callback) {
        UserInfo info = UserInfo.getInstance();
        info.setUser_name(request.getUsername());
        info.setPassword(request.getPassword());
        info.setStatus(1);
        info.saveCash(AppBaseApplication.context());
        info.readCash(AppBaseApplication.context());
        AppBaseApplication.myApi.setCredentials(info.getUser_name(), info.getUser_password());
        AppBaseApplication.mPreferences.edit().putString("my_phone",info.getUser_name()).apply();
        callback.onSuccess();
        Log.DebugApi("sending broadcast");
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(AppBaseApplication.context());
        // Create intent with action
        Intent localIntent = new Intent(Constants.ACTION_REGISTER);
        //already registered user.
        localIntent.putExtra(Constants.KEY_NAME_STATUS, true);
        // Send local broadcast
        localBroadcastManager.sendBroadcast(localIntent);
//        ChatProvider.testNewUserMessageInsert("14087770010");
    }
}