package com.ecrio.iota.db.tables;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */

/**
 * TableChatContract represents the contract for testing chats in the SQLite database.
 */
public final class TableChatContract {

    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_VIDEO = "video";
    public static final String SQL_CREATE_VIDEO_TABLE = "CREATE TABLE " + ChatEntry.TABLE_NAME +
            " (" +
            ChatEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            ChatEntry.COLUMN_1 + " TEXT UNIQUE NOT NULL, " +
            ChatEntry.COLUMN_2 + " TEXT NOT NULL, " + // Make the URL unique.
            ChatEntry.COLUMN_5 + " TEXT NOT NULL, " +
            " UNIQUE (" + ChatEntry.COLUMN_1 + ") ON CONFLICT REPLACE" +
            " ) ;";

    public static final class ChatEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_VIDEO).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_VIDEO;
        public static final String TABLE_NAME = "video";

        public static final String COLUMN_1 = "col_1";
        public static final String COLUMN_2 = "col_2";
        public static final String COLUMN_5 = "col_5";

        public static Uri buildVideoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}