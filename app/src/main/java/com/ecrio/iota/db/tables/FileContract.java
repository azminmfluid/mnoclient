package com.ecrio.iota.db.tables;

import android.provider.BaseColumns;

/**
 * FileContract represents the contract for storing file chats in the SQLite database.
 */
public final class FileContract{

	public static final String TAG = "FileContract";

	public static final String CREATE_TABLE = "CREATE TABLE " + FileEntry.TABLE_NAME
			+ " ("
			+ FileEntry._ID + " integer primary key on conflict replace, "
			+ FileEntry.FIELD_COMPLETED + " text, "
			+ FileEntry.FOLDER_ID + " integer, "
			+ FileEntry.FIELD_FILE_NAME + " text, "
			+ FileEntry.FIELD_FILE_PATH + " text, "
			+ FileEntry.FIELD_SESSION_ID + " text, "
			+ FileEntry.FIELD_USER_ID + " text, "
			+ FileEntry.FIELD_WHO + " text, "
			+ FileEntry.FIELD_PROGRESS + " text)";

	public static final class FileEntry implements BaseColumns {

		// Name of the conversation table.
		public static final String TABLE_NAME = "filetable";
		// The file id of the conversation.
		public static final String FOLDER_ID = "fileID";
		// The progress of file in the conversation.
		public static final String FIELD_PROGRESS = "progress";
		// The session id of the conversation.
		public static final String FIELD_SESSION_ID = "session_id";
		// The user id of the user in the conversation.
		public static final String FIELD_USER_ID = "user_id";
		// The name of the file send/received.
		public static final String FIELD_FILE_NAME = "f_name";
		// The path of the file saved in the device.
		public static final String FIELD_FILE_PATH = "f_path";
		// The owner of the file indicating sending or receiving.
		public static final String FIELD_WHO = "who";
		// This field indicate the file is completed or in progress.
		public static final String FIELD_COMPLETED = "completed";

	}

}