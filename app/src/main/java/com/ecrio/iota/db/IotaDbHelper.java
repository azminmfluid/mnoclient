package com.ecrio.iota.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ecrio.iota.db.tables.FileContract;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.db.tables.TableChatContract;
import com.ecrio.iota.db.tables.TableCompositionContract;
import com.ecrio.iota.db.tables.TableConversationGroupContract;
import com.ecrio.iota.db.tables.TableConversationMainContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableInternalSession;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.db.tables.TableRichCardGroupContract;
import com.ecrio.iota.db.tables.TableRichCardsContract;
import com.ecrio.iota.db.tables.TableUsersContract;

/**
 * IotaDbHelper manages the creation and upgrade of the database used in sbim implementations.
 */
public class IotaDbHelper extends SQLiteOpenHelper {

    // Change this when you change the database schema.
    private static final int DATABASE_VERSION = 1;

    // The name of our database.
    private static final String DATABASE_NAME = "iotachats.db";

    public IotaDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create a table to hold videos.

        // Do the creating of the databases.
        db.execSQL(TableChatContract.SQL_CREATE_VIDEO_TABLE);

        db.execSQL(TableConversationsContract.SQL_CREATE_CONVERSATION_MESSAGE_TABLE);

        db.execSQL(TableConversationMainContract.SQL_CREATE_CONVERSATION_TABLE);

        db.execSQL(TableConversationGroupContract.SQL_CREATE_CONVERSATION_GROUP_TABLE);

        db.execSQL(TableCompositionContract.SQL_CREATE_CONVERSATION_GROUP_TABLE);

        db.execSQL(TableReplyContract.SQL_CREATE_CONVERSATION_GROUP_TABLE);

        db.execSQL(TableUsersContract.SQL_CREATE_USER_TABLE);

        db.execSQL(FileContract.CREATE_TABLE);

        db.execSQL(TableInternalSession.SQL_CREATE_USER_TABLE);

        db.execSQL(TableRichCardGroupContract.SQL_CREATE_CONVERSATION_GROUP_TABLE);

        db.execSQL(TableRichCardsContract.SQL_CREATE_RICH_CARD_MESSAGE_TABLE);

        db.execSQL(TableBotUsersContract.SQL_CREATE_USER_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Simply discard all old data and start over when upgrading.
        db.execSQL("DROP TABLE IF EXISTS " + TableChatContract.ChatEntry.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Do the same thing as upgrading...
        onUpgrade(db, oldVersion, newVersion);
    }
}
