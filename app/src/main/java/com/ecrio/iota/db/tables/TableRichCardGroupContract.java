package com.ecrio.iota.db.tables;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */

/**
 * TableConversationGroupContract represents the contract for storing new conversation group in the SQLite database.
 */
public final class TableRichCardGroupContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";

    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // The content paths.
    public static final String PATH_CONVERSATION_GROUP = "chat_rich_group";

    // The content paths.
    public static final String PATH_USER = "c_group_user";


    public static final String SQL_CREATE_CONVERSATION_GROUP_TABLE =
            "CREATE TABLE " + GroupEntry.TABLE_NAME +
                    " (" +
                    GroupEntry.COLUMN_GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + // For internal conversation id.
                    GroupEntry.COLUMN_USER_ID + " INTEGER, " + // For saving user id.
                    GroupEntry.COLUMN_STATUS + " INTEGER, " + // For saving status.
                    GroupEntry.COLUMN_CONVERSATION_ID + " INTEGER " + // For saving contribution id.
                    " ) ;";


    public static final class GroupEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_GROUP).build();

        public static final Uri CONTENT_USER_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_CONVERSATION_GROUP;

        // Name of the conversation table.
        public static final String TABLE_NAME = "rich_card_group";
        // The id of the conversation.
        public static final String COLUMN_GROUP_ID = "rc_grp_id";
        // The user id of the conversation.
        public static final String COLUMN_USER_ID = "user_id_fk";
        // The session id of conversation created.
        public static final String COLUMN_CONVERSATION_ID = "c_id_fk";
        // The status of the conversation.
        public static final String COLUMN_STATUS = "g_status";

        // Returns the Uri referencing a conversation group with the specified id.
        public static Uri buildConvGroupUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);

        }
        // Returns the Uri referencing a conversation with the specified id.
        public static Uri buildUserUri(int id) {

            return ContentUris.withAppendedId(CONTENT_USER_URI, id);

        }
    }
}
