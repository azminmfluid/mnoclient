package com.ecrio.iota.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.widget.Toast;

import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.tables.FileContract;
import com.ecrio.iota.db.tables.TableCompositionContract;
import com.ecrio.iota.db.tables.TableConversationGroupContract;
import com.ecrio.iota.db.tables.TableConversationMainContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableInternalSession;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.db.tables.TableRichCardGroupContract;
import com.ecrio.iota.db.tables.TableRichCardsContract;
import com.ecrio.iota.db.tables.TableUsersContract;
import com.ecrio.iota.enums.ConversationSender;
import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.ui.chat.item.file_send.FileData;
import com.ecrio.iota.ui.chat.item.file_send.MFiles;
import com.ecrio.iota.ui.chat.model.CompositionMsg;
import com.ecrio.iota.ui.chat.model.Conversation;
import com.ecrio.iota.ui.chat.model.ConversationGroup;
import com.ecrio.iota.ui.chat.model.ConversationMsg;
import com.ecrio.iota.ui.chat.model.ConversationUser;
import com.ecrio.iota.ui.chat.model.InternalSession;
import com.ecrio.iota.ui.chat.model.RichCardSession;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */
public class MyDatabase {

    private static Context mContext;
    private static MyDatabase instance = null;
    public static IotaDbHelper mOpenHelper;
    private String TAG = "MoviesList";

    private MyDatabase(Context context) {
        mContext = context;
        mOpenHelper = new IotaDbHelper(context);
//        mOpenHelper.getReadableDatabase();
//        mOpenHelper.close();
    }

    public static synchronized MyDatabase getInstance(Context context) {
        if (null == instance) {
            instance = new MyDatabase(context);
            return instance;
        }

        return instance;
    }

    public static void deleteAll() {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        Db.delete(TableUsersContract.UserEntry.TABLE_NAME, null, null);
        Db.delete(TableCompositionContract.CompositionEntry.TABLE_NAME, null, null);
        Db.delete(TableReplyContract.CompositionEntry.TABLE_NAME, null, null);
        Db.delete(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, null);
        Db.delete(TableConversationMainContract.ConversationEntry.TABLE_NAME, null, null);
        Db.delete(TableConversationsContract.ChatEntry.TABLE_NAME, null, null);
    }
/*

    public int getUpdatedConversationID(int userID) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID);

        int conversationID = 0;

        // If there is no group for this userID, create new one.
        if (conversation == null) {


            Conversation conversations = new Conversation();
            // set status = 0.
            conversations.setMsgState(0);
            // set time to current time.
            conversations.setTime(System.currentTimeMillis());
            // Insert new item in the conversation table.
            insertConversation(conversations);// table conversation
            // newly created conversationID for creating conversation group.
            int conversationIDNew = getConversation().getConversationID();


            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;

        } else {

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public int getConversationID_UsingFSessionID(int userID) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID);

        int conversationID = 0;

        // If there is no group for this userID, create new one.
        if (conversation == null) {


            Conversation conversations = new Conversation();
            // set status = 0.
            conversations.setMsgState(0);
            // set time to current time.
            conversations.setTime(System.currentTimeMillis());
            // Insert new item in the conversation table.
            insertConversation(conversations);// table conversation
            // newly created conversationID for creating conversation group.
            int conversationIDNew = getConversation().getConversationID();


            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;

        } else {

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public int getConversationGroupUserID(int conversationID) {

        ConversationGroup group = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_SESSION_ID + "=?";

        String[] selectionArgs = new String[]{"" + conversationID};

        Cursor query = Db.query(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            group = ConversationGroup.parseDetails(query);

            query.close();

            return group.getUserID();
        }

        return -1;
    }

    public ConversationUser getUpdatedConversationUser(String number) {

        int userID = 0;

        ConversationUser conversationUser = null;

        conversationUser = getConversationUser(number);

        if (conversationUser == null) {

            ConversationUser newuser = new ConversationUser();

            newuser.setInternalID(number);

            insertConversationUser(newuser);

            conversationUser = getConversationUser(number);

        }

        return conversationUser;
    }

    public void updateConversationUserStatus(int userID, String status) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableUsersContract.UserEntry.COLUMN_STATUS, status);

        String where = TableUsersContract.UserEntry.COLUMN_CONVERSATION_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + userID};

        cr.update(TableUsersContract.UserEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

    }

    public int getUpdatedConversationIDNew(int userID, int conversationIDNew) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID, conversationIDNew);


        int conversationID = 0;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

//            Toast.makeText(mContext, "conversation == null", Toast.LENGTH_SHORT).show();

            Conversation conversations = new Conversation();
            // set status = 0.
            conversations.setMsgState(0);
            // set time to current time.
            conversations.setTime(System.currentTimeMillis());
            // Insert new item in the conversation table.
            insertConversation(conversations);// table conversation
            // newly created conversationID for creating conversation group.
//            int conversationIDNew = getConversation().getConversationID();


            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;

        } else {


            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public int getUpdatedConversationIDNewWithPHNo(int userID, int conversationIDNew) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID, conversationIDNew);


        int conversationID = 0;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

//            Toast.makeText(mContext, "conversation == null", Toast.LENGTH_SHORT).show();

            Conversation conversations = new Conversation();
            // set status = 0.
            conversations.setMsgState(0);
            // set time to current time.
            conversations.setTime(System.currentTimeMillis());
            // Insert new item in the conversation table.
            insertConversation(conversations);// table conversation
            // newly created conversationID for creating conversation group.
//            int conversationIDNew = getConversation().getConversationID();


            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;

        } else {


            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Updating new session to conversation group against this new userID.
            updateConversationGroup(conversationGroupNew);

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public void updateConversationGroup_BySessionID(ConversationGroup group) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_SESSION_ID, group.getConversationID());

        String where = TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + group.getUserID()};

        cr.update(TableConversationGroupContract.GroupEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

    }

    public int getConversationUserId(String number) {

        int userid = 0;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableUsersContract.UserEntry.COLUMN_USER_NUMBER + "=?";

        String[] selectionArgs = new String[]{"" + number};

        Cursor query = Db.query(TableUsersContract.UserEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            if (query.moveToFirst()) {

                userid = query.getInt(query.getColumnIndex(TableUsersContract.UserEntry.COLUMN_CONVERSATION_USER_ID));

                query.close();

            }

        }

        return userid;
    }

    public void updateConversationGroup(ConversationGroup group) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_SESSION_ID, group.getConversationID());

        String where = TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + group.getUserID()};

        cr.update(TableConversationGroupContract.GroupEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

    }

    public int getConversationID(int userID) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID);

        int conversationID = 0;

        // If there is no group for this userID, create new one.
        if (conversation == null) {


            Conversation conversations = new Conversation();
            // set status = 0.
            conversations.setMsgState(0);
            // set time to current time.
            conversations.setTime(System.currentTimeMillis());
            // Insert new item in the conversation table.
            insertConversation(conversations);// table conversation
            // newly created conversationID for creating conversation group.
            int conversationIDNew = getConversation().getConversationID();


            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;

        } else {

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

*/

//    =============================================

    public int getNewOrUpdatedConversationUserID(String number, String status) {

        int userID;

        ConversationUser conversationUser;

        // check whether the user/phoneNo is already saved.
        conversationUser = getConversationUser(number);

        if (conversationUser == null) {

            Log.DebugDB("insert/update user status(" + status + ") :" + " inserting new user");

            // create and inserting new details for the user/phoneNo.
            ConversationUser newuser = new ConversationUser();

            newuser.setPhoneNumber(number);

            newuser.setStatus(status);

            // inserting new user
            insertConversationUser(newuser);

            conversationUser = getConversationUser(number);

            // fetching user id of the user/phoneNo.
            userID = conversationUser.getUserID();

        } else {

            Log.DebugDB("insert/update user status(" + status + ") :" + " updating user (user already exist)");

            // fetching user id of the user/phoneNo.
            userID = conversationUser.getUserID();

            // updating the status of the user/phoneNo.
            updateConversationUserStatus(userID, status);

        }

        return userID;
    }

    public int updateConversationUsersToFailed(String status) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableUsersContract.UserEntry.COLUMN_STATUS, status);

        String where = null;

        String[] selectionArgs = null;

        int update = cr.update(TableUsersContract.UserEntry.CONTENT_URI_ALL_USERS, values, where, selectionArgs);

        return update;
    }

    public int getConversationUserID(String number) {

        int userID = -1;

        ConversationUser conversationUser = null;

        conversationUser = getConversationUser(number);

        userID = conversationUser.getUserID();

        return userID;
    }

    public String getConversationPhoneNumber(long userID) {

        ConversationUser user = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableUsersContract.UserEntry.COLUMN_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + userID};

        Cursor query = Db.query(TableUsersContract.UserEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            user = ConversationUser.parseDetails(query);

            query.close();

            return user.getPhoneNumber();
        }

        return null;
    }

    public int updateAllConversationGroup(int conversationID) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_STATUS, 0);

        String selection = TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID + " = ?";

        String[] selectionArgs = {"" + conversationID};

        return cr.update(TableConversationGroupContract.GroupEntry.CONTENT_URI, values, selection, selectionArgs);
    }

    public void insertNewConversationGroup(ConversationGroup group) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_USER_ID, group.getUserID());

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID, group.getConversationID());

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_STATUS, 1);

        cr.insert(TableConversationGroupContract.GroupEntry.CONTENT_URI, values);

    }

    public void updateConversationGroup(ConversationGroup group) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID, group.getConversationID());

        values.put(TableConversationGroupContract.GroupEntry.COLUMN_STATUS, 1);

        String where = TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + group.getUserID()};

        cr.update(TableConversationGroupContract.GroupEntry.CONTENT_URI, values, where, selectionArgs);

    }

    public int insertOrUpdateNewConversationID(int userID, int conversationIDNew) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup_ByUserID(userID);

        int conversationID;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

            Log.DebugDB("insert/update group status(" + conversationIDNew + ") :" + "inserting new conversation group");

            // disable all chats related with the same ID.
            updateAllConversationGroup(conversationIDNew);

            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;

        } else {

            Log.DebugDB("insert/update group status(" + conversationIDNew + ") :" + "updating conversation group (group already exist)");

            // disable all chats related with the same ID.
            updateAllConversationGroup(conversationIDNew);

            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDNew);
            // Updating new session to conversation group against this new userID.
            updateConversationGroup(conversationGroupNew);

            conversationID = conversationIDNew;
        }

        return conversationID;
    }

    public int insertOrUpdateNewConversationTempID(int userID, int conversationIDTemp) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID, conversationIDTemp);

        int conversationID = 0;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

            Log.d2("InsertingToDb", "insertOrUpdateNewConversationTempID :" + "inserting new conversation group");

            // disable all chats related with the same ID.
            updateAllConversationGroup(conversationIDTemp);

            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDTemp);
            // Inserting new item to conversation group against this new userID and conversationID.
            insertNewConversationGroup(conversationGroupNew);

            conversationID = conversationIDTemp;

        } else {

            Log.Debug("InsertingToDb", "insertOrUpdateNewConversationTempID :" + "conversation group already exist");

            // disable all chats related with the same ID.
            updateAllConversationGroup(conversationIDTemp);

            ConversationGroup conversationGroupNew = new ConversationGroup();
            // set user id of the new number.
            conversationGroupNew.setUserID(userID);
            // set conversationID to newly created ID.
            conversationGroupNew.setConversationID(conversationIDTemp);
            // Updating new session to conversation group against this new userID.
            updateConversationGroup(conversationGroupNew);

            conversationID = conversationIDTemp;
        }

        return conversationID;
    }

    public Uri insertNewTextMessages(ConversationMsg conversationMsg) {

        Log.d2("InsertingToDb", "insertNewTextMessages :" + conversationMsg.toString());

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = makeConversationValues(conversationMsg);

        Uri insert = cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        return insert;
    }

    /* related to message composition */
    public Uri insertNewCompositionMessage(CompositionMsg compositionMsg) {

        Log.d2("InsertingToDb", "insertNewCompositionMessage :" + compositionMsg.toString());

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = makeCompositionValues(compositionMsg);

        Uri insert = cr.insert(TableCompositionContract.CompositionEntry.CONTENT_URI_ALL_COMPOSITIONS, values);

        return insert;
    }

    private ContentValues makeCompositionValues(CompositionMsg compositionMsg) {

        ContentValues values = new ContentValues();

        values.put(TableCompositionContract.CompositionEntry.COLUMN_CONVERSATION_ID, compositionMsg.getConversationID());

        values.put(TableCompositionContract.CompositionEntry.COLUMN_STATUS, compositionMsg.getCompositionState());

        values.put(TableCompositionContract.CompositionEntry.COLUMN_USER_ID, compositionMsg.getUserID());

        return values;
    }

    public CompositionMsg getMessageComposition(long conversationID, int userID) {

        CompositionMsg compositionMsg = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableCompositionContract.CompositionEntry.COLUMN_CONVERSATION_ID + "=?" +
                " AND "
                + TableCompositionContract.CompositionEntry.COLUMN_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + conversationID, "" + userID};

        Cursor query = Db.query(TableCompositionContract.CompositionEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            compositionMsg = CompositionMsg.parseDetails(query);

            query.close();

            return compositionMsg;
        }

        return null;
    }

    public int updateAllComposition() {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableCompositionContract.CompositionEntry.COLUMN_STATUS, 0);

        String selection = TableCompositionContract.CompositionEntry.COLUMN_STATUS + "=?";

        String[] selectionArgs = new String[]{"" + 1};

        // updating the status to 0 on all rows that has status 1
        return cr.update(TableCompositionContract.CompositionEntry.CONTENT_URI_ALL_COMPOSITIONS, values, selection, selectionArgs);
    }

    /* --------------------------------- */
    public Uri insertNewFileMessage(ConversationMsg conversationMsg) {

        Log.d2("InsertingToDb", "insertNewFileMessage :" + conversationMsg.toString());

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = makeConversationValues(conversationMsg);

        Uri insert = cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        return insert;
    }

    public Uri insertNewInfoMessage(ConversationMsg conversationMsg) {

        Log.DebugDB("inserting info message :" + conversationMsg.toString());

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = makeConversationValues(conversationMsg);

        Uri insert = cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        return insert;
    }

    private ContentValues makeConversationValues(ConversationMsg conversationMsg) {

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationMsg.getUserID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, conversationMsg.getMessage());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID, conversationMsg.getReplyId());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "");

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, conversationMsg.getMsgState());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, conversationMsg.getTime());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationMsg.getConversationID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, conversationMsg.getConversationFileID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, conversationMsg.getConversationFileProgress());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, conversationMsg.getConversationType());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, conversationMsg.getConversationSender());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID, conversationMsg.getConversationRichCardID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE, conversationMsg.getRichType());

        if (conversationMsg.getFileThumb() != null)
            values.put(TableConversationsContract.ChatEntry.COLUMN_THUMB, conversationMsg.getFileThumb());

        return values;
    }

    public ConversationGroup getConversationGroup(int userid) {

        ConversationGroup group = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + userid};

        Cursor query = Db.query(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null) {

            group = ConversationGroup.parseDetails(query);

            query.close();
        }

        return group;
    }

    public ConversationGroup getConversationGroup(int userid, int conversationID) {

        ConversationGroup group = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + "=? AND " + TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID + "=?";

        String[] selectionArgs = new String[]{"" + userid, "" + conversationID};

        Cursor query = Db.query(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null) {

            group = ConversationGroup.parseDetails(query);

            query.close();
        }

        return group;
    }

    public ConversationGroup getConversationGroup_ByUserID(int userid) {

        ConversationGroup group = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + userid};

        Cursor query = Db.query(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null) {

            group = ConversationGroup.parseDetails(query);

            query.close();
        }

        return group;
    }

    public int getConversationGroupUserIDLatest(int conversationID) {

        Log.DebugDB("Looking for chat group that belongs to the session ID:" + conversationID);

        ConversationGroup group = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String SELECT_MAX_GROUP_ID = "SELECT MAX(" + TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID + ")";

        String SELECT_ONLY_CHAT_GROUP_WITH_SESSION_ID = SELECT_MAX_GROUP_ID
                + " FROM " + TableConversationGroupContract.GroupEntry.TABLE_NAME
                + " WHERE " + TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID + " = " + conversationID
                + " AND " + TableConversationGroupContract.GroupEntry.COLUMN_STATUS + " = " + 1;

        Log.DebugDB("Query is " + SELECT_ONLY_CHAT_GROUP_WITH_SESSION_ID);
//        Cursor mCursor = Db.rawQuery("SELECT * FROM "+TableConversationGroupContract.GroupEntry.TABLE_NAME+", Table2 " +
//                "WHERE Table1.id = Table2.id_table1 " +
//                "GROUP BY Table1.data1", null);

        Cursor query = Db.rawQuery(SELECT_ONLY_CHAT_GROUP_WITH_SESSION_ID, null);

        if (query != null && query.getCount() > 0) {

            group = ConversationGroup.parseDetails2(query);

            Log.DebugDB("group ID of conversation is " + group.getGroupID());
            query.close();

            String selection = TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID + "=? AND " + TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID + "=?";

            String[] selectionArgs = new String[]{"" + conversationID, "" + group.getGroupID()};

            Log.DebugDB("Looking for the user ID of group ");
            Cursor query2 = Db.query(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

            group = ConversationGroup.parseDetails3(query2);

            if (group != null) {
                Log.Debug("user ID of group is " + group.getUserID());
                query2.close();
                return group.getUserID();
            } else {
                Log.Error("group is not found ");
                return -1;
            }
        }

        Log.Error("No Group found....");
        return -1;
    }

    public int getConversationGroupUserID_UserID(int conversationUserID) {

        ConversationGroup group = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + "=?";

        String[] selectionArgs = new String[]{"" + conversationUserID};

        Cursor query = Db.query(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            group = ConversationGroup.parseDetails(query);

            query.close();

            return group.getUserID();
        }

        return -1;
    }

    public int getUpdatedConversationUserID(String phone) {

        int userID = 0;

        ConversationUser conversationUser = null;

        conversationUser = getConversationUser(phone);

        if (conversationUser == null) {

            ConversationUser new_user = new ConversationUser();

            new_user.setPhoneNumber(phone);

            new_user.setStatus("0");

            insertConversationUser(new_user);

//            ConversationGroup conversationGroupNew = new ConversationGroup();
//            // set user id of the new number.
//            conversationGroupNew.setUserID(userID);
//            // set conversationID to newly created ID.
//            conversationGroupNew.setConversationID(-1);
//            // Inserting new item to conversation group against this new userID and conversationID.
//            insertNewConversationGroup(conversationGroupNew);

            conversationUser = getConversationUser(phone);

            userID = conversationUser.getUserID();

            insertInfoMessage(userID, 0, "");//session started
        } else {

            userID = conversationUser.getUserID();

        }

        return userID;
    }
    private void insertInfoMessage(int conversationGroupUserID, int conversationID, String requested) {
        // creating new object for saving data to conversation table.
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        // set status of this user(phone no) to 0(requested) and return the user id.
        conversationMsg.setUserID(conversationGroupUserID);
        // setting conversationID.
        conversationMsg.setConversationID(conversationID);
        // setting the message.
        conversationMsg.setMessage(requested);
        conversationMsg.setReplyId("-1");
        // setting conversationFileID to -1.
        conversationMsg.setConversationFileID(-1);
        // setting progress to 0.
        conversationMsg.setConversationFileProgress("0%");
        // setting isOutGouing value to 0
        // 0-attendee
        // 1-myself
        conversationMsg.setConversationSender(ConversationSender.OUT.ordinal());
        // setting status value to 0
        // 0 - not delivered
        // 1 - delivered
        // 2 -composing
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // 0 - text message
        // 1 - file message
        // 2 - info message
        conversationMsg.setConversationType(ConversationType.INFO.ordinal());
        // Insert new conversation to "conversation reply" table
        insertNewInfoMessage(conversationMsg);
    }

    public int getUpdatedInternalRichCardID(int userID, int conversationID) {
//        int internalID;
//        RichCardSession session = null;
//        session = getRichCard(conversationID);
//        if (session == null) {
//            RichCardSession new_session = new RichCardSession();
//            new_session.setUserID(userID);
//            new_session.setSessionID(conversationID);
//            insertRichCardSession(new_session);
//            session = getRichCard(conversationID);
//            internalID = session.getInternalID();
//        } else {
//            internalID = session.getInternalID();
//        }
        return -1;
    }

    public int getUpdatedInternalSessionID(int userID, String conversationID) {

        int internalID;

        InternalSession conversationUser = null;

        conversationUser = getConversation(conversationID);

        if (conversationUser == null) {

            Log.DebugDB("Internal session table not found for conversation ID " + conversationID);
            Log.DebugDB("inserting new record");

            InternalSession new_user = new InternalSession();

            new_user.setUserID(userID);

            new_user.setSessionID(conversationID);

            insertInternalSession(new_user);

            conversationUser = getConversation(conversationID);

            internalID = conversationUser.getInternalID();

        } else {
            Log.DebugDB("Internal session table found for conversation ID " + conversationID);

            internalID = conversationUser.getInternalID();

        }

        return internalID;
    }

    public RichCardSession getRichCard(int sessionID) {
        RichCardSession session = null;
        final SQLiteDatabase DB = mOpenHelper.getReadableDatabase();
        String selection = TableRichCardGroupContract.GroupEntry.COLUMN_CONVERSATION_ID + "=?";
        String[] selectionArgs = new String[]{"" + sessionID};
        Cursor query = DB.query(TableRichCardGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        if (query != null && query.getCount() > 0) {
            session = RichCardSession.parseDetails(query);
            query.close();
        }
        return session;
    }

    public InternalSession getConversation(String sessionID) {

        InternalSession user = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableInternalSession.SessionEntry.COLUMN_SESSION_ID + "=?";

        String[] selectionArgs = new String[]{"" + sessionID};

        Cursor query = Db.query(TableInternalSession.SessionEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            user = InternalSession.parseDetails(query);

            query.close();
        }

        return user;
    }

    public void insertRichCardSession(RichCardSession session) {
        final ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableRichCardGroupContract.GroupEntry.COLUMN_CONVERSATION_ID, session.getSessionID());
        values.put(TableRichCardGroupContract.GroupEntry.COLUMN_USER_ID, session.getUserID());
        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = TableRichCardGroupContract.GroupEntry.COLUMN_USER_ID + "=?";
        String[] selectionArgs = new String[]{"" + session.getUserID()};
        Cursor query = Db.query(TableRichCardGroupContract.GroupEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        if (query == null || (!query.moveToFirst())) {
            try {
                cr.insert(TableRichCardGroupContract.GroupEntry.CONTENT_URI, values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            cr.update(TableRichCardGroupContract.GroupEntry.CONTENT_URI, values, selection, selectionArgs);
        }
        if (query != null) {
            query.close();
        }
    }

    public void insertInternalSession(InternalSession user) {

        Log.d2("InsertingToDb", "insertConversationUser :" + user.toString());

        final ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableInternalSession.SessionEntry.COLUMN_SESSION_ID, user.getSessionID());
        values.put(TableInternalSession.SessionEntry.COLUMN_5, user.getUserID());
//        values.put(TableUsersContract.UserEntry.COLUMN_USER_SESSION, user.getSessionID());
        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = TableInternalSession.SessionEntry.COLUMN_5 + "=?";
        String[] selectionArgs = new String[]{"" + user.getUserID()};
        Cursor query = Db.query(TableInternalSession.SessionEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        // Insert new number to the users table if there is not exist. Otherwise do nothing.
        if (query == null || (query != null && !query.moveToFirst())) {

            try {
                cr.insert(TableInternalSession.SessionEntry.CONTENT_URI, values);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            values.put(TableInternalSession.SessionEntry.COLUMN_1, user.getUserID());
            cr.update(TableInternalSession.SessionEntry.CONTENT_URI, values, selection, selectionArgs);
            query.close();
        }
    }

    public void updateConversationUserStatus(int userID, String status) {

        if(userID != -1) {
            final ContentResolver cr = mContext.getContentResolver();

            ContentValues values = new ContentValues();

            values.put(TableUsersContract.UserEntry.COLUMN_STATUS, status);

            String where = TableUsersContract.UserEntry.COLUMN_USER_ID + "=?";

            String[] selectionArgs = new String[]{"" + userID};

            cr.update(TableUsersContract.UserEntry.buildUserUri(userID), values, where, selectionArgs);
        }else{
            Log.Error("user id is -1");
        }
    }

    public int deleteConversationMessages(int sessionID, int userID) {
        final ContentResolver cr = mContext.getContentResolver();
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID + "=? AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=? AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED + "=?";
        String[] selectionArgs = new String[]{"" + sessionID, "" + userID, "" + 1};
        return cr.delete(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, where, selectionArgs);
    }

    public int getConversationIDNew(int userID) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID);

        int conversationID = -1;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

//            Toast.makeText(mContext, "conversation == null", Toast.LENGTH_SHORT).show();

        } else {

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public int getConversationGroupSID(int userID) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID);

        int conversationID = -1;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

//            Toast.makeText(mContext, "conversation == null", Toast.LENGTH_SHORT).show();

        } else {

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public int getConversationGroupSessionID(int userID) {

        // read existing group that has this userID from conversation group table.
        ConversationGroup conversation = getConversationGroup(userID);

        int conversationID = -1;

        // If there is no group for this userID, create new one.
        if (conversation == null) {

//            Toast.makeText(mContext, "conversation == null", Toast.LENGTH_SHORT).show();

        } else {

            conversationID = conversation.getConversationID();
        }

        return conversationID;
    }

    public void insertConversationUser(ConversationUser user) {

        Log.d2("InsertingToDb", "insertConversationUser :" + user.toString());

        final ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableUsersContract.UserEntry.COLUMN_PHONE, user.getPhoneNumber());
        values.put(TableUsersContract.UserEntry.COLUMN_STATUS, user.getStatus());

//        values.put(TableUsersContract.UserEntry.COLUMN_USER_SESSION, user.getSessionID());

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableUsersContract.UserEntry.COLUMN_PHONE + "=?";

        String[] selectionArgs = new String[]{"" + user.getPhoneNumber()};

        Cursor query = Db.query(TableUsersContract.UserEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        // Insert new number to the users table if there is not exist. Otherwise do nothing.
        if (query == null || (query != null && !query.moveToFirst())) {

            try {
                cr.insert(TableUsersContract.UserEntry.CONTENT_URI_CONVERSATION, values);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            query.close();
        }
    }

    public ConversationUser getConversationUser(String number) {

        ConversationUser user = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableUsersContract.UserEntry.COLUMN_PHONE + "=?";

        String[] selectionArgs = new String[]{"" + number};

        Cursor query = Db.query(TableUsersContract.UserEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (query != null && query.getCount() > 0) {

            user = ConversationUser.parseDetails(query);

            query.close();
        }

        return user;
    }

    public Conversation getConversation() {

        Conversation conversation = null;

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = null;

        String[] selectionArgs = null;

        String orderBy = TableConversationMainContract.ConversationEntry.COLUMN_CONVERSATION_ID + " DESC";

        Cursor query = Db.query(TableConversationMainContract.ConversationEntry.TABLE_NAME, null, selection, selectionArgs, null, null, orderBy);

        if (query != null && query.getCount() > 0) {

            conversation = Conversation.parseConversation(query);

            query.close();

        }

        return conversation;
    }

    public int getUnReadMessages(int sessionID, int userID) {

        final SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=?"
                + " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE + "!=?"
                + " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS + "=?";
        String[] selectionArgs = new String[]{"" + userID, "" + DeliveryStatus.READ.ordinal(), "" + ConversationSender.IN.ordinal()};
//        String selection = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID + "=?"
//                + " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=?"
//                + " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE + "=?";
//
//        String[] selectionArgs = new String[]{"" + sessionID, "" + userID, "" + DeliveryStatus.READ.ordinal()};

        String orderBy = null;

        Cursor query = Db.query(TableConversationsContract.ChatEntry.TABLE_NAME, null, selection, selectionArgs, null, null, orderBy);
//        String s = DatabaseUtils.dumpCursorToString(query);
//        Log.d2("SQLITE_CURSOR", s);
        int count = 0;

        if (query != null && query.getCount() > 0) {

            count = query.getCount();

            query.close();

        }

        return count;
    }

    public int[] getUserIDAndSessionIDFromMsgTable(int MsgID) {

        int conversationID = -1;

        int conversationUserID = -1;

        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();

        String selection = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";

        String[] selectionArgs = new String[]{"" + MsgID};

        Cursor query = Db.query(TableConversationsContract.ChatEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        MFiles parse = null;

        if (query != null) {

            if (query.moveToFirst()) {

                conversationID = query.getInt(query.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID));

                conversationUserID = query.getInt(query.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID));

            }
        }
        return new int[]{conversationUserID, conversationID};

    }

    public int updatedMessageStatusWithMsgIDToMsgTable(ConversationMsg conversationMsg, int MsgID) {

        Log.d2("InsertingToDb", "updatedMessageStatusWithMsgIDToMsgTable :" + conversationMsg.toString() + " MsgID==" + MsgID);

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, conversationMsg.getMsgState());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationMsg.getConversationID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationMsg.getUserID());

        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";

        String[] selectionArgs = new String[]{"" + MsgID};

        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

        if (update < 0) {

            Log.d2("InsertingToDb", "updatedMessageStatusWithMsgIDToMsgTable :" + "inserted......");

            values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, conversationMsg.getConversationFileID());

//            cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        } else {

            Log.d2("InsertingToDb", "updatedMessageStatusWithMsgIDToMsgTable :" + "updated......");

        }

        return update;
    }

    public int updatedMessageSelectedWithMsgIDToMsgTable(int userID, int conversationID, int MsgID, int selected) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED, selected);

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, userID);

        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";

        String[] selectionArgs = new String[]{"" + MsgID};

        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

        return update;
    }

    public int updatedAllMessageSelectedWithMsgIDToMsgTable(int userID) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED, 0);

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, userID);

        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=? AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED + "=?";

        String[] selectionArgs = new String[]{"" + userID, "" + 1};

        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

        return update;
    }

    public int updatedQueuedMessageStatusWithMsgIDToMsgTable(ConversationMsg conversationMsg, int MsgID) {

        Log.d2("InsertingToDb", "updatedQueuedMessageStatusWithMsgIDToMsgTable :" + conversationMsg.toString() + " MsgID==" + MsgID);

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, conversationMsg.getMsgState());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationMsg.getConversationID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationMsg.getUserID());

        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";

        String[] selectionArgs = new String[]{"" + MsgID};

        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_QUEUED_CONVERSATION_BY_USER, values, where, selectionArgs);

        if (update < 0) {

            Log.d2("InsertingToDb", "updatedQueuedMessageStatusWithMsgIDToMsgTable :" + "inserted......");

            values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, conversationMsg.getConversationFileID());

//            cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        } else {

            Log.d2("InsertingToDb", "updatedQueuedMessageStatusWithMsgIDToMsgTable :" + "updated......");

        }

        return update;
    }

    public int updatedMessageReadStatusWithReplyIDToMsgTable(ConversationMsg conversationMsg, String ReplyID) {

        Log.d2("InsertingToDb", "updatedMessageStatusWithMsgIDToMsgTable :" + conversationMsg.toString() + " ReplyID==" + ReplyID);

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, conversationMsg.getMsgState());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationMsg.getConversationID());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationMsg.getUserID());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID + "=?";
        String[] selectionArgs = new String[]{"" + ReplyID};

        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

        if (update < 0) {

            Log.d2("InsertingToDb", "updatedMessageStatusWithMsgIDToMsgTable :" + "inserted......");

//            values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, conversationMsg.getConversationFileID());

//            cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        } else {

            Log.d2("InsertingToDb", "updatedMessageStatusWithMsgIDToMsgTable :" + "updated......");

        }

        return update;
    }

    public int insertUpdatedConversationMessage(ConversationMsg conversationMsg) {

        Log.d2("InsertingToDb", "insertUpdatedConversationMessage :" + conversationMsg.toString());

        Log.d2("ConversationMessage", conversationMsg.toString());

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationMsg.getUserID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, conversationMsg.getMessage());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "");

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, conversationMsg.getMsgState());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, conversationMsg.getTime());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, conversationMsg.getConversationFileProgress());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationMsg.getConversationID());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, conversationMsg.getConversationType());

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, conversationMsg.getConversationSender());

        if (conversationMsg.getFileThumb() != null)
            values.put(TableConversationsContract.ChatEntry.COLUMN_THUMB, conversationMsg.getFileThumb());

        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID + "=? AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID + "=?";

        String[] selectionArgs = new String[]{"" + conversationMsg.getConversationID(), "" + conversationMsg.getConversationFileID()};

        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

        if (update < 0) {

            values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, conversationMsg.getConversationFileID());

            cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);

        }

        return update;
    }

    public void insertConversationMessageStatus(String status, int id) {

        final ContentResolver cr = mContext.getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, status);

        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID + "=?";

        String[] selectionArgs = new String[]{"" + id};

        cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values, where, selectionArgs);

    }

    public MFiles getDriverDetailNotCompleted(int userID, int id) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FOLDER_ID + "=? AND " + FileContract.FileEntry.FIELD_USER_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=?";
        String[] selectionArgs = new String[]{"" + id, "" + userID, "" + "0"};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getDriverDetailNotCompleted(int id) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=?";
        String[] selectionArgs = new String[]{"" + id, "" + "0"};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getOutgoingFileSessionDetailsNotCompleted(int id) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + id, "" + "0", "" + ConversationSender.OUT.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getIFileSessionDetailsNotCompleted(int fsID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + fsID, "" + "0", "" + ConversationSender.IN.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getIFileSessionDetailsCompleted(int sessionID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry._ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + sessionID, "" + "1", "" + ConversationSender.IN.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getOFileSessionDetailsCompleted(int fileInternalID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry._ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + fileInternalID, "" + "1", "" + ConversationSender.OUT.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getOFileSessionDetailsNotCompleted(int fileInternalID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry._ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + fileInternalID, "" + "0", "" + ConversationSender.OUT.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getIFileSessionDetailsNotCompleted(int userID, int sessionID, int folderID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FIELD_USER_ID + "=? AND " + FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=? AND " + FileContract.FileEntry.FOLDER_ID + "=?";
        String[] selectionArgs = new String[]{"" + userID, "" + sessionID, "" + "0", "" + ConversationSender.IN.ordinal(), "" + folderID};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getIFileSessionDetailsNotCompleted2(int fsID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + fsID, "" + "0", "" + ConversationSender.IN.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse(query);
        }
        return parse;

    }

    public MFiles getIncomingFileSessionDetails(int sessionID) {
        SQLiteDatabase Db = mOpenHelper.getReadableDatabase();
        String selection = FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FIELD_COMPLETED + "=? AND " + FileContract.FileEntry.FIELD_WHO + "=?";
        String[] selectionArgs = new String[]{"" + sessionID, "" + "0", "" + ConversationSender.IN.ordinal()};
        Cursor query = Db.query(FileContract.FileEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        MFiles parse = null;
        if (query != null) {
            parse = MFiles.parse2(query);
        }
        return parse;

    }

    public void clearData() {
        UserInfo info = UserInfo.getInstance();
        info.readCash(mContext);
        String prevUser = info.getPrevUser();
        boolean isPrevUser = prevUser.contentEquals(info.getUser_name());
        if (!isPrevUser) {
            SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
            Db.delete(FileContract.FileEntry.TABLE_NAME, null, null);
            Db.delete(TableRichCardGroupContract.GroupEntry.TABLE_NAME, null, null);
            Db.delete(TableRichCardsContract.ChatEntry.TABLE_NAME, null, null);
            Db.delete(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, null);
            Db.delete(TableConversationMainContract.ConversationEntry.TABLE_NAME, null, null);
            Db.delete(TableConversationsContract.ChatEntry.TABLE_NAME, null, null);
            Db.delete(TableInternalSession.SessionEntry.TABLE_NAME, null, null);
            Db.delete(TableCompositionContract.CompositionEntry.TABLE_NAME, null, null);
        }
    }

    public void clearDetail(int driverInternalId) {
        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        String whereClause1 = FileContract.FileEntry.FOLDER_ID + " =?";
        String[] whereArgs1 = new String[]{"" + driverInternalId};
        Db.delete(FileContract.FileEntry.TABLE_NAME, whereClause1, whereArgs1);
    }

    public long insertFileSession(FileData response) {
        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = makeFileSessionValues(response);
        long id = Db.insert(FileContract.FileEntry.TABLE_NAME, null, initialValues);
        return id;
    }

    private ContentValues makeFileSessionValues(FileData response) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FOLDER_ID, response.getId());
        initialValues.put(FileContract.FileEntry.FIELD_SESSION_ID, response.getSessionId());
        initialValues.put(FileContract.FileEntry.FIELD_USER_ID, "" + response.getUserId());
        initialValues.put(FileContract.FileEntry.FIELD_COMPLETED, response.getCompleted());
        initialValues.put(FileContract.FileEntry.FIELD_WHO, "" + response.getSender());
        initialValues.put(FileContract.FileEntry.FIELD_FILE_NAME, response.getFileName());
        initialValues.put(FileContract.FileEntry.FIELD_FILE_PATH, response.getFilePath());
        initialValues.put(FileContract.FileEntry.FIELD_PROGRESS, "");
        return initialValues;
    }

    public void insertDetailsToComplete(int id) {
        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_COMPLETED, 1);
        String whereClause = FileContract.FileEntry.FIELD_SESSION_ID + " =?";
        String[] whereArgs = new String[]{"" + id};
        Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
    }

    public int updateIncomingFileProgress(int userID, int fileSessionID, int folderID, String progress) {

        Log.d2("LOG_FILE_RECEIVED", "inserting value to db " + progress);

        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_PROGRESS, progress);
        String whereClause = FileContract.FileEntry.FOLDER_ID + "=? AND "
                + FileContract.FileEntry.FIELD_USER_ID + "=? AND "
                + FileContract.FileEntry.FIELD_SESSION_ID + "=? AND "
                + FileContract.FileEntry.FIELD_WHO + "=? AND "
                + FileContract.FileEntry.FIELD_COMPLETED + "=?";
        String[] whereArgs = new String[]{"" + folderID, "" + userID, "" + fileSessionID, "" + ConversationSender.IN.ordinal(), "0"};
        int update = Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
        return update;
    }

    public int updateOutgoingFileProgress(int userID, String fsID, int fID, String progress) {

        Log.d2("LOG_FILE_RECEIVED", "inserting value to db " + progress);

        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_PROGRESS, progress);
        String whereClause = FileContract.FileEntry.FOLDER_ID + "=? AND "
                + FileContract.FileEntry.FIELD_USER_ID + "=? AND "
                + FileContract.FileEntry.FIELD_SESSION_ID + "=? AND "
                + FileContract.FileEntry.FIELD_WHO + "=? AND "
                + FileContract.FileEntry.FIELD_COMPLETED + "=?";
        String[] whereArgs = new String[]{"" + fID, "" + userID, "" + fsID, "" + ConversationSender.OUT.ordinal(), "0"};
        int update = Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
        return update;
    }

    public int updateOutgoingFileProgress2(int userID, String fsID, int fID) {

        Log.DebugFile("inserting value to db ");

        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_SESSION_ID, fsID);
        String whereClause = FileContract.FileEntry._ID + "=? AND "
                + FileContract.FileEntry.FIELD_USER_ID + "=? AND "
                + FileContract.FileEntry.FIELD_WHO + "=? AND "
                + FileContract.FileEntry.FIELD_COMPLETED + "=?";
        String[] whereArgs = new String[]{"" + fID, "" + userID, "" + ConversationSender.OUT.ordinal(), "0"};
        int update = Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
        Log.DebugFile("inserting value to db ");
        return update;
    }

    public void updateOutgoingFileToComplete(int fsID) {
        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_COMPLETED, 1);
        String whereClause = FileContract.FileEntry.FIELD_SESSION_ID + " =? AND " + FileContract.FileEntry.FIELD_WHO + " =? AND " + FileContract.FileEntry.FIELD_COMPLETED + " =?";
        String[] whereArgs = new String[]{"" + fsID, "" + ConversationSender.OUT.ordinal(), "0"};
        Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
    }

    public void updateIncomingFileToComplete(int fsID) {
        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_COMPLETED, 1);
        String whereClause = FileContract.FileEntry.FIELD_SESSION_ID + " =? AND " + FileContract.FileEntry.FIELD_WHO + " =? AND " + FileContract.FileEntry.FIELD_COMPLETED + " =?";
        String[] whereArgs = new String[]{"" + fsID, "" + ConversationSender.IN.ordinal(), "0"};
        Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
    }

    public void updateFilePath(int userID, int sessionID, int folderID, String name, String progress) {

        Log.d2("LOG_FILE_RECEIVED", "inserting value to db " + progress);

        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(FileContract.FileEntry.FIELD_FILE_NAME, name);
        initialValues.put(FileContract.FileEntry.FIELD_PROGRESS, progress);
        String whereClause = FileContract.FileEntry.FIELD_USER_ID + "=? AND " + FileContract.FileEntry.FIELD_SESSION_ID + "=? AND " + FileContract.FileEntry.FOLDER_ID + "=?";
        String[] whereArgs = new String[]{"" + userID, "" + sessionID, "" + folderID};
        Db.update(FileContract.FileEntry.TABLE_NAME, initialValues, whereClause, whereArgs);
    }

    public void test(String select_chat_of_each_conversation_and_count) {
        Log.d2("LOG_FILE_RECEIVED", "query " + select_chat_of_each_conversation_and_count);
        SQLiteDatabase Db = mOpenHelper.getWritableDatabase();
        Cursor query = Db.rawQuery(select_chat_of_each_conversation_and_count, null);
        if (query != null) {
            if (query.moveToFirst())
                while (query.moveToNext()) {
                    Log.d2("LOG_FILE_RECEIVED", "size " + query.getCount());
                    String count = query.getString(query.getColumnIndex("Count"));
                    Log.d2("LOG_FILE_RECEIVED", "name is " + query.getString(query.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID)));
                    Log.d2("LOG_FILE_RECEIVED", "count is " + count);
                }
        }
    }
}