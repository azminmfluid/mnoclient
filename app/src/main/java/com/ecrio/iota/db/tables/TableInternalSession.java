package com.ecrio.iota.db.tables;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */
/**
 * TableInternalSession represents the contract for storing sessions in the SQLite database.
 */
public class TableInternalSession {
    public static final String SQL_CREATE_USER_TABLE =
            "CREATE TABLE " + SessionEntry.TABLE_NAME +
                    " (" +
                    SessionEntry.COLUMN_1 + " INTEGER PRIMARY KEY AUTOINCREMENT," + // For saving user id.
                    SessionEntry.COLUMN_SESSION_ID + " TEXT NOT NULL," + // For saving user status.
                    SessionEntry.COLUMN_5 + " TEXT NOT NULL " + // For saving user phone number.
                    " ) ;";

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";

    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // The content paths.
    public static final String PATH_CONVERSATION = "sessions";

    public static final class SessionEntry implements BaseColumns {

        // Uri for getting sessions.
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION).build();

        public static final String TABLE_NAME = "session";
        // The internal session id of the session.
        public static final String COLUMN_1 = "id";
        // The contribution id of the session.
        public static final String COLUMN_SESSION_ID = "session_id";
        // The id of user in the session.
        public static final String COLUMN_5 = "session_user_id";

        // Returns the Uri referencing a session with the specified contribution id.
        public static Uri buildVideoUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI, id);

        }
    }
}