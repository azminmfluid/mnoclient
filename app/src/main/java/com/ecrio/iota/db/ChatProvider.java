package com.ecrio.iota.db;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.db.tables.TableChatContract;
import com.ecrio.iota.db.tables.TableCompositionContract;
import com.ecrio.iota.db.tables.TableConversationGroupContract;
import com.ecrio.iota.db.tables.TableConversationMainContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableInternalSession;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.db.tables.TableRichCardGroupContract;
import com.ecrio.iota.db.tables.TableUsersContract;
import com.ecrio.iota.enums.ConversationSender;
import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by user on 2018.
 */
public class ChatProvider extends ContentProvider {


    private static ContentResolver mContentResolver;

    private static final int CHATS_TEST = 1;

    private static final int CHAT_WITH_USER = 2;

    private static final int USER = 3;

    private static final int CHATS = 4;

    private static final int CHATS_READ = 44;

    private static final int CHATS_CONVERSATION = 5;

    private static final int CCHATS_CONVERSATION_GROUPS = 6;

    private static final int CCHATS_CONVERSATIONS = 7;

    private static final int CCHATS_CONVERSATION_LIST = 8;

    private static final int INDIVIDUAL_USER = 9;

    private static final int CCHATS_CONVERSATION_BY_USER = 10;

    private static final int CCHATS_CONVERSATION_GROUP_USER = 11;

    private static final int COMPOSITIONS = 12;

    private static final int COMPOSITION_SESSION = 13;

    private static final int ALL_USER = 14;

    private static final int CHATS_CONVERSATION_BY_USER_QUEUED = 15;

    private static final int CHATS_CONVERSATION_ALL_QUEUED = 16;

    private static final int CHATS_UPDTAE_QUEUED_MESSAGES = 17;

    private static final int SESSION_INTERNAL = 18;

    private static final int RICH_INTERNAL = 19;

    private static final int BOT_CONTACTS = 20;
    private static final int BOT_CONTACTS2 = 200;
    private static final int BOT_CONTACT_USER = 21;
    private static final int INSERT_BOT_CONTACT_USER = 22;

    private static final int REPLIES = 23;

    private static final int REPLY_SESSION = 24;

    private static final int USERS = 25;

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private IotaDbHelper mOpenHelper;

    private static final SQLiteQueryBuilder sSingleConversationQueryBuilder, sCompositionQueryBuilder, sSuggestionQueryBuilder;

    private static final SQLiteQueryBuilder sSingleUserQueryBuilder;

    private static final SQLiteQueryBuilder sChatGroupQueryBuilder, sUserSessionQueryBuilder;

    private static final String[] sSingleConversationQueryColumns;

    private static final String[] sSingleUserQueryColumns;

    private static final SQLiteQueryBuilder sAllConversationQueryBuilder;

    private static final String[] sAllConversationQueryColumns;

    private static final String[] sChatGroupQueryColumns, sUserSessionQueryColumns;

    private static final HashMap<String, String> sColumnMap = buildColumnMap();

    private static final HashMap<String, String> sUserColumnMap = buildUserColumnMap();

    private static final HashMap<String, String> sGroupColumnMap = buildGroupColumnMap();

    private static final HashMap<String, String> sUserSessionColumnMap = buildUserSessionColumnMap();

    private static final String[] sCompositionQueryColumns, sSuggestionQueryColumns;

    static {

        String JOIN_MAIN = " JOIN " + TableConversationMainContract.ConversationEntry.TABLE_NAME + " cm";

        String JOIN_GROUP = " JOIN " + TableConversationGroupContract.GroupEntry.TABLE_NAME + " cg";

        String JOIN_USER = " JOIN " + TableUsersContract.UserEntry.TABLE_NAME + " usr";

        sSingleConversationQueryBuilder = new SQLiteQueryBuilder();
        sSingleConversationQueryBuilder.setTables(TableConversationsContract.ChatEntry.TABLE_NAME + " c ");
        sSingleConversationQueryBuilder.setProjectionMap(sColumnMap);
        sSingleConversationQueryColumns = new String[]{
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE,
                "c." + TableConversationsContract.ChatEntry.COLUMN_THUMB,
                "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID
        };

        sSingleUserQueryBuilder = new SQLiteQueryBuilder();
        sSingleUserQueryBuilder.setTables(TableUsersContract.UserEntry.TABLE_NAME);
        sSingleUserQueryBuilder.setProjectionMap(sUserColumnMap);
        sSingleUserQueryColumns = new String[]{
                TableUsersContract.UserEntry.COLUMN_USER_ID,
                TableUsersContract.UserEntry.COLUMN_PHONE,
                TableUsersContract.UserEntry.COLUMN_STATUS
        };
        // sql query builder for getting conversation list.
        sAllConversationQueryBuilder = new SQLiteQueryBuilder();

//        String SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION = "SELECT MAX(" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME + " GROUP BY " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID;
        String SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION = "SELECT MAX(" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME + " WHERE " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG + " IS NOT NULL AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG + " != '' AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID+"!=-1" + " GROUP BY " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID;

        sAllConversationQueryBuilder.setTables(
                TableConversationsContract.ChatEntry.TABLE_NAME
                        + " WHERE (" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + " IN " + "(" + SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION + ")" + ")");

//        String SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION = "SELECT MAX(" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME +" WHERE "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" IS NOT NULL AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" != ''"+ " GROUP BY " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID;
//        String SELECT_CHAT_OF_EACH_CONVERSATION_AND_COUNT = " (SELECT *,(SELECT count(" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME
//                +" WHERE "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" IS NOT NULL AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" != ''"
//                + " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=CE.c_user_id AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE+ "!=" + DeliveryStatus.READ.ordinal() + " "+") Count FROM " + TableConversationsContract.ChatEntry.TABLE_NAME +" as CE WHERE (" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + " IN " + "(" + SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION + ")" + ") tbl";
//
//        sAllConversationQueryBuilder.setTables(TableConversationsContract.ChatEntry.TABLE_NAME+ SELECT_CHAT_OF_EACH_CONVERSATION_AND_COUNT);

        sAllConversationQueryColumns = new String[]{
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE,
                TableConversationsContract.ChatEntry.COLUMN_THUMB,
                TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID
        };

        String SELECT_MAX_GROUP_ID = "SELECT MAX(" + TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID + ")";
        String FROM = " FROM " + TableConversationGroupContract.GroupEntry.TABLE_NAME + "";
        String WHERE = " WHERE " + TableConversationGroupContract.GroupEntry.COLUMN_STATUS + " = " + 1;

        // sql query builder for getting conversation list.
        sChatGroupQueryBuilder = new SQLiteQueryBuilder();
        sChatGroupQueryBuilder.setTables(TableConversationGroupContract.GroupEntry.TABLE_NAME + " WHERE (" + TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID + " IN " + "(" + SELECT_MAX_GROUP_ID + FROM + WHERE + ")" + ")");
        sChatGroupQueryColumns = new String[]{
                TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID,
                TableConversationGroupContract.GroupEntry.COLUMN_USER_ID,
                TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID,
                TableConversationGroupContract.GroupEntry.COLUMN_STATUS
        };

        // sql query builder for getting conversation list.
        sUserSessionQueryBuilder = new SQLiteQueryBuilder();
        sUserSessionQueryBuilder.setTables(TableConversationGroupContract.GroupEntry.TABLE_NAME);
        sUserSessionQueryColumns = new String[]{
                TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID,
                TableConversationGroupContract.GroupEntry.COLUMN_USER_ID,
                TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID,
                TableConversationGroupContract.GroupEntry.COLUMN_STATUS
        };

        sCompositionQueryBuilder = new SQLiteQueryBuilder();
        sCompositionQueryBuilder.setTables(TableCompositionContract.CompositionEntry.TABLE_NAME);
        sCompositionQueryColumns = new String[]{
                TableCompositionContract.CompositionEntry.COLUMN_ID,
                TableCompositionContract.CompositionEntry.COLUMN_CONVERSATION_ID,
                TableCompositionContract.CompositionEntry.COLUMN_STATUS,
                TableCompositionContract.CompositionEntry.COLUMN_USER_ID
        };
        sSuggestionQueryBuilder = new SQLiteQueryBuilder();
        sSuggestionQueryBuilder.setTables(TableReplyContract.CompositionEntry.TABLE_NAME);
        sSuggestionQueryColumns = new String[]{
                TableReplyContract.CompositionEntry.COLUMN_ID,
                TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID,
                TableReplyContract.CompositionEntry.COLUMN_STATUS,
                TableReplyContract.CompositionEntry.COLUMN_USER_ID,
                TableReplyContract.CompositionEntry.COLUMN_MESSAGE
        };
//        String SELECT_ONLY_CHAT_GROUP_WITH_SESSION_ID = SELECT_MAX_GROUP_ID + " FROM " + TableConversationGroupContract.GroupEntry.TABLE_NAME
//                + " WHERE " + TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID + " = " + conversationID
//                + " AND " + TableConversationGroupContract.GroupEntry.COLUMN_STATUS + " = " + 1;

    }

    //Add the query column
    private static HashMap<String, String> buildColumnMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_THUMB, "c." + TableConversationsContract.ChatEntry.COLUMN_THUMB);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID);
        map.put("c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE, "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE);
        return map;
    }

    //Add the query column
    private static HashMap<String, String> buildGroupColumnMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID, "cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_USER_ID);
        map.put("cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_USER_ID, "cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_USER_ID);
        map.put("cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID, "cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID);
        map.put("cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_STATUS, "cg_main." + TableConversationGroupContract.GroupEntry.COLUMN_STATUS);
        return map;
    }

    //Add the query column
    private static HashMap<String, String> buildUserSessionColumnMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put(TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID, TableConversationGroupContract.GroupEntry.COLUMN_USER_ID);
        map.put(TableConversationGroupContract.GroupEntry.COLUMN_USER_ID, TableConversationGroupContract.GroupEntry.COLUMN_USER_ID);
        map.put(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID, TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID);
        map.put(TableConversationGroupContract.GroupEntry.COLUMN_STATUS, TableConversationGroupContract.GroupEntry.COLUMN_STATUS);
        return map;
    }

    //Add the query column
    private static HashMap<String, String> buildUserColumnMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put(TableUsersContract.UserEntry.COLUMN_USER_ID, TableUsersContract.UserEntry.COLUMN_USER_ID);
        map.put(TableUsersContract.UserEntry.COLUMN_PHONE, TableUsersContract.UserEntry.COLUMN_PHONE);
        map.put(TableUsersContract.UserEntry.COLUMN_STATUS, TableUsersContract.UserEntry.COLUMN_STATUS);
        return map;
    }

    private Cursor getConversations(int conversationID) {

        String groupBy = null;
        String selection = "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID + " =?";
        String selection2 = " AND c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE + " !=?";
        String[] selectionArgs = new String[]{"" + conversationID, "" + 2};
        return sSingleConversationQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sSingleConversationQueryColumns,
                selection + selection2,
                selectionArgs,
                groupBy,
                null,
                null
        );
    }

    private Cursor getComposition(int conversationID) {

        String groupBy = null;
        String selection = TableCompositionContract.CompositionEntry.COLUMN_CONVERSATION_ID + " =?";
        String[] selectionArgs = new String[]{"" + conversationID};
        return sCompositionQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sCompositionQueryColumns,
                selection,
                selectionArgs,
                groupBy,
                null,
                null
        );
    }

    private Cursor getReply(int conversationID) {

        String groupBy = null;
        String selection = TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID + " =?";
        String[] selectionArgs = new String[]{"" + conversationID};
        return sSuggestionQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sSuggestionQueryColumns,
                selection,
                selectionArgs,
                groupBy,
                null,
                null
        );
    }

    private Cursor getUserSession(int userID) {
        String groupBy = null;
        String selection = TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + " =?";
        String[] selectionArgs = new String[]{"" + userID};
        return sUserSessionQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sUserSessionQueryColumns,
                selection,
                selectionArgs,
                groupBy,
                null,
                null
        );
    }

    private Cursor getConversationsByUserID(int conversationUserID) {

        String groupBy = null;

        String selection = "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + " =?";

        String selection2 = " AND c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE + " NOT IN ( ? )";

        String[] selectionArgs = new String[]{"" + conversationUserID, "" + ConversationType.IGNORE.ordinal()/*, "" + ConversationType.INFO.ordinal()*/};

        Cursor query = sSingleConversationQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sSingleConversationQueryColumns,
                selection + selection2,
                selectionArgs,
                groupBy,
                null,
                null
        );
//        String s = DatabaseUtils.dumpCursorToString(query);
//        com.ecrio.iota.utility.Log.d2("SQLITE_CURSOR", s);
        return query;
    }

    private Cursor getQueuedConversationsByUserID(int conversationUserID) {

        String groupBy = null;
        String selection = "c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + " =?";
        String selection2 = " AND c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE + " !=?";
        String selection3 = " AND c." + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE + " =?";
        String[] selectionArgs = new String[]{"" + conversationUserID, "" + ConversationType.IGNORE.ordinal(), "" + DeliveryStatus.QUEUED.ordinal()};
        return sSingleConversationQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sSingleConversationQueryColumns,
                selection + selection2 + selection3,
                selectionArgs,
                groupBy,
                null,
                null
        );
    }

    private Cursor getUserDetails(int userID) {


        String groupBy = null;
        String selection = TableUsersContract.UserEntry.COLUMN_USER_ID + " =?";
        String[] selectionArgs = new String[]{"" + userID};
        return sSingleUserQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sSingleUserQueryColumns,
                selection,
                selectionArgs,
                groupBy,
                null,
                null
        );
    }

    private Cursor getAllConversations() {

        String ORDER_BY = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + " DESC";
        return sAllConversationQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                sAllConversationQueryColumns,
                null,
                null,
                null,
                null,
                ORDER_BY
        );
    }

    @Override
    public boolean onCreate() {

        Context context = getContext();

        if(context == null) {

            context = AppBaseApplication.context();
        }

        mContentResolver = context.getContentResolver();

        mOpenHelper = new IotaDbHelper(context);

        return false;
    }

    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String authority = TableChatContract.CONTENT_AUTHORITY;

        // For each type of URI to add, create a corresponding code.
        matcher.addURI(authority, TableChatContract.PATH_VIDEO, CHATS_TEST);

        matcher.addURI(authority, TableChatContract.PATH_VIDEO + "/*", CHAT_WITH_USER);

        matcher.addURI(authority, TableUsersContract.PATH_CONVERSATION, USER);

        matcher.addURI(authority, TableInternalSession.PATH_CONVERSATION, SESSION_INTERNAL);

        matcher.addURI(authority, TableRichCardGroupContract.PATH_CONVERSATION_GROUP, RICH_INTERNAL);

        matcher.addURI(authority, TableUsersContract.PATH_ALL_USERS, ALL_USER);

        matcher.addURI(authority, TableUsersContract.PATH_INDIVIDUAL_USER + "/*", INDIVIDUAL_USER);

        matcher.addURI(authority, TableUsersContract.PATH_INDIVIDUAL_USER, USERS);

        matcher.addURI(authority, TableConversationGroupContract.PATH_CONVERSATION_GROUP, CCHATS_CONVERSATION_GROUPS);

        matcher.addURI(authority, TableConversationMainContract.PATH_CONVERSATION, CHATS_CONVERSATION);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_REPLY, CHATS);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_READ, CHATS_READ);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_LIST, CCHATS_CONVERSATION_LIST);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_BY_SESSION + "/*", CCHATS_CONVERSATIONS);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_BY_USER + "/*", CCHATS_CONVERSATION_BY_USER);

        matcher.addURI(authority, TableConversationGroupContract.PATH_USER + "/*", CCHATS_CONVERSATION_GROUP_USER);

        matcher.addURI(authority, TableCompositionContract.PATH_ALL_COMPOSITIONS, COMPOSITIONS);

        matcher.addURI(authority, TableReplyContract.PATH_ALL_REPLIES, REPLIES);

        matcher.addURI(authority, TableCompositionContract.PATH_COMPOSITION_SESSION + "/*", COMPOSITION_SESSION);

        matcher.addURI(authority, TableReplyContract.PATH_REPLY_SESSION + "/*", REPLY_SESSION);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_QUEUED + "/*", CHATS_CONVERSATION_BY_USER_QUEUED);

        matcher.addURI(authority, TableConversationsContract.PATH_CONVERSATION_QUEUED, CHATS_UPDTAE_QUEUED_MESSAGES);

        matcher.addURI(authority, TableConversationsContract.PATH_ALL_CONVERSATION_QUEUED, CHATS_CONVERSATION_ALL_QUEUED);

        matcher.addURI(authority, TableBotUsersContract.PATH_ALL_USERS, BOT_CONTACTS);

        matcher.addURI(authority, TableBotUsersContract.PATH_ALL_USERS + "/*", BOT_CONTACTS2);

        matcher.addURI(authority, TableBotUsersContract.PATH_INDIVIDUAL_USER + "/*", BOT_CONTACT_USER);

        matcher.addURI(authority, TableBotUsersContract.PATH_INDIVIDUAL_USER, INSERT_BOT_CONTACT_USER);
        return matcher;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {

            case CHATS_TEST: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableChatContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case USER: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableUsersContract.UserEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case BOT_CONTACTS: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableBotUsersContract.UserEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }
            case BOT_CONTACT_USER: {
                String userNo = uri.getPathSegments().get(1);
                String groupBy = null;
                String selection2 = TableBotUsersContract.UserEntry.COLUMN_PHONE + " =?";
                String[] selectionArgs2 = new String[]{"" + userNo};
                retCursor = mOpenHelper.getReadableDatabase().query(TableBotUsersContract.UserEntry.TABLE_NAME, projection, selection2, selectionArgs2, null, null, sortOrder);

                break;
            }

            case INDIVIDUAL_USER: {

                String userID = uri.getPathSegments().get(1);
                retCursor = getUserDetails(Integer.parseInt(userID));
//                retCursor = mOpenHelper.getReadableDatabase().query(TableUsersContract.UserEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case USERS: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableUsersContract.UserEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case CCHATS_CONVERSATION_GROUPS: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationGroupContract.GroupEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case CCHATS_CONVERSATION_GROUP_USER: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationGroupContract.GroupEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String s = uri.getPathSegments().get(1);

                retCursor = getUserSession(Integer.parseInt(s));

                break;
            }

            case CHATS_CONVERSATION: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationMainContract.ConversationEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case CHATS: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case CCHATS_CONVERSATIONS: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String s = uri.getPathSegments().get(1);

                retCursor = getConversations(Integer.parseInt(s));

                break;
            }

            case CCHATS_CONVERSATION_BY_USER: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String userID = uri.getPathSegments().get(1);

                retCursor = getConversationsByUserID(Integer.parseInt(userID));

                break;
            }

            case CHATS_CONVERSATION_BY_USER_QUEUED: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String userID = uri.getPathSegments().get(1);

                retCursor = getQueuedConversationsByUserID(Integer.parseInt(userID));

                break;
            }

            case CCHATS_CONVERSATION_LIST: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                retCursor = getAllConversations();

                break;
            }

            case COMPOSITIONS: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableCompositionContract.CompositionEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case REPLIES: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableReplyContract.CompositionEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }

            case COMPOSITION_SESSION: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String sID = uri.getPathSegments().get(1);

                int sessionID = Integer.parseInt(sID);

                retCursor = getComposition(sessionID);

                break;
            }

            case REPLY_SESSION: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String sID = uri.getPathSegments().get(1);

                int sessionID = Integer.parseInt(sID);

                retCursor = getReply(sessionID);

                break;
            }
            case INSERT_BOT_CONTACT_USER: {

                retCursor = mOpenHelper.getReadableDatabase().query(TableBotUsersContract.UserEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                break;
            }
            case BOT_CONTACTS2: {
                String userID = uri.getPathSegments().get(1);
                retCursor = mOpenHelper.getReadableDatabase().query(TableBotUsersContract.UserEntry.TABLE_NAME, projection, TableBotUsersContract.UserEntry.COLUMN_USER_ID+"=?", new String[]{userID}, null, null, sortOrder);

                break;
            }
            default: {

                throw new UnsupportedOperationException("Unknown uri: " + uri);

            }
        }

        retCursor.setNotificationUri(mContentResolver, uri);

        return retCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            // The application is querying the db for its own contents.
            case CHATS:
                return TableChatContract.ChatEntry.CONTENT_TYPE;

            // We aren't sure what is being asked of us.
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final Uri returnUri;
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case CHATS_TEST: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableChatContract.ChatEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableChatContract.ChatEntry.buildVideoUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case SESSION_INTERNAL: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableInternalSession.SessionEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableInternalSession.SessionEntry.buildVideoUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case USER: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableUsersContract.UserEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableUsersContract.UserEntry.buildConversationUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case BOT_CONTACTS: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableBotUsersContract.UserEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    mContentResolver.notifyChange(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS, null);

                    returnUri = TableBotUsersContract.UserEntry.buildUserUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case INSERT_BOT_CONTACT_USER: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableBotUsersContract.UserEntry.TABLE_NAME, null, values);

                com.ecrio.iota.utility.Log.d2("Log_Size_", _id+"");
                if (_id > 0) {

//                    returnUri = TableBotUsersContract.UserEntry.buildUserUri(_id);

                    returnUri = TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS;

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case CCHATS_CONVERSATION_GROUPS: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableConversationGroupContract.GroupEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableConversationGroupContract.GroupEntry.buildConvGroupUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case CHATS_CONVERSATION: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableConversationMainContract.ConversationEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableConversationMainContract.ConversationEntry.buildConversationUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case CHATS: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableConversationsContract.ChatEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(_id);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.CONTENT_CHATLIST_URI, null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case CHATS_CONVERSATION_ALL_QUEUED: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                long _id = db.insert(TableConversationsContract.ChatEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(_id);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildQueuedChatsWithUserIDUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.CONTENT_CHATLIST_URI, null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case COMPOSITIONS: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                String conversationID = values.getAsString(TableCompositionContract.CompositionEntry.COLUMN_CONVERSATION_ID);

                long _id = db.insert(TableCompositionContract.CompositionEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableCompositionContract.CompositionEntry.buildCompositionUri(_id);

                    Uri sessionUri = TableCompositionContract.CompositionEntry.buildSessionUri(Integer.parseInt(conversationID));

                    mContentResolver.notifyChange(sessionUri, null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case REPLIES: {

                SQLiteDatabase db = mOpenHelper.getWritableDatabase();

                String conversationID = values.getAsString(TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID);

                long _id = db.insert(TableReplyContract.CompositionEntry.TABLE_NAME, null, values);

                if (_id > 0) {

                    returnUri = TableReplyContract.CompositionEntry.buildReplyUri(_id);

                    Uri sessionUri = TableReplyContract.CompositionEntry.buildSessionUri(Integer.parseInt(conversationID));

                    mContentResolver.notifyChange(sessionUri, null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case RICH_INTERNAL:
                SQLiteDatabase db = mOpenHelper.getWritableDatabase();
                long _id = db.insert(TableRichCardGroupContract.GroupEntry.TABLE_NAME, null, values);
                if (_id > 0) {

                    returnUri = TableRichCardGroupContract.GroupEntry.buildConvGroupUri(_id);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            default: {

                throw new UnsupportedOperationException("Unknown uri: " + uri);

            }
        }

        mContentResolver.notifyChange(uri, null);

        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {

        final int rowsDeleted;

        if (selection == null) {

            throw new UnsupportedOperationException("Cannot delete without selection specified.");

        }

        switch (sUriMatcher.match(uri)) {

            case CHATS: {

                rowsDeleted = mOpenHelper.getWritableDatabase().delete(TableConversationsContract.ChatEntry.TABLE_NAME, selection, selectionArgs);

                if (selectionArgs.length > 0 && selectionArgs[0] != null)
                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsUri(Integer.parseInt(selectionArgs[0])), null);
                if (selectionArgs.length > 1 && selectionArgs[1] != null)
                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(Integer.parseInt(selectionArgs[1])), null);

                break;

            }

            case BOT_CONTACTS2: {

                rowsDeleted = mOpenHelper.getWritableDatabase().delete(TableBotUsersContract.UserEntry.TABLE_NAME, selection, selectionArgs);

                if (rowsDeleted > 0) {
                    com.ecrio.iota.utility.Log.Debug("Deleted", "...........[success]" + selectionArgs[0]);
//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);
//                    mContentResolver.notifyChange(TableBotUsersContract.UserEntry.buildUserUri(Integer.parseInt(values.getAsString(TableBotUsersContract.UserEntry.COLUMN_USER_ID))), null);
//                    mContentResolver.notifyChange(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS, null);
                } else {
                    com.ecrio.iota.utility.Log.Debug("Deleted", "...........[failed]" + selectionArgs[0]);
                    throw new SQLException("Failed to delete row" + uri);

                }
                break;
            }
            case CCHATS_CONVERSATION_BY_USER: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationsContract.ChatEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String userID = uri.getPathSegments().get(1);

                rowsDeleted = mOpenHelper.getWritableDatabase().delete(TableConversationsContract.ChatEntry.TABLE_NAME, selection, selectionArgs);

                break;
            }
            case CCHATS_CONVERSATION_GROUP_USER: {

//                retCursor = mOpenHelper.getReadableDatabase().query(TableConversationGroupContract.GroupEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

                String s = uri.getPathSegments().get(1);

                rowsDeleted = mOpenHelper.getWritableDatabase().delete(TableConversationGroupContract.GroupEntry.TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted > 0) {
                } else {

                    throw new SQLException("Failed to delete row " + uri);

                }
                break;
            }
            case REPLIES: {

                rowsDeleted = mOpenHelper.getWritableDatabase().delete(TableReplyContract.CompositionEntry.TABLE_NAME, selection, selectionArgs);

                break;
            }
            default: {

                throw new UnsupportedOperationException("Unknown uri: " + uri);

            }
        }

        if (rowsDeleted != 0) {

            mContentResolver.notifyChange(uri, null);

        }

        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final int rowsUpdated;

        switch (sUriMatcher.match(uri)) {

            case CHATS: {

                Log.v("Log_Update", "called update ...........");
                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableConversationsContract.ChatEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    if (values.containsKey(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))
                        mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID))), null);

                } else {

//                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case CHATS_READ: {

                Log.v("Log_Update", "called update ...........");
                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableConversationsContract.ChatEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {
                    if (values.containsKey(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))
                        mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))), null);
                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.CONTENT_CHATLIST_URI, null);
                } else {
//                    throw new SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case CHATS_UPDTAE_QUEUED_MESSAGES: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(
                        TableConversationsContract.ChatEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID))), null);

                    mContentResolver.notifyChange(TableConversationsContract.ChatEntry.buildQueuedChatsWithUserIDUri(Integer.parseInt(values.getAsString(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID))), null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case USER: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableUsersContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableUsersContract.UserEntry.buildUserUri(Integer.parseInt(values.getAsString(TableUsersContract.UserEntry.COLUMN_USER_ID))), null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case BOT_CONTACTS: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableBotUsersContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);

                if(rowsUpdated > 0) {
                    mContentResolver.notifyChange(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS, null);
                }
                break;
            }
            case BOT_CONTACT_USER: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableBotUsersContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);
                    mContentResolver.notifyChange(TableBotUsersContract.UserEntry.buildUserUri(Integer.parseInt(values.getAsString(TableBotUsersContract.UserEntry.COLUMN_USER_ID))), null);
                    mContentResolver.notifyChange(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS, null);
                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case SESSION_INTERNAL: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableInternalSession.SessionEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableInternalSession.SessionEntry.buildVideoUri(rowsUpdated), null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case ALL_USER: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableUsersContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableUsersContract.UserEntry.CONTENT_URI_ALL_USERS, null);

                }
                break;
            }

            case INDIVIDUAL_USER: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableUsersContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableUsersContract.UserEntry.buildUserUri(Integer.parseInt(selectionArgs[0])), null);

                } else {

                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }
            case CCHATS_CONVERSATION_GROUPS: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableConversationGroupContract.GroupEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

                    mContentResolver.notifyChange(TableConversationGroupContract.GroupEntry.buildUserUri(Integer.parseInt(selectionArgs[0])), null);

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

//                    mContentResolver.notifyChange(TableUsersContract.UserEntry.buildUserUri(Integer.parseInt(selectionArgs[0])), null);

                } else {

//                    throw new SQLException("Failed to insert row into " + uri);

                }
                break;
            }

            case COMPOSITIONS: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableCompositionContract.CompositionEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableCompositionContract.CompositionEntry.CONTENT_URI_ALL_COMPOSITIONS, null);

                }
                break;
            }

            case REPLIES: {

                rowsUpdated = mOpenHelper.getWritableDatabase().update(TableReplyContract.CompositionEntry.TABLE_NAME, values, selection, selectionArgs);

                if (rowsUpdated > 0) {

//                    returnUri = TableConversationsContract.ChatEntry.buildChatsUri(rowsUpdated);

                    mContentResolver.notifyChange(TableReplyContract.CompositionEntry.CONTENT_URI_ALL_REPLIES, null);

                }
                break;
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }

        if (rowsUpdated != 0) {

            mContentResolver.notifyChange(uri, null);
        }

        return rowsUpdated;
    }

    public static void testBulkInsert() {

    }

    public static void testNewUserMessageInsert(String phone) {

        // The user number which send this message.
        String phoneNumber = phone;

        // The message that the user send.
        String Message = String.valueOf("Message~~" + Calendar.getInstance().getTimeInMillis());

        // unique id of the this user.
        int conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(phoneNumber);

        // unique conversation id of the chat occurred with the user
        int ConversationID = AppBaseApplication.mDb.getConversationIDNew(conversationUserID);

        // setting sender value to 0
        // 0-attendee
        // 1-sender
        // setting status value to 0
        // 0 - attendee
        // 1 - sender
        // Insert new conversation to "conversation reply" table
//        YtsApplication.mDb.insertConversationReply(conversationReply);

        final ContentResolver cr = AppBaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, ConversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, Message);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.DELIVERED.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.IN.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID, 0);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "");
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "0");
        cr.insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, values);
    }

    public static void testInsertBots() {
        if (!botContactExists("0000000")) {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            for (int i = 0; i < 50; i++) {

                // Adding insert operation to operations list
                // to insert a new raw contact in the table
                // ContactsContract.RawContacts
                String value = "" + i + "" + i + "" + i + "" + i + "" + i + "" + i + "" + i + "";
                ops.add(ContentProviderOperation.newInsert(TableBotUsersContract.UserEntry.CONTENT_URI_INDIVIDUAL)
                        .withValue(TableBotUsersContract.UserEntry.COLUMN_NAME, "name" + i)
                        .withValue(TableBotUsersContract.UserEntry.COLUMN_PHONE, value).build());

            }
            insertContacts(ops);
        }
    }

    private static boolean botContactExists(String number) {
        // / number is the phone number
        Uri uri = TableBotUsersContract.UserEntry.buildBotUserUri(number);
        Cursor cur = AppBaseApplication.context().getContentResolver().query(uri, null, null, null, null);
        try {
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

    private static void insertContacts(ArrayList<ContentProviderOperation> ops) {

        try {
            // Executing all the insert operations as a single database
            // transaction
            AppBaseApplication.context().getContentResolver().applyBatch(TableBotUsersContract.CONTENT_AUTHORITY, ops);

        } catch (RemoteException | OperationApplicationException e) {
            e.printStackTrace();
        }

        mContentResolver.notifyChange(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS, null);

    }
}