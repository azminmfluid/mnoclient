package com.ecrio.iota.db.tables;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * TableUsersContract represents the contract for storing users in the SQLite database.
 */
public final class TableBotUsersContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";

    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // The user conversation paths.
    public static final String PATH_CONVERSATION = "bot_user_c";

    // The all users paths.
    public static final String PATH_ALL_USERS = "bot_user_all";

    // The user paths.
    public static final String PATH_INDIVIDUAL_USER = "bot_user_i";

    public static final String SQL_CREATE_USER_TABLE =
            "CREATE TABLE " + UserEntry.TABLE_NAME +
                    " (" +
                    UserEntry.COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + // For saving user id.
                    UserEntry.COLUMN_STATUS + " TEXT," + // For saving user's session status.
                    UserEntry.COLUMN_NAME + " TEXT NOT NULL," + // For saving user's session status.
                    UserEntry.COLUMN_PHONE + " TEXT NOT NULL, " + // For saving user's phone number.
                    "UNIQUE ("+UserEntry.COLUMN_PHONE+") ON CONFLICT REPLACE) ;";

    public static final class UserEntry implements BaseColumns {

        // Uri for getting details of particular user's conversation.
        public static final Uri CONTENT_URI_CONVERSATION = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION).build();

        // Uri for getting all the users.
        public static final Uri CONTENT_URI_ALL_USERS = BASE_CONTENT_URI.buildUpon().appendPath(PATH_ALL_USERS).build();
        // Uri for getting all the users.
        public static final Uri CONTENT_URI_DELETE_USERS = BASE_CONTENT_URI.buildUpon().appendPath(PATH_ALL_USERS).build();

        // Uri for getting details of particular user.
        public static final Uri CONTENT_URI_INDIVIDUAL = BASE_CONTENT_URI.buildUpon().appendPath(PATH_INDIVIDUAL_USER).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_CONVERSATION;
        // The name of the user table.
        public static final String TABLE_NAME = "bot_users";
        // The id of the user.
        public static final String COLUMN_USER_ID = "bot_user_id_fk";
        // The phone number of the bot user.
        public static final String COLUMN_PHONE = "bot_phone";
        // The phone number of the bot user.
        public static final String COLUMN_NAME = "bot_name";
        // The status of the conversation.
        public static final String COLUMN_STATUS = "u_status";

        // Returns the Uri referencing a conversation with the specified user id.
        public static Uri buildConversationUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI_CONVERSATION, id);

        }

        // Returns the Uri referencing a user with the specified user id.
        public static Uri buildUserUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_ALL_USERS, id);

        }

        // Returns the Uri referencing a user with the specified user id.
        public static Uri buildUserUri2(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_ALL_USERS, id);

        }
        // Returns the Uri referencing a user with the specified user id.
        public static Uri buildBotUserUri(String id) {

            return Uri.withAppendedPath(CONTENT_URI_INDIVIDUAL, id);

        }
    }
}
