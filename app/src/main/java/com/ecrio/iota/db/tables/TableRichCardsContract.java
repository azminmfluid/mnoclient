package com.ecrio.iota.db.tables;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */

/**
 * TableConversationsContract represents the contract for storing conversation(messages) in the SQLite database.
 */
public final class TableRichCardsContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";

    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // The content paths.
    public static final String PATH_CONVERSATION_REPLY = "rc_chat_all";

    // The content paths.
    public static final String PATH_CONVERSATION_BY_SESSION = "ch_chat_by_session";

    public static final String SQL_CREATE_RICH_CARD_MESSAGE_TABLE =
            "CREATE TABLE " + ChatEntry.TABLE_NAME +
                    " (" +
                    ChatEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ChatEntry.COLUMN_RICH_CARD_ID + " INTEGER, " + // For saving the user id.
                    ChatEntry.COLUMN_RICH_CARD_USER_ID + " INTEGER, " + // For saving the user id.
                    ChatEntry.COLUMN_RICH_CARD_ADDRESS + " INTEGER, " + // For saving indication for isOutGouing or attendee.
                    ChatEntry.COLUMN_RICH_CARD_TYPE + " INTEGER, " + // For saving conversation type.
                    ChatEntry.COLUMN_RICH_CARD_MSG + " TEXT, " + // For saving message NOT NULL
                    ChatEntry.COLUMN_RICH_CARD_REPLY_ID + " INTEGER, " + // For saving message NOT NULL
                    ChatEntry.COLUMN_RICH_CARD_STATE + " INTEGER, " + // For saving status of message.
                    ChatEntry.COLUMN_RICH_CARD_SELECTED + " INTEGER, " + // For saving status of message.
                    ChatEntry.COLUMN_RICH_CARD_TIME + " DOUBLE, " + // For saving send time.
                    ChatEntry.COLUMN_CONVERSATION_SESSION_ID + " INTEGER, " + // For conversation id.
                    ChatEntry.COLUMN_RICH_CARD_TITLE + " TEXT, " + // For conversation id.
                    ChatEntry.COLUMN_RICH_CARD_LINK + " TEXT, " + // For conversation id.
                    ChatEntry.COLUMN_CONVERSATION_STATUS + " TEXT, " + // For conversation id. NOT NULL
                    ChatEntry.COLUMN_BG_THUMB + " BYTES BLOB, " + // For saving conversation type.
                    ChatEntry.COLUMN_COUPON_THUMB + " BYTES BLOB " + // For saving conversation type.
                    " ) ;";


    public static final class ChatEntry implements BaseColumns {

        // Uri for inserting/updating conversation messages.
        public static final Uri CONTENT_URI_CONVERSATION = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_REPLY).build();
        // Uri for getting conversations.
        public static final Uri CONTENT_URI_CONVERSATION_BY_SESSION_ID = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_BY_SESSION).build();

        // Name of the conversation table.
        public static final String TABLE_NAME = "rich_card";
        // The internal message id of the conversation.
        public static final String COLUMN_RICH_CARD_ID = "rc_id_fk";
        // The internal session id of the conversation created.
        public static final String COLUMN_CONVERSATION_SESSION_ID = "c_id_fk";
        // The user id of the the user in the conversation.
        public static final String COLUMN_RICH_CARD_USER_ID = "rc_user_id";
        // The type of conversation to indicate text/file/stickers.
        public static final String COLUMN_RICH_CARD_TYPE = "rc_type";
        // The flow of conversation (incoming or outgoing).
        public static final String COLUMN_RICH_CARD_ADDRESS = "rc_sender";
        // The message id of the conversation to respond read status.
        public static final String COLUMN_RICH_CARD_REPLY_ID = "rc_reply_id";
        // The text message in the conversation.
        public static final String COLUMN_RICH_CARD_MSG = "rc_msg";
        // The status of the message in the conversation(send,delivered,read,failed).
        public static final String COLUMN_RICH_CARD_STATE = "rc_state";
        // The status indicate selection(message selected for deletion).
        public static final String COLUMN_RICH_CARD_SELECTED = "rc_delete";
        // The time of conversation created.
        public static final String COLUMN_RICH_CARD_TIME = "rc_time";
        // The file id of the message in the conversation.
        public static final String COLUMN_RICH_CARD_TITLE = "rc_title";
        // The progress of file sending/receiving in the conversation.
        public static final String COLUMN_RICH_CARD_LINK = "rc_link";
        // The status of the message in the conversation(send,delivered,read,failed).
        public static final String COLUMN_CONVERSATION_STATUS = "c_id_status";
        // The thumbnail of the file send/received in the conversation.
        public static final String COLUMN_BG_THUMB = "rc_bg_thumb";
        // The thumbnail of the file send/received in the conversation.
        public static final String COLUMN_COUPON_THUMB = "rc_cpn_thumb";

        // Returns the Uri referencing a chat with the specified session id.
        public static Uri buildChatsUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_CONVERSATION_BY_SESSION_ID, id);

        }

    }
}
