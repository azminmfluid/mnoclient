package com.ecrio.iota.db.tables;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */
/**
 * TableConversationMainContract represents the contract for storing new conversation in the SQLite database.
 */
public final class TableConversationMainContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";

    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // The content paths.
    public static final String PATH_CONVERSATION = "chat_conversation";

    public static final String SQL_CREATE_CONVERSATION_TABLE =
            "CREATE TABLE " + ConversationEntry.TABLE_NAME +
                    " (" +
                    ConversationEntry.COLUMN_CONVERSATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + // For internal conversation id.
                    ConversationEntry.COLUMN_STATUS + " TEXT NOT NULL, " + // For saving status of chat.
                    ConversationEntry.COLUMN_TIME + " DOUBLE " + // For saving time.
                    " ) ;";


    public static final class ConversationEntry implements BaseColumns {

        // Uri for getting all conversation.
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_CONVERSATION;

        // Name of the conversation table.
        public static final String TABLE_NAME = "chat_conversation";
        // The internal session id of the conversation.
        public static final String COLUMN_CONVERSATION_ID = "c_id";
        // The status of the conversation(started or stopped).
        public static final String COLUMN_STATUS = "status";
        // The time of conversation created.
        public static final String COLUMN_TIME = "time";

        // Returns the Uri referencing a conversation with the specified internal id.
        public static Uri buildConversationUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI, id);

        }
    }
}