package com.ecrio.iota.db.tables;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */

/**
 * TableCompositionContract represents the contract for storing message compositions in the SQLite database.
 */
public final class TableReplyContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";
    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    // The content paths.
    public static final String PATH_ALL_REPLIES = "chat_replies";
    // The content paths.
    public static final String PATH_REPLY_SESSION = "chat_reply_session";

    public static final String SQL_CREATE_CONVERSATION_GROUP_TABLE =
            "CREATE TABLE " + CompositionEntry.TABLE_NAME +
                    " (" +
                    CompositionEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + // For primary key.
                    CompositionEntry.COLUMN_USER_ID + " INTEGER, " + // For saving user id.
                    CompositionEntry.COLUMN_STATUS + " INTEGER, " + // For saving status.
                    CompositionEntry.COLUMN_MESSAGE + " TEXT NOT NULL, " + // For saving status.
                    CompositionEntry.COLUMN_CONVERSATION_ID + " INTEGER, " + // For saving conversation id.
                    " UNIQUE ("+ CompositionEntry.COLUMN_CONVERSATION_ID+") ON CONFLICT REPLACE );";// Replace records, if conversation id duplication occurs.

    public static final class CompositionEntry implements BaseColumns {

        public static final Uri CONTENT_URI_ALL_REPLIES = BASE_CONTENT_URI.buildUpon().appendPath(PATH_ALL_REPLIES).build();

        public static final Uri CONTENT_URI_BY_SESSION_ID = BASE_CONTENT_URI.buildUpon().appendPath(PATH_REPLY_SESSION).build();

        // Name of the conversation table.
        public static final String TABLE_NAME = "replies";
        // The id of the record.
        public static final String COLUMN_ID = "reply_id";
        // The user id of the conversation.
        public static final String COLUMN_USER_ID = "reply_user_id";
        // The IMDN status of the conversation.
        public static final String COLUMN_STATUS = "reply_status";
        // The session id of the conversation.
        public static final String COLUMN_CONVERSATION_ID = "reply_session_id";
        // The session id of the conversation.
        public static final String COLUMN_MESSAGE = "reply_message";
        // Returns the Uri referencing a chat composition with the specified id.
        public static Uri buildReplyUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_ALL_REPLIES, id);

        }
        // Returns the Uri referencing a chat composition with the specified session id.
        public static Uri buildSessionUri(int id) {

            return ContentUris.withAppendedId(CONTENT_URI_BY_SESSION_ID, id);

        }
    }
}
