package com.ecrio.iota.db.tables;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by s.s on 2018.
 */
/**
 * TableConversationsContract represents the contract for storing conversation(messages) in the SQLite database.
 */
public final class TableConversationsContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.ecrio.iota";

    // Base of all URIs that will be used to contact the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // The content paths.
    public static final String PATH_CONVERSATION_REPLY = "chats";
    // The content paths.
    public static final String PATH_CONVERSATION_READ = "chats_read";

    // The content paths.
    public static final String PATH_CONVERSATION_BY_SESSION = "chats2";

    // The chat paths.
    public static final String PATH_CONVERSATION_BY_USER = "chats_by_user";

    // The content paths.
    public static final String PATH_CONVERSATION_QUEUED = "chats_queued";

    // The content paths.
    public static final String PATH_ALL_CONVERSATION_QUEUED = "all_chats_queued";

    // The content paths.
    public static final String PATH_CONVERSATION_LIST = "chat_list";

    public static final String SQL_CREATE_CONVERSATION_MESSAGE_TABLE =
            "CREATE TABLE " + ChatEntry.TABLE_NAME +
                    " (" +
                    ChatEntry.COLUMN_CONVERSATION_MSG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ChatEntry.COLUMN_CONVERSATION_USER_ID + " INTEGER, " + // For saving the user id.
                    ChatEntry.COLUMN_CONVERSATION_MSG + " TEXT, " + // For saving message NOT NULL
                    ChatEntry.COLUMN_CONVERSATION_REPLY_ID + " INTEGER, " + // For saving message NOT NULL
                    ChatEntry.COLUMN_CONVERSATION_MSG_STATE + " INTEGER, " + // For saving status of message.
                    ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED + " INTEGER, " + // For saving status of message.
                    ChatEntry.COLUMN_CONVERSATION_MSG_TIME + " DOUBLE, " + // For saving send time.
                    ChatEntry.COLUMN_CONVERSATION_SESSION_ID + " INTEGER, " + // For conversation id.
                    ChatEntry.COLUMN_CONVERSATION_FILE_ID + " INTEGER, " + // For conversation id.
                    ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS + " TEXT, " + // For conversation id.
                    ChatEntry.COLUMN_CONVERSATION_RICH_ID + " INTEGER, " + // For conversation id.
                    ChatEntry.COLUMN_CONVERSATION_RICH_TYPE + " INTEGER, " + // For conversation id.
                    ChatEntry.COLUMN_CONVERSATION_STATUS + " TEXT, " + // For conversation id. NOT NULL
                    ChatEntry.COLUMN_CONVERSATION_TYPE + " INTEGER, " + // For saving conversation type.
                    ChatEntry.COLUMN_THUMB + " BYTES BLOB, " + // For saving conversation type.
                    ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS + " INTEGER " + // For saving indication for isOutGouing or attendee.
                    " ) ;";


    public static final class ChatEntry implements BaseColumns {

        // Uri for inserting/updating conversation messages.
        public static final Uri CONTENT_URI_CONVERSATION = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_REPLY).build();
        // Uri for inserting/updating conversation messages.
        public static final Uri CONTENT_URI_CONVERSATION_READ = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_READ).build();
        // Uri for getting conversations.
        public static final Uri CONTENT_URI_CONVERSATION_BY_SESSION_ID = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_BY_SESSION).build();
        // Uri for getting conversations.
        public static final Uri CONTENT_URI_CONVERSATION_BY_USER = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_BY_USER).build();
        // Uri for getting all queued conversations.
        public static final Uri CONTENT_URI_QUEUED_CONVERSATIONS = BASE_CONTENT_URI.buildUpon().appendPath(PATH_ALL_CONVERSATION_QUEUED).build();
        // Uri for getting queued conversations.
        public static final Uri CONTENT_URI_QUEUED_CONVERSATION_BY_USER = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_QUEUED).build();

        // Returns the Uri referencing list of latest conversation.
        public static final Uri CONTENT_CHATLIST_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONVERSATION_LIST).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_CONVERSATION_REPLY;

        // Name of the conversation table.
        public static final String TABLE_NAME = "chats";
        // The internal message id of the conversation.
        public static final String COLUMN_CONVERSATION_MSG_ID = "c_msg_id";
        // The type of conversation to indicate text/file/stickers.
        public static final String COLUMN_CONVERSATION_TYPE = "c_type";
        // The flow of conversation (incoming or outgoing).
        public static final String COLUMN_CONVERSATION_MSG_ADDRESS = "c_sender";
        // The user id of the the user in the conversation.
        public static final String COLUMN_CONVERSATION_USER_ID = "c_user_id";
        // The message id of the conversation to respond read status.
        public static final String COLUMN_CONVERSATION_REPLY_ID = "c_reply_id";
        // The text message in the conversation.
        public static final String COLUMN_CONVERSATION_MSG = "c_mesg";
        // The status of the message in the conversation(send,delivered,read,failed).
        public static final String COLUMN_CONVERSATION_MSG_STATE = "c_msg_imdn";
        // The status indicate selection(message selected for deletion).
        public static final String COLUMN_CONVERSATION_MSG_SELECTED = "c_msg_delete";
        // The time of conversation created.
        public static final String COLUMN_CONVERSATION_MSG_TIME = "c_msg_time";
        // The file id of the message in the conversation.
        public static final String COLUMN_CONVERSATION_FILE_ID = "c_file_id_fk";
        // The progress of file sending/receiving in the conversation.
        public static final String COLUMN_CONVERSATION_FILE_PROGRESS = "c_progress_fk";
        // The internal session id of the conversation created.
        public static final String COLUMN_CONVERSATION_SESSION_ID = "c_id_fk";
        // The status of the message in the conversation(send,delivered,read,failed).
        public static final String COLUMN_CONVERSATION_STATUS = "c_id_status";
        // The thumbnail of the file send/received in the conversation.
        public static final String COLUMN_THUMB = "c_thumb";
        // The file id of the message in the conversation.
        public static final String COLUMN_CONVERSATION_RICH_ID = "c_rich_id_fk";
        // The file id of the message in the conversation.
        public static final String COLUMN_CONVERSATION_RICH_TYPE = "c_rich_type";

        // Returns the Uri referencing a chat with the specified session id.
        public static Uri buildChatsUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_CONVERSATION_BY_SESSION_ID, id);

        }

        // Returns the Uri referencing a chat with the specified user id.
        public static Uri buildChatsWithUserIDUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_CONVERSATION_BY_USER, id);

        }

        // Returns the Uri referencing a video with the specified user id.
        public static Uri buildQueuedChatsWithUserIDUri(long id) {

            return ContentUris.withAppendedId(CONTENT_URI_QUEUED_CONVERSATION_BY_USER, id);

        }

    }
}
