package com.ecrio.iota.ui.chat.item.info;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ecrio.iota.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class InfoMsgView extends FrameLayout {

    private View mRoot;

    @BindView(R.id.id_info)
    public TextView infoView;

    public InfoMsgView(Context context) {
        this(context, null);
    }

    public InfoMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public InfoMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(false);
//
        setFocusableInTouchMode(false);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        mRoot = inflater.inflate(R.layout.item_chat_rich_info, this);

        ButterKnife.bind(this, mRoot);

    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }

    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setTitle(String message) {

    }

    public void setWho(String username) {

    }

    public void setState(int status) {

    }

    public void setImage(String phone) {

    }

}