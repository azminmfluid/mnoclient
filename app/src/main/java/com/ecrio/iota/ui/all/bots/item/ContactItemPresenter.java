package com.ecrio.iota.ui.all.bots.item;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.all.bots.model.PhoneContact;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class ContactItemPresenter extends Presenter {

    private String TAG = "CardPresenter";
    private int mSelectedBackgroundColor = -1;
    private int mDefaultBackgroundColor = -1;
    private Drawable mDefaultIcon;
    private Drawable mSelectedIcon;

    private int color;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2(TAG,"called onCreateViewHolder");
        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.background);
        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.sel_background);

        mDefaultIcon = ContextCompat.getDrawable(parent.getContext(), R.drawable.ic_page_5bg_normal_view);
        mSelectedIcon = ContextCompat.getDrawable(parent.getContext(), R.drawable.ic_mark_bg);

        ContactItemView movieCardView = new ContactItemView(parent.getContext()){
            @Override
            public void setSelected(boolean selected) {
                super.setSelected(updateCardBackgroundColor(this, selected));
            }
        };

        color = ContextCompat.getColor(parent.getContext(), R.color.sel_background);

        updateCardBackgroundColor(movieCardView);

        movieCardView.setFocusableInTouchMode(false);

        return new ViewHolder(movieCardView);
    }

    private void updateCardBackgroundColor(ContactItemView view) {

        view.setMainBackgroundColor(color);

    }
    private boolean updateCardBackgroundColor(ContactItemView view, boolean selected) {

//        if (selected) {
//            if (!view.isSelected()) {
//                selected = true;
//            } else {
//                selected = false;
//            }
//        }
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;
        view.setMainBackgroundColor(color);
        view.setImageStyle(selected);
//        view.setContentText(selected);
        return selected;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        PhoneContact phoneContactItem = (PhoneContact) item;

        viewHolder.tag = ""+phoneContactItem.getContactID();

        ContactItemView movieCardView = (ContactItemView)viewHolder.view;

        // setting the contact name
        movieCardView.setContactNameText(phoneContactItem.getContactName());

        // setting the last message
        movieCardView.setContactPhoto(phoneContactItem.getContactID(),phoneContactItem.getThumbNailPhoto());

        Log.d2(TAG,viewHolder.tag+" called onBindViewHolder");

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG,viewHolder.tag+" called onUnbindViewHolder");

        ContactItemView movieCardView = (ContactItemView)viewHolder.view;

//        movieCardView.setContactNameText(null);

//        movieCardView.setContactPhoto(null, null);
    }

}