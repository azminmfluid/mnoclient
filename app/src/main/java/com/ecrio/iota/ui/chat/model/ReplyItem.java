/*
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ecrio.iota.ui.chat.model;

/**
 * Video is an immutable object that holds the various metadata associated with a single video.
 */
public final class ReplyItem {
    public final int conversationID;
    public final int userid;
    public final int compStatus;
    public final String statusMsg;

    public ReplyItem(){
        conversationID = 0;
        userid = 0;
        compStatus = 0;
        statusMsg = "0";
    }
    public ReplyItem(
            final int conversationID,
            final int userid,
            final int compStatus,
            final String statusMsg) {
        this.conversationID = conversationID;
        this.userid = userid;
        this.compStatus = compStatus;
        this.statusMsg = statusMsg;
    }

    @Override
    public String toString() {
        String s = "Video{";
        s += "conversationID=" + conversationID;
        s += ", userid='" + userid + "'";
        s += ", compStatus='" + compStatus + "'";
        s += "}";
        return s;
    }

}