package com.ecrio.iota.ui.chat.item.rc_reply;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.intf.ReplyInterface;
import com.ecrio.iota.model.rich.Action;
import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.GeneralPurposeCard;
import com.ecrio.iota.model.rich.GeneralPurposeCardCarousel;
import com.ecrio.iota.model.rich.Message;
import com.ecrio.iota.model.rich.Postback;
import com.ecrio.iota.model.rich.Reply;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.model.rich.Suggestion;
import com.ecrio.iota.ui.chat.data.SingleClick;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.Utilities;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 27-Aug-17.
 */

public class RcReplyMsgView extends FrameLayout {

    private View root;
    private LayoutInflater inflater;
    private ReplyInterface richInterface;
    @BindView(R.id.id_reply_container)
    public LinearLayout mReplyContainer;
    public HashMap<String, Reply> mReplies = new HashMap<String, Reply>();
    Handler handler = new Handler(Looper.getMainLooper());

    public RcReplyMsgView(Context context) {
        this(context, null);
    }

    public RcReplyMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public RcReplyMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(true);

        setFocusableInTouchMode(true);

//        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater = LayoutInflater.from(getContext());

        root = inflater.inflate(R.layout.receiver_reply_card_view, this);

        ButterKnife.bind(this, root);

    }

    private void addReplyView(LinearLayout container, Reply reply) {
        if(mReplies.containsKey(reply.getDisplayText())) return;
        mReplies.put(reply.getDisplayText(), reply);
        String displayText = reply.getDisplayText();
        Postback postback = reply.getPostback();
        handler.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_reply_button_new, container, false);
                TextView mReply = (TextView) layout.findViewById(R.id.id_rc_reply_txt);
                mReply.setText(displayText);
//                JumpingBeans.with(mReply).appendJumpingDots().build();
                mReply.setOnClickListener(new SingleClick() {
                    @Override
                    public void onSingleClick(View view) {
                        Reply tag = (Reply) view.getTag();
                        if (tag != null) {
                            String data = new Gson().toJson(tag);
                            if (richInterface != null)
                                if (!Strings.isNullOrEmpty(data)) {
                                    richInterface.doReply(data);
                                }
                        }
                    }
                });
                mReply.setTag(reply);
                layout.setTag(reply);
                container.addView(layout);
            }
        });
    }
    public void setAvatarImage(String phone) {

    }
    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setComposingText(String message) {
        mReplies.clear();
        mReplyContainer.removeAllViews();
        new Thread(new Runnable() {
            @Override
            public void run() {
//                String message = getMockJsonData();
                RichCardEntity richCard = new Gson().fromJson(message, RichCardEntity.class);
                Log.DebugRich("===PARSING===");
                if (richCard == null) {
                    Log.DebugRich("rich card object is null");
                } else {
                    Message richMsg = richCard.getMessage();
                    if (richMsg != null) {
                        if (richMsg.isGeneralPurpose()) {
                            GeneralPurposeCard card = richMsg.getGeneralPurposeCard();
                            if (card != null) {
                                Content content = card.getContent();
                                String title = content.getTitle();
                                Log.DebugAdapter("title is " + title);
                                String description = content.getDescription();
                                Log.DebugAdapter("description is " + description);
                                Handler handler = new Handler(Looper.getMainLooper());

                                List<Suggestion> suggestions = content.getSuggestions();
                                if (suggestions != null) {
                                    for (Suggestion suggestion : suggestions) {
                                        if (suggestion != null) {
                                            Action action = suggestion.getAction();
                                            Reply reply = suggestion.getReply();
                                            if (reply != null) {
                                                Log.DebugAdapter(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                addReplyView(mReplyContainer, reply);
                                            }
                                        }
                                    }
                                }
                            }
                        }else if (richMsg.isCarousal()) {
                            GeneralPurposeCardCarousel cardCarousel = richMsg.getGeneralPurposeCardCarousel();
                            if (cardCarousel != null) {
                                List<Content> content = cardCarousel.getContent();
                                for (Content c : content) {
                                    String title = c.getTitle();
                                    Log.DebugAdapter("title is " + title);
                                    String description = c.getDescription();
                                    Log.DebugAdapter("description is " + description);
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    List<Suggestion> suggestions = c.getSuggestions();
                                    if (suggestions != null) {
                                        for (Suggestion suggestion : suggestions) {
                                            if (suggestion != null) {
                                                Action action = suggestion.getAction();
                                                Reply reply = suggestion.getReply();
                                                if (reply != null) {
                                                    Log.DebugAdapter(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                    addReplyView(mReplyContainer, reply);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                Log.DebugRich("===COMPLETED===");
            }
        }).start();
    }

    @Nullable
    private String getMockJsonData() {
        String message = null;
        try {
            JSONObject jsonObject = Utilities.readJson("response_rich_general.json");
            message = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }

    public void stopJumping() {
//        build.stopJumping();
    }

    public void setRichInterface(ReplyInterface richInterface) {
        this.richInterface = richInterface;
    }
}