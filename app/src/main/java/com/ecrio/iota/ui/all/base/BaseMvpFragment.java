package com.ecrio.iota.ui.all.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.ecrio.iota.ui.all.AllContactsFragmentChild;
import com.ecrio.iota.ui.base.BaseCallback;
import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;

/**
 * Created by user on 2018.
 */

public abstract class BaseMvpFragment<V extends BaseView<P>, P extends BasePresenter> extends AllContactsFragmentChild implements BaseCallback<V, P>, BaseView<P> {

    protected P presenter;

    @NonNull
    public abstract P createPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        P presenter = createPresenter();
        setPresenter(presenter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setPresenter(@NonNull P presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public P getBasePresenter() {
        return presenter;
    }

    @NonNull
    @Override
    public V getMvpView() {
        return (V) this;
    }

}
