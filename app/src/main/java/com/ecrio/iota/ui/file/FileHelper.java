package com.ecrio.iota.ui.file;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Mfluid on 3/17/2016.
 */
public class FileHelper {

    public static final String FOLDER_NAME = "Rcs";

    public static boolean checkFile(String fileUrl, String contentType) {
//        String fileFormatFromUrl = FileHelper.getFileFormatFromUrl(fileUrl);
//        String fileFormatFromUrl = getFileFormatFromContentType(contentType);
        String fileFormatFromUrl = getFileFormatFromUrlAndContentType(fileUrl, contentType);
        String fileNameFromUrl = FileHelper.getFileNameFromUrl(fileUrl);
        String downloadingFile = fileNameFromUrl + fileFormatFromUrl;
        String tmpdownloadingFile = FileHelper.getFileNameFromUrl(fileUrl) + ".tmp";
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
//          savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/OSChina/Update/";
            String savePath = FileHelper.getRootDir() + "/Downloads/Sessions/";
            File file = new File(savePath);
            if (!file.exists()) {
                boolean mkdirs = file.mkdirs();
            }
            String targetFilePath = savePath + downloadingFile;
            File targetFile = new File(targetFilePath);
            return targetFile.exists();
        } else {
            return false;
        }
    }

    public static ArrayList<String> getSignatures(String offlineTicketNo) {
        ArrayList<String> signatures = new ArrayList<String>();
        String main_directory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/mforce/" + offlineTicketNo;
        File signature1 = new File(main_directory + "/signature/");
        if (signature1.exists()) {
            try {
                File[] listSignatures = signature1.listFiles();
                for (File file : listSignatures) {
                    signatures.add(file.getAbsolutePath());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return signatures;
    }

    /**
     * Get File Size
     *
     * @param size byte
     * @return
     */
    public static String getFileSize(long size) {
        if (size <= 0) return "0";
        java.text.DecimalFormat df = new java.text.DecimalFormat("##.##");
        float temp = (float) size / 1024;
        if (temp >= 1024) {
            return df.format(temp / 1024) + "M";
        } else {
            return df.format(temp) + "K";
        }
    }

    /**
     * Get File Size
     *
     * @param filePath
     * @return
     */
    public static long getFileSize(String filePath) {
        long size = 0;

        File file = new File(filePath);
        if (file != null && file.exists()) {
            size = file.length();
        }
        return size;
    }

    public static ArrayList<String> getRedLines(String offlineTicketNo) {
        ArrayList<String> redLines = new ArrayList<String>();
        String main_directory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/mforce/" + offlineTicketNo;
        File signature1 = new File(main_directory + "/signature/");
        if (signature1.exists()) {
            try {
                File[] redlines = signature1.listFiles();
                for (File file : redlines) {
                    redLines.add(file.getAbsolutePath());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return redLines;
    }

    public static void clearAttachment(String path) {
        new File(path).delete();
    }

    public static File getRootDir() {
//        return MyApplication.getContext().getExternalFilesDir("Golf");
        return new File(Environment.getExternalStorageDirectory() + File.separator + "RCSFiles");
    }

    public static void deleteall(String RootDirectory) {
        File file = new File(RootDirectory);
        if (file.exists()) {
            if (file.isDirectory()) {

                File[] subdirectories = file.listFiles();
                for (int i = 0; i < subdirectories.length; i++) {
                    deleteall(subdirectories[i].getPath());
                }

            }
            file.delete();
        }
    }

    public static void deleteall(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {

                File[] subdirectories = file.listFiles();
                for (int i = 0; i < subdirectories.length; i++) {
                    deleteall(subdirectories[i].getPath());
                }

            }
            file.delete();
        }
    }

    public static String getFileName(String filePath) {
        if (StringUtils.isEmpty(filePath)) return "";
        return filePath.substring(filePath.lastIndexOf(File.separator) + 1);
    }


    public static String getFileNameFromUrl(String filePath) {
//        String lastIndexOfSeperator = filePath.substring(0,filePath.lastIndexOf(File.separator));
//        return lastIndexOfSeperator.substring(lastIndexOfSeperator.lastIndexOf(File.separator)+1);
        if (StringUtils.isEmpty(filePath)) return "";
        if (StringUtils.hasNoSeperator(filePath)) return "";
        String lastIndexOfSeperator = filePath.substring(filePath.lastIndexOf(File.separator) + 1, filePath.length());
        if (StringUtils.hasNoDot(lastIndexOfSeperator)) return lastIndexOfSeperator;
        return lastIndexOfSeperator.substring(0, lastIndexOfSeperator.lastIndexOf('.'));
    }

    public static String getFileNameWithExtension(String fileUrl) {
        if (StringUtils.isEmpty(fileUrl)) return "";
        if (StringUtils.hasNoSeperator(fileUrl)) return "";
        String lastIndexOfSeperator = fileUrl.substring(fileUrl.lastIndexOf(File.separator) + 1, fileUrl.length());
        return lastIndexOfSeperator;
    }

    public static String getFileNameWithOutExtension(String fileUrl) {
        if (StringUtils.isEmpty(fileUrl)) return "";
        if (StringUtils.hasNoSeperator(fileUrl)) return "";
        String lastIndexOfSeperator = fileUrl.substring(fileUrl.lastIndexOf(File.separator) + 1, fileUrl.length());
        if (StringUtils.hasNoDot(lastIndexOfSeperator)) return lastIndexOfSeperator;
        return lastIndexOfSeperator.substring(0, lastIndexOfSeperator.lastIndexOf('.'));
    }

    /**
     * Get the file extension
     *
     * @param fileName
     * @return
     */
    public static String getFileFormatFromUrl(String fileName) {
        fileName = getFileNameWithExtension(fileName);
        if (StringUtils.isEmpty(fileName)) return "";
        if (StringUtils.hasNoDot(fileName)) return "";
        int point = fileName.lastIndexOf('.');
        return fileName.substring(point);
    }

    /**
     * Get the file extension
     *
     * @param contentType
     * @return
     */
    public static String getFileFormatFromContentType(String contentType) {
        String extension = "";
        switch (contentType) {
            case "image/jpeg":
                extension = ".jpeg";
                break;
            case "image/jpg":
                extension = ".jpg";
                break;
            case "video/mp4":
                extension = ".mp4";
                break;
            case "video/mpeg":
                extension = ".mpeg";
                break;
            case "image/gif":
                extension = ".gif";
                break;
            case "video/x-msvideo":
                extension = ".avi";
                break;
            case "video/webm":
                extension = ".webm";
                break;
            case "image/png":
                extension = ".png";
                break;
        }
        return extension;
    }

    /**
     * Get the file extension
     *
     * @param contentType
     * @return
     */
    public static String getFileFormatFromUrlAndContentType(String url, String contentType) {
        String extension = "";
        switch (contentType) {
            case "image/jpeg":
                extension = ".jpeg";
                break;
            case "image/jpg":
                extension = ".jpg";
                break;
            case "video/mp4":
                extension = ".mp4";
                break;
            case "video/mpeg":
                extension = ".mpeg";
                break;
            case "image/gif":
                extension = ".gif";
                break;
            case "video/x-msvideo":
                extension = ".avi";
                break;
            case "video/webm":
                extension = ".webm";
                break;
            case "image/png":
                extension = ".png";
                break;
        }
        return (url.endsWith(extension)) ? "" : extension;
    }
    /**
     * Get the file extension
     *
     * @param contentType
     * @return
     */
    public static String getFileURLFromUrlAndContentType(String url, String contentType) {
        String extension = "";
        switch (contentType) {
            case "image/jpeg":
                extension = ".jpeg";
                break;
            case "image/jpg":
                extension = ".jpg";
                break;
            case "video/mp4":
                extension = ".mp4";
                break;
            case "video/mpeg":
                extension = ".mpeg";
                break;
            case "image/gif":
                extension = ".gif";
                break;
            case "video/x-msvideo":
                extension = ".avi";
                break;
            case "video/webm":
                extension = ".webm";
                break;
            case "image/png":
                extension = ".png";
                break;
        }
        return (url.endsWith(extension)) ? url : url+extension;
    }
    /**
     * Get the file extension
     *
     * @param contentType
     * @return
     */
    public static String getFileThumbURLFromUrlAndContentType(String url, String contentType) {
        String extension = "";
        switch (contentType) {
            case "image/jpeg":
                extension = ".jpeg";
                break;
            case "image/jpg":
                extension = ".jpg";
                break;
            case "video/mp4":
                extension = ".mp4";
                break;
            case "video/mpeg":
                extension = ".mpeg";
                break;
            case "image/gif":
                extension = ".gif";
                break;
            case "video/x-msvideo":
                extension = ".avi";
                break;
            case "video/webm":
                extension = ".webm";
                break;
            case "image/png":
                extension = ".png";
                break;
        }
        return (url.endsWith(extension)) ? url : url;
    }
    public static boolean isAudioVideo(String filePath) {
        boolean isAudioVideo = false;

        String fName = FileHelper.getFileName(filePath);

        String end = null;
        if (StringUtils.hasNoDot(fName)) {
            isAudioVideo = false;
            return isAudioVideo;

        } else {
            end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        }
        if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg")
                || end.equals("wav")) {
            isAudioVideo = true;
        } else if (end.equals("3gp") || end.equals("mp4") || end.equals("avi")) {
            isAudioVideo = true;
        }
        return isAudioVideo;
    }

    public static boolean isGif(String filePath) {
        boolean isGif = false;
        String fName = FileHelper.getFileName(filePath);

        String end = null;
        if (StringUtils.hasNoDot(fName)) {
            return false;

        } else {
            end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        }
        if (end.toLowerCase().equals("gif")) {
            isGif = true;
        }
        return isGif;
    }

    public static boolean isImage(String filePath) {
        boolean isImage = false;
        String fName = FileHelper.getFileName(filePath);

        String end = null;
        if (StringUtils.hasNoDot(fName)) {
            return false;

        } else {
            end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        }
        end = end.toLowerCase();
        if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
            isImage = true;
        }
        return isImage;
    }

    public static String getMIMEType(String filePath) {
        String type = "";
        String fName = FileHelper.getFileName(filePath);
        /* 取得扩展名 */
        String end = null;
        if (StringUtils.hasNoDot(fName)) {
            end = "";
        } else {
            end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        }

        /* 按扩展名的类型决定MimeType */
        if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg")
                || end.equals("wav")) {
            type = "audio";
        } else if (end.equals("3gp") || end.equals("mp4") || end.equals("avi")) {
            type = "video";
        } else if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
            type = "image";
        } else if (end.equals("doc") || end.equals("docx")) {
            type = "application/msword";
        } else if (end.equals("xls")) {
            type = "application/vnd.ms-excel";
        } else if (end.equals("ppt") || end.equals("pptx") || end.equals("pps") || end.equals("dps")) {
            type = "application/vnd.ms-powerpoint";
        } else {
            type = "*";
        }
        /* 如果无法直接打开，就弹出软件列表给用户选择 */
        type += "/*";
        return type;
    }

    public static File createFolder(String folderPath) {
        File destDir = new File(folderPath);
        if (!destDir.exists()) {
            destDir.mkdirs();
        }
        return destDir;
    }


}