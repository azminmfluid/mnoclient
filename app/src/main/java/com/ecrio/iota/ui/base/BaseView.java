package com.ecrio.iota.ui.base;

public interface BaseView< P extends BasePresenter> {

    /**
     * @return BasePresenter
     */
    P getBasePresenter();
}
