package com.ecrio.iota.ui.file;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.chat.ChatFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mfluiduser on 11/25/2016.
 */

public class FilePic extends DialogFragment implements View.OnClickListener{

    private View root;
    private LinearLayout camera, gallery;
//    private MainActivity bookingFragment;
    private ChatFragment fragment;
    private String tag = "ProfilePic";
    private String picturePath = "";

    @BindView(R.id.LAYgallery)
    LinearLayout mGalleryLayout;

    @BindView(R.id.LAYcamera)
    LinearLayout mCameraLayout;

    @BindView(R.id.BTNcancel)
    Button mCancelBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        root = inflater.inflate(R.layout.dialouge_attchment, container, false);
        ButterKnife.bind(this, root);
        mCameraLayout.setOnClickListener(this);
        mGalleryLayout.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
        return root;
    }

    private static final int GALLERY = 1;

    public static final int CAMERA = 2;

    private void handleGallery() {
        if (fragment != null) {
            fragment.handleGallery();
            FilePic.this.dismiss();
        }
    }

    private void handleCamera() {
        if (fragment != null) {
            fragment.handleCamera();
            FilePic.this.dismiss();
        }
    }

    public void setParent(ChatFragment chatFragment) {
        this.fragment = chatFragment;
    }

    @Override
    public void onClick(View v) {
        if(v == mCameraLayout) {

            handleCamera();

        }else if(v == mGalleryLayout) {

            handleGallery();

        }else if(v == mCancelBtn) {

            FilePic.this.dismiss();

        }
    }

}