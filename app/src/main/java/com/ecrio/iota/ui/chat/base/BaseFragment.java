package com.ecrio.iota.ui.chat.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public abstract class BaseFragment<V extends BaseView<P>, P extends BasePresenter> extends BaseMvpFragment<V, P> {


    private View mRoot;

    protected boolean D = true;

    protected abstract int getLayoutId();

    protected void initWidget(View root) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (mRoot != null) {

            if (D) Log.d2("mvp.Log", "mRoot is not null.");

            ViewGroup parent = (ViewGroup) mRoot.getParent();

            if (parent != null) {

                if (D) Log.d2("mvp.Log", "parent is not null.");

                parent.removeView(mRoot);

            }

        } else {

            if (D) Log.d2("mvp.Log", "mRoot is null, hence inflating layout");

            mRoot = inflater.inflate(getLayoutId(), container, false);

            initWidget(mRoot);

//            initData();

        }
        return mRoot;

//        return inflater.inflate(getLayoutId(), container, false);
    }

}