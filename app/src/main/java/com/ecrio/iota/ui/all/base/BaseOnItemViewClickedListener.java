package com.ecrio.iota.ui.all.base;

import com.ecrio.iota.ui.list_base.Presenter;

/**
 * Interface for receiving notification when an item view holder is clicked.
 */
public interface BaseOnItemViewClickedListener {

    /**
     * Called when an item inside a row gets clicked.
     *
     * @param itemViewHolder The view holder of the item that is clicked.
     * @param item           The item that is currently selected.
     */
    void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, int position);
}
