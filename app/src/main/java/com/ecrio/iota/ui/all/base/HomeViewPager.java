package com.ecrio.iota.ui.all.base;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Mfluid on 8/4/2016.
 */
public class HomeViewPager extends ViewPager {

    private Context mContext;
    private boolean isSwipable;
    private int currentPage = 0;


    public HomeViewPager(Context context) {
        super(context);
    }

    public HomeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setActivity(Context context) {
        mContext = context;
        isSwipable = true;
        setOffscreenPageLimit(1);
        setOnPageChangeListener(new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }
        });
        addOnPageChangeListener(new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }
        });
    }

    public HomeViewPager setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
        return this;
    }

    public final int getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setOnScrollChangeListener(OnScrollChangeListener l) {
        super.setOnScrollChangeListener(l);
    }

    public void setSwipable(boolean b) {
        isSwipable = b;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return isSwipable && super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isSwipable && super.onInterceptTouchEvent(ev);
    }
}