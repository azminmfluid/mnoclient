package com.ecrio.iota.ui.chat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.CalendarContract;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.chat.ChatRepository;
import com.ecrio.iota.data.chat.DataSource;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.enums.IotaConstants;
import com.ecrio.iota.intf.CarousalInterface;
import com.ecrio.iota.intf.CarousalProgressListner;
import com.ecrio.iota.intf.ReplyInterface;
import com.ecrio.iota.intf.RichInterface;
import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.ui.base.OnItemViewSelectedListener;
import com.ecrio.iota.ui.chat.data.SingleClick;
import com.ecrio.iota.ui.chat.item.file_rcv.IncomingFilePresenter;
import com.ecrio.iota.ui.chat.item.file_rcv.IncomingFileView;
import com.ecrio.iota.ui.chat.item.file_send.MFiles;
import com.ecrio.iota.ui.chat.item.file_send.OutgoingFilePresenter;
import com.ecrio.iota.ui.chat.item.info.InfoMsgPresenter;
import com.ecrio.iota.ui.chat.item.info.InfoMsgView;
import com.ecrio.iota.ui.chat.item.rc_carousal.RcCarousalMsgPresenter;
import com.ecrio.iota.ui.chat.item.rc_carousal.RcCarousalMsgView;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellView;
import com.ecrio.iota.ui.chat.item.rc_general.RcGeneralMsgPresenter;
import com.ecrio.iota.ui.chat.item.rc_general.RcGeneralMsgView;
import com.ecrio.iota.ui.chat.item.rc_reply.RcReplyMsgPresenter;
import com.ecrio.iota.ui.chat.item.rc_reply.RcReplyMsgView;
import com.ecrio.iota.ui.chat.item.txt_recv.IncomingMsgPresenter;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.chat.model.ReplyItem;
import com.ecrio.iota.ui.file.FileHelper;
import com.ecrio.iota.ui.file.FileUtils;
import com.ecrio.iota.ui.list_base.ChatItemBridgeAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.utility.FileLoader;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.StringUtils;
import com.google.common.base.Strings;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChatPresenter implements ChatContract.Presenter {

    private final FileLoader fileLoader;
    private ChatContract.View mView;
    private ChatRepository mRepository;

    private boolean started;
    List<String> selectedMsgIDs = new ArrayList<String>();
    private OnItemViewSelectedListener viewSelectedListener;


    ChatPresenter(ChatContract.View mView, ChatRepository repository) {

        this.mView = mView;

        this.mRepository = repository;

        fileLoader = new FileLoader();

    }

    public void testDownload() {

    }

    @Override
    public void startIMSession(final String mNumber) {

        mRepository.startIMSession(new String[]{mNumber}, IotaConstants.sessionType_OneToOne, IotaConstants.uriType_TEL, 0, new DataSource.GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {
                // successfully started new session.
            }

            @Override
            public void onFailed() {
                // failed to start new session.
            }
        });

    }

    @Override
    public void performSendMessage() {

        String conversationPhNo = this.mView.getConversationPhNo();

        int conversationUserID = this.mView.getConversationUserId();

        int conversationID = this.mView.getConversationId();

        // getting text from message field.
        String newMessage = this.mView.getMessageText();

        // return error message, if the message is empty.
        if (newMessage.isEmpty()) {

            this.mView.showError(R.string.empty_message);

            return;

        }

        // if the session is not active then add message to the queue.
        if (!mView.isConnected()) {
            Log.DebugChat("session is not connected.");
            // clear the message field.
            this.mView.clearMessageText();
            Log.DebugChat("message saving to queue.");
            // save message as queued and send later in order.
            mRepository.queueMessage(conversationID, conversationUserID, newMessage, new DataSource.GetSaveCallback() {

                @Override
                public void onSuccess(String msgID) {
                    Log.DebugChat("message Internal ID is " + msgID);
                }

                @Override
                public void onFailed() {

                }

            });

            mView.initiateSession();
            return;
        }

        // clear the typed message.
        this.mView.clearMessageText();

        // If there is message queue exists. then add the message to the queue
        if (this.mView.hasQueuedMessage()) {
            Log.DebugChat(" has queued message");
            mRepository.queueMessage(conversationID, conversationUserID, newMessage, new DataSource.GetSaveCallback() {

                @Override
                public void onSuccess(String msgID) {

                }

                @Override
                public void onFailed() {

                }

            });
        } else {
            Log.DebugChat("sending message to server.");
            // sending text message to server.
            mRepository.saveMessage(conversationID, conversationUserID, newMessage, new DataSource.GetSaveCallback() {

                @Override
                public void onSuccess(String msgID) {

                    mRepository.sendMessage(conversationID, conversationPhNo, conversationUserID, newMessage, msgID, new DataSource.GetChatCallback() {

                        @Override
                        public void onSuccess(String conversationID) {
                            Log.DebugChat("message send successfully.");
                        }

                        @Override
                        public void onFailed() {
                            Log.DebugChatE("message send failed.");
                        }

                    });

                }

                @Override
                public void onFailed() {

                }

            });
        }

    }

    @Override
    public void performSendReply() {

        String conversationPhNo = this.mView.getConversationPhNo();

        int conversationUserID = this.mView.getConversationUserId();

        int conversationID = this.mView.getConversationId();

        String replyData = mView.getReplyData();
        if (!mView.isConnected()) {
            Log.DebugChat("session is not connected.");
            Log.DebugChat("reply message not saving to queue.");
            mView.showError(R.string.session_ended);
            // save message as queued and send later in order.
            mRepository.queueReplyMessage(conversationID, conversationUserID, replyData, new DataSource.GetSaveCallback() {

                @Override
                public void onSuccess(String msgID) {
                    Log.DebugChat("message Internal ID is " + msgID);
                }

                @Override
                public void onFailed() {

                }

            });
            mView.initiateSession();
            return;
        }

        mRepository.saveReplyMessage(conversationID, conversationUserID, replyData, new DataSource.GetReplySaveCallback() {
            @Override
            public void onSuccess(String msgID) {
                mRepository.sendReplyMessage(conversationID, conversationPhNo, conversationUserID, replyData, msgID, new DataSource.GetReplyCallback() {

                    @Override
                    public void onSuccess(String conversationID) {
                        Log.DebugChat("message send successfully.");
                    }

                    @Override
                    public void onFailed() {
                        Log.DebugChatE("message send failed.");
                    }

                });
            }

            @Override
            public void onFailed() {

            }
        });
    }

    @Override
    public void setOnItemViewSelectedListener(OnItemViewSelectedListener viewSelectedListener) {

        this.viewSelectedListener = viewSelectedListener;
    }

    public OnItemViewSelectedListener getViewSelectedListener() {
        return viewSelectedListener;
    }

    @Override
    public void startComposing() {

        int conversationID = this.mView.getConversationId();

        mRepository.sendComposing(conversationID, 1, new DataSource.GetChatCallback() {

            @Override
            public void onSuccess(String conversationID) {
                // send composing start success.
            }

            @Override
            public void onFailed() {
                // send composing start failed.
            }

        });

    }

    @Override
    public void stopComposing() {

        int conversationID = this.mView.getConversationId();

        mRepository.sendComposing(conversationID, 0, new DataSource.GetChatCallback() {

            @Override
            public void onSuccess(String conversationID) {
                // send composing stop success.
            }

            @Override
            public void onFailed() {
                // send composing stop failed.
            }

        });

    }

    @Override
    public void performReadMessage(int sessionID, int msgID) {

        int conversationUserID = this.mView.getConversationUserId();

        int conversationID = this.mView.getConversationId();

        // if current session ID & message's session ID is same, then send read status.
        if (sessionID == conversationID) {

            mRepository.saveMessageReadStatus(conversationID, conversationUserID, String.valueOf(msgID), new DataSource.GetChatCallback() {

                @Override
                public void onSuccess(String conversationID) {

                    mRepository.sendMessageReadStatus(conversationID, DeliveryStatus.READ.ordinal(), msgID, new DataSource.GetChatCallback() {

                        @Override
                        public void onSuccess(String conversationID) {

                        }

                        @Override
                        public void onFailed() {

                        }

                    });
                }

                @Override
                public void onFailed() {

                }

            });

        }
    }

    @Override
    public void canGoBack() {
        Log.Debug("Log_Size_", "size is ..........." + selectedMsgIDs);
        if (selectedMsgIDs.size() == 0) {
            mView.goBack(true);
        } else {
            selectedMsgIDs.clear();
            mView.hideDeleteMenu();
            mView.goBack(false);
        }
    }

    @Override
    public void deleteMessages() {

        int conversationID = this.mView.getConversationId();

        int conversationUserID = this.mView.getConversationUserId();

        mRepository.deleteMessage(conversationID, conversationUserID, new DataSource.DeleteCallback() {
            @Override
            public void onSuccess() {

                selectedMsgIDs.clear();
                mView.hideDeleteMenu();
            }

            @Override
            public void onFailed() {

            }
        });
    }

    @Override
    public void performSendFile() {

        mView.showFilePickerDialog();
    }

    @Override
    public void attemptQueuedMessageSend(int msgID, String msgText) {

        String conversationPhNo = this.mView.getConversationPhNo();

        int conversationUserID = this.mView.getConversationUserId();

        int conversationID = this.mView.getConversationId();

        // if the session is not active then add message to the queue.
        if (!mView.isConnected()) {

            Log.DebugQueue("Session is not Established...");
            return;
        }
        Log.DebugQueue("Session is Established...");
        // save message as queued and send later in order.
        mRepository.updateQueuedMessage(conversationID, conversationUserID, msgID, new DataSource.UpdateQueueCallback() {
            @Override
            public void onSuccess(int msgID) {

            }

            @Override
            public void onFailed() {

            }
        });


        // sending data to server.
        mRepository.sendMessage(conversationID, conversationPhNo, conversationUserID, msgText, String.valueOf(msgID), new DataSource.GetChatCallback() {

            @Override
            public void onSuccess(String conversationID) {

            }

            @Override
            public void onFailed() {

            }

        });

    }

    @Override
    public void attemptQueuedReplySend(int msgID, String msgText) {

        String conversationPhNo = this.mView.getConversationPhNo();

        int conversationUserID = this.mView.getConversationUserId();

        int conversationID = this.mView.getConversationId();

        // if the session is not active then add message to the queue.
        if (!mView.isConnected()) {

            Log.DebugQueue("Session is not Established...");
            return;
        }
        Log.DebugQueue("Session is Established...");
        // save message as queued and send later in order.
        mRepository.updateQueuedReplyMessage(conversationID, conversationUserID, msgID, new DataSource.UpdateQueueCallback() {
            @Override
            public void onSuccess(int msgID) {

            }

            @Override
            public void onFailed() {

            }
        });


        // sending data to server.
        mRepository.sendReplyMessage(conversationID, conversationPhNo, conversationUserID, msgText, String.valueOf(msgID), new DataSource.GetReplyCallback() {

            @Override
            public void onSuccess(String conversationID) {
                Log.DebugChat("queued reply send successfully.");
            }

            @Override
            public void onFailed() {
                Log.DebugChatE("queued reply send failed.");
            }

        });

    }

    @Override
    public void attemptQueuedFileSend(int msgID, int fileID, String filePath, byte[] thumb) {
        Log.DebugFile("trying to send queued file message. fileID: " + fileID);
        String conversationPhNo = this.mView.getConversationPhNo();
        int conversationUserID = this.mView.getConversationUserId();
        int conversationID = this.mView.getConversationId();
        // if the session is not active then add message to the queue.
        if (!mView.isConnected()) {
            Log.DebugChat("session is not connected.");
            return;
        }

        // sending data to server.
        mRepository.updateQueuedFile(conversationID, conversationUserID, fileID, msgID, new DataSource.UpdateQueueFileCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailed() {

            }
        });

        // sending data to server.
        mRepository.sendQueuedFile(conversationID, conversationPhNo, conversationUserID, filePath, fileID, new DataSource.GetFileCallback() {
            @Override
            public void onSuccess(int internalFileSessionID) {
                mRepository.updateQueuedFile(conversationID, conversationUserID, internalFileSessionID, msgID, new DataSource.UpdateQueueFileCallback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onFailed() {
                    }
                });
            }

            @Override
            public void onFailed() {

                mRepository.updateFailedFile(conversationID, conversationUserID, msgID, new DataSource.UpdateQueueFileCallback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onFailed() {
                    }
                });
            }
        });
    }

    @Override
    public void clearSession(String mNumber) {
        mRepository.resetIMSessionDetail(mNumber, -1);
    }

    @Override
    public void performImogiView() {
        mView.showEmogiView();
    }

    @Override
    public void attemptFileSend() {
        final String conversationPhNo = this.mView.getConversationPhNo();
        final int conversationUserID = this.mView.getConversationUserId();
        final int conversationID = this.mView.getConversationId();
        final String filePath = this.mView.getFilePath();
        final int fileID = -1;
        // if the session is not active then add message to the queue.
        if (!mView.isConnected()) {
            Log.DebugChat("session is not connected.");
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.DebugFile("file message saving to queue.");
                    // saving data to local db and send later in order.
                    mRepository.queueFile(conversationID, conversationPhNo, conversationUserID, filePath, fileID, new DataSource.GetFileCallback() {

                        @Override
                        public void onSuccess(int internalFileSessionID) {

                            mRepository.queueFileMessage(conversationID, conversationUserID, internalFileSessionID, filePath, getBytes(filePath), new DataSource.SaveFileCallback() {
                                @Override
                                public void onSuccess(String msgID) {

                                    Log.DebugChat("message Internal ID is " + msgID);

                                    mView.fetchQueuedMessage();
                                }

                                @Override
                                public void onFailed() {

                                }
                            });

                        }

                        @Override
                        public void onFailed() {
                            // show error message here.
                        }

                    });
                }
            });
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.DebugFile("sending message to server.");
                    // sending data to server.
                    mRepository.sendFile(conversationID, conversationPhNo, conversationUserID, filePath, fileID, new DataSource.SendFileCallback() {

                        @Override
                        public void onSuccess(int internalFileSessionID) {

                            mRepository.saveFileMessage(conversationID, conversationUserID, internalFileSessionID, filePath, getBytes(filePath), new DataSource.SaveFileCallback() {

                                @Override
                                public void onSuccess(String msgID) {
                                    // show success message here.
                                }

                                @Override
                                public void onFailed() {
                                    // show error message here.
                                }

                            });

                        }

                        @Override
                        public void onFailed() {
                            // show error message here.
                        }

                    });
                }
            });
        }
    }

    private byte[] getBytes(String filePath) {
        byte[] thumbnailBitmapBytes = new byte[]{};
        Bitmap thumbnailBitmap = FileUtils.getBitmapByPath(filePath); // Get it with your approach
        if (thumbnailBitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnailBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            thumbnailBitmapBytes = stream.toByteArray();
        }
        return thumbnailBitmapBytes;
    }

    @Override
    public void onDestroy() {

        String conversationPhNo = this.mView.getConversationPhNo();

        int conversationID = this.mView.getConversationId();

        int conversationUserID = this.mView.getConversationUserId();

        mRepository.endIMSession(conversationID, conversationUserID, conversationPhNo, new DataSource.GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {

            }

            @Override
            public void onFailed() {

            }
        });

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        ViewHolder vh = createGridViewHolder(parent);

        vh.mAdapter = new GridItemBridgeAdapter();

        initializeGridViewHolder(vh);

        return vh;
    }

    @Override
    public void onBindViewHolder(ChatContract.Presenter.ViewHolder viewHolder, Object item) {

        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter((ObjectAdapter) item);

        vh.getGridView().setAdapter(vh.mAdapter);
    }

    @Override
    public void onUnbindViewHolder(ChatContract.Presenter.ViewHolder viewHolder) {

        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter(null);

        vh.getGridView().setAdapter(null);
    }

    /**
     * inflate list layout.
     */
    protected ViewHolder createGridViewHolder(ViewGroup parent) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_chat_grid, parent, false);

        return new ViewHolder((RecyclerView) root.findViewById(R.id.browse_grid));

    }

    /**
     * Called after a {@link ViewHolder} is created.
     * Subclasses may override this method and start by calling
     * super.initializeGridViewHolder(ViewHolder).
     *
     * @param vh The ViewHolder to initialize for the vertical grid.
     */
    protected void initializeGridViewHolder(ViewHolder vh) {

//        FocusHighlightHelper.setupBrowseItemFocusHighlight(vh.mAdapter, FocusHighlight.ZOOM_FACTOR_NONE, false);

        Context context = vh.mGridView.getContext();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
//        layoutManager.setReverseLayout(false);
        vh.getGridView().setLayoutManager(layoutManager);

    }

    @Override
    public void onViewReady() {

    }

    /**
     * ViewHolder for the VerticalGridPresenter.
     */
    public static class ViewHolder extends ChatContract.Presenter.ViewHolder {

        final RecyclerView mGridView;

        ChatItemBridgeAdapter mAdapter;

        public ViewHolder(RecyclerView view) {
            super(view);

            mGridView = view;

        }

        public RecyclerView getGridView() {

            return mGridView;
        }

    }

    class GridItemBridgeAdapter extends ChatItemBridgeAdapter {

        public static final int STATE_EMPTY_ITEM = 0;

        public static final int STATE_LOAD_MORE = 1;

        @Override
        protected void onBind(final ViewHolder viewHolder, int position) {

            View itemView = viewHolder.mHolder.view;
//            setAnimation(mView.context(), itemView, position);

            Object item = viewHolder.mItem;

            if (viewHolder.mItem instanceof ChatItem) {
                // if the message is incoming and it's status is not read. then send status as displayed.
                if (viewHolder.mPresenter instanceof IncomingMsgPresenter) {
                    ChatItem mItem = (ChatItem) viewHolder.mItem;
                    if (!mItem.isMsgRead) {
                        int msgID = mItem.msgID;
                        int sessionID = mItem.conversationID;
                        performReadMessage(sessionID, msgID);
                    }
                } else if (viewHolder.mPresenter instanceof IncomingFilePresenter) {
                    ChatItem mItem = (ChatItem) item;
                    if (!mItem.isMsgRead) {
                        int msgID = mItem.msgID;
                        int sessionID = mItem.conversationID;
                        performReadMessage(sessionID, msgID);
                    }
                    IncomingFileView incomingFileView = (IncomingFileView) viewHolder.mHolder.view;
                    if (incomingFileView != null) {
                        incomingFileView.mIVPic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String fileURL = (String) v.getTag();
                                Log.DebugChat("fileURL is " + fileURL);
                                if (incomingFileView.callback != null) {
//                                    fileLoader.get(fileURL, fileExtension, incomingFileView.callback);
                                }
                            }
                        });
                    }
                } else if (viewHolder.mPresenter instanceof RcGeneralMsgPresenter) {
                    ChatItem mItem = (ChatItem) item;
                    int adapterPosition = viewHolder.getAdapterPosition();
                    if (!mItem.isMsgRead) {
                        int msgID = mItem.msgID;
                        int sessionID = mItem.conversationID;
                        performReadMessage(sessionID, msgID);
                    }
                    RcGeneralMsgView richGeneralMsgView = (RcGeneralMsgView) viewHolder.mHolder.view;
                    if (richGeneralMsgView != null) {
                        richGeneralMsgView.setRichInterface(new RichInterface() {
                            @Override
                            public void doCall(String phoneNumber) {
                                if (Strings.isNullOrEmpty(phoneNumber)) {
                                    mView.showNumberError(R.string.no_number);
                                    return;
                                }
                                handleCall(phoneNumber);
                            }

                            @Override
                            public void doOpenLink(String linkURL) {
                                Log.DebugChat("linkURL is " + linkURL);
                                if (Strings.isNullOrEmpty(linkURL)) {
                                    mView.showLinkError(R.string.no_link);
                                    return;
                                }
                                handleLink(linkURL);
                            }

                            @Override
                            public void doReply(String data) {
                                StringBuilder builder = new StringBuilder();
                                builder.append("callback data: ");
                                builder.append(data);
//                                mView.showToast(builder.toString());
                                mView.setReplyData(data);
                            }

                            @Override
                            public void openFile(String filePath) {
                                if(getViewSelectedListener() != null) {
                                    getViewSelectedListener().onItemSelected(viewHolder.mHolder);
                                }
                                displayImage(new File(filePath));
                            }

                            @Override
                            public void doOpenCalendar(CreateCalendarEvent event) {
                                String title = event.getTitle();
                                String description = event.getDescription();
                                String startTimeString = event.getStartTime();
                                String endTimeString = event.getEndTime();
                                Calendar beginTime = StringUtils.getStringToDate(startTimeString);
                                Calendar endTime = StringUtils.getStringToDate(endTimeString);
                                if (beginTime != null && endTime != null) {
                                    Intent intent = new Intent(Intent.ACTION_INSERT)
                                            .setData(CalendarContract.Events.CONTENT_URI)
                                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                                            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                                            .putExtra(CalendarContract.Events.TITLE, title)
                                            .putExtra(CalendarContract.Events.DESCRIPTION, description)
                                            .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false) // just included for completeness
                                            .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                                            .putExtra(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE);
                                    mView.context().startActivity(intent);
                                }
                            }

                            @Override
                            public void downloadFile(View view) {
                                String[] tag = (String[]) view.getTag();
                                if(tag != null) {
                                    String fileURL = tag[1];
                                    String contentType = tag[2];
                                    Log.DebugChat("fileURL is " + fileURL);
                                    Log.DebugChat("fileExtension is " + contentType);
                                    if (richGeneralMsgView.callback != null) {
                                        fileLoader.get(fileURL, contentType, richGeneralMsgView.callback);
                                    } else {
                                        Log.DebugChat("callback is " + null);
                                    }
                                }else{
                                    if (richGeneralMsgView.callback != null) {
                                        richGeneralMsgView.callback.failed(null);
                                    }
                                }
                            }
                        });
                        richGeneralMsgView.progressBarView.setOnClickListener(new SingleClick() {
                            @Override
                            public void onSingleClick(View view) {
                                String[] tag = (String[]) view.getTag();
                                if(tag != null) {
                                    String fileURL = tag[1];
                                    String contentType = tag[2];
                                    Log.DebugChat("fileURL is " + fileURL);
                                    Log.DebugChat("fileExtension is " + contentType);
                                    if (richGeneralMsgView.callback != null) {
                                        fileLoader.get(fileURL, contentType, richGeneralMsgView.callback);
                                    } else {
                                        Log.DebugChat("callback is " + null);
                                    }
                                }else{
                                    if (richGeneralMsgView.callback != null) {
                                        richGeneralMsgView.callback.failed(null);
                                    }
                                }
                            }
                        });
                    }
                } else if (viewHolder.mPresenter instanceof RcCarousalMsgPresenter) {
                    ChatItem mItem = (ChatItem) item;
                    int adapterPosition = viewHolder.getAdapterPosition();
                    if (!mItem.isMsgRead) {
                        int msgID = mItem.msgID;
                        int sessionID = mItem.conversationID;
                        performReadMessage(sessionID, msgID);
                    }
                    RcCarousalMsgView richCarousalMsgView = (RcCarousalMsgView) viewHolder.mHolder.view;
                    if (richCarousalMsgView != null) {
                        richCarousalMsgView.setRichInterface(new CarousalInterface() {
                            @Override
                            public void doCall(String phoneNumber) {
                                if (Strings.isNullOrEmpty(phoneNumber)) {
                                    mView.showNumberError(R.string.no_number);
                                    return;
                                }
                                handleCall(phoneNumber);
                            }

                            @Override
                            public void doOpenLink(String linkURL) {
                                Log.DebugChat("linkURL is " + linkURL);
                                if (Strings.isNullOrEmpty(linkURL)) {
                                    mView.showLinkError(R.string.no_link);
                                    return;
                                }
                                handleLink(linkURL);
                            }

                            @Override
                            public void doReply(String data) {

                                StringBuilder builder = new StringBuilder();
                                builder.append("callback data: ");
                                builder.append(data);
//                                mView.showToast(builder.toString());
                                mView.setReplyData(data);
                            }

                            @Override
                            public void openFile(String filePath) {
                                if(getViewSelectedListener() != null) {
                                    getViewSelectedListener().onItemSelected(viewHolder.mHolder);
                                }
                                displayImage(new File(filePath));
                            }

                            @Override
                            public void downloadFile(View view, CarousalCellView cell) {
                                String[] tag = (String[]) view.getTag();
                                if(tag != null) {
                                    String fileURL = tag[1];
                                    String contentType = tag[2];
                                    // String fileURL = (String) view.getTag();
                                    Log.DebugChat("fileURL is " + fileURL);
                                    if (cell.callback != null) {
                                        fileLoader.get(fileURL, contentType, cell.callback);
                                    }
                                }else{
                                    if (cell.callback != null) {
                                        cell.callback.failed(null);
                                    }
                                }
                            }

                            @Override
                            public void doOpenCalendar(CreateCalendarEvent event) {
                                String title = event.getTitle();
                                String description = event.getDescription();
                                String startTimeString = event.getStartTime();
                                String endTimeString = event.getEndTime();
                                Calendar beginTime = StringUtils.getStringToDate(startTimeString);
                                Calendar endTime = StringUtils.getStringToDate(endTimeString);
                                if (beginTime != null && endTime != null) {
                                    Intent intent = new Intent(Intent.ACTION_INSERT)
                                            .setData(CalendarContract.Events.CONTENT_URI)
                                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                                            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                                            .putExtra(CalendarContract.Events.TITLE, title)
                                            .putExtra(CalendarContract.Events.DESCRIPTION, description)
                                            .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false) // just included for completeness
                                            .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                                            .putExtra(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE);
                                    mView.context().startActivity(intent);
                                }
                            }

                        });
                        richCarousalMsgView.setProgressClickListener(new CarousalProgressListner() {
                            @Override
                            public void onProgressClick(View view, CarousalCellView cell) {
                                String[] tag = (String[]) view.getTag();
                                if(tag != null) {
                                    String fileURL = tag[1];
                                    String contentType = tag[2];
                                    // String fileURL = (String) view.getTag();
                                    Log.DebugChat("fileURL is " + fileURL);
                                    if (cell.callback != null) {
                                        fileLoader.get(fileURL, contentType, cell.callback);
                                    }
                                }else{
                                    if (cell.callback != null) {
                                        cell.callback.failed(null);
                                    }
                                }
                            }
                        });
                    }
                } else if (viewHolder.mPresenter instanceof InfoMsgPresenter) {
                    InfoMsgView infoMsgView = (InfoMsgView) viewHolder.mHolder.view;
                    infoMsgView.infoView.setOnClickListener(new SingleClick() {
                        @Override
                        public void onSingleClick(View view) {
                            mView.showInfoDetail();
                        }
                    });
                }
//                itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View view) {
//
//                        ChatItem mItem = (ChatItem) viewHolder.mItem;
//
//                        int msgID = mItem.msgID;
//
//                        int conversationID = mItem.conversationID;
//
//                        int userID = mItem.userid;
//
//                        msgID = applyMultiSelect(userID, conversationID, msgID);
//
//                        if (msgID != -1) {
//                            // select a new cell.
//                            if (selectedMsgIDs.size() == 0 || !selectedMsgIDs.contains(String.valueOf(msgID))) {
//
//                                mView.showDeleteMenu();
//
//                                selectedMsgIDs.add("" + msgID);
//
//                                Log.Debug("Log_Update_", "called update ..........." + 1);
//                                AppBaseApplication.mDb.updatedMessageSelectedWithMsgIDToMsgTable(userID, conversationID, msgID, 1);
//
//                            }
//                        }
//                        if (selectedMsgIDs.size() == 0) {
//                            mView.hideDeleteMenu();
//                        }
//                        Log.Debug("Log_Size_", "size is ..........." + selectedMsgIDs);
//                        return false;
//                    }
//                });

//                itemView.setOnClickListener(new View.OnClickListener() {
//
//                    public void onClick(View view) {
//
//                        ChatItem mItem = (ChatItem) viewHolder.mItem;
//
//                        int msgID = mItem.msgID;
//
//                        int conversationID = mItem.conversationID;
//
//                        int userID = mItem.userid;
//
//                        if (selectedMsgIDs.size() != 0) {
//
//                            msgID = applyMultiSelect(userID, conversationID, msgID);
//
//                            if (msgID != -1) {
//                                // select a new cell.
//                                if (selectedMsgIDs.size() == 0 || !selectedMsgIDs.contains(String.valueOf(msgID))) {
//
//                                    selectedMsgIDs.add("" + msgID);
//
//                                    Log.Debug("Log_Update_", "called update ..........." + 1);
//                                    AppBaseApplication.mDb.updatedMessageSelectedWithMsgIDToMsgTable(userID, conversationID, msgID, 1);
//
//                                }
//                            }
//                        }
//                        if (selectedMsgIDs.size() == 0) {
//                            mView.hideDeleteMenu();
//                        }
//                        Log.Debug("Log_Size_", "size is ..........." + selectedMsgIDs);
//                        if (viewHolder.mItem instanceof ChatItem) {
//                            if (viewHolder.mPresenter instanceof OutgoingFilePresenter) {
//
//                                Log.DebugFile("File preview for file id" + viewHolder.mHolder.tag);
//                                MFiles oFileSessionDetails = AppBaseApplication.mDb.getOFileSessionDetailsCompleted(Integer.parseInt(viewHolder.mHolder.tag));
//
//                                if (oFileSessionDetails == null) {
//                                    Toast.makeText(mView.context(), "file not delivered", Toast.LENGTH_LONG).show();
//                                    oFileSessionDetails = AppBaseApplication.mDb.getOFileSessionDetailsNotCompleted(Integer.parseInt(viewHolder.mHolder.tag));
//                                }
//                                if (oFileSessionDetails != null) {
//                                    int fileID = oFileSessionDetails.getFileID();
//
//                                    if (fileID == 0)
//                                        openSendFile(oFileSessionDetails.getFilePAth());
//                                    else
//                                        openFile(fileID);
//                                }
//
//                            }
//                        }
//                    }
//                });
            } else if (viewHolder.mItem instanceof ReplyItem) {
                if (viewHolder.mPresenter instanceof RcReplyMsgPresenter) {
                    ReplyItem mItem = (ReplyItem) item;
                    RcReplyMsgView richReplyMsgView = (RcReplyMsgView) viewHolder.mHolder.view;
                    if (richReplyMsgView != null) {
                        richReplyMsgView.setRichInterface(new ReplyInterface() {
                            @Override
                            public void doReply(String data) {
                                StringBuilder builder = new StringBuilder();
                                builder.append("callback data: ");
                                builder.append(data);
//                                mView.showToast(builder.toString());
                                mView.setReplyData(data);
                            }
                        });
                    }
                }
            }
        }

        private int applyMultiSelect(int userID, int conversationID, int msgID) {

            return -1;
//            for (String selectedMsgID : selectedMsgIDs) {
//
//                if (selectedMsgID.contentEquals(String.valueOf(msgID))) {
//
//                    selectedMsgIDs.remove(selectedMsgID);
//                    Log.Debug("Log_Update_", "called update ..........." + 0);
//
//                    AppBaseApplication.mDb.updatedMessageSelectedWithMsgIDToMsgTable(userID, conversationID, msgID, 0);
//
//                    msgID = -1;
//
//                    break;
//                }
//            }
//            return msgID;
        }

        @Override
        protected void onCreate(ViewHolder viewHolder) {


        }

    }

    private void handleLink(String linkURL) {
        mView.openLink(linkURL);
    }

    private void handleCall(String number) {
        boolean has_plus = false;
        String phone = "" + number;
        if (phone.contains("+")) {
            has_plus = true;
        }
        String Regex = "[^\\d]";
        String PhoneDigits = phone.replaceAll(Regex, "");
        if (!isValidPhone(PhoneDigits)) {
            mView.showNumberError(R.string.invalid_number);
            return;
        }
        if (has_plus) {
            phone = "+";
        } else {
            phone = "";
        }
        phone = phone.concat(PhoneDigits);

        mView.makeCall(phone);
    }

    private boolean isValidPhone(String phone) {
        return !TextUtils.isEmpty(phone) && TextUtils.isDigitsOnly(phone);
    }

    private void openSendFile(String targetFilePath) {

        File targetPath = new File(targetFilePath);

        if (!targetPath.exists()) {

            Toast.makeText(mView.context(), "no file found.", Toast.LENGTH_LONG).show();
            return;
        }
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.fromFile(targetPath), FileHelper.getMIMEType(targetPath.getAbsolutePath()));
            PackageManager packageManager = mView.context().getPackageManager();
            ComponentName componentName = i.resolveActivity(packageManager);
            if (componentName != null) {
                mView.context().startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            File file = new File(targetPath.getAbsolutePath());
            Uri photoURI = FileProvider.getUriForFile(mView.context(), mView.context().getApplicationContext().getPackageName() + ".my.package.name.provider", file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(photoURI, FileHelper.getMIMEType(targetPath.getAbsolutePath()));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            mView.context().startActivity(intent);
        }

    }

    private void openFile(int targetFilePath) {
        String FilePath = Environment.getExternalStorageDirectory().toString() + File.separator + FileHelper.FOLDER_NAME + File.separator + targetFilePath;

        boolean exists = FileUtils.checkFolderExists(FilePath);

        if (exists) {
            File targetPath = new File(FilePath + File.separator + "session_files");
            if (!targetPath.exists()) {
                Toast.makeText(mView.context(), "no file found.", Toast.LENGTH_LONG).show();
                return;
            }
            File[] files = targetPath.listFiles();

            if (files.length == 0) {
                Toast.makeText(mView.context(), "no file found.", Toast.LENGTH_LONG).show();
                return;
            }
            File targetFile = files[0];
            displayImage(targetFile);
        } else {
            Toast.makeText(mView.context(), "no file found.", Toast.LENGTH_LONG).show();
        }
    }

    private void displayImage(File targetFile) {
//        Intent i = new Intent(Intent.ACTION_VIEW);
//        Uri uri = GenericFileProvider.getUriForFile(mView.context(), mView.context().getApplicationContext().getPackageName() + ".my.package.name.provider", targetFile);
////        i.setDataAndType(Uri.fromFile(targetFile), FileHelper.getMIMEType(targetFile.getAbsolutePath()));
//        i.setDataAndType(uri, FileHelper.getMIMEType(targetFile.getAbsolutePath()));
//        PackageManager packageManager = mView.context().getPackageManager();
//        ComponentName componentName = i.resolveActivity(packageManager);
//        if (componentName != null) {
//            mView.context().startActivity(i);
//        }

        Intent i = new Intent(Intent.ACTION_VIEW);
        Uri contentUri = FileProvider.getUriForFile(mView.context(), mView.context().getApplicationContext().getPackageName() + ".provider", targetFile);
        i.setDataAndType(contentUri, FileHelper.getMIMEType(targetFile.getAbsolutePath()));
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        ComponentName componentName = i.resolveActivity(mView.context().getPackageManager());
        if (componentName != null) {
            mView.context().startActivity(i);
        }
    }

}