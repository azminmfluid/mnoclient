package com.ecrio.iota.ui.chat.item.rc_general;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.AvatarHelper;
import com.ecrio.iota.data.cache.DpToPxCache;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.data.glide.GlideApp;
import com.ecrio.iota.enums.RichType;
import com.ecrio.iota.intf.RichInterface;
import com.ecrio.iota.model.rich.Action;
import com.ecrio.iota.model.rich.CalendarAction;
import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.model.rich.DialPhoneNumber;
import com.ecrio.iota.model.rich.DialerAction;
import com.ecrio.iota.model.rich.GeneralPurposeCard;
import com.ecrio.iota.model.rich.MapAction;
import com.ecrio.iota.model.rich.Media;
import com.ecrio.iota.model.rich.Message;
import com.ecrio.iota.model.rich.OpenUrl;
import com.ecrio.iota.model.rich.Postback;
import com.ecrio.iota.model.rich.Reply;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.model.rich.ShowLocation;
import com.ecrio.iota.model.rich.Suggestion;
import com.ecrio.iota.ui.chat.data.SingleClick;
import com.ecrio.iota.ui.file.FileHelper;
import com.ecrio.iota.utility.FileLoader;
import com.ecrio.iota.utility.Log;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class RcGeneralMsgView extends FrameLayout {

    private final Context imgcontext;
    private View mRoot;

    @BindView(R.id.TVdesc)
    TextView mDesc;

    @BindView(R.id.TVtext)
    TextView mTitle;

    @BindView(R.id.id_rc_image)
    public ImageView mBGImage;

    @BindView(R.id.id_rc_profile_pic)
    public CircularImageView mIVPic;

    @BindView(R.id.image_layout)
    public ViewSwitcher mViewSwitcher;

    @BindView(R.id.imageView2)
    public ImageView thumbNail;

    @BindView(R.id.update_progress)
    public ProgressBar progressBar;

    @BindView(R.id.update_progress_text)
    public TextView progressBarView;

    @BindView(R.id.id_action_container)
    public LinearLayout mContainer;

    @BindView(R.id.id_reply_container)
    public LinearLayout mReplyContainer;

    public FileLoader.VideoLoaderCallback callback;
    private LayoutInflater inflater;
    private RichInterface listener;

    private int MAX_STANDALONE_CARD_WIDTH = 480;

    //width
    private int SMALL_WIDTH = 120;
    private int MEDIUM_WIDTH = 232;
    //heigth
    private int MIN_CARD_HEIGHT = 112;
    private int MAX_CARD_HEIGHT = 344;
    private int MAX_SMALL_WIDTH_CARD_HEIGHT = 224;

    private int STANDALONE_MAX_CARD_WIDTH = MAX_STANDALONE_CARD_WIDTH;
    private int STANDALONE_MIN_CARD_HEIGHT = MIN_CARD_HEIGHT;
    private int STANDALONE_MAX_CARD_HEIGHT = MAX_CARD_HEIGHT;

    private int CAROUSAL_SMALL_WIDTH = SMALL_WIDTH;
    private int CAROUSAL_SMALL_MIN_CARD_HEIGHT = MIN_CARD_HEIGHT;
    private int CAROUSAL_SMALL_MAX_CARD_HEIGHT = MAX_SMALL_WIDTH_CARD_HEIGHT;

    private int CAROUSAL_MEDIUM_WIDTH = MEDIUM_WIDTH;
    private int CAROUSAL_MEDIUM_MIN_CARD_HEIGHT = MIN_CARD_HEIGHT;
    private int CAROUSAL_MEDIUM_MAX_CARD_HEIGHT = MAX_CARD_HEIGHT;
    private String mMessageText = "",mMessageId = "";

    public RcGeneralMsgView(Context context) {
        this(context, null);
    }

    public RcGeneralMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public RcGeneralMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        imgcontext = context;

        setFocusable(false);

        setFocusableInTouchMode(false);

        inflater = LayoutInflater.from(getContext());

        mRoot = inflater.inflate(R.layout.item_chat_rich_general, this);

        ButterKnife.bind(this, mRoot);
        Log.d2("Final_RC", "" + thumbNail);

        mRoot.setOnClickListener(new SingleClick() {
            @Override
            public void onSingleClick(View view) {

//                listener.handleClick((CarousalCellItem) view.getTag());

            }
        });

        callback = getCallback();

        clear();
        progressBar.setOnClickListener(new SingleClick() {
            @Override
            public void onSingleClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] tag = (String[]) view.getTag();
                        String thumbURL = tag[0];
                        String fileURL = tag[1];
                        String contentType = tag[2];
                        Log.DebugChat("fileURL is " + fileURL);
                        String fileFormat = FileHelper.getFileFormatFromContentType(contentType);
                        Log.DebugChat("fileExtension is " + fileFormat);
                        String  downloadingFile= FileHelper.getFileNameFromUrl(fileURL) + fileFormat;
                        final String targetFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + downloadingFile;
                        File targetFile = new File(targetFilePath);
                        android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
                        if (targetFile.exists()) {
                            Log.d2("Final_RC", "targetFile exists ");
                            Log.DebugChat(fileURL + "\nfile is already downloaded");
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    int displayedChild = mViewSwitcher.getDisplayedChild();
                                    if (displayedChild == 0)
                                        mViewSwitcher.setDisplayedChild(1);
                                }
                            });
                            boolean audioVideo = FileHelper.isAudioVideo(targetFilePath);
                            boolean gif = FileHelper.isGif(targetFilePath);
                            boolean image = FileHelper.isImage(targetFilePath);
                            if (audioVideo) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!Strings.isNullOrEmpty(thumbURL)) {
                                            GlideApp
                                                    .with(context)
                                                    .load(thumbURL).error(R.drawable.dummy_image)
                                                    .into(mBGImage);
                                        } else {

                                            GlideApp
                                                    .with(context)
                                                    .asBitmap()
                                                    .load(Uri.fromFile(new File(targetFilePath)))
                                                    .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                                    .into(mBGImage);
                                        }
                                    }
                                });
                            } else if (gif) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
//                                        GlideApp
//                                                .with(context)
//                                                .asGif()
//                                                .load(Uri.fromFile(new File(targetFilePath)))
//                                                .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
//                                                .into(mBGImage);

                                        if (!Strings.isNullOrEmpty(thumbURL)) {
                                            GlideApp
                                                    .with(context)
                                                    .load(thumbURL).error(R.drawable.dummy_image)
                                                    .into(mBGImage);
                                        } else {
                                            GlideApp
                                                    .with(context)
                                                    .asGif()
//                                                .load("http://i.kinja-img.com/gawker-media/image/upload/s--B7tUiM5l--/gf2r69yorbdesguga10i.gif")
//                                                .load("https://raw.githubusercontent.com/Giphy/GiphyAPI/master/api_giphy_header.gif")
                                                    .load(Uri.fromFile(new File(targetFilePath)))
                                                    .error(R.drawable.dummy_image)
                                                    .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                                    .into(mBGImage);
                                        }
                                    }
                                });
                            } else if (image) {
//                                bmThumbnail = ImageUtils.getMiddlePictureInTimeLineGif(targetFile.getAbsolutePath(), DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_short_height));
//                                if (bmThumbnail != null) {
//                                    bmThumbnail = ImageUtils.getResizedBitmap(bmThumbnail, DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_short_height));
//                                }
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
//                                        mBGImage.setImageBitmap(bmThumbnail);
                                        if (!Strings.isNullOrEmpty(thumbURL)) {
                                            GlideApp
                                                    .with(context)
                                                    .load(thumbURL).error(R.drawable.dummy_image)
                                                    .into(mBGImage);
                                        } else {
                                            GlideApp
                                                    .with(context)
                                                    .asBitmap()
                                                    .load(Uri.fromFile(new File(targetFilePath)))
                                                    .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                                    .into(mBGImage);
                                        }
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
//                                        mBGImage.setImageBitmap(bmThumbnail);
                                        if (!Strings.isNullOrEmpty(thumbURL)) {
                                            GlideApp
                                                    .with(context)
                                                    .load(thumbURL).error(R.drawable.dummy_image)
                                                    .into(mBGImage);
                                        } else {
                                            GlideApp
                                                    .with(context)
                                                    .asBitmap()
                                                    .load(Uri.fromFile(new File(targetFilePath)))
                                                    .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                                    .into(mBGImage);
                                        }
                                    }
                                });
                            }
                        } else {
                            Log.d2("Final_RC", "targetFile not exists ");
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (!Strings.isNullOrEmpty(thumbURL)) {
                                        GlideApp
                                                .with(context)
                                                .load(thumbURL).error(R.drawable.dummy_image)
                                                .into(mBGImage);
                                        GlideApp
                                                .with(context)
                                                .load(thumbURL)
                                                .placeholder(R.drawable.dummy_image)
                                                .into(thumbNail);
                                    }
                                    // performing click action to download actual media in background.
//                                    progressBarView.performClick();
                                    if (listener != null)
                                        listener.downloadFile(progressBarView);
                                }
                            });
                        }
                    }
                })/*.start()*/;
            }
        });
        mBGImage.setOnClickListener(new SingleClick() {
            @Override
            public void onSingleClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] tag = (String[]) progressBarView.getTag();
                        String fileURL = tag[1];
                        String contentType = tag[2];
                        Log.DebugChat("fileURL is " + fileURL);
                        Log.DebugChat("contentType is " + contentType);
                        if (!Strings.isNullOrEmpty(fileURL) && !Strings.isNullOrEmpty(contentType)) {
//                        String fileFormat = FileHelper.getFileFormatFromContentType(contentType);
                            String fileFormat = FileHelper.getFileFormatFromUrlAndContentType(fileURL, contentType);
                            Log.DebugChat("fileExtension is " + fileFormat);
                            String downloadingFile = FileHelper.getFileNameFromUrl(fileURL) + fileFormat;
                            Log.DebugChat("fullFileName is " + downloadingFile);
                            final String targetFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + downloadingFile;
                            File targetFile = new File(targetFilePath);
                            Handler handler = new Handler(Looper.getMainLooper());
                            if (targetFile.exists()) {
                                Log.DebugChat("file exists");
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.openFile(targetFilePath);
                                    }
                                });
                            } else {
                                Log.DebugChat("file doesn't exists");
                            }
                        }
                    }
                }).start();
            }
        });
    }

    @NonNull
    private FileLoader.VideoLoaderCallback getCallback() {
        return new FileLoader.VideoLoaderCallback() {
            @Override
            public void refresh(String url, boolean status) {
                if (status) {
                    Log.Debug(url + " file downloaded successfully");
                    mViewSwitcher.setDisplayedChild(1);
//                    progressBar.performClick();
                    checkFile((String[]) progressBar.getTag());
                }
            }

            @Override
            public void failed(String url) {
                Log.d("downloaded failed " + url);
                int displayedChild = mViewSwitcher.getDisplayedChild();
                if (displayedChild == 1) {
                    mViewSwitcher.setDisplayedChild(0);
                }
                progressBar.setProgress(0);
                progressBarView.setText(0 + "%");
//                String downloadingFile = FileHelper.getFileNameFromUrl(url);
                Toast.makeText(getContext(), "Failed to download file.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void refresh(String url, int progress, int percentage, boolean status) {
                if (!status) {
                    int displayedChild = mViewSwitcher.getDisplayedChild();
                    if (displayedChild == 1) {
                        mViewSwitcher.setDisplayedChild(0);
                    }
                    progressBar.setProgress(progress);
                    progressBarView.setText(progress + "%");
                } else {
                    Log.d("downloaded success");
                    mViewSwitcher.setDisplayedChild(1);
//                    progressBar.performClick();
                    checkFile((String[]) progressBar.getTag());
                }
            }
        };
    }


    public void setMessageTedxt(String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mContainer.removeAllViews();
                        mReplyContainer.removeAllViews();
                    }
                });
                boolean added = false;
                boolean replyadded = false;
                RichCardEntity richCard = new Gson().fromJson(message, RichCardEntity.class);
                Log.DebugRich("===PARSING===");
                int richTYPE = -1;
                if (richCard == null) {
                    Log.DebugRich("rich card object is null");
                } else {
                    Message richMsg = richCard.getMessage();
                    if (richMsg != null) {
                        if (richMsg.isGeneralPurpose()) {
                            richTYPE = RichType.GENERAL.ordinal();
                            GeneralPurposeCard card = richMsg.getGeneralPurposeCard();
                            if (card != null) {
                                Content content = card.getContent();
                                String title = content.getTitle();
                                Log.DebugAdapter("title is " + title);
                                String description = content.getDescription();
                                Log.DebugAdapter("description is " + description);

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTitle.setText(title);
                                        mDesc.setText(description);
                                        if (Strings.isNullOrEmpty(title)) {
                                            mTitle.setVisibility(INVISIBLE);
                                        }
                                        if (Strings.isNullOrEmpty(description)) {
                                            mDesc.setVisibility(GONE);
                                        }
                                    }
                                });
                                Media media = content.getMedia();
                                if (media != null) {
                                    String mediaUrl = media.getMediaUrl();
                                    String mediaContentType = media.getMediaContentType();
                                    if (mediaUrl != null && !Strings.isNullOrEmpty(mediaUrl)) {
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                String thumbnail = "";
                                                if (!Strings.isNullOrEmpty(media.getThumbnailUrl()) && !Strings.isNullOrEmpty(media.getThumbnailContentType())) {
//                                    thumbnail = media.getThumbnailUrl() + FileHelper.getFileFormatFromContentType(media.getThumbnailContentType());
//                                                    thumbnail = media.getThumbnailUrl() + FileHelper.getFileFormatFromUrlAndContentType(media.getThumbnailUrl(), media.getThumbnailContentType());
                                                    thumbnail = FileHelper.getFileThumbURLFromUrlAndContentType(media.getThumbnailUrl(), media.getThumbnailContentType());
                                                }
                                                Log.DebugAdapter("final thumbnail URL:-\n" + thumbnail);

                                                if (!Strings.isNullOrEmpty(mediaUrl)) {
                                                    mViewSwitcher.setVisibility(View.VISIBLE);
                                                    // mBGImage.setTag(mediaUrl);
                                                    String[] tag = {thumbnail, mediaUrl, mediaContentType};
                                                    // set tag
                                                    progressBarView.setTag(tag);
                                                    // set tag
                                                    progressBar.setTag(tag);
                                                    Log.DebugAdapter("performing file check...");
                                                    // progressBar.performClick();
                                                    checkFile((String[]) progressBar.getTag());
                                                }
                                            }
                                        });
                                    } else {
                                        // hide media
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                mViewSwitcher.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                } else {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            mViewSwitcher.setVisibility(View.GONE);
                                        }
                                    });
                                }

                                List<Suggestion> suggestions = content.getSuggestions();
                                if (suggestions != null) {
                                    for (Suggestion suggestion : suggestions) {
                                        if (suggestion != null) {
                                            Action action = suggestion.getAction();
                                            Reply reply = suggestion.getReply();
                                            if (reply != null) {
                                                Log.DebugAdapter(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                addReplyView(mReplyContainer, reply);
                                            } else if (action != null) {
                                                if (added) {
                                                    if (suggestions.size() >= 1) {
                                                        addDividerView(mContainer);
                                                    }
                                                }
                                                added = true;
                                                Log.DebugAdapter(String.format("action data found \"%s\"", action.getDisplayText()));
                                                addActionView(mContainer, action);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Log.DebugRich("===COMPLETED===");
            }
        }).start();
    }

    public void setMessageText(String message2, String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mMessageText.equals(message2) && String.valueOf(mMessageId).equals(String.valueOf(id))) return;
                mMessageText = message2;
                mMessageId = id;
                android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mContainer.removeAllViews();
                        mReplyContainer.removeAllViews();
                    }
                });
                boolean added = false;
                boolean replyadded = false;
                RichCardEntity richCard = new Gson().fromJson(mMessageText, RichCardEntity.class);
                Log.DebugRich("===PARSING===");
                int richTYPE = -1;
                if (richCard == null) {
                    Log.DebugRich("rich card object is null");
                } else {
                    Message richMsg = richCard.getMessage();
                    if (richMsg != null) {
                        if (richMsg.isGeneralPurpose()) {
                            richTYPE = RichType.GENERAL.ordinal();
                            GeneralPurposeCard card = richMsg.getGeneralPurposeCard();
                            if (card != null) {
                                Content content = card.getContent();
                                String title = content.getTitle();
                                Log.DebugAdapter("title is " + title);
                                String description = content.getDescription();
                                Log.DebugAdapter("description is " + description);

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTitle.setText(title);
                                        mDesc.setText(description);
                                        if (Strings.isNullOrEmpty(title)) {
                                            mTitle.setVisibility(GONE);
                                        } else {
                                            mTitle.setVisibility(VISIBLE);
                                        }
                                        if (Strings.isNullOrEmpty(description)) {
                                            mDesc.setVisibility(GONE);
                                        } else {
                                            mDesc.setVisibility(VISIBLE);
                                        }
                                    }
                                });
                                Media media = content.getMedia();
                                if (media != null) {
                                    String mediaUrl = media.getMediaUrl();
                                    String mediaContentType = media.getMediaContentType();
                                    Log.DebugAdapter("mediaUrl is " + mediaUrl);
                                    Log.DebugAdapter("mediaContentType is " + mediaContentType);
                                    // if media url is empty or null.hide the media section.
                                    if (mediaUrl != null && !Strings.isNullOrEmpty(mediaUrl)) {
                                        // display media
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                String thumbnail = "";
                                                if (!Strings.isNullOrEmpty(media.getThumbnailUrl()) && !Strings.isNullOrEmpty(media.getThumbnailContentType())) {
//                                    thumbnail = media.getThumbnailUrl() + FileHelper.getFileFormatFromContentType(media.getThumbnailContentType());
//                                                    thumbnail = media.getThumbnailUrl() + FileHelper.getFileFormatFromUrlAndContentType(media.getThumbnailUrl(), media.getThumbnailContentType());
                                                    thumbnail = FileHelper.getFileThumbURLFromUrlAndContentType(media.getThumbnailUrl(), media.getThumbnailContentType());
                                                }
                                                Log.DebugAdapter("final thumbnail URL:-\n" + thumbnail);

                                                if (!Strings.isNullOrEmpty(mediaUrl)) {
                                                    mViewSwitcher.setVisibility(View.VISIBLE);
                                                    // mBGImage.setTag(mediaUrl);
                                                    String[] tag = {thumbnail, mediaUrl, mediaContentType};
                                                    // set tag
                                                    progressBarView.setTag(tag);
                                                    // set tag
                                                    progressBar.setTag(tag);
                                                    Log.DebugAdapter("performing file check...");
                                                    // progressBar.performClick();
                                                    checkFile((String[]) progressBar.getTag());
                                                }
                                            }
                                        });
                                    } else {
                                        // hide media
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                mViewSwitcher.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                } else {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            mViewSwitcher.setVisibility(View.GONE);
                                        }
                                    });
                                }

                                TransitionManager.beginDelayedTransition(mReplyContainer);//layoutlocation is parent layout(In my case relative layout) of the view which you gonna add.
                                List<Suggestion> suggestions = content.getSuggestions();
                                if (suggestions != null) {
                                    for (Suggestion suggestion : suggestions) {
                                        if (suggestion != null) {
                                            Action action = suggestion.getAction();
                                            Reply reply = suggestion.getReply();
                                            if (reply != null) {
                                                if (replyadded) {
                                                    if (suggestions.size() >= 1) {
                                                        addReplyDividerView(mReplyContainer);
                                                    }
                                                }
                                                replyadded = true;
                                                Log.DebugAdapter(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                addReplyView(mReplyContainer, reply);
                                            } else if (action != null) {
                                                if (added) {
                                                    if (suggestions.size() >= 1) {
                                                        addDividerView(mContainer);
                                                    }
                                                }
                                                added = true;
                                                Log.DebugAdapter(String.format("action data found \"%s\"", action.getDisplayText()));
                                                addActionView(mContainer, action);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Log.DebugRich("===COMPLETED===");

            }
        }).start();
    }

    Handler handler = new Handler(Looper.getMainLooper());

    private void addReplyView(LinearLayout container, Reply reply) {
        String displayText = reply.getDisplayText();
        Postback postback = reply.getPostback();
        handler.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_reply_button, container, false);
                TextView mReply = (TextView) layout.findViewById(R.id.id_rc_reply_txt);
                mReply.setText(displayText);
                mReply.setOnClickListener(new SingleClick() {
                    @Override
                    public void onSingleClick(View view) {
                        Reply tag = (Reply) view.getTag();
                        if (tag != null) {
                            String data = new Gson().toJson(tag);
                            if (listener != null)
                                if (!Strings.isNullOrEmpty(data)) {
                                    listener.doReply(data);
                                }
                        }
                    }
                });
                mReply.setTag(reply);
                layout.setTag(reply);
                container.addView(layout);
            }
        });
    }

    private void addDividerView(LinearLayout container) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                View divider = (View) inflater.inflate(R.layout.bottom_div, container, false);
                container.addView(divider);
            }
        });
    }

    private void addReplyDividerView(LinearLayout container) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                View divider = (View) inflater.inflate(R.layout.bottom_div_reply, container, false);
                container.addView(divider);
            }
        });
    }

    private void addActionView(LinearLayout container, Action action) {
        if (action.hasDialerAction()) {
            Log.DebugAdapter("action is dialer");
            DialerAction dialerAction = action.getDialerAction();
            if (dialerAction.hasDialPhone()) {
                DialPhoneNumber dialPhoneNumber = dialerAction.getDialPhoneNumber();
                String displayText = action.getDisplayText();
                String phoneNumber = dialPhoneNumber.getPhoneNumber();
                Log.DebugAdapter("dialing number is " + phoneNumber);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_action_call, container, false);
                        ImageView mActionCall = (ImageView) layout.findViewById(R.id.id_rc_action_call);
                        layout.setOnClickListener(new SingleClick() {
                            @Override
                            public void onSingleClick(View view) {
                                String number = (String) view.getTag();
                                if (listener != null)
                                    listener.doCall(number);
                            }
                        });
                        TextView mActionText = (TextView) layout.findViewById(R.id.id_rc_action_call_txt);
                        mActionText.setText(displayText);
                        mActionCall.setTag(phoneNumber);
                        layout.setTag(phoneNumber);
                        container.addView(layout);
                    }
                });
            }
        } else if (action.hasURLAction()) {
            Log.DebugAdapter("action is URL");
            OpenUrl openUrl = action.getUrlAction().getOpenUrl();
            if (openUrl != null) {
                String displayText = action.getDisplayText();
                String url = openUrl.getUrl();
                Log.DebugAdapter("URL is " + url);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_action_link, container, false);
                        ImageView mActionCall = (ImageView) layout.findViewById(R.id.id_rc_action_link);
                        layout.setOnClickListener(new SingleClick() {
                            @Override
                            public void onSingleClick(View view) {
                                String link = (String) view.getTag();
                                if (listener != null)
                                    listener.doOpenLink(link);
                            }
                        });
                        TextView mActionText = (TextView) layout.findViewById(R.id.id_rc_action_link_txt);
                        mActionText.setText(displayText);
                        mActionCall.setTag(url);
                        layout.setTag(url);
                        container.addView(layout);
                    }
                });
            }
        } else if (action.hasMapAction()) {
            Log.DebugAdapter("action is MAP");
            String displayText = action.getDisplayText();
            MapAction mapAction = action.getMapAction();
            if (mapAction.hasShowLocation()) {
                ShowLocation showLocation = mapAction.getShowLocation();
                String fallbackUrl = showLocation.getFallbackUrl();
                if (!Strings.isNullOrEmpty(fallbackUrl)) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_action_map, container, false);
                            ImageView mActionCall = (ImageView) layout.findViewById(R.id.id_rc_action_link);
                            layout.setOnClickListener(new SingleClick() {
                                @Override
                                public void onSingleClick(View view) {
                                    String link = (String) view.getTag();
                                    if (listener != null)
                                        listener.doOpenLink(link);
                                }
                            });
                            TextView mActionText = (TextView) layout.findViewById(R.id.id_rc_action_link_txt);
                            mActionText.setText(displayText);
                            mActionCall.setTag(fallbackUrl);
                            layout.setTag(fallbackUrl);
                            container.addView(layout);
                        }
                    });
                }
            }
        } else if (action.hasCalendarAction()) {
            Log.DebugAdapter("action is Calendar");
            String displayText = action.getDisplayText();
            CalendarAction calendarAction = action.getCalendarAction();
            if (calendarAction.hasCreateCalendarEvent()) {
                CreateCalendarEvent calendarEvent = calendarAction.getCreateCalendarEvent();
                String title = calendarEvent.getTitle();
                String description = calendarEvent.getDescription();
                String startTime = calendarEvent.getStartTime();
                String endTime = calendarEvent.getEndTime();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_action_calendar, container, false);
                        ImageView mActionCall = (ImageView) layout.findViewById(R.id.id_rc_action_link);
                        layout.setOnClickListener(new SingleClick() {
                            @Override
                            public void onSingleClick(View view) {
                                CreateCalendarEvent event = (CreateCalendarEvent) view.getTag();
                                if (listener != null)
                                    listener.doOpenCalendar(event);
                            }
                        });
                        TextView mActionText = (TextView) layout.findViewById(R.id.id_rc_action_link_txt);
                        mActionText.setText(displayText);
                        mActionCall.setTag(calendarEvent);
                        layout.setTag(calendarEvent);
                        container.addView(layout);
                    }
                });
            }
        }
    }

    public void clear() {
//        mIVPic.setImageDrawable(null);
//        mIVPic.setImageURI(null);
//        mBGImage.setImageDrawable(null);
//        mBGImage.setImageURI(null);
    }

    public void checkFile(String[] tag) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String thumbURL = tag[0];
                String fileURL = tag[1];
                String contentType = tag[2];
                Log.DebugChat("fileURL is " + fileURL);
                Log.DebugChat("contentType is " + contentType);
                // String fileFormat = FileHelper.getFileFormatFromContentType(contentType);
                String fileFormat = FileHelper.getFileFormatFromUrlAndContentType(fileURL, contentType);
                Log.DebugAdapter("fileExtension is " + fileFormat);
                String downloadingFile = FileHelper.getFileNameFromUrl(fileURL) + fileFormat;
                Log.DebugChat("fileName is " + downloadingFile);
                final String targetFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + downloadingFile;
                File targetFile = new File(targetFilePath);
                android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
                if (targetFile.exists()) {
                    Log.DebugAdapter("targetFile exists ");
                    Log.DebugChat(fileURL + "\nfile is already downloaded");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            int displayedChild = mViewSwitcher.getDisplayedChild();
                            if (displayedChild == 0)
                                mViewSwitcher.setDisplayedChild(1);
                        }
                    });

                    boolean audioVideo = FileHelper.isAudioVideo(targetFilePath);
                    boolean gif = FileHelper.isGif(targetFilePath);
                    boolean image = FileHelper.isImage(targetFilePath);
                    if (audioVideo) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!Strings.isNullOrEmpty(thumbURL)) {
                                    GlideApp
                                            .with(imgcontext)
                                            .load(thumbURL).error(R.drawable.dummy_image)
                                            .into(mBGImage);
                                } else {

                                    GlideApp
                                            .with(imgcontext)
                                            .asBitmap()
                                            .load(Uri.fromFile(new File(targetFilePath)))
                                            .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                            .into(mBGImage);
                                }
                            }
                        });
                    } else if (gif) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!Strings.isNullOrEmpty(thumbURL)) {
                                    GlideApp
                                            .with(imgcontext)
                                            .load(thumbURL).error(R.drawable.dummy_image)
                                            .into(mBGImage);
                                } else {
                                    GlideApp
                                            .with(imgcontext)
                                            .asGif()
                                            .load(Uri.fromFile(new File(targetFilePath)))
                                            .error(R.drawable.dummy_image)
                                            .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                            .into(mBGImage);
                                }
                            }
                        });
                    } else if (image) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
//                                        mBGImage.setImageBitmap(bmThumbnail);
                                if (!Strings.isNullOrEmpty(thumbURL)) {
                                    /*GlideApp
                                            .with(imgcontext)
                                            .load(thumbURL)
                                            .listener(new RequestListener<Drawable>() {
                                                @Override
                                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                    handler.post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            GlideApp
                                                                    .with(imgcontext)
                                                                    .asBitmap()
                                                                    .load(Uri.fromFile(new File(targetFilePath)))
                                                                    .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                                                    .into(mBGImage);
                                                        }
                                                    });
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                    return false;
                                                }
                                            })
                                            .error(R.drawable.dummy_image)
                                            .into(mBGImage);*/
                                    GlideApp
                                            .with(imgcontext)
                                            .asBitmap()
                                            .load(Uri.fromFile(new File(targetFilePath)))
                                            .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                            .into(mBGImage);
                                } else {
                                    GlideApp
                                            .with(imgcontext)
                                            .asBitmap()
                                            .load(Uri.fromFile(new File(targetFilePath)))
                                            .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                            .into(mBGImage);
                                }
                            }
                        });
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!Strings.isNullOrEmpty(thumbURL)) {
                                    GlideApp
                                            .with(imgcontext)
                                            .load(thumbURL).error(R.drawable.dummy_image)
                                            .into(mBGImage);
                                } else {
                                    GlideApp
                                            .with(imgcontext)
                                            .asBitmap()
                                            .load(Uri.fromFile(new File(targetFilePath)))
                                            .override(DpToPxCache.getMediaWidthPixel(R.dimen.stand_alone_media_min_width), DpToPxCache.getMediaHeightPixel(R.dimen.stand_alone_media_short_height))
                                            .into(mBGImage);
                                }
                            }
                        });
                    }
                } else {
                    Log.DebugAdapter("targetFile not exists ");
                    Log.DebugChat(fileURL + "\nfile is not downloaded");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (!Strings.isNullOrEmpty(thumbURL)) {
                                GlideApp
                                        .with(imgcontext)
                                        .load(thumbURL).error(R.drawable.dummy_image)
                                        .into(mBGImage);
                                GlideApp
                                        .with(imgcontext)
                                        .load(thumbURL).fitCenter()
                                        .placeholder(R.drawable.dummy_image)
                                        .into(thumbNail);
                            }
                            // performing click action to download actual media in background.
                            if (callback == null) {
                                Log.DebugChat(fileURL + "\n created new callback");
                                callback = getCallback();
                            }
//                            progressBarView.performClick();
                            if (listener != null)
                                listener.downloadFile(progressBarView);
                        }
                    });
                }
            }
        }).start();
    }

    public void setUserNameText(String username) {

    }

    public void setStatus(int status) {

    }

    public void setAvatarImage(String phone) {
        mIVPic.setVisibility(GONE);
//        getAvatar(phone);
    }

    // Looks to see if an image is in the file system.
    private void getAvatar(String phoneNumber) {


        Drawable contactPic = DrawableCache.getContactPic(phoneNumber);

        if (contactPic == null) {

            String iD = AvatarHelper.contactIdByPhoneNumber(AppBaseApplication.getContext(), phoneNumber);

            Log.Debug("PhoneToBitmap", "iD : " + iD);

            if (iD == null) {

                Drawable drawable = DrawableCache.mInAvatar;

                mIVPic.setImageDrawable(drawable);

                DrawableCache.putContactPic(phoneNumber, drawable);

                return;
            }
            Uri thumbImageUri = getThumbImageUri(Integer.parseInt(iD));
//            Log.Debug("PhoneToBitmap", "URI : " + thumbImageUri);

            if (thumbImageUri != null) {
                Drawable drawable = null;
                try {
                    drawable = Drawable.createFromStream(AppBaseApplication.getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);

                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                    drawable = DrawableCache.mInAvatar;

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);
                }
            } else {
                mIVPic.setImageDrawable(null);
                mIVPic.setImageURI(null);

            }
        } else {
            mIVPic.setImageDrawable(contactPic);

        }

        if (null == mIVPic.getDrawable()) {

            mIVPic.setBackground(DrawableCache.getProPicBack(1, getContext()));
        }

    }

    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }

    public void setRichInterface(RichInterface richInterface) {
        this.listener = richInterface;
    }

    public void removeActionsAndReplies() {
        mContainer.removeAllViews();
        mReplyContainer.removeAllViews();
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setDescription(String description) {
    }
}