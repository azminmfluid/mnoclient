package com.ecrio.iota.ui.chat.data;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;

public class GMangager extends GridLayoutManager {


    public GMangager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setSpanCount(1);
    }

    public GMangager(Context context, int spanCount) {
        super(context, spanCount);
        setSpanCount(1);
        setOrientation(HORIZONTAL);
    }

    public GMangager(Context context, int spanCount, int orientation, boolean reverseLayout) {
        super(context, spanCount, orientation, reverseLayout);
        setSpanCount(1);
    }
}
