package com.ecrio.iota.ui.chat.item.rc_carousal;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.AvatarHelper;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.intf.CarousalInterface;
import com.ecrio.iota.intf.CarousalProgressListner;
import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.model.rich.GeneralPurposeCardCarousel;
import com.ecrio.iota.model.rich.Message;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.ui.chat.data.AdapterListner;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalAdapter;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellItem;
import com.ecrio.iota.ui.chat.data.ItemOffsetDecoration;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellView;
import com.ecrio.iota.utility.FileLoader;
import com.ecrio.iota.utility.Log;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class RcCarousalMsgView extends FrameLayout {

    private View mRoot;

    @BindView(R.id.TVtime)
    public TextView mTVTime;
    @BindView(R.id.IMGpropic)
    public CircularImageView mIVPic;
    private Context mContext;
    private CarousalAdapter mCarousalRichCardRVAdapter;
    private AdapterListner listener = new CarousalItemListner();

    @BindView(R.id.id_form_answer)
    public RecyclerView mCarousalRichCardRV;
    private CarousalInterface richInterface;
    public FileLoader.VideoLoaderCallback callback;

    private List<List<CarousalCellItem>> selectionRichCards;
    private CarousalProgressListner progressListner;
    private String mMessageText="";
    private int msgid= -1;

    public RcCarousalMsgView(Context context) {
        this(context, null);
    }

    public RcCarousalMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public RcCarousalMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        Log.Debug(Log.TAG_CAROUSAL, "******RcCarousalMsgView******");
        mContext = getContext();

        setFocusable(false);
//
        setFocusableInTouchMode(false);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        Log.Debug(Log.TAG_CAROUSAL, "inflating view....");
        mRoot = inflater.inflate(R.layout.item_chat_rich_carousal, this);
        Log.Debug(Log.TAG_CAROUSAL, "binding view....");
        ButterKnife.bind(this, mRoot);

        // adapter
        if (mCarousalRichCardRVAdapter == null) {
            mCarousalRichCardRVAdapter = new CarousalAdapter(mContext);
            mCarousalRichCardRVAdapter.setListner(listener);
        }

        mCarousalRichCardRV.setNestedScrollingEnabled(true);
        mCarousalRichCardRV.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
//        mCarousalRichCardRV.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL, false));
//        mCarousalRichCardRV.addItemDecoration(new ItemOffsetDecoration((int) mContext.getResources().getDimension(R.dimen.card_space)));
        mCarousalRichCardRV.setAdapter(mCarousalRichCardRVAdapter);
//        mCarousalRichCardRV.setHasTransientState(true);
//        mCarousalRichCardRV.setNestedScrollingEnabled(false);


    }


    class CarousalItemListner implements AdapterListner {

        @Override
        public void onSelected(CarousalCellItem cell) {

        }

        @Override
        public void openFile(String path) {
            richInterface.openFile(path);
        }

        @Override
        public void onProgressClick(View view, CarousalCellView cell) {
//            progressListner.onProgressClick(view, cell);
            richInterface.downloadFile(view, cell);
        }

        @Override
        public void onOpenLink(String link) {
            richInterface.doOpenLink(link);
        }

        @Override
        public void onOpencalendar(CreateCalendarEvent event) {
            richInterface.doOpenCalendar(event);
        }

        @Override
        public void makeCall(String number) {
            richInterface.doCall(number);
        }

        @Override
        public void makeReply(String data) {
            richInterface.doReply(data);
        }
    }

    public void setRichInterface(CarousalInterface richInterface) {
        this.richInterface = richInterface;
    }

    public void setListener(AdapterListner listener) {

        this.listener = listener;
//        this.listener.handleClick(getTag());
    }

    public void initw(List<Object> objects) {
//        selectionRichCards = getRichCards(objects);
        mCarousalRichCardRV.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
        mCarousalRichCardRV.addItemDecoration(new ItemOffsetDecoration((int) mContext.getResources().getDimension(R.dimen.card_space)));
        // adapter
        mCarousalRichCardRVAdapter = new CarousalAdapter(mContext);
        mCarousalRichCardRVAdapter.setListner(listener);
        mCarousalRichCardRVAdapter.setData(selectionRichCards.get(0));
        mCarousalRichCardRV.setAdapter(mCarousalRichCardRVAdapter);
    }

    public void init(List<Content> objects) {
        if (selectionRichCards != null && selectionRichCards.size() > 0) {
            Log.d2("Rich_", "-------------------------------");
            Log.d2("Rich_0", selectionRichCards.get(0).size() + "");
            Log.d2("Rich_1", objects.size() + "");
            Log.d2("Rich_", "-------------------------------");
        }
        selectionRichCards = getRichCards(objects);
        mCarousalRichCardRVAdapter.setData(selectionRichCards.get(0));
    }

    public void clear() {
//        mMessageText = "";
//        if (selectionRichCards != null)
//            selectionRichCards.clear();
//        if (mCarousalRichCardRVAdapter != null)
//            mCarousalRichCardRVAdapter.setData(new ArrayList<>());
    }

    public void initTest() {
        if (selectionRichCards != null) selectionRichCards.clear();
        Log.Debug(Log.TAG_CAROUSAL, "called initTest method");
        List<Content> objects = new ArrayList<>();
        objects.add(new Content());
        objects.add(new Content());
        objects.add(new Content());
        objects.add(new Content());
        objects.add(new Content());

        selectionRichCards = getRichCards(objects);
        mCarousalRichCardRVAdapter.setData(selectionRichCards.get(0));
    }

    List<List<CarousalCellItem>> getRichCards(List<Content> objects) {

        List<List<CarousalCellItem>> cells = new ArrayList<List<CarousalCellItem>>();

        List<CarousalCellItem> weekCells = new ArrayList<CarousalCellItem>();

        cells.add(weekCells);

        for (Content date : objects) {

            boolean isSelectable = true;

            boolean isSelected = false;

            CarousalCellItem cell = new CarousalCellItem(date, isSelectable, isSelected, "test");

            weekCells.add(cell);

        }

        return cells;
    }

    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setMessageText(String message2, int id) {
        if(mMessageText.equals(message2) && String.valueOf(msgid).equals(String.valueOf(id))){
            Log.d2("Rich_onBind", "..........Return.................");
            return;
        }
        mMessageText = message2;
        msgid = id;
        new Thread(new Runnable() {
            @Override
            public void run() {
                RichCardEntity richCard = new Gson().fromJson(mMessageText, RichCardEntity.class);
                Log.DebugRich("===PARSING===");
                if (richCard == null) {
                    Log.DebugRich("rich card object is null");
                } else {
                    Message richMsg = richCard.getMessage();
                    if (richMsg != null) {
                        if (richMsg.isCarousal()) {
                            GeneralPurposeCardCarousel cardCarousel = richMsg.getGeneralPurposeCardCarousel();
                            if (cardCarousel != null) {
                                List<Content> content_s = cardCarousel.getContent();
                                if (content_s != null && !content_s.isEmpty()) {
                                    android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            init(content_s);
                                        }
                                    });
                                    for (Content content_ : content_s) {
                                        String title = content_.getTitle();
                                        Log.DebugAdapter("title is " + title);
                                        String description = content_.getDescription();
                                        Log.DebugAdapter("description is " + description);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })/*.start()*/;
        RichCardEntity richCard = new Gson().fromJson(mMessageText, RichCardEntity.class);
        Log.DebugRich("===PARSING===");
        if (richCard == null) {
            Log.DebugRich("rich card object is null");
        } else {
            Message richMsg = richCard.getMessage();
            if (richMsg != null) {
                if (richMsg.isCarousal()) {
                    GeneralPurposeCardCarousel cardCarousel = richMsg.getGeneralPurposeCardCarousel();
                    if (cardCarousel != null) {
                        List<Content> content_s = cardCarousel.getContent();
                        if (content_s != null && !content_s.isEmpty()) {
                            init(content_s);
                            for (Content content_ : content_s) {
                                String title = content_.getTitle();
                                Log.DebugAdapter("title is " + title);
                                String description = content_.getDescription();
                                Log.DebugAdapter("description is " + description);
                            }
                        }
                    }
                }
            }
        }
    }

    public void setUserNameText(String username) {

    }

    public void setStatus(int status) {

    }

    public void setAvatarImage(String phone) {

        // no need to add avatar image
//        getAvatar(phone);
    }

    // Looks to see if an image is in the file system.
    private void getAvatar(String phoneNumber) {
        Log.Debug("PhoneToBitmap", "phoneNumber : " + phoneNumber);

        Drawable contactPic = DrawableCache.getContactPic(phoneNumber);

        if (contactPic == null) {

            String iD = AvatarHelper.contactIdByPhoneNumber(AppBaseApplication.getContext(), phoneNumber);

            Log.Debug("PhoneToBitmap", "iD : " + iD);

            if (iD == null) {

                Drawable drawable = DrawableCache.mInAvatar;

                mIVPic.setImageDrawable(drawable);

                DrawableCache.putContactPic(phoneNumber, drawable);

                return;
            }
            Uri thumbImageUri = getThumbImageUri(Integer.parseInt(iD));
//            Log.Debug("PhoneToBitmap", "URI : " + thumbImageUri);

            if (thumbImageUri != null) {
                Drawable drawable = null;
                try {
                    drawable = Drawable.createFromStream(AppBaseApplication.getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);

                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                    drawable = DrawableCache.mInAvatar;

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);
                }
            } else {
                mIVPic.setImageDrawable(null);
                mIVPic.setImageURI(null);

            }
        } else {
            mIVPic.setImageDrawable(contactPic);

        }

        if (null == mIVPic.getDrawable()) {

            mIVPic.setBackground(DrawableCache.getProPicBack(1, getContext()));
        }

    }

    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }

    public void setProgressClickListener(CarousalProgressListner progressListner) {

        this.progressListner = progressListner;
    }
}