package com.ecrio.iota.ui.chat.model;

import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;

public class ConversationMsg {
    int ConversationIDReply;
    int UserID;
    String message;
    int ConversationID;
    int conversationType;
    int MsgState;
    int ConversationSender;
    double time;
    private int conversationFileID;
    private String conversationFileProgress;
    private int conversationFileCompleted;
    private String replyId;
    private byte[] fileThumb;
    private int conversationRichCardID=-1;
    private int richType;

    public byte[] getFileThumb() {
        return fileThumb;
    }

    @Override
    public String toString() {
        return "UID==" + UserID + " , " + "CID==" + ConversationID + " , " + "CFID=="+conversationFileID + " , " + "CType=="+ getConversationTypeString()+ " , " + "MsgState=="+ getStatusString() + " , "+ "message=="+message ;
    }

    private String getConversationTypeString() {

        if(conversationType == ConversationType.TEXT.ordinal()) return "Text";

        if(conversationType == ConversationType.FILE.ordinal()) return "File";

        if(conversationType == ConversationType.INFO.ordinal()) return "Info";

        return "Other";
    }

    private String getStatusString() {

        if(MsgState == DeliveryStatus.READ.ordinal()) return "Read";

        if(MsgState == DeliveryStatus.NOT_DELIVERED.ordinal()) return "NotDelivered";

        if(MsgState == DeliveryStatus.DELIVERED.ordinal()) return "Delivered";

        if(MsgState == DeliveryStatus.COMPOSING.ordinal()) return "Composing";

        return "Other";
    }

    public int getConversationIDReply() {
        return ConversationIDReply;
    }

    public void setConversationIDReply(int conversationIDReply) {
        ConversationIDReply = conversationIDReply;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgState() {
        return MsgState;
    }

    public void setMsgState(int msgState) {
        MsgState = msgState;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int getConversationID() {
        return ConversationID;
    }

    public void setConversationID(int conversationID) {
        ConversationID = conversationID;
    }

    public int getConversationType() {
        return conversationType;
    }

    public void setConversationType(int conversationType) {
        this.conversationType = conversationType;
    }

    public int getConversationSender() {
        return ConversationSender;
    }

    public void setConversationSender(int conversationSender) {
        ConversationSender = conversationSender;
    }

    public void setConversationFileID(int conversationFileID) {
        this.conversationFileID = conversationFileID;
    }

    public int getConversationFileID() {
        return conversationFileID;
    }

    public void setConversationFileProgress(String conversationFileProgress) {
        this.conversationFileProgress = conversationFileProgress;
    }

    public void setConversationFileThumb(byte[] fileThumb) {
        this.fileThumb = fileThumb;
    }

    public String getConversationFileProgress() {
        return conversationFileProgress;
    }

    public int getConversationFileCompleted() {
        return conversationFileCompleted;
    }

    public void setConversationFileCompleted(int conversationFileCompleted) {
        this.conversationFileCompleted = conversationFileCompleted;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setConversationRichCardID(int conversationRichCardID) {
        this.conversationRichCardID = conversationRichCardID;
    }

    public int getConversationRichCardID() {
        return conversationRichCardID;
    }

    public void setRichType(int richType) {
        this.richType = richType;
    }

    public int getRichType() {
        return richType;
    }
}