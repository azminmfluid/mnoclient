package com.ecrio.iota.ui.chatlist.item;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.AvatarHelper;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.StringUtils;
import com.google.common.base.Strings;

import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListItemView extends FrameLayout {

    private TextView mLastMessageView;
    private View root;

    @BindView(R.id.TVPhoneNumber)
    TextView mLastMessage;

    @BindView(R.id.TVname)
    TextView mContactName;

    @BindView(R.id.TVtime)
    TextView mChatTime;

    @BindView(R.id.TVchatCount)
    TextView mUnReadCount;

    @BindView(R.id.IMGpropic)
    ImageView mIVPic;

    public ChatListItemView(Context context) {
        this(context, null);
    }

    public ChatListItemView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public ChatListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(true);

        setFocusableInTouchMode(true);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        root = inflater.inflate(R.layout.item_chats, this);

        ButterKnife.bind(this, root);
    }

    // changing the background color if the chat item in the list, if need.
    public void setMainBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    /**
     * @param message last message occurred in the chat conversation.
     */
    public void setLastMessageText(String message) {
        if(Strings.isNullOrEmpty(message)){
            mLastMessage.setVisibility(GONE);
        }else {
            mLastMessage.setVisibility(VISIBLE);
        }
        mLastMessage.setText(message);
    }

    /**
     * @param message last message occurred in the chat conversation.
     */
    public void setTypingText(String message) {

        mLastMessage.setText(message);

    }

    /**
     * @param name phone number of the person
     */
    public void setContactNameText(String name) {

        mContactName.setText(name);

    }

    public void setTime(Double sendTime) {
        mChatTime.setText(StringUtils.getDateToString(sendTime));
    }

    public void setMessageCount(int msgCount) {
        mUnReadCount.setText(String.valueOf(msgCount));
    }

    public void setAvatarImage(String phone) {

        getAvatar(phone);
    }

    // Looks to see if an image is in the file system.
    private void getAvatar(String phoneNumber) {

        Drawable contactPic = DrawableCache.getContactPic(phoneNumber);
        Log.Debug("PhoneToBitmap", "contact pic is "+contactPic);
        if(contactPic == null) {

            String iD = AvatarHelper.contactIdByPhoneNumber(AppBaseApplication.getContext(), phoneNumber);
            Log.Debug("PhoneToBitmap", "iD is " + iD);

            if(iD == null) {

                Drawable drawable = DrawableCache.mInAvatar;
                Log.Debug("PhoneToBitmap", "setting default out Avatar icon ");
                mIVPic.setImageDrawable(drawable);

//                DrawableCache.putContactPic(phoneNumber, drawable);

                return;
            }
            Uri thumbImageUri = getThumbImageUri(Integer.parseInt(iD));
            Log.Debug("PhoneToBitmap", "thumb URI is : " + thumbImageUri);

            if (thumbImageUri != null) {
                Drawable drawable = null;
                try {
                    drawable = Drawable.createFromStream(AppBaseApplication.getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());

                    mIVPic.setImageDrawable(drawable);
                    Log.Debug("PhoneToBitmap", "setting contact icon");
//                    DrawableCache.putContactPic(phoneNumber, drawable);

                } catch (FileNotFoundException e) {

                    drawable = DrawableCache.mInAvatar;

                    mIVPic.setImageDrawable(drawable);
                    Log.Debug("PhoneToBitmap", "setting default out Avatar icon ");
//                    DrawableCache.putContactPic(phoneNumber, drawable);
                }
            } else {
                mIVPic.setImageDrawable(null);
                mIVPic.setImageURI(null);

            }
        } else {
            mIVPic.setImageDrawable(contactPic);

        }

        if (null == mIVPic.getDrawable()) {

            mIVPic.setBackground(DrawableCache.getProPicBack(0, getContext()));
        }

    }

    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

}