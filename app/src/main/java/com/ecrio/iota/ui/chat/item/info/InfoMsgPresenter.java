package com.ecrio.iota.ui.chat.item.info;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class InfoMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2("Log_View", "----------onCreateViewHolder-----------------");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        InfoMsgView infoMsgView = new InfoMsgView(parent.getContext());

//        updateViewBackgroundColor(incomingMsgView, false);

        return new ViewHolder(infoMsgView);
    }

    private void updateViewBackgroundColor(InfoMsgView view, boolean selected) {

        Log.d2("Log_View", "selected is " + selected);
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        Log.d2(TAG, item + " called onBindViewHolder");

        ChatItem message = (ChatItem) item;

        viewHolder.tag = message.msgText;

        InfoMsgView view = (InfoMsgView) viewHolder.view;

        updateViewBackgroundColor(view, (message.isSelected != 0));
        view.setTitle(message.msgText);
        view.setImage(message.mPhone);
        view.setWho("" + message.userid);
        view.setState(message.deliveryStatus);

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG, viewHolder.tag + " called onUnbindViewHolder");

        InfoMsgView view = (InfoMsgView) viewHolder.view;

        view.setSelected(false);

    }

}
