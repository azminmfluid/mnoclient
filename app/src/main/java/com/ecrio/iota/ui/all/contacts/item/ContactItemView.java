package com.ecrio.iota.ui.all.contacts.item;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.utility.Log;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactItemView extends FrameLayout {

    @BindView(R.id.IMGpropic)
    public CircularImageView mPhotoView;

    @BindView(R.id.TVname)
    public TextView mContactName;

    private View root;

    public ContactItemView(Context context) {
        this(context, null);
    }

    public ContactItemView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public ContactItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(true);

        setFocusableInTouchMode(true);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        root = inflater.inflate(R.layout.item_contacts, this);

        ButterKnife.bind(this, root);
    }

    // changing the background color if the chat item in the list, if need.
    public void setMainBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    /**
     * @param name name of the contact.
     */
    public void setContactNameText(String name) {
        mContactName.setText(String.valueOf(name));
    }

    public void setContactPhoto(String contactID, String thumbNailPhotoUri) {

//        Log.Debug("PhoneToDrawable","contactID : "+contactID);

        if (thumbNailPhotoUri != null) {

            Uri thumbImageUri = getThumbImageUri(Integer.parseInt(contactID));
//            Log.Debug("PhoneToDrawable","URI : "+thumbImageUri);

            try {
                Drawable pic = Drawable.createFromStream(getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());
//                Log.Debug("PhoneToDrawable","URI : "+thumbImageUri);

                mPhotoView.setImageDrawable(pic);
            } catch (FileNotFoundException e) {
//                e.printStackTrace();
                mPhotoView.setImageURI(thumbImageUri);
            }


        } else {
            mPhotoView.setImageDrawable(null);
            mPhotoView.setImageURI(null);

        }
        if (null == mPhotoView.getDrawable()) {

            mPhotoView.setBackground(DrawableCache.getProPicBack(0, getContext()));
        }

    }

    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

}