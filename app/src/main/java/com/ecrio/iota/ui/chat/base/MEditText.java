package com.ecrio.iota.ui.chat.base;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Mfluid on 5/17/2018.
 */

public class MEditText extends android.support.v7.widget.AppCompatEditText {
    public MEditText(Context context) {
        super(context);
    }

    public MEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }
}