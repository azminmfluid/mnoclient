package com.ecrio.iota.ui.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.all.AllContactsFragmentController;
import com.ecrio.iota.ui.all.AllContactsFragmentParent;
import com.ecrio.iota.ui.base.ChildFragment;
import com.ecrio.iota.ui.chat.ChatContract;
import com.ecrio.iota.ui.chat.ChatFragment;
import com.ecrio.iota.ui.chat.base.BaseMvpParentFragment;
import com.ecrio.iota.ui.chatlist.ChatListBaseFragment;
import com.ecrio.iota.ui.chatlist.model.ChatListItem;
import com.ecrio.iota.utility.Log;
import com.google.common.base.Strings;

import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class MainFragment extends BaseMvpParentFragment<MainContract.View, MainContract.Presenter> implements MainContract.View{

    public static final String FRAGMENT_SETTINGS = "FRAGMENT_SETTINGS";
    public static final String FRAGMENT_ALL_CONTACTS = "FRAGMENT_ALL_CONTACTS";
    public static final String FRAGMENT_ALL_CHATS = "FRAGMENT_ALL_CHATS";
    public static final String FRAGMENT_CHAT = "FRAGMENT_CHAT";

    private ChatListItem chatListItem;
    private String chatPhoneNumber;
    private String chatUserName;

    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public MainContract.Presenter createPresenter() {
        return new MainPresenter(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        TAG = this.getClass().getName();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewReady();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.Debug("Visibility", "MainFragment Menu is null");
        if (childFragment != null) {
            if(isVisibleToUser) {
                Log.Debug("Visibility", "MainFragment Menu is visible");
            }else{
                Log.Debug("Visibility", "MainFragment Menu is not visible");
            }
        }
    }

    @Override
    public void performDelete() {
        if (childFragment != null && childFragment instanceof AllContactsFragmentParent){
            ((AllContactsFragmentParent)childFragment).performDelete();
        }
    }

    @Override
    public void canGoBack() {
        if (childFragment != null && childFragment instanceof AllContactsFragmentParent){
            ((AllContactsFragmentParent)childFragment).canGoBack();
        }
    }

    @Override
    public void performEdit() {
        if (childFragment != null && childFragment instanceof AllContactsFragmentParent){
            ((AllContactsFragmentParent)childFragment).performEdit();
        }
    }

    @Override
    public void clearSelection() {
        if (childFragment != null && childFragment instanceof AllContactsFragmentParent){
            ((AllContactsFragmentParent)childFragment).clearSelection();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.new_fragment_main, container, false);

        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void showDefaultView() {

        if (childFragment != null && childFragment instanceof AllContactsFragmentParent)
            return;
        presenter.openAllContacts();
    }

    @Override
    public void showAllContactsFragment() {

        showChild(FRAGMENT_ALL_CONTACTS);
    }

    @Override
    public void showAllChatsFragment() {

        showChild(FRAGMENT_ALL_CHATS);
    }

    @Override
    public void showChatFragment(String phoneNumber) {

        this.chatListItem = null;

        this.chatPhoneNumber = phoneNumber;

        if (childFragment != null && childFragment instanceof ChatFragment) {
            ChatContract.View view = (ChatContract.View) this.childFragment;
            view.refreshChat(phoneNumber);
            return;
        }
        showChild(FRAGMENT_CHAT);
    }

    @Override
    public void showChatFragment(String phoneNumber, String requestorName) {
        this.chatListItem = null;

        this.chatPhoneNumber = phoneNumber;
        this.chatUserName = requestorName;

        if (childFragment != null && childFragment instanceof ChatFragment)
            return;
        showChild(FRAGMENT_CHAT);
    }

    @Override
    public void showChatFragment(ChatListItem chatListItem) {

        this.chatListItem = chatListItem;

        this.chatPhoneNumber = null;

        if (childFragment != null && childFragment instanceof ChatFragment)
            return;
        showChild(FRAGMENT_CHAT);
    }

    @Override
    public void showSettingsFragment() {

        showChild(FRAGMENT_SETTINGS);
    }

    // This method is used to pop fragment from the stack
    public void popBackStack(String tag) {

        FragmentManager manager = getChildFragmentManager();

        int backStackCount = manager.getBackStackEntryCount();

        if (backStackCount > 0) {

            manager.popBackStack(tag, 0);

            manager.executePendingTransactions();

            childFragment = (ChildFragment) manager.findFragmentByTag(tag);

        }
    }

    private void showChild(String tag) {
        childFragment = createFragment(tag);
        childFragment.setParent(this);
        addChildFragmentToFragment(getChildFragmentManager(), childFragment, R.id.parent_frame, tag);
    }

    private ChildFragment createFragment(String tag) {

        // child fragment manager.
        FragmentManager manager = getChildFragmentManager();

        ChildFragment childFragment = null;
        switch (tag) {

            case FRAGMENT_ALL_CONTACTS:

                // looking for fragment that is already loaded.
                AllContactsFragmentParent allContacts = (AllContactsFragmentParent) manager.findFragmentByTag(tag);

                // checking fragment is null or not.
                if (allContacts == null) {

                    // creating new fragment.
                    allContacts = createAllContactsFragment();

                }

                // returns fragment to be loaded in the container view.
                childFragment = allContacts;

                break;
            case FRAGMENT_ALL_CHATS:

                // looking for fragment that is already loaded.
                ChatListBaseFragment chatListFragment = (ChatListBaseFragment) manager.findFragmentByTag(tag);

                // checking fragment is null or not.
                if (chatListFragment == null) {

                    // creating new fragment.
                    chatListFragment = createChatListFragment();

                }

                // returns fragment to be loaded in the container view.
                childFragment = chatListFragment;

                break;

            case FRAGMENT_CHAT:

                // looking for fragment that is already loaded.
                ChatFragment chatFragment = (ChatFragment) manager.findFragmentByTag(tag);

                // checking fragment is null or not.
                if (chatFragment == null) {

                    // creating new fragment.
                    chatFragment = createChatFragment();

                }

                // returns fragment to be loaded in the container view.
                childFragment = chatFragment;

                break;
        }
        return childFragment;
    }

    private ChatFragment createChatFragment() {
        ChatFragment chatFragment = ChatFragment.newInstance();

        Bundle bundle = new Bundle();

        if (chatListItem != null)
            bundle.putParcelable("chat", chatListItem);
        else if (!Strings.isNullOrEmpty(chatUserName)) {
            bundle.putString("name", chatUserName);
            bundle.putString("number2", chatPhoneNumber.replace("+","").replaceAll("[^0-9]",""));
        } else
            bundle.putString("number2", chatPhoneNumber.replace("+","").replaceAll("[^0-9]",""));

        chatFragment.setArguments(bundle);

        return chatFragment;

    }

    private ChatListBaseFragment createChatListFragment() {

        ChatListBaseFragment chatListBaseFragment = ChatListBaseFragment.newInstance();

        return chatListBaseFragment;

    }

    private AllContactsFragmentParent createAllContactsFragment() {

        AllContactsFragmentParent allContacts = AllContactsFragmentController.newInstance();

        return allContacts;

    }

    private Fragment getCurrentFragment(String tag) {

        FragmentManager fragmentManager = getChildFragmentManager();

        Fragment fragment = fragmentManager.findFragmentByTag(tag);

        return fragment;
    }

    @Override
    public boolean onBackPressed() {

        boolean backPressed = super.onBackPressed();

        return backPressed;
    }

    @Override
    public MainContract.Presenter getBasePresenter() {
        return null;
    }

}