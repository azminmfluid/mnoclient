package com.ecrio.iota.ui.main;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecrio.iota.Injection;
import com.ecrio.iota.Iota;
import com.ecrio.iota.R;
import com.ecrio.iota.ResourceState;
import com.ecrio.iota.ResourceType;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.enums.NetworkStatus;
import com.ecrio.iota.enums.RegistrationStatus;
import com.ecrio.iota.enums.SubscriptionStatus;
import com.ecrio.iota.model.Action;
import com.ecrio.iota.model.PermissionAction;
import com.ecrio.iota.model.impl.permission.PermissionActionFactory;
import com.ecrio.iota.pref.Constants;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.test.DeferredFragmentTransaction;
import com.ecrio.iota.ui.all.AllContactsFragmentParent;
import com.ecrio.iota.ui.all.add.AddNewContactFragment;
import com.ecrio.iota.ui.base.BaseMvpActivity;
import com.ecrio.iota.ui.chat.ChatFragment;
import com.ecrio.iota.ui.chatlist.ChatListFragment;
import com.ecrio.iota.ui.file.PermissionPresenter;
import com.ecrio.iota.ui.home.MainFragment;
import com.ecrio.iota.ui.login.LoginContract;
import com.ecrio.iota.ui.login.LoginFragment;
import com.ecrio.iota.ui.login.MyApi;
import com.ecrio.iota.ui.settings.SettingsDialog;
import com.ecrio.iota.ui.splash.SplashFragment;
import com.ecrio.iota.utility.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseMvpActivity<MainContract.View, MainContract.Presenter> implements MainContract.View, ImageView.OnClickListener, PermissionPresenter.PermissionCallbacks {

    @BindView(R.id.mToolbar)
    Toolbar mToolbar;

    @BindView(R.id.IMGchats)
    ImageView mMenuChats;

    @BindView(R.id.id_menu_count)
    TextView mMenuDeleteCount;

    @BindView(R.id.IMGmenuRight1)
    ImageView mMenuContacts;

    @BindView(R.id.TVtitle)
    TextView mMenuTitle;

    @BindView(R.id.IMGback)
    ImageView mMenuBack;

    @BindView(R.id.IMGmenuDelete)
    ImageView mMenuDelete;

    @BindView(R.id.id_menu_edit)
    ImageView mMenuEdit;

    @BindView(R.id.IMGmenuAdd)
    ImageView mMenuAdd;

    @BindView(R.id.IMGmenuRight2)
    ImageView mMenuSettings;

    @Inject
    Iota mIota;
    @Inject
    ArrayList<Iota.ResourceListener> mListeners;
    @Inject
    MyApi mApi;
    @Inject
    NetworkStatus NetworkStatus;
    @Inject
    RegistrationStatus RegistrationStatus;
    @Inject
    SubscriptionStatus SubscriptionStatus;

    private static final int PERMISSIONS_REQUEST = 1;

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 2;
    private PermissionPresenter permissionPresenter;
    private SettingsDialog settingsDialog;
    private String number;
    private boolean isRunning;
    public volatile Queue<DeferredFragmentTransaction> runUITransactionInBackground = new LinkedList<DeferredFragmentTransaction>();
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        PermissionActionFactory permissionActionFactory = new PermissionActionFactory(this);

        PermissionAction permissionAction = permissionActionFactory.getPermissionAction();

        permissionPresenter = new PermissionPresenter(permissionAction, this);

        mMenuChats.setOnClickListener(this);

        mMenuContacts.setOnClickListener(this);

        mMenuBack.setOnClickListener(this);

        mMenuDelete.setOnClickListener(this);

        mMenuEdit.setOnClickListener(this);

        mMenuAdd.setOnClickListener(this);

        mMenuSettings.setOnClickListener(this);

        UserInfo instance = UserInfo.getInstance();

        instance.readCash(this);

        AppBaseApplication.getInstance().getComponent().inject(this);
        mListeners.add(new Iota.ResourceListener() {
            @Override
            public void onResourceStateUpdate(Iota iota, ResourceType resource, ResourceState status) {
                if (resource == ResourceType.Network) {
                    if (status == ResourceState.Unavailable) {

                    }
                }
                if (resource == ResourceType.Registration) {
                    if (status == ResourceState.Unavailable) {
                        AppBaseApplication.getInstance().getComponent().inject(MainActivity.this);
                        Log.DebugApi("_instance refreshed: " + mIota);
                        updateGrantedPermissions();
                    }
                }
                if (resource == ResourceType.Subscription) {

                }
            }
        });
        Log.Debug("_instance", mIota + "");

        updateGrantedPermissions();

        requestMissingPermissions();

        enableReceiver();

        if (instance.getLogin_status() == -1) {

            // If app is not registered show splash screen and login screen.
            presenter.openSplash();

        } else {

            // If app is registered show dashboard.
            presenter.openContactList();
            presenter.listenNewSession();
        }

        logBackStack();

    }

    private void test() {
        String SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION2 = "SELECT MAX(" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME +" WHERE "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" IS NOT NULL AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" != ''"+ " GROUP BY " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID;
//
//        String SELECT_CHAT_OF_EACH_CONVERSATION_AND_COUNT = "SELECT *,(SELECT count("
//                + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID
//                + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME +" WHERE "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" IS NOT NULL AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" != ''"+ " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=CE.COLUMN_CONVERSATION_USER_ID ) Count FROM " + TableConversationsContract.ChatEntry.TABLE_NAME +" as CE WHERE (" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + " IN " + "(" + SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION2 + ")" + ")";



        String SELECT_CHAT_OF_EACH_CONVERSATION_AND_COUNT = "SELECT *,(SELECT count(" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + ") FROM " + TableConversationsContract.ChatEntry.TABLE_NAME
                +" WHERE "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" IS NOT NULL AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG+" != ''"
                + " AND " + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=CE.c_user_id AND "+TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE+ "!=" + DeliveryStatus.READ.ordinal() + " "+") Count FROM " + TableConversationsContract.ChatEntry.TABLE_NAME +" as CE WHERE (" + TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + " IN " + "(" + SELECT_ONLY_LAST_CHAT_OF_EACH_CONVERSATION2 + ")" + ")";
        AppBaseApplication.mDb.test(SELECT_CHAT_OF_EACH_CONVERSATION_AND_COUNT);
    }

    private void logBackStack() {
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                showBackStack();
            }
        });
    }

    private void showBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        int backStackCount = manager.getBackStackEntryCount();
        Log.d2("Back_", "......................................");
        for (int i = 0; i < backStackCount; i++) {
            String tag = manager.getBackStackEntryAt(i).getName();
            Log.d2("Back_", tag);
        }
    }

    private void updateGrantedPermissions() {

        // Retrieve required permissions
        List<Iota.Permission> permissions = mIota.getRequiredPermissions();

        for (Iota.Permission p : permissions) {

            if (ContextCompat.checkSelfPermission(this, p.getPermission()) == PackageManager.PERMISSION_GRANTED) {

                // If permission is already granted, update Iota instance immediately
                mIota.permissionUpdate(p.getPermission(), true);

                Log.Debug(Log.TAG_IOTA_LOG, "Permission already granted: %s", p.getPermission());

            }
        }
    }

    private void requestMissingPermissions() {

        /* Retrieve missing and required permissions */
        List<Iota.Permission> permissions = mIota.getRequiredPermissions();

        if (permissions.size() > 0) {

            String missingPermissions[] = new String[permissions.size()];

            for (int i = 0; i < permissions.size(); i++) {

                missingPermissions[i] = permissions.get(i).getPermission();

                Log.Debug(Log.TAG_IOTA_LOG, "Requesting permission: %s", missingPermissions[i]);

            }
            // Request all missing permissions
            ActivityCompat.requestPermissions(this, missingPermissions, PERMISSIONS_REQUEST);
        } else {
            requestContactPermissions();
        }
    }

    private void requestContactPermissions() {
        // Request contact permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }
    }

    private void enableReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(listener, new IntentFilter(Constants.ACTION_REGISTER));
    }

    private void disableReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.unregisterReceiver(listener);
    }

    BroadcastReceiver listener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isRegisteredUser = intent.getBooleanExtra(Constants.KEY_NAME_STATUS, false);

            boolean isLogin = mApi.isLoggedIn();
            Log.Debug("is logged in : " + isLogin);
            if (isLogin) {
                Log.Debug("showing contact list screen.");
                new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(getSupportFragmentManager().findFragmentByTag(MAIN) != null) return;
                        getPresenter().openContactList();
                    }
                }, 2000);

            } else {
                showSnackBarErrorMessage(R.string.error_register_unavailable);
                if (isRegisteredUser) {
                    //if the user is not registered, then we need to show login screen after splash.
                    getPresenter().openSplash();
                }else{
                    // dismiss error in register page
                    Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragmentById instanceof LoginContract.View) {
                        ((LoginContract.View) fragmentById).setLoadingIndicator(false);
                    }
                }
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mIota == null)
            return;
        switch (requestCode) {
            case PERMISSIONS_REQUEST:
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        // Once user grants permission, update Iota instance
                        mIota.permissionUpdate(permissions[i], true);
//                    display("Permission granted: %s", permissions[i]);
                    }
                }
                requestContactPermissions();
                Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof LoginFragment) {
                    ((LoginFragment) fragmentById).handleSDCard();
                }

            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            case Action.ACTION_CODE_READ_IMAGE:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
                break;
            case Action.ACTION_CODE_TAKE_PICTURE:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
                break;
            case Action.ACTION_CODE_CALL_PHONE:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
                break;
            case Action.ACTION_CODE_READ_CONTACTS:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
            case Action.ACTION_CODE_ALL:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
                break;
        }

    }

    @NonNull
    @Override
    public MainContract.Presenter createPresenter() {
        return new MainPresenter(this, Injection.provideMainRepository(AppBaseApplication.context()));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("bundle")) {

            Bundle b = intent.getBundleExtra("bundle");
//            handleIntent2(b);
            setIntent(null);

        } else {
            handleIntent(intent);
//            setIntent(null);
        }
    }

    private void handleIntent(Intent intent) {
        if (intent.getExtras() != null) {
            Set<String> strings = intent.getExtras().keySet();
            if (strings.contains("PhoneNumber")) {
                new Handler(Looper.getMainLooper()).post(handlePushNotification(intent.getExtras()));
            }
        }
    }

    String phoneNumber;

    public Runnable handlePushNotification(final Bundle bundle) {
        return new Runnable() {
            @Override
            public void run() {
                phoneNumber = bundle.getString("PhoneNumber");
                closeAllNotification();
                Log.d("number is "+phoneNumber);
                Log.d("clearing all notifications");
                if (phoneNumber != null) {
                    phoneNumber = phoneNumber.replace("+", "");
                    int conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(phoneNumber);
                    Log.d("notification is message");
                    Log.d("Notification is either new or updated");
                    Log.d("requesting detail page for the Ticket already available.");
                    showChatDetail(phoneNumber);
                    clearNotificationData();
                } else {
                    Log.d("Notification null");
                    clearNotificationData();
                }
            }
        };
    }

    private void showChatDetail(String phoneNumber) {
        FragmentManager manager = getSupportFragmentManager();
        int index = manager.getBackStackEntryCount() - 1;
        if (settingsDialog != null && settingsDialog.getDialog().isShowing()) {
            settingsDialog.dismiss();
            settingsDialog = null;
        }
        if (index == -1) {
            Fragment fragmentByTag = manager.findFragmentByTag(MAIN);
            if (fragmentByTag instanceof MainFragment) {
//                MainFragment mainFragment = (MainFragment) fragmentByTag;
//                mainFragment.showChatFragment(phoneNumber);
                ChatFragment chatFragment = new ChatFragment();
                Bundle bundle = new Bundle();
                bundle.putString("number2", "" + phoneNumber.replace("+","").replaceAll("[^0-9]",""));
                chatFragment.setArguments(bundle);
                loadChatThread(chatFragment);
            }
            return;
        }
        FragmentManager.BackStackEntry backStackEntryAt = manager.getBackStackEntryAt(index);
        String name = backStackEntryAt.getName();
        Fragment fragmentByTag = manager.findFragmentByTag(name);
        if (fragmentByTag instanceof MainFragment) {
            ChatFragment chatFragment = new ChatFragment();
            Bundle bundle = new Bundle();
            bundle.putString("number2", "" + phoneNumber.replace("+","").replaceAll("[^0-9]",""));
            chatFragment.setArguments(bundle);
            loadChatThread(chatFragment);
        } else if (fragmentByTag instanceof ChatListFragment) {
            ChatFragment chatFragment = new ChatFragment();
            Bundle bundle = new Bundle();
            bundle.putString("number2", "" + phoneNumber.replace("+","").replaceAll("[^0-9]",""));
            chatFragment.setArguments(bundle);
            loadChatThread(chatFragment);
        } else if (fragmentByTag instanceof ChatFragment) {
            ChatFragment mainFragment = (ChatFragment) fragmentByTag;
            mainFragment.refreshChat(phoneNumber);
        } else if (fragmentByTag instanceof AddNewContactFragment) {
            ChatFragment chatFragment = new ChatFragment();
            Bundle bundle = new Bundle();
            bundle.putString("number2", "" + phoneNumber.replace("+","").replaceAll("[^0-9]",""));
            chatFragment.setArguments(bundle);
            removePageAndLoadChatThread(chatFragment);
        }
    }

    private void clearNotificationData() {
        phoneNumber = null;
    }

    void closeAllNotification() {
        if (Context.NOTIFICATION_SERVICE != null) {
            NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nMgr.cancelAll();
        }
    }

    @Override
    public void showChatListToolBarMenu() {
        mMenuChats.setVisibility(View.GONE);
        mMenuBack.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuContacts.setVisibility(View.VISIBLE);
        mMenuSettings.setVisibility(View.VISIBLE);
        mMenuDelete.setVisibility(View.GONE);
        mMenuEdit.setVisibility(View.GONE);
        mMenuDeleteCount.setVisibility(View.GONE);
        mMenuTitle.setText("All Chats");
    }

    @Override
    public void showContactListToolBarMenu() {
        mToolbar.setVisibility(View.VISIBLE);
        mMenuBack.setVisibility(View.GONE);
        mMenuContacts.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.VISIBLE);
        mMenuChats.setVisibility(View.VISIBLE);
        mMenuSettings.setVisibility(View.VISIBLE);
        mMenuTitle.setText("All Contacts");
    }

    @Override
    public void updateToolbarTitle(String name) {
        mMenuTitle.setText(name);
    }

    @Override
    public void showSplashToolBar() {
        mToolbar.setVisibility(View.GONE);
    }

    @Override
    public void showLoginToolBarMenu() {
        mToolbar.setVisibility(View.VISIBLE);
        mMenuContacts.setVisibility(View.GONE);
        mMenuChats.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuBack.setVisibility(View.GONE);
        mMenuSettings.setVisibility(View.GONE);
        mMenuDelete.setVisibility(View.GONE);
        mMenuEdit.setVisibility(View.GONE);
        mMenuDeleteCount.setVisibility(View.GONE);
        mMenuTitle.setText("Settings");
    }

    @Override
    public void showChatToolBarDeleteMenu() {
        mMenuDelete.setVisibility(View.VISIBLE);
        mMenuEdit.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBotToolBarDeleteMenu(String userId) {
        if(userId != null) {
            this.userId = userId;
            mMenuEdit.setVisibility(View.VISIBLE);
        }else {
            mMenuEdit.setVisibility(View.GONE);
        }
    }

    @Override
    public void showBotToolBarDeleteMenu() {
        mMenuChats.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuSettings.setVisibility(View.GONE);
        mMenuDelete.setVisibility(View.VISIBLE);
        mMenuBack.setVisibility(View.VISIBLE);
        mMenuDeleteCount.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBotToolBarMenu() {
        mToolbar.setVisibility(View.VISIBLE);
        mMenuBack.setVisibility(View.GONE);
        mMenuContacts.setVisibility(View.GONE);
        mMenuDelete.setVisibility(View.GONE);
        mMenuEdit.setVisibility(View.GONE);
        mMenuDeleteCount.setVisibility(View.GONE);
        mMenuChats.setVisibility(View.VISIBLE);
        mMenuTitle.setText("All Contacts");
        mMenuAdd.setVisibility(View.VISIBLE);
        mMenuSettings.setVisibility(View.VISIBLE);
    }

    @Override
    public void showAddContactToolBarMenu() {
        mMenuContacts.setVisibility(View.GONE);
        mMenuChats.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuBack.setVisibility(View.VISIBLE);
        mMenuSettings.setVisibility(View.VISIBLE);
        mMenuDelete.setVisibility(View.GONE);
        mMenuEdit.setVisibility(View.GONE);
        mMenuDeleteCount.setVisibility(View.GONE);
        mMenuTitle.setText("Add Contact");
    }

    @Override
    public void showEditContactToolBarMenu() {
        mMenuContacts.setVisibility(View.GONE);
        mMenuChats.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuBack.setVisibility(View.VISIBLE);
        mMenuSettings.setVisibility(View.VISIBLE);
        mMenuDelete.setVisibility(View.GONE);
        mMenuEdit.setVisibility(View.GONE);
        mMenuDeleteCount.setVisibility(View.GONE);
        mMenuTitle.setText("Edit Contact");
    }

    @Override
    public void showChatToolBarMenu() {
        mMenuContacts.setVisibility(View.GONE);
        mMenuChats.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuBack.setVisibility(View.VISIBLE);
        mMenuSettings.setVisibility(View.VISIBLE);
        mMenuDelete.setVisibility(View.GONE);
        mMenuEdit.setVisibility(View.GONE);
        mMenuDeleteCount.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void loadChatThread(ChatFragment fragment) {
        LoadFragment(fragment, true, CHATS);
    }

    public void removePageAndLoadChatThread(ChatFragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack();
        LoadFragment(fragment, true, CHATS);
    }

    @Override
    public void showAllContactsFragment() {
        // loading dashboard screen.
        MainFragment fragment = MainFragment.newInstance();
        if (!isRunning) {
            DeferredFragmentTransaction deferredFragmentTransaction = new DeferredFragmentTransaction() {
                @Override
                public void commit() {
                    popAndLoadFragment(getReplacingFragment(), false, MAIN);
                    presenter.listenNewSession();
                }
            };

            deferredFragmentTransaction.setReplacingFragment(fragment);
            runUITransactionInBackground.clear();
            runUITransactionInBackground.add(deferredFragmentTransaction);
        }else{
            popAndLoadFragment(fragment, false, MAIN);
            presenter.listenNewSession();
        }
    }

    @Override
    public void showChatListFragment() {
        // loading splash screen.
        ChatListFragment fragment = ChatListFragment.newInstance();
        popAndLoadFragment(fragment, true, ALL_CHATS);
    }

    @Override
    public void showLoginFragment() {
        // loading login fragment
        LoginFragment fragment = LoginFragment.newInstance();
        if (!isRunning) {
            DeferredFragmentTransaction deferredFragmentTransaction = new DeferredFragmentTransaction() {
                @Override
                public void commit() {
                    LoadFragment(getReplacingFragment(), false, null);
                }
            };
            deferredFragmentTransaction.setReplacingFragment(fragment);
            runUITransactionInBackground.clear();
            runUITransactionInBackground.add(deferredFragmentTransaction);
        }else {
            LoadFragment(fragment, false, null);
        }
    }

    @Override
    public void showSplashFragment() {
        // loading splash screen.
        SplashFragment fragment = SplashFragment.newInstance();

        if (!isRunning) {
            DeferredFragmentTransaction deferredFragmentTransaction = new DeferredFragmentTransaction() {
                @Override
                public void commit() {
                    getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    LoadFragment(getReplacingFragment(), false, null);
                }
            };
            deferredFragmentTransaction.setReplacingFragment(fragment);
            runUITransactionInBackground.clear();
            runUITransactionInBackground.add(deferredFragmentTransaction);
        }else{
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            LoadFragment(fragment, false, null);
        }
    }

    @Override
    public void loadPagerMessageComposePage() {

    }

    @Override
    public void loadAddContactFragment() {

    }

    @Override
    public void loadSettingsPageFragment() {

    }

    @Override
    public void showPrevFragment() {
        onBackPressed();
    }

    @Override
    public void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(null);
        builder.setMessage("Do you want to delete this contact?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FragmentManager supportFragmentManager = getSupportFragmentManager();
                Fragment fragmentById = supportFragmentManager.findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof ChatFragment) {
                    ((ChatFragment) fragmentById).performDelete();
                }else if (fragmentById instanceof MainFragment) {
                    ((MainFragment) fragmentById).performDelete();
                }
            }
        });
        builder.setNegativeButton("Dismiss", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void showAddContactFragment() {
        // loading add contact fragment
        AddNewContactFragment fragment = AddNewContactFragment.newInstance();
        fragment.isEdit(false, null);
        LoadFragment(fragment, true, ADD_CONTACTS);
    }

    @Override
    public void showEditContactFragment() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        Fragment fragmentById = supportFragmentManager.findFragmentById(R.id.fragment_container);
        if (fragmentById instanceof MainFragment) {
            ((MainFragment) fragmentById).clearSelection();
        }
        // loading add contact fragment
        AddNewContactFragment fragment = AddNewContactFragment.newInstance();
        fragment.isEdit(true, userId);
        LoadFragment(fragment, true, ADD_CONTACTS);
    }

    @Override
    public void requestReadContacts() {
        permissionPresenter.requestAllPermission();
    }

    @Override
    public void requestReadImage() {
        permissionPresenter.requestWriteExternalStorangePermission();
    }

    @Override
    public void requestTakePhoto() {
        permissionPresenter.requestCameraPermission();
    }

    @Override
    public void showSettingsDialog() {
        if (settingsDialog != null && settingsDialog.isVisible()) {

            return;
        }
        settingsDialog = new SettingsDialog();

        settingsDialog.show(getSupportFragmentManager(), "SettingsDialog");

    }

    @Override
    public void requestCallPermission(String number) {
        this.number = number;
        permissionPresenter.requestCallingPermission();
    }

    @Override
    public void showEditContactPage() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        Fragment fragmentById = supportFragmentManager.findFragmentById(R.id.fragment_container);
        if (fragmentById instanceof ChatFragment) {
        }else if (fragmentById instanceof MainFragment) {
            ((MainFragment) fragmentById).performEdit();
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        Fragment fragmentById = supportFragmentManager.findFragmentById(R.id.fragment_container);
        if (fragmentById instanceof ChatFragment) {
            ((ChatFragment) fragmentById).canGoBack();
        } else if (fragmentById instanceof MainFragment) {
            ((MainFragment) fragmentById).canGoBack();
        } else {
            backPressed(true);
        }
    }

    public void backPressed(boolean b) {

        super.onBackPressed();
    }

    public static final String MAIN = "main";
    public static final String ADD_CONTACTS = "add";
    public static final String CHATS = "chats";
    public static final String ALL_CHATS = "all.chats";

    public void LoadFragment(final Fragment fragment, final boolean addToBackStack, String tag) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // initializing fragmentTransaction object
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                // checking for back stack entry
                if (addToBackStack) {
                    // replacing current fragment with specified fragment
                    fragmentTransaction.replace(R.id.fragment_container, fragment, tag);

                    fragmentTransaction.addToBackStack(tag);
                } else {

                    // replacing current fragment with specified fragment
                    fragmentTransaction.replace(R.id.fragment_container, fragment, tag);

                }

                // commit fragment transaction
                fragmentTransaction.commit();

            }
        });

    }

    public void popAndLoadFragment(final Fragment fragment, final boolean addToBackStack, String tag) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                FragmentManager supportFragmentManager = getSupportFragmentManager();

                if (supportFragmentManager.findFragmentByTag(fragment.getClass().getName()) == null) {
                    Log.d2("Back_","not found fragment");
                    // initializing fragmentTransaction object
                    FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();


                    // checking for back stack entry
                    if (addToBackStack) {
                        // replacing current fragment with specified fragment
                        fragmentTransaction.replace(R.id.fragment_container, fragment, tag);
                        fragmentTransaction.addToBackStack(tag);
                    } else {

                        // replacing current fragment with specified fragment
                        fragmentTransaction.replace(R.id.fragment_container, fragment, tag);
                    }

                    // commit fragment transaction
                    try {
                        fragmentTransaction.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d2("Back_","found fragment");
                    supportFragmentManager.popBackStack(fragment.getTag(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }
        });

    }

    public static boolean isTab() {
        return false;
    }

    @Override
    public void onClick(View view) {

        if (view == mMenuChats) {

            presenter.openChatList();
        } else if (view == mMenuContacts) {

            presenter.openContactList();

        } else if (view == mMenuBack) {

            presenter.openPrevPage();
        } else if (view == mMenuDelete) {

            presenter.openDeletePage();
        } else if (view == mMenuEdit) {

            presenter.openEditContactPage();
        } else if (view == mMenuAdd) {

            presenter.openAddPage();

        } else if (view == mMenuSettings) {

            presenter.openSettingsPage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void permissionAccepted(int actionCode) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragmentById;
        switch (actionCode) {

            case Action.ACTION_CODE_READ_CONTACTS:

                fragmentById = fragmentManager.findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof LoginFragment) {
                    updateGrantedPermissions();
                    ((LoginFragment) fragmentById).contactPermissionSuccess();
                }
                else if (fragmentById instanceof AllContactsFragmentParent) {
                    ((AllContactsFragmentParent) fragmentById).fetchPhoneBookContacts();
                }
                break;

            case Action.ACTION_CODE_READ_IMAGE:
                fragmentById = fragmentManager.findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof ChatFragment) {
                    ((ChatFragment) fragmentById).handleCamera();
                } else if (fragmentById instanceof LoginFragment) {
                    ((LoginFragment) fragmentById).handleSDCard();
                }
                break;

            case Action.ACTION_CODE_TAKE_PICTURE:
                fragmentById = fragmentManager.findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof ChatFragment) {
                    ((ChatFragment) fragmentById).handleCamera();
                }
                break;

            case Action.ACTION_CODE_CALL_PHONE:
                fragmentById = fragmentManager.findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof ChatFragment) {
                    ((ChatFragment) fragmentById).makeCall(number);
                    number = null;
                }
            case Action.ACTION_CODE_ALL:
                fragmentById = fragmentManager.findFragmentById(R.id.fragment_container);
                if (fragmentById instanceof LoginFragment) {
                    updateGrantedPermissions();
                    ((LoginFragment) fragmentById).contactPermissionSuccess();
                }
                break;
        }
    }

    @Override
    public void permissionDenied(int actionCode) {
        if (dismissPermissionRationale() == 0) {
            showSnackBarPermissionMessage(actionCode);
        }
    }

    @Override
    public void showRationale(int actionCode) {
        switch (actionCode) {
            case Action.ACTION_CODE_READ_CONTACTS:
//                createAndShowPermissionRationale(actionCode, R.string.rationale_read_contacts_title, R.string.rationale_read_contacts_subtitle);
                permissionPresenter.requestReadContactsPermissionAfterRationale();
                break;
            case Action.ACTION_CODE_READ_IMAGE:
//                createAndShowPermissionRationale(actionCode, R.string.rationale_save_image_title, R.string.rationale_save_image_subtitle);
                permissionPresenter.requestWriteExternalStorangePermissionAfterRationale();
                break;
            case Action.ACTION_CODE_TAKE_PICTURE:
//                createAndShowPermissionRationale(actionCode, R.string.rationale_take_photo_title, R.string.rationale_take_photo_subtitle);
                permissionPresenter.requestCameraPermissionAfterRationale();
                break;
            case Action.ACTION_CODE_CALL_PHONE:
//                createAndShowPermissionRationale(actionCode, R.string.rationale_take_photo_title, R.string.rationale_take_photo_subtitle);
                permissionPresenter.requestCallingPermissionAfterRationale();
                break;
        }
    }

    public void onAcceptRationaleClick(View view) {
        int action = dismissPermissionRationale();
        switch (action) {
            case Action.ACTION_CODE_READ_CONTACTS:
                permissionPresenter.requestReadContactsPermissionAfterRationale();
                break;
            case Action.ACTION_CODE_READ_IMAGE:
                permissionPresenter.requestWriteExternalStorangePermissionAfterRationale();
                break;
            case Action.ACTION_CODE_TAKE_PICTURE:
                permissionPresenter.requestCameraPermissionAfterRationale();
                break;
            case Action.ACTION_CODE_CALL_PHONE:
                permissionPresenter.requestCallingPermissionAfterRationale();
                break;
        }
    }

    @Override
    protected void showSnackBarPermissionMessage(int action) {
        switch (action) {
            case Action.ACTION_CODE_READ_CONTACTS:
                super.showSnackBarPermissionMessage(R.string.snackbar_read_contacts);
                break;
            case Action.ACTION_CODE_READ_IMAGE:
                super.showSnackBarPermissionMessage(R.string.snackbar_save_image);
                break;
            case Action.ACTION_CODE_TAKE_PICTURE:
                super.showSnackBarPermissionMessage(R.string.snack_bar_camera);
                break;
            case Action.ACTION_CODE_ALL:
                super.showSnackBarPermissionMessage(R.string.snack_bar_all);
                break;
        }
    }

    @Override
    public void updateCount(int size) {
        mMenuDeleteCount.setText(String.valueOf(size));
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
//        GenricFunction.exportDatabse("iotachats.db");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        while (!runUITransactionInBackground.isEmpty()){
            runUITransactionInBackground.remove().commit();
        }
    }

    @Override
    protected void onDestroy() {
        disableReceiver();
        if(isFinishing()){

            AppBaseApplication.getInstance().onTerminate();
            System.exit(0);
        }
        super.onDestroy();
    }
}