package com.ecrio.iota.ui.chat.model;

/**
 * Video is an immutable object that holds the various metadata associated with a single video.
 */
public final class ChatItem {
    public final int conversationID;
    public final int userid;
    public final int fileid;
    public final String category;
    public final String msgText;
    public final String bgImageUrl;
    public final boolean isOutgoing, isMsgRead;
    public final boolean isFile;
    public final boolean isRich;
    public final boolean isInfo;
    public final String progress;
    public final int deliveryStatus;
    public final int msgID, msgReplyID;
    public final String mPhone;
    public final byte[] thumb;
    public final Double sendTime;
    public final int isSelected;
    public final boolean isGeneral;
    public final boolean isCarousal;


    private ChatItem(
            final int conversationID,
            final int userid,
            final String mPhone,
            final String category,
            final String msgText,
            final int msgID,
            final int msgReplyID,
            final boolean isOutgoing,
            final boolean isMsgRead,
            final int deliveryStatus,
            final boolean isFile,
            final boolean isRich,
            final boolean isGeneral,
            final boolean isCarousal,
            final byte[] thumb,
            final int fileid,
            final String progress,
            final boolean isInfo,
            final String bgImageUrl,
            final Double time, int isSelected) {
        this.conversationID = conversationID;
        this.userid = userid;
        this.mPhone = mPhone;
        this.category = category;
        this.msgText = msgText;
        this.msgReplyID = msgReplyID;
        this.msgID = msgID;
        this.isOutgoing = isOutgoing;
        this.isMsgRead = isMsgRead;
        this.deliveryStatus = deliveryStatus;
        this.isFile = isFile;
        this.isRich = isRich;
        this.isGeneral = isGeneral;
        this.isCarousal = isCarousal;
        this.thumb = thumb;
        this.fileid = fileid;
        this.progress = progress;
        this.isInfo = isInfo;
        this.bgImageUrl = bgImageUrl;
        this.sendTime = time;
        this.isSelected = isSelected;
    }

    @Override
    public String toString() {
        String s = "Chat{";
        s += "id=" + conversationID;
        s += ", category='" + category + "'";
        s += ", msgID='" + msgID + "'";
//        s += ", msgText='" + (msgText+"   ").substring(0,3)+"..." + "'";
        s += ", msgText='\n" + (msgText+"   ") + "'\n";
        s += ", bgImageUrl='" + bgImageUrl + "'";
        s += ", isFile='" + isFile + "'";
        s += ", isInfo='" + isInfo + "'";
        s += ", isSelected='" + isSelected + "'";
        s += "}";
        return s;
    }

    // Builder for Video object.
    public static class VideoBuilder {
        private int id;
        private int userid;
        private int fileid;
        private String category;
        private String bgImageUrl;
        private String chatMessage;
        private boolean isOutGoing;
        private boolean isFile;
        private boolean isRich;
        private boolean isInfo;
        private String progress;
        private int DeliveryStatus;
        private int msgID, msgReplyID;
        private boolean isMsgRead;
        private String userPhone;
        private byte[] fileByte;
        private Double time;
        private int isSelected;
        private boolean richGeneral;
        private boolean richCarousal;

        public VideoBuilder id(int id) {
            this.id = id;
            return this;
        }

        public VideoBuilder userid(int id) {
            this.userid = id;
            return this;
        }

        public VideoBuilder fileid(int id) {
            this.fileid = id;
            return this;
        }

        public VideoBuilder category(String category) {
            this.category = category;
            return this;
        }


        public VideoBuilder chatMessage(String chatMessage) {
            this.chatMessage = chatMessage;
            return this;
        }

        public VideoBuilder isFile(boolean isFile) {
            this.isFile = isFile;
            return this;
        }

        public VideoBuilder setRich(boolean rich) {
            isRich = rich;
            return this;
        }

        public VideoBuilder isInfo(boolean isInfo) {
            this.isInfo = isInfo;
            return this;
        }

        public VideoBuilder progress(String progress) {
            this.progress = progress;
            return this;
        }

        public VideoBuilder bgImageUrl(String bgImageUrl) {
            this.bgImageUrl = bgImageUrl;
            return this;
        }

        public VideoBuilder phone(String userPhone) {
            this.userPhone = userPhone;
            return this;
        }

        public ChatItem build() {
            return new ChatItem(
                    id,
                    userid,
                    userPhone,
                    category,
                    chatMessage,
                    msgID,
                    msgReplyID,
                    isOutGoing,
                    isMsgRead,
                    DeliveryStatus,
                    isFile,
                    isRich,
                    richGeneral,
                    richCarousal,
                    fileByte,
                    fileid,
                    progress,
                    isInfo,
                    bgImageUrl,
                    time,
                    isSelected
            );
        }

        public VideoBuilder isOutGouing(boolean status) {
            this.isOutGoing = status;
            return this;
        }

        public VideoBuilder deliveryStatus(int status) {
            this.DeliveryStatus = status;
            return this;
        }

        public VideoBuilder msgReplyID(int msgReplyID) {
            this.msgReplyID = msgReplyID;
            return this;
        }

        public VideoBuilder msgID(int msgID) {
            this.msgID = msgID;
            return this;
        }

        public VideoBuilder isMsgRead(boolean isMsgRead) {
            this.isMsgRead = isMsgRead;
            return this;
        }

        public VideoBuilder thumb(byte[] fileByte) {
            this.fileByte = fileByte;
            return this;
        }

        public VideoBuilder Time(Double time) {
            this.time = time;
            return this;
        }

        public VideoBuilder isSelected(int msgSelected) {
            isSelected = msgSelected;
            return this;
        }

        public VideoBuilder setRichGeneral(boolean richGeneral) {
            this.richGeneral = richGeneral;
            return this;
        }

        public VideoBuilder setRichCarousal(boolean richCarousal) {
            this.richCarousal = richCarousal;
            return this;
        }
    }
}