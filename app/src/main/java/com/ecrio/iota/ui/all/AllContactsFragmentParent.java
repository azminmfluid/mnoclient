package com.ecrio.iota.ui.all;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.CursorLoader;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.ui.all.base.HomeViewPager;
import com.ecrio.iota.ui.all.bots.BotsFragment;
import com.ecrio.iota.ui.all.contacts.PhoneContactFragment;
import com.ecrio.iota.ui.base.ChildFragment;
import com.ecrio.iota.ui.chat.ChatFragment;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.utility.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class AllContactsFragmentParent extends ChildFragment implements View.OnFocusChangeListener {
    private static String TAG;
    //    public static final String CHILD_CHAT = "SHOPPING_CART";
//    public static final String CHILD_ALL_CONTACTS = "SHOPPING_ADDRESS";
    public static final String CHILD_CONTACTS = "child.contacts";
    public static final String CHILD_BOTS = "child.bots";
    private boolean isVisibleToUser;
    private MainViewpagerAdapter viewpagerAdapter;
    private View root;

    boolean isSearchViewEnable = false;

    public static String searchText = "";

    public abstract AllContactsFragmentChild createBotsFragment();

    public abstract AllContactsFragmentChild createContactsFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        getChildFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                showBackStack();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (isVisibleToUser) {
//            ((HomeWithNavDrawer)mActivity).showActionBar();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mSelectedView != null) {
            if (mSelectedView.getId() == mBots.getId()) {
                mBots.performClick();
            } else {
                mContacts.performClick();
            }
        }
        if ((getActivity()) != null) {
            ((MainActivity) getActivity()).showContactListToolBarMenu();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        this.isVisibleToUser = isVisibleToUser;
        Log.Debug("Visibility", "AllContactsFragmentParent ---> calling super method with isVisibleToUser: " + isVisibleToUser);
        super.setUserVisibleHint(isVisibleToUser);
        Log.Debug("Visibility", "AllContactsFragmentParent ---> isVisibleToUser: " + isVisibleToUser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.Debug("Visibility", "AllContactsFragmentParent ---> calling super");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void refreshSettings() {

        try {
            Intent intent = new Intent("com.ecrio.settings.changed");
            getActivity().sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSearch() {
        mSearchContainer.setVisibility(View.VISIBLE);
    }

    public void showSearch() {
        mSearchContainer.setVisibility(View.VISIBLE);
    }

    public void fetchPhoneBookContacts() {

    }

    public void canGoBack(){
        int childPosition = getChildPosition();
        if(childPosition == 1){
            Fragment childFragment = viewpagerAdapter.getItem(getChildPosition());
            if (childFragment instanceof BotsFragment) {
                ((BotsFragment) childFragment).canGoBack();
            }else {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).backPressed(true);
            }
        }else {
            if (getActivity() != null)
                ((MainActivity) getActivity()).backPressed(true);
        }
    }

    public void performDelete() {
        int childPosition = getChildPosition();
        if(childPosition == 1){
            Fragment childFragment = viewpagerAdapter.getItem(getChildPosition());
            if (childFragment instanceof BotsFragment) {
                ((BotsFragment) childFragment).deleteMessages();
            }
        }
    }

    public void performEdit(){
        int childPosition = getChildPosition();
        if(childPosition == 1){
            Fragment childFragment = viewpagerAdapter.getItem(getChildPosition());
            if (childFragment instanceof BotsFragment) {
                ((BotsFragment) childFragment).editContact();
            }
        }
    }

    public void clearSelection() {
        int childPosition = getChildPosition();
        if(childPosition == 1){
            Fragment childFragment = viewpagerAdapter.getItem(getChildPosition());
            if (childFragment instanceof BotsFragment) {
                ((BotsFragment) childFragment).clearSelection();
            }
        }
    }

    public class MainViewpagerAdapter extends FragmentPagerAdapter {

        private SparseArray<Fragment> sparseArray;

        private final String[] TITLES = {"Contacts", "Bots"};

        private MainViewpagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private MainViewpagerAdapter(Context context, FragmentManager fm) {
            this(fm);
            sparseArray = new SparseArray<>(2);
            sparseArray.clear();
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = sparseArray.get(position);

            if (fragment == null && position == 0) {

                fragment = getContactsFragment();

                sparseArray.put(position, fragment);

            } else if (fragment == null && position == 1) {

                fragment = getBotsFragment();

                sparseArray.put(position, fragment);

            }
            return fragment;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

    }

    @BindView(R.id.loadingView)
    public ProgressBar mProgressView;
    @BindView(R.id.id_login_container)
    public HomeViewPager mainViewPager;
    @BindView(R.id.but_contacts)
    public Button mContacts;
    @BindView(R.id.but_bots)
    public Button mBots;
    @BindView(R.id.EDTsearch)
    public EditText mSearchView;
    @BindView(R.id.id_search_container)
    public View mSearchContainer;
    private View mSelectedView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.i_fragment_home, container, false);

        ButterKnife.bind(this, root);

        View viewById = (View) root.findViewById(R.id.relativeLayout1);

        viewById.setVisibility(View.GONE);

        mProgressView.setVisibility(View.GONE);

        mainViewPager.setOffscreenPageLimit(0);

        mainViewPager.setSwipable(false);

        if (viewpagerAdapter != null) {

            mainViewPager.setAdapter(viewpagerAdapter);

        } else {

            viewpagerAdapter = new MainViewpagerAdapter(getActivity(), getChildFragmentManager());

            mainViewPager.setAdapter(viewpagerAdapter);

        }


//        tabSelected(tabLayout);

        AllContactsFragmentParent.TAG = getTag();

//        int searchPlateId = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
//
//        EditText searchPlate = (EditText) mSearchView.findViewById(searchPlateId);
//
//        searchPlate.setTextColor(getActivity().getResources().getColor(R.color.white));
//
//        searchPlate.setHintTextColor(getActivity().getResources().getColor(R.color.white));
//
//        searchPlate.setHint("Search text");

//        int searchIconId = mSearchView.getContext().getResources().getIdentifier("android:id/search_button", null, null);
//
//        ImageView searchIcon = (ImageView) mSearchView.findViewById(searchIconId);
//
//        searchIcon.setImageResource(R.drawable.search);

        // searchView.setBackgroundResource(R.drawable.search);

//        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                isSearchViewEnable = true;
//
//            }
//        });

//        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
//
//            @Override
//            public boolean onClose() {
//
//                searchText = "";
//
//                isSearchViewEnable = false;
//
//                mSearchView.clearFocus();
//
//                return false;
//            }
//        });

//        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//
//                searchText = newText;
//
//                Log.Debug("onQueryTextChange", searchText);
//
//                if (childFragment instanceof PhoneContactFragment) {
//
//                    ((PhoneContactFragment) childFragment).updateSearchText(searchText);
//
//                }
//
//                return false;
//            }
//        });
        mSearchView.setFocusableInTouchMode(true);
        mSearchView.setFocusable(true);
        mSearchView.clearFocus();
        mSearchView.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                searchText = mSearchView.getText().toString();

                if (TextUtils.isEmpty(searchText)) {

                    isSearchViewEnable = false;

                } else {

                    isSearchViewEnable = true;

                }

                Log.Debug("onQueryTextChange", searchText);

//                if (isSearchViewEnable) {
                Fragment childFragment = viewpagerAdapter.getItem(getChildPosition());

                if (childFragment instanceof PhoneContactFragment) {

                    ((PhoneContactFragment) childFragment).updateSearchText(searchText);

                } else if (childFragment instanceof BotsFragment) {

                    ((BotsFragment) childFragment).updateSearchText(searchText);

                }
//                }
            }

        });

        mContacts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // updating UI on clicking button

                mSelectedView = v;
                setChildPosition(0);
                updateUI(v);

            }
        });

        // updateUIforMoreOption();

        mBots.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // updating UI on clicking button

                mSelectedView = v;
                setChildPosition(1);
                updateUI(v);

            }
        });
        if (mSelectedView == null)
            mContacts.performClick();
        return root;
    }

    public void setChildPosition(int page) {
        mainViewPager.setCurrentPage(page);
    }

    public void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + getActivity().getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "iotachats.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public int getChildPosition() {
        int currentItem = mainViewPager.getCurrentPage();
        return currentItem;
    }

    private AllContactsFragmentChild getBotsFragment() {

//        YtsApplication.mDb.updateAllComposition();

        return createChildFragment(CHILD_BOTS);
    }

    private AllContactsFragmentChild getContactsFragment() {

        return createChildFragment(CHILD_CONTACTS);
    }

    private AllContactsFragmentChild createChildFragment(String tag) {

        AllContactsFragmentChild childFragment = null;

        if (tag == CHILD_BOTS) {

//            LumiereApplication.mDb.clearViewCart();

            childFragment = createBotsFragment();

            childFragment.setParentFragment(this);

        } else if (tag == CHILD_CONTACTS) {

            childFragment = createContactsFragment();

            childFragment.setParentFragment(this);

        }
        return childFragment;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if (hasFocus) {
            // focus change
        }

    }

    public void onTabSelected(int position) {

        showLoading();

        switch (position) {
            case 0:
                ((ImageView) root.findViewById(R.id.img_contacts)).setVisibility(View.VISIBLE);
                ((ImageView) root.findViewById(R.id.img_add_contact)).setVisibility(View.GONE);
                // selecting phone contacts screen.
                mainViewPager.setCurrentItem(position);
                break;
            case 1:
                ((ImageView) root.findViewById(R.id.img_contacts)).setVisibility(View.GONE);
                ((ImageView) root.findViewById(R.id.img_add_contact)).setVisibility(View.VISIBLE);
                // selecting bot contacts screen.
                mainViewPager.setCurrentItem(position);
                break;
        }
    }

    public void onTabUnselected(TabLayout.Tab tab) {

        int position = tab.getPosition();

        switch (position) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    public void onTabReselected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        switch (position) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    private void tabUnSelected(TabLayout tab, int position) {

    }

    private void tabSelected(TabLayout tab, int position) {

    }

    private void tabSelected(TabLayout tab) {

    }

    public void showLoading() {

//        mProgressView.setVisibility(View.VISIBLE);

    }

    public void hideLoading() {

        mProgressView.setVisibility(View.GONE);

    }

    public int selectedContactPostion = 0;

    public void updateContactDetailsFragment(Cursor mCursor, int position) {

        if (mCursor.moveToPosition(position)) {

            final int ID = 0;

            List<Contact> contacts = fetchContactNumbers("" + mCursor.getInt(ID));

            if (contacts.isEmpty()) {

                return;
            }

            ChatFragment chatFragment = new ChatFragment();

            Bundle bundle = new Bundle();
            String my_phone = AppBaseApplication.mPreferences.getString("my_phone", "");
            String contactPhone = "" + contacts.get(0).getPhoneNumber().replace("+", "").replaceAll("[^0-9]", "");
            bundle.putString("number2", contactPhone);

            chatFragment.setArguments(bundle);

            if (getActivity() != null) {
                if (!my_phone.contentEquals(contactPhone)) {
                    ((MainActivity) getActivity()).loadChatThread(chatFragment);
                } else {
                    Log.Debug("ItemClick", "unable to start session with your own number");
                    try {
                        Toast.makeText(getActivity(), "unable to create session with your own number", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public void updateBotContactDetailsFragment(Cursor mCursor, int position) {

        if (mCursor.moveToPosition(position)) {

            final int ID = 0;

            List<Contact> contacts = fetchBotContactNumbers("" + mCursor.getInt(ID));

            if (contacts.isEmpty()) {

                return;
            }

            ChatFragment chatFragment = new ChatFragment();

            Bundle bundle = new Bundle();
            String my_phone = AppBaseApplication.mPreferences.getString("my_phone", "");
            String botContact = "" + contacts.get(0).getPhoneNumber().replace("+", "").replaceAll("[^0-9]", "");
            bundle.putString("number2", botContact);

            chatFragment.setArguments(bundle);

            if (getActivity() != null) {
                if (!my_phone.contentEquals(botContact)) {
                ((MainActivity) getActivity()).loadChatThread(chatFragment);
                } else {
                    Log.Debug("ItemClick", "unable to start session with your own number");
                    try {
                        Toast.makeText(getActivity(), "unable to create session with your own number", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    List<Contact> contactList = new ArrayList<Contact>();

    public List<Contact> fetchContactNumbers(String id) {

        contactList.clear();
        // Get numbers
        final String[] numberProjection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.TYPE,};
        Cursor phone = new CursorLoader(getActivity(), ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                numberProjection, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "= ?",
                new String[]{String.valueOf(id)}, null).loadInBackground();

        final int contactNumberColumnIndex = phone
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        final int contactTypeColumnIndex = phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);


        if (phone.moveToFirst()) {

            phone.moveToPrevious();

            while (phone.moveToNext()) {

                String number = phone.getString(contactNumberColumnIndex);
                int type = phone.getInt(contactTypeColumnIndex);


                String typeString = getActivity().getResources().getString(ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(type));

                contactList.add(new Contact(number, typeString));


            }

        }
        phone.close();

        return contactList;

    }

    public List<Contact> fetchBotContactNumbers(String id) {

        contactList.clear();
        // Get numbers
        final String[] numberProjection = new String[]{TableBotUsersContract.UserEntry.COLUMN_PHONE};
        Cursor phone = new CursorLoader(getActivity(), TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS,
                numberProjection, TableBotUsersContract.UserEntry.COLUMN_USER_ID + "= ?",
                new String[]{String.valueOf(id)}, null).loadInBackground();

        if (phone != null) {
            final int contactNumberColumnIndex = phone.getColumnIndex(TableBotUsersContract.UserEntry.COLUMN_PHONE);

            if (phone.moveToFirst()) {

                phone.moveToPrevious();

                while (phone.moveToNext()) {

                    String number = phone.getString(contactNumberColumnIndex);

                    String typeString = "";

                    contactList.add(new Contact(number, typeString));


                }

            }
            phone.close();
        }
        return contactList;

    }

    public class Contact {

        String phoneNumber;
        String Type;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public Contact(String phoneNumber, String type) {
            super();
            this.phoneNumber = phoneNumber;
            Type = type;
        }

    }

    public String view = "all";

    public void updateUI(View v) {

        // updating UI based on the view supplied
        mSelectedView = v;

        switch (v.getId()) {

            case R.id.but_contacts:

                view = "contacts";

                v.setBackgroundResource(R.drawable.top_tab);

                mBots.setBackgroundResource(R.drawable.top_tab_normal);

                onTabSelected(0);

                break;

            case R.id.but_bots:

                view = "bots";

                v.setBackgroundResource(R.drawable.top_tab);

                mContacts.setBackgroundResource(R.drawable.top_tab_normal);

                onTabSelected(1);

                break;

            default:
                break;
        }

    }

    private void showBackStack() {
        FragmentManager manager = getChildFragmentManager();
        int backStackCount = manager.getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            String tag = manager.getBackStackEntryAt(i).getName();
//            android.util.Log.d(TAG, "" + tag);
        }
    }

}