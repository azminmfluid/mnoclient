package com.ecrio.iota.ui.chat.data;

public interface OnRecyclerItemClickListener {
    void onItemClick(int pos);
}
