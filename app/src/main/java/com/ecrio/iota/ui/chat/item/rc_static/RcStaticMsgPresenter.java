package com.ecrio.iota.ui.chat.item.rc_static;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class RcStaticMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2("Log_View", "----------onCreateViewHolder-----------------");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        RcStaticMsgView rcStaticMsgView = new RcStaticMsgView(parent.getContext());

//        updateCardBackgroundColor(incomingMsgView, false);

        return new ViewHolder(rcStaticMsgView);
    }

    private void updateCardBackgroundColor(RcStaticMsgView view, boolean selected) {

        Log.d2("Log_View", "selected is " + selected);
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        ChatItem video = (ChatItem) item;

        Log.d2("Log_Selected", "selected is " + video.isSelected);
        viewHolder.tag = video.msgText;

        RcStaticMsgView rcStaticMsgView = (RcStaticMsgView) viewHolder.view;

        updateCardBackgroundColor(rcStaticMsgView, (video.isSelected != 0));

        rcStaticMsgView.setMessageText(video.msgText);

        rcStaticMsgView.setAvatarImage(video.mPhone);

        rcStaticMsgView.setUserNameText("" + video.userid);

        rcStaticMsgView.setStatus(video.deliveryStatus);

        Log.d2(TAG, video.msgText + " called onBindViewHolder");
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG, viewHolder.tag + " called onUnbindViewHolder");

        RcStaticMsgView rcStaticMsgView = (RcStaticMsgView) viewHolder.view;

        rcStaticMsgView.setSelected(false);

    }

}
