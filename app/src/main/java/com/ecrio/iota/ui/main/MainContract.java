package com.ecrio.iota.ui.main;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;

/**
 * Created by Mfluid on 5/15/2018.
 */

public interface MainContract {

    interface View extends BaseView<Presenter> {

        void showAllContactsFragment();

        void showLoginFragment();

        void showSplashFragment();

        void loadPagerMessageComposePage();

        void loadAddContactFragment();

        void loadSettingsPageFragment();

        void showChatListFragment();

        void showPrevFragment();

        void showDeleteDialog();

        void showAddContactFragment();

        void showEditContactFragment();

        void requestReadContacts();

        void requestReadImage();

        void requestTakePhoto();

        void showSettingsDialog();

        void requestCallPermission(String number);

        void showEditContactPage();
    }

    interface Presenter extends BasePresenter {

        void openContactList();

        void openSplash();

        void openChatList();

        void openPrevPage();

        void openDeletePage();

        void openAddPage();

        void listenNewSession();

        void openSettingsPage();

        void openEditContactPage();
    }
}