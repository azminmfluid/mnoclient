package com.ecrio.iota.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public abstract class BaseMvpFragment<V extends BaseView<P>, P extends BasePresenter> extends Fragment implements BaseCallback<V, P>, BaseView<P> {

    protected P presenter;

    @NonNull
    public abstract P createPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        P presenter = createPresenter();
        setPresenter(presenter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setPresenter(@NonNull P presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public P getBasePresenter() {
        return presenter;
    }

    @NonNull
    @Override
    public V getMvpView() {
        return (V) this;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.Debug("Visibility", "BaseMvpFragment Menu is visible:"+this.getClass().getSimpleName());
        } else {
            Log.Debug("Visibility", "BaseMvpFragment Menu is not visible:"+this.getClass().getSimpleName());
        }
    }
}