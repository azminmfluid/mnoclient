package com.ecrio.iota.ui.all.bots;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.chat.base.BaseLceoView;
import com.ecrio.iota.ui.list_base.ItemBridgeAdapter;

/**
 * Created by user on 2018.
 */

public interface BotsContract {

    interface View extends BaseLceoView<Presenter> {

        void showDeleteMenu();

        void showDeleteMenu(int size, String id);

        void hideDeleteMenu();

        void goBack(boolean b);

        void updateDeleteCount(int size);

        void editContact();

        void clearSelection();
    }

    interface Presenter extends BasePresenter {
        /**
         * Creates a new {@link android.view.View}.
         */
        Presenter.ViewHolder onCreateViewHolder(ViewGroup parent);

        /**
         * Binds a {@link android.view.View} to an item.
         */
        void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item);

        /**
         * Unbinds a {@link android.view.View} from an item. Any expensive references may be
         * released here, and any fields that are not bound for every item should be
         * cleared here.
         */
        void onUnbindViewHolder(Presenter.ViewHolder viewHolder);

        void canGoBack();

        void deleteMessages();

        void checkSelection();

        void clearSelection();

        void performEditBotContact();

        /**
         * ViewHolder can be subclassed and used to cache any view accessors needed
         * to improve binding performance (for example, results of findViewById)
         * without needing to subclass a View.
         */
        class ViewHolder {

            RecyclerView mRecyclerView;

            ItemBridgeAdapter mAdapter;

            public final android.view.View view;

            public ViewHolder(android.view.View view) {

                this.view = view;
            }

        }

    }
}