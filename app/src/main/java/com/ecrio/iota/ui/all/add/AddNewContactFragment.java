package com.ecrio.iota.ui.all.add;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.ui.chat.ChatFragment;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.utility.Log;
import com.google.common.base.Strings;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AddNewContactFragment extends Fragment {

    FrameLayout mFrameLayout;
    View mView;
    EditText mPhoneNumberET;
    RecyclerView messageThreadList;
    boolean isRecipientsAdded = false;

    ArrayList<String> selectedPhoneNumbers = new ArrayList<String>();

    private Button updateBtn;

    private EditText mUserName;

    private Button cancelButton;
    private boolean isEdit;
    private String userId;

    public static AddNewContactFragment newInstance() {

        AddNewContactFragment pagerModeMessageComposingFragment = new AddNewContactFragment();
        return pagerModeMessageComposingFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if ((getActivity()) != null) {
            if (Strings.isNullOrEmpty(userId))
                ((MainActivity) getActivity()).showAddContactToolBarMenu();
            else
                ((MainActivity) getActivity()).showEditContactToolBarMenu();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFrameLayout = new FrameLayout(getActivity());
        return populateView();
    }

    public View populateView() {

        mView = LayoutInflater.from(getActivity()).inflate(R.layout.i_compose_pagermode_message_fragment_layout, null, false);

        mFrameLayout.addView(mView);

        updateBtn = ((Button) mView.findViewById(R.id.id_btn_update));

        cancelButton = ((Button) mView.findViewById(R.id.id_btn_cancel));

        mUserName = (EditText) mView.findViewById(R.id.id_et_new_name);

        updateBtn.setVisibility(View.VISIBLE);

        mPhoneNumberET = ((EditText) mView.findViewById(R.id.reciepientList));

        ContentResolver content = getActivity().getContentResolver();

        Cursor cursor = content.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PEOPLE_PROJECTION, null, null, null);

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).onBackPressed();
            }
        });

        updateBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mPhoneNumberET.clearFocus();

                String newUserName = mUserName.getText().toString().trim();

                String phoneNo = mPhoneNumberET.getText().toString().trim();

                if (Strings.isNullOrEmpty(phoneNo)) {
                    Toast.makeText(getActivity(), "Please enter phone number.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (Strings.isNullOrEmpty(newUserName)) {
                    Toast.makeText(getActivity(), "Please enter name.", Toast.LENGTH_SHORT).show();
                    return;
                }
                phoneNo = phoneNo.replaceAll("[()-.^:,]", "");

                phoneNo = phoneNo.replaceAll(" ", "");

                selectedPhoneNumbers.clear();

                selectedPhoneNumbers.add(phoneNo);

                System.out.println(selectedPhoneNumbers.size());

                if (selectedPhoneNumbers.size() > 0) {

                    if (getActivity() instanceof MainActivity) {
                        for (String phoneNumber : selectedPhoneNumbers) {
                            saveContactToDevice(newUserName, phoneNumber);
                        }
                    }

                    ChatFragment chatFragment = new ChatFragment();
                    Bundle bundle = new Bundle();
                    String my_phone = AppBaseApplication.mPreferences.getString("my_phone", "");
                    String newNumber = selectedPhoneNumbers.get(0).replace("+", "").replaceAll("[^0-9]", "");
                    bundle.putString("number2", "" + newNumber);
                    chatFragment.setArguments(bundle);
                    if (getActivity() != null) {
                        if (!my_phone.contentEquals(newNumber)) {
                            if (isEdit)
                                ((MainActivity) getActivity()).onBackPressed();
                            else
                                ((MainActivity) getActivity()).removePageAndLoadChatThread(chatFragment);
                        } else {
                            Log.Debug("ItemClick", "unable to start session with your own number");
                            try {
                                Toast.makeText(getActivity(), "unable to create session with your own number", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    selectedPhoneNumbers.clear();

                }
            }
        });

        if (isEdit) {
            //content://com.android.contacts/phone_lookup/12345
            Uri lookupUri = TableBotUsersContract.UserEntry.buildUserUri2(Long.valueOf(userId));

            if (getActivity() != null) {
                Cursor contact = getActivity().getContentResolver().query(lookupUri, new String[]{TableBotUsersContract.UserEntry.COLUMN_NAME, TableBotUsersContract.UserEntry.COLUMN_PHONE}, null, null, null);
                if (contact != null) {
                    try {
                        if (contact.moveToFirst()) {
                            String name = contact.getString(0);
                            String phone = contact.getString(1);
                            mUserName.setText(name.trim());
                            mPhoneNumberET.setText(phone);
                            updateBtn.setText("Update Contact");
                        }
                    } finally {
                        contact.close();
                    }
                }
            }
        }
        return mFrameLayout;
    }

    public void saveContactToDevice(String contactName, String mobileNumber) {

        String lName = " ";

        String email = " ";

        if (mobileNumber.equalsIgnoreCase("")) {

            Toast.makeText(getActivity(), "Please enter the mobile number", Toast.LENGTH_SHORT).show();

        } else if (contactName.equalsIgnoreCase("")) {

            Toast.makeText(getActivity(), "Please enter the name", Toast.LENGTH_SHORT).show();

        } else {

            if (!this.isEdit) {
                boolean isInserted = insertBotContacts(contactName + " " + lName, mobileNumber, email, null);

                if (isInserted) {

                } else {

                    Toast.makeText(getActivity(), "Error in inserting contacts", Toast.LENGTH_SHORT).show();
                }
            } else {
                updateBotContact(getActivity(), contactName + " " + lName, mobileNumber, this.userId);
            }
        }
    }

    public boolean updateBotContact(Context _context, String newName, String newPhone, String rawContactId) {
        UserCache.resetUser(rawContactId);
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        // Name
        ContentProviderOperation.Builder name_builder = ContentProviderOperation.newUpdate(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS);
        name_builder.withSelection(TableBotUsersContract.UserEntry.COLUMN_USER_ID + "=?", new String[]{String.valueOf(rawContactId)});
        name_builder.withValue(TableBotUsersContract.UserEntry.COLUMN_NAME, newName);
        name_builder.withValue(TableBotUsersContract.UserEntry.COLUMN_PHONE, newPhone);

        // Adding update operation to operations list
        // to insert display name in the table ContactsContract.Data
        ops.add(name_builder.build());


        try {
            ContentProviderResult[] contentProviderResults = _context.getContentResolver().applyBatch(TableBotUsersContract.CONTENT_AUTHORITY, ops);
            String s = contentProviderResults.toString();
            Log.d2("error", "error whule inserting image:  " + s + "");
        } catch (RemoteException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "" + "Remote Exception while adding contact", Toast.LENGTH_LONG).show();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Operation Exception While adding contact", Toast.LENGTH_LONG).show();
        }
        Toast.makeText(getActivity(), "Contact updated", Toast.LENGTH_LONG).show();
        return true;
    }

    public boolean updateBotContact(Context _context, String newName, String number, String emailid, Bitmap mBitmap) {

        //content://com.android.contacts/phone_lookup/12345
        Uri lookupUri = TableBotUsersContract.UserEntry.buildBotUserUri(number);

        Cursor cursor = _context.getContentResolver().query(lookupUri, new String[]{TableBotUsersContract.UserEntry.COLUMN_USER_ID}, null, null, null);

        try {

            if (cursor != null && cursor.moveToFirst()) {
                //_id
                String rawContactId = cursor.getString(0);
                UserCache.resetUser(rawContactId);

                ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

                // Name
                ContentProviderOperation.Builder name_builder = ContentProviderOperation.newUpdate(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS);
                name_builder.withSelection(TableBotUsersContract.UserEntry.COLUMN_USER_ID + "=?", new String[]{String.valueOf(rawContactId)});
                name_builder.withValue(TableBotUsersContract.UserEntry.COLUMN_NAME, newName);

                // Adding update operation to operations list
                // to insert display name in the table ContactsContract.Data
                ops.add(name_builder.build());


                try {
                    ContentProviderResult[] contentProviderResults = _context.getContentResolver().applyBatch(TableBotUsersContract.CONTENT_AUTHORITY, ops);
                    String s = contentProviderResults.toString();
                    Log.d2("error", "error whule inserting image:  " + s + "");
                } catch (RemoteException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "" + "Remote Exception while adding contact", Toast.LENGTH_LONG).show();
                } catch (OperationApplicationException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Operation Exception While adding contact", Toast.LENGTH_LONG).show();
                }
                Toast.makeText(getActivity(), "Contact updated", Toast.LENGTH_LONG).show();
                return true;
            }
        } finally {
            cursor.close();
        }

        Toast.makeText(getActivity(), "Contact adding failed", Toast.LENGTH_LONG).show();
        return false;
    }

    public boolean updateContact(Context _context, String newName, String number, String emailid, Bitmap mBitmap) {

        //content://com.android.contacts/phone_lookup/12345
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        Cursor cursor = _context.getContentResolver().query(lookupUri, new String[]{ContactsContract.Contacts._ID}, null, null, null);

        try {

            if (cursor.moveToFirst()) {

                //_id
                String rawContactId = cursor.getString(0);

                ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

                // Name
                ContentProviderOperation.Builder name_builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                name_builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" + " AND " + ContactsContract.Data.MIMETYPE + "=?", new String[]{String.valueOf(rawContactId), ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE});
                name_builder.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, newName);

                // Adding update operation to operations list
                // to insert display name in the table ContactsContract.Data
                ops.add(name_builder.build());

                try {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (mBitmap != null) { // If an image is selected successfully

                        mBitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);

                        ContentProviderOperation.Builder picBuilder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                        picBuilder.withSelection(ContactsContract.Data.CONTACT_ID + "=? AND " + ContactsContract.Data.MIMETYPE + "=?", new String[]{rawContactId, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE});
                        picBuilder.withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray());
                        // Adding update operation to operations list
                        // to insert Photo in the table ContactsContract.Data
                        ops.add(picBuilder.build());

                        try {
                            stream.flush();
                        } catch (IOException e) {
                            e.printStackTrace();

                        }
                    }

                } catch (Exception e) {
                    Log.d2("error", "error whule inserting image:  " + e + "");
                }
                try {
                    ContentProviderResult[] contentProviderResults = _context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                    String s = contentProviderResults.toString();
                    Log.d2("error", "error whule inserting image:  " + s + "");
                } catch (RemoteException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "" + "Remote Exception while adding contact", Toast.LENGTH_LONG).show();
                } catch (OperationApplicationException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Operation Exception While adding contact", Toast.LENGTH_LONG).show();
                }
                Toast.makeText(getActivity(), "Contact updated", Toast.LENGTH_LONG).show();
                return true;
            }
        } finally {
            cursor.close();
        }

        Toast.makeText(getActivity(), "Contact adding failed", Toast.LENGTH_LONG).show();
        return false;
    }

    public boolean insertBotContacts(String name, String number, String emailid, Bitmap mBitmap) {
        if (!botContactExists(getActivity(), number)) {
            ContentValues values = new ContentValues();
            values.put(TableBotUsersContract.UserEntry.COLUMN_NAME, name);
            values.put(TableBotUsersContract.UserEntry.COLUMN_PHONE, number);
            if (getActivity() != null) {
                ContentResolver cr = getActivity().getContentResolver();
                cr.insert(TableBotUsersContract.UserEntry.CONTENT_URI_INDIVIDUAL, values);
            }
            return true;
        }
        return updateBotContact(getActivity(), name, number, emailid, mBitmap);
    }

    public boolean insertContacts(String name, String number, String emailid, Bitmap mBitmap) {

        if (!contactExists(getActivity(), number)) {

            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            int rawContactID = ops.size();

            // Adding insert operation to operations list
            // to insert a new raw contact in the table
            // ContactsContract.RawContacts
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());

            // Adding insert operation to operations list
            // to insert display name in the table ContactsContract.Data
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name).build());

            // Adding insert operation to operations list
            // to insert Mobile Number in the table ContactsContract.Data
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());

            // Adding insert operation to operations list
            // to insert Email Id in the table ContactsContract.Data
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, emailid)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .build());

            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                if (mBitmap != null) { // If an image is selected successfully
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 75, stream);

                    // Adding insert operation to operations list
                    // to insert Photo in the table ContactsContract.Data
                    ops.add(ContentProviderOperation
                            .newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                            .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray()).build());

                    try {
                        stream.flush();
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }

            } catch (Exception e) {
                Log.d2("error", "error whule inserting image:  " + e + "");
            }
            try {
                // Executing all the insert operations as a single database
                // transaction
                getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "" + "Remote Exception while adding contact", Toast.LENGTH_LONG).show();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Operation Exception While adding contact", Toast.LENGTH_LONG).show();
            }
            Toast.makeText(getActivity(), "Contact added", Toast.LENGTH_LONG).show();
            return true;
        } else {
//            Toast.makeText(getActivity(), "Contact already exists1", Toast.LENGTH_LONG).show();
            return updateContact(getActivity(), name, number, emailid, mBitmap);
        }
//        return false;
    }

    public boolean botContactExists(Context context, String number) {
        // / number is the phone number
        Uri uri = TableBotUsersContract.UserEntry.buildBotUserUri(number);
        Cursor cur = context.getContentResolver().query(uri, null, null, null, null);
        try {
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

    public boolean contactExists(Context context, String number) {
        // / number is the phone number
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        try {
            if (cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

    public void isEdit(boolean isEdit, String userId) {

        this.isEdit = isEdit;
        this.userId = userId;
    }

    public static class ContactListAdapter extends CursorAdapter implements Filterable {
        @SuppressWarnings("deprecation")
        public ContactListAdapter(Context context, Cursor c) {
            super(context, c);
            mContent = context.getContentResolver();
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {

            View view = LayoutInflater.from(context).inflate(
                    R.layout.contact_custom_layout_for_chat, parent, false);

            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            ((TextView) view.findViewById(R.id.txt_contactName)).setText(cursor.getString(2));

            String typeString = context.getResources().getString(ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(Integer.valueOf(cursor.getString(4))));

            ((TextView) view.findViewById(R.id.txt_ContactNumber)).setText(cursor.getString(1) + "    " + typeString.toUpperCase());

            ((TextView) view.findViewById(R.id.txt_ContactNumber)).setTag(cursor.getString(1));

            if (null != cursor.getString(3)) {

                ((ImageView) view.findViewById(R.id.imgContactImage)).setImageURI(Uri.parse(cursor.getString(3)));

                ((ImageView) view.findViewById(R.id.imgContactImage)).setTag(cursor.getString(3));

            } else {

                ((ImageView) view.findViewById(R.id.imgContactImage)).setImageResource(R.drawable.no_img_ico);

                ((ImageView) view.findViewById(R.id.imgContactImage)).setTag(null);

            }

        }

        @Override
        public String convertToString(Cursor cursor) {
            return cursor.getString(2);
        }

        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            if (getFilterQueryProvider() != null) {
                return getFilterQueryProvider().runQuery(constraint);
            }

            StringBuilder buffer = null;
            String[] args = null;
            if (constraint != null) {

                buffer = new StringBuilder();

                try {
                    int i = Integer.valueOf((String) constraint);

                    buffer.append("(");
                    buffer.append(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    buffer.append(") like ?");

                    args = new String[]{"%"
                            + constraint.toString().toUpperCase() + "%"};

                } catch (Exception e) {

                    buffer.append("UPPER(");
                    buffer.append(ContactsContract.Contacts.DISPLAY_NAME);

                    buffer.append(") like ?");

                    args = new String[]{"%"
                            + constraint.toString().toUpperCase() + "%"};

                }

            }

            return mContent.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    PEOPLE_PROJECTION,
                    buffer == null ? null : buffer.toString(), args, null);
        }

        private ContentResolver mContent;
    }

    private static final String[] PEOPLE_PROJECTION = new String[]{
            ContactsContract.Contacts._ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Contacts.PHOTO_URI,
            ContactsContract.CommonDataKinds.Phone.TYPE,};

    @SuppressWarnings("deprecation")
    public static Object convertViewToDrawable(View view) {
        int spec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        c.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(c);
        view.setDrawingCacheEnabled(true);
        Bitmap cacheBmp = view.getDrawingCache();
        Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
        view.destroyDrawingCache();
        return new BitmapDrawable(viewBmp);

    }

}