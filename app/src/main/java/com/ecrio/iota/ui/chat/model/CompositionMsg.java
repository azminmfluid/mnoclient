package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableCompositionContract;

public class CompositionMsg {
    int UserID;
    int CompositionState;
    int ConversationID;

    @Override
    public String toString() {
        return "ConversationID==" + ConversationID + " , " + "CompositionState==" + CompositionState + " , " + "UserID==" + UserID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getCompositionState() {
        return CompositionState;
    }

    public void setCompositionState(int compositionState) {
        CompositionState = compositionState;
    }

    public int getConversationID() {
        return ConversationID;
    }

    public void setConversationID(int conversationID) {
        ConversationID = conversationID;
    }



    public static CompositionMsg parseDetails(Cursor cursor) {

        CompositionMsg compositionMsg = null;

        if (cursor.moveToFirst()) {

            compositionMsg = new CompositionMsg();

            compositionMsg.ConversationID = cursor.getInt(cursor.getColumnIndex(TableCompositionContract.CompositionEntry.COLUMN_CONVERSATION_ID));

            compositionMsg.CompositionState = cursor.getInt(cursor.getColumnIndex(TableCompositionContract.CompositionEntry.COLUMN_STATUS));

//            conversationUser.sessionID = cursor.getString(cursor.getColumnIndex(TableUsersContract.UserEntry.COLUMN_USER_SESSION));

        }
        return compositionMsg;
    }

}