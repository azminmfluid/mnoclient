package com.ecrio.iota.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;

import com.ecrio.iota.ui.home.MainContract;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by user on 2018.
 */

public abstract class ParentFragment extends Fragment implements MainContract.View {

    public String TAG;
    public ChildFragment childFragment;
    String requestID;
    String requestorName;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            com.ecrio.iota.utility.Log.Debug("Visibility", "ParentFragment Menu is visible");
        } else {
            com.ecrio.iota.utility.Log.Debug("Visibility", "ParentFragment Menu is not visible");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getChildFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                showBackStack();
            }
        });
    }

    private void showBackStack() {
        FragmentManager manager = getChildFragmentManager();
        int backStackCount = manager.getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            String tag = manager.getBackStackEntryAt(i).getName();
        }
    }

    /**
     * The {@code fragment} is added to the container view with id {@code frameId}. The operation is
     * performed by the {@code fragmentManager}.
     */
    public static void addChildFragmentToFragment(@NonNull FragmentManager fragmentManager,
                                                  @NonNull Fragment fragment,
                                                  int frameId,
                                                  String tag) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(tag);
        transaction.replace(frameId, fragment, tag);
        transaction.commit();
    }


    @Override
    public boolean onBackPressed() {
        boolean popupStatus = false;
        FragmentManager manager = getChildFragmentManager();
        int backStackCount = manager.getBackStackEntryCount();
        if (backStackCount > 1) {
            popupStatus = true;
            manager.popBackStack();
            backStackCount--;// 1
            String tag = manager.getBackStackEntryAt(backStackCount - 1).getName();
            childFragment = (ChildFragment) manager.findFragmentByTag(tag);//self
        } else if (backStackCount == 1) {
            String tag = manager.getBackStackEntryAt(backStackCount - 1).getName();
            childFragment = (ChildFragment) manager.findFragmentByTag(tag);//self
        }
        return popupStatus;
    }

    public void refreshMenu() {
        refreshBottomMenu();
    }

    protected void refreshBottomMenu() {
    }

    public void setHistoryRequesterDetails(String requestID, String requestorName) {
        this.requestID = requestID;
        this.requestorName = requestorName;
    }

    public String getRequestID() {
        return requestID;
    }

    public String getRequestorName() {
        return requestorName;
    }

    //    public abstract void showCalendarFragment();

//    public abstract void showHistoryFragment();
}
