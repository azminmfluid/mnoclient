package com.ecrio.iota.ui.all.add;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.utility.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class AddContactsDialog {


	private EditText edit_FirstName;
	private EditText edit_LastName;
	private EditText edit_Number;
	private EditText edit_MailId;
	private Button button_Save;
	private Button button_Cancel;
	private Context mContext;
	private Dialog dialog;
	private EditText image_User;
	private ImageView image_UserImage;
	public AddContactsDialog(Context context) {
		dialog = new Dialog(context);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.addcontacts);
		dialog.show();
		edit_FirstName=(EditText)dialog.findViewById(R.id.ed_firstname);
		edit_LastName=(EditText)dialog.findViewById(R.id.ed_lastname);
		edit_Number=(EditText)dialog.findViewById(R.id.ed_mobilenumber);
		edit_MailId=(EditText)dialog.findViewById(R.id.ed_emailid);
		image_UserImage=(ImageView)dialog.findViewById(R.id.img_userImage);
		button_Save=(Button)dialog.findViewById(R.id.but_save);
		button_Cancel=(Button)dialog.findViewById(R.id.but_cancel);


	}
	public String getName() {
		return edit_FirstName.getText().toString()+" "+edit_LastName.getText().toString();

	}

	public String getNum() {
		return edit_Number.getText().toString();

	}

	public String getEmail() {
		return edit_MailId.getText().toString();

	}

	public Button saveButton(){
		return button_Save;
	}

	public Button cancelButton(){
		return button_Cancel;
	}

	public ImageView image_UserImageV(){
		return image_UserImage;
	}

	public void dismissDialog() {
		dialog.dismiss();
	}

	public boolean insertContacts(Activity activity, String name,String number,String emailid,Bitmap mBitmap){
		if(!contactExists(activity, number)){
			ArrayList<ContentProviderOperation> ops =
					new ArrayList<ContentProviderOperation>();

			int rawContactID = ops.size();

			// Adding insert operation to operations list
			// to insert a new raw contact in the table ContactsContract.RawContacts
			ops.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)
					.withValue(RawContacts.ACCOUNT_TYPE, null)
					.withValue(RawContacts.ACCOUNT_NAME, null)
					.build());

			// Adding insert operation to operations list
			// to insert display name in the table ContactsContract.Data
			ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
					.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
					.withValue(ContactsContract.Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
					.withValue(StructuredName.DISPLAY_NAME, name)
					.build());

			// Adding insert operation to operations list
			// to insert Mobile Number in the table ContactsContract.Data
			ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
					.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
					.withValue(ContactsContract.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
					.withValue(Phone.NUMBER,number)
					.withValue(Phone.TYPE, Phone.TYPE_MOBILE)
					.build());

			// Adding insert operation to operations list
			// to insert Email Id in the table ContactsContract.Data
			ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
					.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
					.withValue(ContactsContract.Data.MIMETYPE,
							CommonDataKinds.Email.CONTENT_ITEM_TYPE)
							.withValue(CommonDataKinds.Email.DATA, emailid)
							.withValue(CommonDataKinds.Email.TYPE, CommonDataKinds.Email.TYPE_WORK)
							.build());


			try{
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				if(mBitmap!=null){    // If an image is selected successfully
					mBitmap.compress(Bitmap.CompressFormat.PNG , 75, stream); 

					// Adding insert operation to operations list
					// to insert Photo in the table ContactsContract.Data
					ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
							.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
							.withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
							.withValue(ContactsContract.Data.MIMETYPE,Photo.CONTENT_ITEM_TYPE)
							.withValue(Photo.PHOTO,stream.toByteArray())
							.build());

					try {
						stream.flush();
					}catch (IOException e) {
						e.printStackTrace();

					}
				}

			}catch (Exception e) {
				Log.Debug("error whule inserting image", e+"");
			}
			try{
				// Executing all the insert operations as a single database transaction
				activity.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

			}catch (RemoteException e) {
				e.printStackTrace();
				Toast.makeText(activity, "" +
						"Remote Exception while adding contact", Toast.LENGTH_LONG).show();
			}catch (OperationApplicationException e) {
				e.printStackTrace();
				Toast.makeText(activity, "Operation Exception While adding contact", Toast.LENGTH_LONG).show();
			}
			return true;
		}else
			Toast.makeText(activity, "Contact already exists", Toast.LENGTH_LONG).show();
		return false;
	}

	public boolean contactExists(Context context, String number) {
		/// number is the phone number
		Uri lookupUri = Uri.withAppendedPath(
				PhoneLookup.CONTENT_FILTER_URI, 
				Uri.encode(number));
		String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
		Cursor cur = context.getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);
		try {
			if (cur.moveToFirst()) {
				return true;
			}
		} finally {
			if (cur != null)
				cur.close();
		}
		return false;
	}
}
