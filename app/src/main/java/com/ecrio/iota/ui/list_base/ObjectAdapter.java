package com.ecrio.iota.ui.list_base;

import android.database.Observable;

public abstract class ObjectAdapter {

    private final DataObservable mObservable = new DataObservable();

    public ObjectAdapter(PresenterSelector presenterSelector) {
        this.setPresenterSelector(presenterSelector);
    }

    /**
     * Constructs an adapter that uses the given {@link Presenter} for all items.
     */
    public ObjectAdapter(Presenter presenter) {

        setPresenterSelector(new SinglePresenterSelector(presenter));
    }

    /**
     * Sets the presenter selector.  May not be null.
     */
    public final void setPresenterSelector(PresenterSelector presenterSelector) {

        final boolean update = (mPresenterSelector != null);

        final boolean selectorChanged = update && mPresenterSelector != presenterSelector;

        mPresenterSelector = presenterSelector;

        if (selectorChanged) {

            onPresenterSelectorChanged();

        }

        if (update) {

            notifyChanged();

        }

    }

    /**
     * Called when {@link #setPresenterSelector(PresenterSelector)} is called
     * and the PresenterSelector differs from the previous one.
     */
    protected void onPresenterSelectorChanged() {

    }

    /**
     * Indicates that an id has not been set.
     */
    public static final int NO_ID = -1;
    private PresenterSelector mPresenterSelector;

    /**
     * Registers a DataObserver for data change notifications.
     */
    public final void registerObserver(DataObserver observer) {

        mObservable.registerObserver(observer);

    }

    /**
     * Unregisters a DataObserver for data change notifications.
     */
    public final void unregisterObserver(DataObserver observer) {

        mObservable.unregisterObserver(observer);

    }


    /**
     * A DataObserver can be notified when an ObjectAdapter's underlying data
     * changes. Separate methods provide notifications about different types of
     * changes.
     */
    public static abstract class DataObserver {

        /**
         * Called whenever the ObjectAdapter's data has changed in some manner
         * outside of the set of changes covered by the other range-based change
         * notification methods.
         */
        public void onChanged() {
        }

        /**
         * Called when a range of items in the ObjectAdapter has changed. The
         * basic ordering and structure of the ObjectAdapter has not changed.
         *
         * @param positionStart The position of the first item that changed.
         * @param itemCount     The number of items changed.
         */
        public void onItemRangeChanged(int positionStart, int itemCount) {
        }

        /**
         * Called when a range of items is inserted into the ObjectAdapter.
         *
         * @param positionStart The position of the first inserted item.
         * @param itemCount     The number of items inserted.
         */
        public void onItemRangeInserted(int positionStart, int itemCount) {
        }

        /**
         * Called when a range of items is removed from the ObjectAdapter.
         *
         * @param positionStart The position of the first removed item.
         * @param itemCount     The number of items removed.
         */
        public void onItemRangeRemoved(int positionStart, int itemCount) {
        }
    }

    private static final class DataObservable extends Observable<DataObserver> {

        DataObservable() {
        }

        public void notifyChanged() {

            for (int i = mObservers.size() - 1; i >= 0; i--) {

                mObservers.get(i).onChanged();

            }
        }
        public void notifyRangeChanged(int positionStart, int itemCount) {

            for (int i = mObservers.size() - 1; i >= 0; i--) {

                mObservers.get(i).onItemRangeChanged(positionStart, itemCount);

            }
        }

        public void onItemRangeInserted(int positionStart, int itemCount) {

            for (int i = mObservers.size() - 1; i >= 0; i--) {

                mObservers.get(i).onItemRangeInserted(positionStart, itemCount);

            }
        }
        public void onItemRangeRemoved(int positionStart, int itemCount) {

            for (int i = mObservers.size() - 1; i >= 0; i--) {

                mObservers.get(i).onItemRangeRemoved(positionStart, itemCount);

            }
        }
    }


    /**
     * Notifies UI that the underlying data has changed.
     */
    protected void notifyChanged() {

        mObservable.notifyChanged();

    }
    /**
     * Notifies UI that the underlying data has changed.
     */
    protected void notifyRangeChanged(int positionStart, int itemCount) {

        mObservable.notifyRangeChanged(positionStart, itemCount);

    }
    /**
     * Notifies UI that the underlying data has changed.
     */
    protected void notifyRangeInserted(int positionStart, int itemCount) {

        mObservable.onItemRangeInserted(positionStart, itemCount);

    }
    /**
     * Notifies UI that the underlying data has changed.
     */
    protected void notifyRangeRemoved(int positionStart, int itemCount) {

        mObservable.onItemRangeRemoved(positionStart, itemCount);

    }
    /**
     * Returns the presenter selector for this ObjectAdapter.
     */
    public final PresenterSelector getPresenterSelector() {

        return mPresenterSelector;
    }

    /**
     * Returns the item for the given position.
     */
    public abstract Object get(int position);

    /**
     * Returns the number of items in the adapter.
     */
    public abstract int size();

    /**
     * Returns the id for the given position.
     */
    public long getId(int position) {
        return NO_ID;
    }

}