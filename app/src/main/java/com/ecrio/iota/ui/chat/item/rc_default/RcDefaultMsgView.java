package com.ecrio.iota.ui.chat.item.rc_default;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.ecrio.iota.R;

import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class RcDefaultMsgView extends FrameLayout {

    private View mRoot;

    public RcDefaultMsgView(Context context) {
        this(context, null);
    }

    public RcDefaultMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public RcDefaultMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(false);
//
        setFocusableInTouchMode(false);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        mRoot = inflater.inflate(R.layout.item_chat_rich_default, this);

        ButterKnife.bind(this, mRoot);

    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }

    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setTitle(String message) {

    }

    public void setWho(String username) {

    }

    public void setState(int status) {

    }

    public void setImage(String phone) {

    }

}