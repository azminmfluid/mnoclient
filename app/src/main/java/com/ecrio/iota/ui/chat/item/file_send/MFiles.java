package com.ecrio.iota.ui.chat.item.file_send;

import android.database.Cursor;

import com.ecrio.iota.db.tables.FileContract;

/**
 * Created by Mfluid on 7/20/2016.
 */
public class MFiles {
    private String firstName;
    private int iD;
    private int fileID;
    private int fileSessionID;
    private int userID;
    private String sendBy;
    private String filePAth;

    public String getFilePAth() {
        return filePAth;
    }

    public void setFileSessionID(int fileSessionID) {
        this.fileSessionID = fileSessionID;
    }

    public int getFileSessionID() {
        return fileSessionID;
    }

    public int getFileID() {
        return fileID;
    }

    public void setFileID(int fileID) {
        this.fileID = fileID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getiD() {
        return iD;
    }

    public String getFirstName() {
        return firstName;
    }

    public static MFiles parse(Cursor query) {
        MFiles mTypes = null;
        while (query.moveToNext())
        {
            mTypes = new MFiles();
            mTypes.firstName = query.getString(query.getColumnIndex(FileContract.FileEntry.FIELD_FILE_NAME));
            mTypes.filePAth = query.getString(query.getColumnIndex(FileContract.FileEntry.FIELD_FILE_PATH));
            mTypes.iD = query.getInt(query.getColumnIndex(FileContract.FileEntry._ID));
            mTypes.fileID = query.getInt(query.getColumnIndex(FileContract.FileEntry.FOLDER_ID));
            mTypes.fileSessionID = query.getInt(query.getColumnIndex(FileContract.FileEntry.FIELD_SESSION_ID));
            mTypes.userID = query.getInt(query.getColumnIndex(FileContract.FileEntry.FIELD_USER_ID));
            mTypes.sendBy = query.getString(query.getColumnIndex(FileContract.FileEntry.FIELD_WHO));
        }
        return mTypes;
    }

    public static MFiles parse2(Cursor query) {
        MFiles mTypes = null;
        while (query.moveToNext())
        {
            mTypes = new MFiles();
            mTypes.firstName = query.getString(query.getColumnIndex(FileContract.FileEntry.FIELD_FILE_NAME));
            mTypes.filePAth = query.getString(query.getColumnIndex(FileContract.FileEntry.FIELD_FILE_PATH));
            mTypes.iD = query.getInt(query.getColumnIndex(FileContract.FileEntry._ID));
            mTypes.userID = query.getInt(query.getColumnIndex(FileContract.FileEntry.FIELD_USER_ID));
            mTypes.fileSessionID = query.getInt(query.getColumnIndex(FileContract.FileEntry.FIELD_SESSION_ID));
            mTypes.fileID = query.getInt(query.getColumnIndex(FileContract.FileEntry.FOLDER_ID));
            mTypes.sendBy = query.getString(query.getColumnIndex(FileContract.FileEntry.FIELD_WHO));
        }
        return mTypes;
    }
}