package com.ecrio.iota.ui.chat.item.rc_carousal;

import android.view.View;

/**
 * Created by user on 13-Jan-18.
 */

public class OnFocusChangeListener implements View.OnFocusChangeListener {

    View.OnFocusChangeListener mChainedListener;

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (mChainedListener != null) {

            mChainedListener.onFocusChange(view, hasFocus);

        }

        if(hasFocus) {

            view.performClick();
        }
    }
}
