package com.ecrio.iota.ui.login;

import android.content.Context;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;

public interface LoginContract {

    interface View extends BaseView<Presenter> {

        String getUsername();

        String getPassword();

        String getServerAddress();

        String showEmptyFields();

        void setLoadingIndicator(boolean active);

        void showUserNameError(int username_error);

        void showPasswordError(int password_error);

        void showServerAddressError(int address_error);

        void showLoginFailureMessage(String message);

        void showNetworkFailureMessage(String message);

        void showHome();

        Context getAppContext();

        void setUserName(String name);

        void setPassword(String password);

        void setIpAddress(String address);

        String getPort();

        void showPortError(int message);

        void setPort(int remotePort);

        void contactPermissionSuccess();

        void checkPermissionAndRegister();

        void setAutoFill(boolean b);
    }

    interface Presenter extends BasePresenter {

        void checkFields();

        void performRegister();

        void performClearFields();

        void getCurrentUser();

        void onViewDestroyed();
    }
}