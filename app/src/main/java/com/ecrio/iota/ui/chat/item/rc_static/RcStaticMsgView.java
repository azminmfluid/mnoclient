package com.ecrio.iota.ui.chat.item.rc_static;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.AvatarHelper;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.utility.Log;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class RcStaticMsgView extends FrameLayout {

    private View mRoot;
    @BindView(R.id.TVtime)
    public TextView mTVTime;
    @BindView(R.id.TVtext)
    public TextView mTVMessage;
    @BindView(R.id.IMGpropic)
    public CircularImageView mIVPic;

    public RcStaticMsgView(Context context) {
        this(context, null);
    }

    public RcStaticMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public RcStaticMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(false);
//
        setFocusableInTouchMode(false);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        mRoot = inflater.inflate(R.layout.item_chat_rich_static, this);

        ButterKnife.bind(this, mRoot);

    }

    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setMessageText(String message) {

        mTVMessage.setText(message);

    }

    public void setUserNameText(String username) {

    }

    public void setStatus(int status) {

    }

    public void setAvatarImage(String phone) {

        getAvatar(phone);
    }

    // Looks to see if an image is in the file system.
    private void getAvatar(String phoneNumber) {

        Drawable contactPic = DrawableCache.getContactPic(phoneNumber);

        if(contactPic == null) {
            String iD = AvatarHelper.contactIdByPhoneNumber(AppBaseApplication.getContext(), phoneNumber);
//            Log.Debug("PhoneToBitmap", "iD : " + iD);

            Uri thumbImageUri = getThumbImageUri(Integer.parseInt(iD));
//            Log.Debug("PhoneToBitmap", "URI : " + thumbImageUri);

            if (thumbImageUri != null) {
                Drawable drawable = null;
                try {
                    drawable = Drawable.createFromStream(AppBaseApplication.getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    mIVPic.setImageURI(thumbImageUri);
                }
            } else {
                mIVPic.setImageDrawable(null);
                mIVPic.setImageURI(null);

            }
        } else {
            mIVPic.setImageDrawable(contactPic);

        }

        if (null == mIVPic.getDrawable()) {

            mIVPic.setBackground(DrawableCache.getProPicBack(0, getContext()));
        }

    }

    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }
}