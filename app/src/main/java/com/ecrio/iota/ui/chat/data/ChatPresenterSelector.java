package com.ecrio.iota.ui.chat.data;

import com.ecrio.iota.ui.chat.item.file_send.OutgoingFilePresenter;
import com.ecrio.iota.ui.chat.item.info.InfoMsgPresenter;
import com.ecrio.iota.ui.chat.item.rc_carousal.RcCarousalMsgPresenter;
import com.ecrio.iota.ui.chat.item.rc_general.RcGeneralMsgPresenter;
import com.ecrio.iota.ui.chat.item.rc_reply.RcReplyMsgPresenter;
import com.ecrio.iota.ui.chat.item.txt_recv.IncomingMsgPresenter;
import com.ecrio.iota.ui.chat.item.txt_send.OutgoingMsgPresenter;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.chat.model.ReplyItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.ui.list_base.PresenterSelector;

/**
 * A {@link com.ecrio.iota.ui.list_base.PresenterSelector} that always returns the same {@link com.ecrio.iota.ui.list_base.Presenter}.
 * Useful for rows of items of the same type that are all rendered the same way.
 */
public final class ChatPresenterSelector extends PresenterSelector {

    private final Presenter mSenderPresenter;

    private final Presenter mFileSenderPresenter;

    private final Presenter mReceiverPresenter;

    private final Presenter mRCGeneralPresenter;

    private final Presenter mRCCarousalPresenter;

    private final RcReplyMsgPresenter mRCReplyPresenter;
    private final Presenter mInfoPresenter;

    public ChatPresenterSelector() {

        mSenderPresenter = new OutgoingMsgPresenter();

        mFileSenderPresenter = new OutgoingFilePresenter();

        mReceiverPresenter = new IncomingMsgPresenter();

        mRCGeneralPresenter = new RcGeneralMsgPresenter();

        mRCCarousalPresenter = new RcCarousalMsgPresenter();

        mRCReplyPresenter = new RcReplyMsgPresenter();

        mInfoPresenter = new InfoMsgPresenter();
    }

    @Override
    public Presenter getPresenter(Object item) {

        if (item instanceof ReplyItem)
            return mRCReplyPresenter;

        ChatItem chatItem = (ChatItem) item;

        if (chatItem.isOutgoing) {
            if (chatItem.isInfo) {
                return mInfoPresenter;
            }
            if (chatItem.isFile) {
                return mFileSenderPresenter;
            }
            // presenter for text send by isOutGouing.
            return mSenderPresenter;
        } else {
            if (chatItem.isInfo) {
                return mInfoPresenter;
            }
            if (chatItem.isRich) {
                if (chatItem.isGeneral) {
                    return mRCGeneralPresenter;
                }
                if (chatItem.isCarousal) {
                    return mRCCarousalPresenter;
                }
            }
            // presenter for text receiving by receiver.
            return mReceiverPresenter;
        }
    }

    @Override
    public Presenter[] getPresenters() {

        return new Presenter[]{mSenderPresenter, mReceiverPresenter, mFileSenderPresenter, mRCGeneralPresenter, mRCCarousalPresenter, mRCReplyPresenter, mInfoPresenter};
    }

}