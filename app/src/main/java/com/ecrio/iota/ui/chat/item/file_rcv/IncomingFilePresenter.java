package com.ecrio.iota.ui.chat.item.file_rcv;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class IncomingFilePresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int color;

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2(TAG,"called onCreateViewHolder");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        IncomingFileView movieCardView = new IncomingFileView(parent.getContext());

//        color = ContextCompat.getColor(parent.getContext(), R.color.grey);

//        updateCardBackgroundColor(movieCardView);

        return new ViewHolder(movieCardView);
    }

    private void updateCardBackgroundColor(IncomingFileView view, boolean selected) {
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);
//        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        ChatItem video = (ChatItem) item;

        viewHolder.tag = String.valueOf(video.fileid);

        IncomingFileView incomingFileView = (IncomingFileView)viewHolder.view;

        updateCardBackgroundColor(incomingFileView, (video.isSelected != 0));

        incomingFileView.setMessageText(video.msgText);

        incomingFileView.setUserNameText(AppBaseApplication.mPreferences.getString("my_phone", ""));

        incomingFileView.setStatus(video.deliveryStatus);

        incomingFileView.setAvatarImage(video.mPhone);

        incomingFileView.setTime(video.sendTime);

        incomingFileView.setThumbnailImage(video.thumb);
        Log.d2(TAG,video.msgText +" called onBindViewHolder");

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG,viewHolder.tag+" called onUnbindViewHolder");

    }

}
