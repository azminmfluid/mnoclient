package com.ecrio.iota.ui.all.bots.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mfluid on 5/21/2018.
 */

public class PhoneContact {

    @SerializedName("contactID")
    private String contactID;
    @SerializedName("contactName")
    private String contactName;
    @SerializedName("thumbNailPhoto")
    private String thumbNailPhoto;

    public String getContactID() {
        return contactID;
    }

    public String getContactName() {
        return contactName;
    }

    public String getThumbNailPhoto() {
        return thumbNailPhoto;
    }

    public PhoneContact setContactID(String contactID) {
        this.contactID = contactID;
        return this;
    }

    public PhoneContact setContactName(String contactName) {
        this.contactName = contactName;
        return this;
    }

    public PhoneContact setThumbNailPhoto(String thumbNailPhoto) {
        this.thumbNailPhoto = thumbNailPhoto;
        return this;
    }
}