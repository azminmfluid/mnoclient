package com.ecrio.iota.ui.all.contacts.item;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.all.contacts.model.PhoneContact;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class ContactItemPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int color;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2(TAG,"called onCreateViewHolder");

        ContactItemView movieCardView = new ContactItemView(parent.getContext());

        color = ContextCompat.getColor(parent.getContext(), R.color.grey);

        updateCardBackgroundColor(movieCardView);

        movieCardView.setFocusableInTouchMode(false);

        return new ViewHolder(movieCardView);
    }

    private void updateCardBackgroundColor(ContactItemView view) {

        view.setMainBackgroundColor(color);

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        PhoneContact phoneContactItem = (PhoneContact) item;

        viewHolder.tag = ""+phoneContactItem.getContactID();

        ContactItemView movieCardView = (ContactItemView)viewHolder.view;

        // setting the contact name
        movieCardView.setContactNameText(phoneContactItem.getContactName());

        // setting the last message
        movieCardView.setContactPhoto(phoneContactItem.getContactID(),phoneContactItem.getThumbNailPhoto());

        Log.d2(TAG,viewHolder.tag+" called onBindViewHolder");

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG,viewHolder.tag+" called onUnbindViewHolder");

        ContactItemView movieCardView = (ContactItemView)viewHolder.view;

        movieCardView.setContactNameText(null);

        movieCardView.setContactPhoto(null, null);
    }

}