package com.ecrio.iota.ui.settings;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.login.LoginFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mfluiduser on 11/25/2016.
 */

public class SettingsDialog extends DialogFragment {

    private View root;
    private LinearLayout camera, gallery;
    //    private MainActivity bookingFragment;
    private String tag = "ProfilePic";
    private String picturePath = "";

    @BindView(R.id.IMGchats)
    ImageView mMenuChats;

    @BindView(R.id.IMGmenuRight1)
    ImageView mMenuContacts;

    @BindView(R.id.TVtitle)
    TextView mMenuTitle;

    @BindView(R.id.IMGback)
    ImageView mMenuBack;

    @BindView(R.id.IMGmenuDelete)
    ImageView mMenuDelete;

    @BindView(R.id.IMGmenuAdd)
    ImageView mMenuAdd;

    @BindView(R.id.IMGmenuRight2)
    ImageView mMenuSettings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.full_screen_login);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        root = inflater.inflate(R.layout.fragment_settings_dialog, container, false);
        ButterKnife.bind(this, root);
        mMenuContacts.setVisibility(View.GONE);
        mMenuChats.setVisibility(View.GONE);
        mMenuAdd.setVisibility(View.GONE);
        mMenuSettings.setVisibility(View.GONE);
        mMenuBack.setVisibility(View.VISIBLE);
        mMenuBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClick();
            }
        });
        mMenuTitle.setText("Settings");
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        LoginFragment settingsFragment = LoginFragment.newInstance();
        settingsFragment.hasParent(true);
        settingsFragment.setParent(this);
        transaction.replace(R.id.container, settingsFragment, "");
        transaction.commit();
        return root;
    }

    public void onCancelClick() {

        SettingsDialog.this.dismiss();

    }

    public void onSaveClick() {

        SettingsDialog.this.dismiss();


    }
}