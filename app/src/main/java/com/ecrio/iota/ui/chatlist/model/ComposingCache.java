package com.ecrio.iota.ui.chatlist.model;

import android.util.Log;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.ui.chat.model.CompositionMsg;

import java.util.HashMap;

/**
 * Created by user on 29-Sep-17.
 */

public class ComposingCache {

    private static String TAG = "Composing";
    private static HashMap<Integer, String> composingCache = new HashMap<>();

    public static void clear() {
        composingCache.clear();
    }


    public static Boolean getIsComposing(int conversationID, int userID) {

        Log.d(TAG, "checking user " + userID + " is composing in session id " + conversationID);

        String composing;

        composing = composingCache.get(userID);

        if (composing == null) {

            try {

                composing = String.valueOf(false);

                CompositionMsg messageComposition = AppBaseApplication.mDb.getMessageComposition(conversationID, userID);

                if (messageComposition != null) {
                    int compositionState = messageComposition.getCompositionState();
                    if (compositionState == 1)
                        composing = String.valueOf(true);
                }
            } catch (Exception e) {

            }

            composingCache.put(userID, composing);
        }

        Log.d(TAG, "composing " + composing);

        return Boolean.parseBoolean(composing);
    }
}
