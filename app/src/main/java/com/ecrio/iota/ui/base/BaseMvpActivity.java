package com.ecrio.iota.ui.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.ecrio.iota.BuildConfig;
import com.ecrio.iota.R;

/**
 * Created by user on 2018.
 */

public abstract class BaseMvpActivity<V extends BaseView<P>, P extends BasePresenter> extends AppCompatActivity
        implements BaseView<P>, BaseCallback<V, P> {

    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        P presenter = createPresenter();
        setPresenter(presenter);
    }

    @NonNull
    @Override
    public abstract P createPresenter();

    @Override
    public void setPresenter(P presenter) {
        this.presenter = presenter;
    }

    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public P getBasePresenter() {
        return presenter;
    }

    @Override
    public V getMvpView() {
        return (V) this;
    }

    public abstract void showChatListToolBarMenu();

    public abstract void showChatToolBarMenu();

    public abstract void showContactListToolBarMenu();

    public abstract void updateToolbarTitle(String name);

    public abstract void showLoginToolBarMenu();

    public abstract void showChatToolBarDeleteMenu();

    public abstract void showAddContactToolBarMenu();

    public abstract void showEditContactToolBarMenu();

    public abstract void showSplashToolBar();

    public abstract void showBotToolBarDeleteMenu(String userId);

    public abstract void showBotToolBarDeleteMenu();
    public abstract void showBotToolBarMenu();

    protected View rationaleView;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (dismissPermissionRationale() != 0) {
                    return true;
                }
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    protected void createAndShowPermissionRationale(int action, int titleResId, int subtitleResId) {
        if (rationaleView == null) {
            rationaleView = ((ViewStub) findViewById(R.id.permission_rationale_stub)).inflate();
        } else {
            rationaleView.setVisibility(View.VISIBLE);
        }
        ((TextView) rationaleView.findViewById(R.id.rationale_title)).setText(titleResId);
        ((TextView) rationaleView.findViewById(R.id.rationale_subtitle)).setText(subtitleResId);
        rationaleView.setTag(action);
    }

    protected void showSnackBarPermissionMessage(int message) {
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_layout);
//        Snackbar snackbar = Snackbar.make(coordinatorLayout, getString(message), Snackbar.LENGTH_LONG)
//                .setAction(getString(R.string.snackbar_settings), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        intent.setData(Uri.parse("package:" + BuildConfig.APPLICATION_ID));
//                        startActivity(intent);
//                    }
//                });
//        snackbar.show();

        Snackbar snackbar = Snackbar.make(coordinatorLayout, getString(message), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.snackbar_settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                        startActivity(intent);
                    }
                });
        // get snackbar view
        View snackbarView = snackbar.getView();

        // change snackbar text color
        int snackbarTextId = android.support.design.R.id.snackbar_text;
        TextView textView = (TextView)snackbarView.findViewById(snackbarTextId);
        textView.setTextColor(getResources().getColor(R.color.white));

        snackbar.show();
    }
    protected void showSnackBarErrorMessage(int message) {
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_layout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, getString(message), Snackbar.LENGTH_LONG);
        // get snackbar view
        View snackbarView = snackbar.getView();

        // change snackbar text color
        int snackbarTextId = android.support.design.R.id.snackbar_text;
        TextView textView = (TextView)snackbarView.findViewById(snackbarTextId);
        textView.setTextColor(getResources().getColor(R.color.white));

        snackbar.show();
    }
    /**
     * Dismiss and returns the action associated to the rationale
     * @return
     */
    protected int dismissPermissionRationale() {
        if (rationaleView != null && rationaleView.getVisibility() == View.VISIBLE) {
            rationaleView.setVisibility(View.GONE);
            return (int) rationaleView.getTag();
        }
        return 0;
    }

    public void onDismissRationaleClick(View view) {
        dismissPermissionRationale();
    }

    public abstract void updateCount(int size);

}