package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableInternalSession;

public class InternalSession {

    private int internalID;

    private int userID;

    private String sessionID;

    private String status;


    public int getInternalID() {
        return internalID;
    }

    public void setInternalID(int internalID) {
        this.internalID = internalID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public static InternalSession parseDetails(Cursor cursor) {

        InternalSession conversationUser = null;

        if (cursor.moveToFirst()) {

            conversationUser = new InternalSession();

            conversationUser.internalID = cursor.getInt(cursor.getColumnIndex(TableInternalSession.SessionEntry.COLUMN_1));

            conversationUser.userID = cursor.getInt(cursor.getColumnIndex(TableInternalSession.SessionEntry.COLUMN_SESSION_ID));

//            conversationUser.sessionID = cursor.getString(cursor.getColumnIndex(TableUsersContract.UserEntry.COLUMN_USER_SESSION));

        }
        return conversationUser;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}