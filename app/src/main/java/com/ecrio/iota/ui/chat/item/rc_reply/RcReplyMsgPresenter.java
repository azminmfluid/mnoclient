package com.ecrio.iota.ui.chat.item.rc_reply;

import android.util.Log;
import android.view.ViewGroup;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.ui.chat.model.ReplyItem;
import com.ecrio.iota.ui.list_base.Presenter;

/**
 * Created by user on 21-Aug-17.
 */

public class RcReplyMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int color;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d(TAG, "called onCreateViewHolder");

        RcReplyMsgView movieCardView = new RcReplyMsgView(parent.getContext());

//        color = ContextCompat.getColor(parent.getContext(), R.color.blue_bg_list);

        movieCardView.setBackgroundColor(color);

        updateCardBackgroundColor(movieCardView);

        return new ViewHolder(movieCardView);
    }

    private void updateCardBackgroundColor(RcReplyMsgView view) {

//        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if(item instanceof ReplyItem) {
            ReplyItem composingItem = (ReplyItem) item;
            viewHolder.tag = composingItem.statusMsg;
            RcReplyMsgView movieCardView = (RcReplyMsgView) viewHolder.view;
            movieCardView.setAvatarImage(AppBaseApplication.mPreferences.getString("current_chat", ""));
            movieCardView.setComposingText(composingItem.statusMsg);
            Log.d(TAG, viewHolder.tag + " called onBindViewHolder");
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d(TAG, viewHolder.tag + " called onUnbindViewHolder");

        RcReplyMsgView movieCardView = (RcReplyMsgView) viewHolder.view;

        movieCardView.stopJumping();
    }

}
