package com.ecrio.iota.ui.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.utility.Log;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class CustomMultiAutoCompleteTextView extends android.support.v7.widget.AppCompatMultiAutoCompleteTextView {

    RecipientsChangeListener recipientsChangeListener;

    private PhoneNoFilter phoneNoFilter;

    public interface RecipientsChangeListener {

        public void onChange();

        public void onDeleted(String name);

        public void contactName(String name);
    }

    Context context;

    boolean isDeleting = false;

    String[] splitString;

    boolean isAddedFormAdaptor = false;
    boolean isTextAddingProgress = false;

    public boolean isTextAdditionInProgress = false,
            isTextDeletedFromTouch = false;

    private int beforeChangeIndex = 0, afterChangeIndex = 0, stringLength = 0;
    private String changeString = "";

    HashMap<String, Contact> selectedContacts = new HashMap<String, Contact>();
    OnClickListener spanClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            deleteString();
            selectedContacts.remove(v.getTag());
            recipientsChangeListener.onChange();

            recipientsChangeListener.onDeleted((String)v.getTag());
            isAddedFormAdaptor = false;

        }
    };

    public void setRecipientsChangeListerner(
            RecipientsChangeListener recipientsChangeListener) {

        this.recipientsChangeListener = recipientsChangeListener;
    }

    public HashMap<String, Contact> getSelectedContacts() {

        return selectedContacts;

    }

    private void deleteString() {
        int[] startEnd = getSelectionStartAndEnd();
        int i = startEnd[0];
        int j = startEnd[1];
        isTextDeletedFromTouch = true;
        isTextAdditionInProgress = true;

        final SpannableStringBuilder sb = new SpannableStringBuilder(this.getText());

        boolean hasCommaAtLast = true;
        try {
            sb.subSequence(Math.min(i, j + 1), Math.max(i, j + 1)).toString();
        } catch (Exception e) {
            hasCommaAtLast = false;
        }

        sb.replace(Math.min(i, hasCommaAtLast ? j + 1 : j),
                Math.max(i, hasCommaAtLast ? j + 1 : j), "");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CustomMultiAutoCompleteTextView.this.setText(sb);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        isTextAdditionInProgress = false;
                        stringLength = CustomMultiAutoCompleteTextView.this
                                .getText().toString().length();
                        isTextDeletedFromTouch = false;

                        CustomMultiAutoCompleteTextView.this
                                .setMovementMethod(LinkMovementMethod
                                        .getInstance());
                    }
                }, 50);

            }
        }, 10);
    }

    private int[] getSelectionStartAndEnd() {
        int[] startEnd = new int[2];
        startEnd[0] = this.getSelectionStart() < 0 ? 0 : this
                .getSelectionStart();
        startEnd[1] = this.getSelectionEnd() < 0 ? 0 : this.getSelectionEnd();
        return startEnd;
    }

    public ArrayList<TextView> selectedContactTextView = new ArrayList<TextView>();

    public CustomMultiAutoCompleteTextView(Context context, AttributeSet attrs,
                                           int defStyle) {
        super(context, attrs, defStyle);
        init(context);

    }

    public CustomMultiAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public CustomMultiAutoCompleteTextView(Context context) {
        super(context);
        init(context);
    }

    public void init(final Context context) {

        if (phoneNoFilter == null)
            phoneNoFilter = new PhoneNoFilter(this);
        this.context = context;
        this.setThreshold(1);
        this.setTokenizer(new CommaTokenizer());
        this.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {

//                isAddedFormAdaptor = true;

                createTextViewWithImage(view);

                recipientsChangeListener.onChange();

                isAddedFormAdaptor = true;
            }
        });

        this.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String addString = s.toString();

                if (!isAddedFormAdaptor) {

                    if (!isDeleting) {

                        if (!isTextAdditionInProgress) {

                            System.out.println("addString " + addString);

                            if (start != 0) {

                                char addedChar = s.toString().charAt(
                                        addString.length() - 1);

                                if (addedChar == ',') {

                                    String array[] = getText().toString()
                                            .split(",");

                                    String addedNewString = array[array.length - 1];

                                    int startString = getSelectionStart() + 1
                                            - (addedNewString.length() + 2);
                                    int endString = getSelectionStart();

                                    isTextAdditionInProgress = true;
                                    isDeleting = true;
                                    getText().replace(startString, endString,
                                            "");

                                    Drawable d = getResources().getDrawable(R.drawable.no_img_ico);

                                    addSpan(addedNewString, addedNewString, d,null);
                                    isTextAdditionInProgress = false;
                                    recipientsChangeListener.onChange();

                                }

                            }
                        }

                    } else {
                        isDeleting = false;
                    }

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                beforeChangeIndex = CustomMultiAutoCompleteTextView.this.getSelectionStart();
                changeString = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

                afterChangeIndex = CustomMultiAutoCompleteTextView.this.getSelectionEnd();
                if (!isTextDeletedFromTouch && s.toString().length() < changeString.length() && !isTextAdditionInProgress) {

                    String deletedString = "";
                    try {
                        deletedString = changeString.substring(afterChangeIndex, beforeChangeIndex);

                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    if (deletedString.length() > 0 && deletedString.contains(","))
                        deletedString = deletedString.replace(",", "");
                    if (!TextUtils.isEmpty(deletedString.trim()))
                        selectedContacts.remove(deletedString);
                    recipientsChangeListener.onChange();

                }

            }
        });

        this.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {

                    if (getText().toString().length() > 0) {

                        if (!getText().toString()
                                .substring(getText().toString().length() - 1)
                                .equals(",")) {

                            getText().append(",");

                        }
                    }
                }

            }
        });

    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return new myInputConnection(super.onCreateInputConnection(outAttrs),
                true);
    }

    private class myInputConnection extends InputConnectionWrapper {

        public myInputConnection(InputConnection target, boolean mutable) {
            super(target, mutable);
        }

        @Override
        public boolean deleteSurroundingText(int beforeLength, int afterLength) {

            isDeleting = true;

            dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
                    KeyEvent.KEYCODE_DEL));

            return false;
        }

    }

    private class TouchSpan extends ClickableSpan {

        OnClickListener onClickListener;

        public TouchSpan(OnClickListener onClickListener) {

            this.onClickListener = onClickListener;
        }

        Object tag;

        @Override
        public void onClick(View widget) {

            widget.setTag(getTag());

            onClickListener.onClick(widget);

        }

        public void setTag(Object obj) {
            this.tag = obj;
        }

        public Object getTag() {
            return tag;
        }

    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, (int) (40 * getResources().getDisplayMetrics().density), (int) (40 * getResources().getDisplayMetrics().density), false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    @SuppressWarnings("deprecation")
    public static Object convertViewToDrawable(View view) {
        int spec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        c.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(c);
        view.setDrawingCacheEnabled(true);
        Bitmap cacheBmp = view.getDrawingCache();
        Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
        view.destroyDrawingCache();
        return new BitmapDrawable(viewBmp);

    }

    public void createTextViewWithImage(View view) {

        String txtName = ((TextView) view.findViewById(R.id.txt_contactName)).getText().toString();

        String txtNumber = ((TextView) view.findViewById(R.id.txt_ContactNumber)).getTag().toString();

        Drawable d = null;
        String imgImage = null;

        if (null != ((ImageView) view.findViewById(R.id.imgContactImage)).getTag()) {
            imgImage = ((ImageView) view.findViewById(R.id.imgContactImage)).getTag().toString();

            try {
                InputStream inputStream = context.getContentResolver().openInputStream(Uri.parse(imgImage));
                d = Drawable.createFromStream(inputStream, imgImage.toString());
            } catch (FileNotFoundException e) {
                d = getResources().getDrawable(R.drawable.no_img_ico);
            }

        } else {

            d = getResources().getDrawable(R.drawable.no_img_ico);

        }

        System.out.println(txtName);

        int start = getSelectionStart() - (txtName.length() + 2);
        int end = getSelectionStart();
        getText().replace(start, end, "");

        addSpan(txtName, txtNumber, d, imgImage);
//		isAddedFormAdaptor = false;
        recipientsChangeListener.contactName(txtName);
    }

    public void addSpan(String txtName, String txtNumber, Drawable d, String imgImage) {

        TextView tv = new TextView(context);
        tv.setText(txtName);
        tv.setTextSize(30);
        tv.setPadding(10, 0, 10, 0);

        tv.setTextColor(getResources().getColor(R.color.black));
        tv.setBackgroundResource(R.drawable.oval);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        tv.setCompoundDrawablePadding(10);
        tv.setCompoundDrawablesWithIntrinsicBounds(resize(d), null, getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cancel), null);
        Contact contact = new Contact(txtName, txtNumber, imgImage);
        tv.setTag(contact);

        BitmapDrawable bd = (BitmapDrawable) convertViewToDrawable(tv);
        bd.setBounds(0, 0, bd.getIntrinsicWidth(), bd.getIntrinsicHeight());
        SpannableStringBuilder sb = new SpannableStringBuilder();

        Spanned coma = Html.fromHtml("<font color='#ffffff'>,</font>");
        sb.append(tv.getText());
        sb.append(coma);
        sb.setSpan(new ImageSpan(bd), sb.length() - (tv.getText().length() + 1), sb.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        this.setMovementMethod(LinkMovementMethod.getInstance());
        TouchSpan clickSpan = new TouchSpan(spanClickListener);
        clickSpan.setTag(txtName.toString());

        sb.setSpan(clickSpan, sb.length() - (tv.getText().length() + 1), sb.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        CustomMultiAutoCompleteTextView.this.append(sb);

        selectedContacts.put(txtName, contact);

    }

    class PhoneNoFilter implements InputFilter {


        public static final String PHONE_PREFIX = "+91 ";
        private EditText editText;

        public PhoneNoFilter(EditText editText) {

            this.editText = editText;
            this.editText.setFilters(new InputFilter[]{this});
        }

        Handler handler = new Handler(Looper.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                editText.setFreezesText(false);
            }
        };

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Log.d2("New Text ", "source......" + start + "............" + end + "......" + source);
            Log.d2("New Text ", "dest  ......" + dstart + "..........." + dend + "......." + dest);

            String text = dest.toString();
            String left_substring = text.substring(0, dstart);
            String right_substring1 = text.substring(dstart, dend);
            String right_substring2 = text.substring(dend, text.length());
            Log.d2("New Text ", "Left......" + left_substring + "......Right......" + right_substring2);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(left_substring);
            stringBuilder.append(source);
            stringBuilder.append(right_substring2);
            Log.d2("New Text ", "final string  .........................." + stringBuilder.toString());
            if (isAddedFormAdaptor) {

                editText.setFreezesText(true);
                handler.postDelayed(runnable, 100);
                return "";
            }
            return source;
        }
    }
}
