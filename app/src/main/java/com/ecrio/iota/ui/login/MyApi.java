package com.ecrio.iota.ui.login;

import android.text.TextUtils;

public class MyApi extends MyApiSupport {

    public boolean isLoggedIn() {
        return isValidCredentials(mUser.getUsername(), mUser.getPassword());
    }

    public boolean hasToken() {
        return isValidToken(mUser.getToken());
    }

    private boolean isValidCredentials(String username, String password) {
        return !TextUtils.isEmpty(username) && !TextUtils.isEmpty(password);
    }

    private boolean isValidToken(String token) {
        return !TextUtils.isEmpty(token);
    }

    public void setCredentials(String mUsername, String mPassword) {
        mUser.setCredentials(mUsername, mPassword);
    }

    public void setToken(String token) {
        mUser.setToken(token);
    }

}
