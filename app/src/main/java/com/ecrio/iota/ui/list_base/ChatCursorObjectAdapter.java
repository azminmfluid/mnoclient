package com.ecrio.iota.ui.list_base;

import android.database.Cursor;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.LruCache;

import com.ecrio.iota.utility.Log;

public class ChatCursorObjectAdapter extends ObjectAdapter {

    private CursorMapper mMapper;
    private CursorMapper mReplyMapper;
    private Cursor mCursor;
    private static final int CACHE_SIZE = 100;
    private final LruCache<Integer, Object> mItemCache = new LruCache<Integer, Object>(CACHE_SIZE);

    /**
     * Constructs an adapter with the given {@link PresenterSelector}.
     */
    public ChatCursorObjectAdapter(PresenterSelector presenterSelector) {
        super(presenterSelector);

    }

    /**
     * Constructs an adapter that uses the given {@link Presenter} for all items.
     */
    public ChatCursorObjectAdapter(Presenter presenter) {

        super(presenter);

    }

    /**
     * Sets the {@link CursorMapper} used to convert {@link Cursor} rows into
     * Objects.
     */
    public final void setMapper(CursorMapper mapper) {

        boolean changed = mMapper != mapper;

        mMapper = mapper;

        if (changed) {

            onMapperChanged();

        }
    }
    /**
     * Sets the {@link CursorMapper} used to convert {@link Cursor} rows into
     * Objects.
     */
    public final void setReplyMapper(CursorMapper mapper) {

        mReplyMapper = mapper;

    }
    private Cursor mReplyCursor;

    public void changeReplyCursor(Cursor cursor) {
        mReplyCursor = cursor;
        onReplyCursorChanged();
    }
    /**
     * Called when {@link #setMapper(CursorMapper)} is called and a different
     * mapper is provided.
     */
    protected void onMapperChanged() {
    }

    /**
     * Changes the underlying cursor to a new cursor. If there is
     * an existing cursor it will be closed if it is different than the new
     * cursor.
     *
     * @param cursor The new cursor to be used.
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void changeCursor(Cursor cursor) {

        if (cursor == mCursor) {

            return;
        }

        if (mCursor != null) {

            mCursor.close();

        }

        mCursor = cursor;

        mItemCache.trimToSize(0);

        onCursorChanged();

    }

    /**
     * Changes the underlying cursor to a new cursor. If there is
     * an existing cursor it will be closed if it is different than the new
     * cursor.
     *
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void refreshCursor() {

        if (mCursor == null) {

            return;
        }

        mItemCache.trimToSize(0);

        onCursorChanged();

    }

    /**
     * Called whenever the cursor changes.
     */
    protected void onCursorChanged() {
        if(hasFooterView()){
            Log.d("====== has footer view ");
        }
        Log.d("onCursorChanged");
        notifyChanged();
    }
    /**
     * Called whenever the cursor changes.
     */
    protected void onReplyCursorChanged() {
        mItemCache.trimToSize(0);
        notifyChanged();
    }
    @Override
    public Object get(int position) {

        // return reply item, if the current position is last and has footer view.
        if (position == size() - 1 && hasFooterView()) {
            Log.d("======has footer"+(size() - 1));
            mReplyCursor.moveToPosition(0);
            // returning a reply item
            return mReplyMapper.convert(mReplyCursor);

        }
//        Log.d("======has chat item");

        if (mCursor == null) {

            return null;

        }

        if (!mCursor.moveToPosition(position)) {

            throw new ArrayIndexOutOfBoundsException();

        }

        Object item = mItemCache.get(position);

        if (item != null) {

            return item;

        }

        item = mMapper.convert(mCursor);

        mItemCache.put(position, item);

        return item;
    }

//    @Override
//    public int size() {
//        if (mCursor == null) {
//            return 0;
//        }
//        return mCursor.getCount();
//    }

    @Override
    public int size() {

        switch (getState()) {
            case STATE_TYPING:
//                Log.d("state s calculated size + 1");
                return geSizePlus1();
            case STATE_SUGGESTIONS:
//                Log.d("state s calculated size + 1");
                return geSizePlus1();
            case STATE_NOT_TYPING:
            case STATE_NO_SUGGESTIONS:
            default:
                break;
        }
//        Log.d("state s calculated size");
        return getSize();
    }


    public static final int STATE_NOT_TYPING = 0;
    public static final int STATE_TYPING = 1;
    public static final int STATE_SUGGESTIONS = 2;
    public static final int STATE_NO_SUGGESTIONS = 3;
    private int state = STATE_NOT_TYPING;

    public int getSize() {

        if (mCursor == null) {

            return 0;

        }

        return mCursor.getCount();
    }

    public int geSizePlus1() {

        if (hasFooterView()) {

            return getSize() + 1;

        }

        return getSize();
    }

    // Override this method and set false if no need of footer view
    protected boolean hasFooterView() {

        return state == STATE_TYPING || state == STATE_SUGGESTIONS;
    }

    protected int getState() {
        return state;
    }

    public void setState(int compState) {

        if(state == STATE_SUGGESTIONS && compState == STATE_NO_SUGGESTIONS){
            Log.d("====== removed footer view ");
            try {
//                mReplyCursor = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        state = compState;

    }

}