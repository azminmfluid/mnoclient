package com.ecrio.iota.ui.all.contacts.model;

import android.database.Cursor;

import com.ecrio.iota.ui.list_base.CursorMapper;

public final class ContactsCursorMapper extends CursorMapper {

    private static int contactIdIndex;
    private static int thumbnailIndex;
    private static int displayNameIndex;

    @Override
    protected void bindColumns(Cursor cursor) {

        contactIdIndex = 0;
        displayNameIndex = 2;
        thumbnailIndex = 3;
    }


    @Override
    protected Object bind(Cursor cursor) {

        // contact id
        String contactID = cursor.getString(contactIdIndex);

        // contact name
        String displayName = cursor.getString(displayNameIndex);

        // contact name
        String thumbnailPhoto = cursor.getString(thumbnailIndex);

        // build a "ChatListItem" object to be processed.
        return new PhoneContact()
                .setContactID(contactID)
                .setContactName(displayName)
                .setThumbNailPhoto(thumbnailPhoto);
    }
}