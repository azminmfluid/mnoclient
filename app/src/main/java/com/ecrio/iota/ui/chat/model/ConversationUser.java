package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableUsersContract;

public class ConversationUser {

    private String phoneNumber;

    private int userID;

    private String sessionID;

    private String status;


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public static ConversationUser parseDetails(Cursor cursor) {

        ConversationUser conversationUser = null;

        if (cursor.moveToFirst()) {

            conversationUser = new ConversationUser();

            conversationUser.phoneNumber = cursor.getString(cursor.getColumnIndex(TableUsersContract.UserEntry.COLUMN_PHONE));

            conversationUser.userID = cursor.getInt(cursor.getColumnIndex(TableUsersContract.UserEntry.COLUMN_USER_ID));

//            conversationUser.sessionID = cursor.getString(cursor.getColumnIndex(TableUsersContract.UserEntry.COLUMN_USER_SESSION));

        }
        return conversationUser;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}