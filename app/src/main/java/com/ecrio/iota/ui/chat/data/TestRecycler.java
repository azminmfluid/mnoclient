package com.ecrio.iota.ui.chat.data;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class TestRecycler extends RecyclerView {

    public TestRecycler(Context context) {
        this(context, null);
    }

    public TestRecycler(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TestRecycler(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayoutManager(new GMangager(context, 2));
//        setAdapter(new Adapter() {
//            @Override
//            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//                View inflate = LayoutInflater.from(getContext()).inflate(R.layout.chatlist_item_card_view, parent, false);
//                return new ViewH(inflate);
//            }
//
//            @Override
//            public void onBindViewHolder(ViewHolder holder, int position) {
//
//            }
//
//            @Override
//            public int getItemCount() {
//                return 5;
//            }
//        });

    }

    class ViewH extends ViewHolder {


        public ViewH(View itemView) {
            super(itemView);
        }
    }
}
