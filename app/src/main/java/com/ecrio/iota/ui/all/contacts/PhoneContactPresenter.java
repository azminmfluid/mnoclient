package com.ecrio.iota.ui.all.contacts;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.all.listner.OnItemViewClickedListener;
import com.ecrio.iota.ui.list_base.ItemBridgeAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;

/**
 * Created by Mfluid on 5/15/2018.
 */

public class PhoneContactPresenter implements PhoneContactContract.Presenter {

    private PhoneContactContract.View view;

    private OnItemViewClickedListener onItemViewClickedListener;

    public PhoneContactPresenter(PhoneContactContract.View view, OnItemViewClickedListener listener) {
        this.view = view;

        onItemViewClickedListener = listener;

    }

    @Override
    public void onViewReady() {

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        // view holder object
        ViewHolder vh = createRecyclerViewHolder(parent);

        vh.mAdapter = new GridItemBridgeAdapter();

        initializeRecyclerViewHolder(vh);

        return vh;
    }

    @Override
    public void onBindViewHolder(PhoneContactContract.Presenter.ViewHolder viewHolder, Object item) {

        // view holder object
        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter((ObjectAdapter) item);

        vh.getGridView().setAdapter(vh.mAdapter);

    }

    @Override
    public void onUnbindViewHolder(PhoneContactContract.Presenter.ViewHolder viewHolder) {

        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter(null);

        vh.getGridView().setAdapter(null);

    }

    /**
     * inflate list layout.
     */
    private ViewHolder createRecyclerViewHolder(ViewGroup parent) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_grid, parent, false);

        return new ViewHolder((RecyclerView) root.findViewById(R.id.browse_grid));

    }

    /**
     * Called after a {@link ViewHolder} is created.
     * @param vh The ViewHolder to initialize for the vertical grid.
     */
    private void initializeRecyclerViewHolder(ViewHolder vh) {

        Context context = vh.mRecyclerView.getContext();

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);

        vh.getGridView().setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, layoutManager.getOrientation());

        vh.getGridView().addItemDecoration(dividerItemDecoration);

    }

    /**
     * ViewHolder for the VerticalGridPresenter.
     */
    public static class ViewHolder extends PhoneContactContract.Presenter.ViewHolder {

        final RecyclerView mRecyclerView;

        ItemBridgeAdapter mAdapter;

        public ViewHolder(RecyclerView view) {
            super(view);
            mRecyclerView = view;
        }

        public RecyclerView getGridView() {

            return mRecyclerView;
        }

    }

    class GridItemBridgeAdapter extends ItemBridgeAdapter {

        @Override
        protected void onBind(final ViewHolder viewHolder, int position) {

            View itemView = viewHolder.mHolder.view;

            itemView.setOnClickListener(new View.OnClickListener() {

                public void onClick(View view) {

                    if(getOnItemViewClickedListener() != null) {

                        getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, viewHolder.mItem, position);

                    }

                }
            });

        }

        @Override
        protected void onCreate(ViewHolder viewHolder) {


        }

    }

    public final OnItemViewClickedListener getOnItemViewClickedListener() {
        return this.onItemViewClickedListener;
    }

}