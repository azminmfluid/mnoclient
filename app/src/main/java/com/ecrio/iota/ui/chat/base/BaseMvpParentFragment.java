package com.ecrio.iota.ui.chat.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.ecrio.iota.ui.base.BaseCallback;
import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;
import com.ecrio.iota.ui.base.ParentFragment;

public abstract class BaseMvpParentFragment<V extends BaseView<P>, P extends BasePresenter> extends ParentFragment implements BaseCallback<V, P> {

    protected P presenter;

    @NonNull
    public abstract P createPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        P presenter = createPresenter();
        setPresenter(presenter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setPresenter(@NonNull P presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public P getPresenter() {
        return presenter;
    }

    @NonNull
    @Override
    public V getMvpView() {
        return (V) this;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    public abstract void performDelete();

    public abstract void canGoBack();

    public abstract void performEdit();

    public abstract void clearSelection();
}