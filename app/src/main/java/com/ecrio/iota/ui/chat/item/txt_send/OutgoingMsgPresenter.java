package com.ecrio.iota.ui.chat.item.txt_send;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.model.rich_response.Reply;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;
import com.google.gson.Gson;

/**
 * Created by user on 2018.
 */

public class OutgoingMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int color;

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2(TAG,"called onCreateViewHolder");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        OutgoingMsgView movieCardView = new OutgoingMsgView(parent.getContext());

//        color = ContextCompat.getColor(parent.getContext(), R.color.grey);

//        updateCardBackgroundColor(movieCardView);

        return new ViewHolder(movieCardView);
    }

    private void updateCardBackgroundColor(OutgoingMsgView view, boolean selected) {
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);
//        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        ChatItem chatItem = (ChatItem) item;

        viewHolder.tag = chatItem.msgText;

        OutgoingMsgView outgoingMsgView = (OutgoingMsgView)viewHolder.view;

        updateCardBackgroundColor(outgoingMsgView, (chatItem.isSelected != 0));

        if(chatItem.isRich){
            String displayText = new Gson().fromJson(chatItem.msgText, Reply.class).getDisplayText();
            outgoingMsgView.setMessageText(displayText);
        }else{
            outgoingMsgView.setMessageText(chatItem.msgText);
        }

        outgoingMsgView.setUserNameText(AppBaseApplication.mPreferences.getString("my_phone", ""));

        outgoingMsgView.setStatus(chatItem.deliveryStatus);

        outgoingMsgView.setAvatarImage(chatItem.mPhone);

        outgoingMsgView.setTime(chatItem.sendTime);

        Log.d2(TAG,chatItem.msgText +" called onBindViewHolder");

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG,viewHolder.tag+" called onUnbindViewHolder");

    }

}
