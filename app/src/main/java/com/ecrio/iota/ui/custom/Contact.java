package com.ecrio.iota.ui.custom;

public class Contact {
	
	public String name;

	public String number;
	
	public String photoUri;

	public Contact(String name, String number, String photoUri) {
		super();
		this.name = name;
		this.number = number;
		this.photoUri = photoUri;
	}
	
	

}
