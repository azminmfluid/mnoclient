package com.ecrio.iota.ui.splash;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;

/**
 * Created by Mfluid on 5/15/2018.
 */

public interface SplashContract {

    interface View extends BaseView<Presenter> {

        void showLoginFragment();

        void showMainFragment();

    }

    interface Presenter extends BasePresenter {

        void openMain();

        void openLogin();

    }
}