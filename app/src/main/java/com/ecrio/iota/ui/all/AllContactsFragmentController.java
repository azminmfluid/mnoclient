package com.ecrio.iota.ui.all;

import android.os.Bundle;

import com.ecrio.iota.ui.all.bots.BotsFragment;
import com.ecrio.iota.ui.all.contacts.PhoneContactFragment;

/**
 * Created by Mfluid on 8/2/2016.
 */
public class AllContactsFragmentController extends AllContactsFragmentParent {

    public static AllContactsFragmentParent newInstance() {
        AllContactsFragmentController fragment = new AllContactsFragmentController();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public AllContactsFragmentChild createBotsFragment() {
        BotsFragment fragment = BotsFragment.newInstance();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public AllContactsFragmentChild createContactsFragment() {

        // Fragment for listing all the device contacts and chat bot contacts.
        PhoneContactFragment fragment = new PhoneContactFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

}
