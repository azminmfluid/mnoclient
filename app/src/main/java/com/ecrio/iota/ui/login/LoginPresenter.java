package com.ecrio.iota.ui.login;

import com.ecrio.iota.Configuration;
import com.ecrio.iota.ConfigurationLoader;
import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.login.DataSource;
import com.ecrio.iota.data.login.IotaRepository;
import com.ecrio.iota.model.LoginRequest;
import com.ecrio.iota.pref.Constants;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.SharedPreferenceHelper;
import com.google.common.base.Strings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

/**
 * Created by user on 2018.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;

    private IotaRepository repository;

    public LoginPresenter(LoginContract.View view, IotaRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void checkFields() {
        String username = this.view.getUsername();

        if (Strings.isNullOrEmpty(username)) {

            this.view.showUserNameError(R.string.username_empty);

            return;

        }

        String password = this.view.getPassword();

        if (Strings.isNullOrEmpty(password)) {

            this.view.showPasswordError(R.string.password_empty);

            return;
        }

        String serverAddress = this.view.getServerAddress();

        if (Strings.isNullOrEmpty(serverAddress)) {

            this.view.showServerAddressError(R.string.address_empty);

            return;

        }
        this.view.checkPermissionAndRegister();
    }

    @Override
    public void performRegister() {

        String username = this.view.getUsername();

        if (Strings.isNullOrEmpty(username)) {

            this.view.showUserNameError(R.string.username_empty);

            return;

        }

        String password = this.view.getPassword();

        if (Strings.isNullOrEmpty(password)) {

            this.view.showPasswordError(R.string.password_empty);

            return;
        }

        String serverAddress = this.view.getServerAddress();

        if (Strings.isNullOrEmpty(serverAddress)) {

            this.view.showServerAddressError(R.string.address_empty);

            return;

        }

        String port = this.view.getPort();

        if (Strings.isNullOrEmpty(port)) {

            this.view.showPortError(R.string.port_empty);

            return;

        }

        view.setLoadingIndicator(true);

        LoginRequest.Builder builder = new LoginRequest.Builder();

        builder.withUsername(username);

        builder.withPassword(password);

        builder.withAddress(serverAddress);

        builder.withPort(port);

        LoginRequest loginRequest = builder.build();

        repository.setRegistrationInvoked(false);

        repository.register(loginRequest, new DataSource.GetRegisterCallback() {
            @Override
            public void onSuccess() {

//                view.setLoadingIndicator(false);

//                handleLoginResult();
                // action taken against the status of Iota API.
            }

            @Override
            public void onFailed(String s) {
                view.setLoadingIndicator(false);
                handleLoginFailed(s);
            }
        });

    }

    @Override
    public void performClearFields() {

        view.setLoadingIndicator(true);

        view.showEmptyFields();

        view.setLoadingIndicator(false);
    }

    @Override
    public void getCurrentUser() {
        Log.d("Config: requesting current user.");
        repository.getCurrentUser(new DataSource.GetUserCallback() {
            @Override
            public void onSuccess(String username, String password, String address, int remotePort) {
                if (!Strings.isNullOrEmpty(username)) {

                    view.setUserName(username.replace("+", "").replace("[^0-9]",""));
                }
                if (!Strings.isNullOrEmpty(password)) {
                    view.setPassword(password);
                }
                if (!Strings.isNullOrEmpty(address)) {
                    view.setIpAddress(address);
                }
                view.setPort(remotePort);
                view.setAutoFill(false);
            }

            @Override
            public void onFailed(String s) {

            }
        });
    }

    @Override
    public void onViewDestroyed() {
        repository.quit();
    }

    private void handleLoginResult() {

        String username = this.view.getUsername();

        String password = this.view.getPassword();

        String serverAddress = this.view.getServerAddress();

        if (view.getAppContext() != null) {
            SharedPreferenceHelper mSharedPreferenceHelper = new SharedPreferenceHelper(view.getAppContext());

            mSharedPreferenceHelper.putString(Constants.USER_NAME, username);

            mSharedPreferenceHelper.putString(Constants.USER_PASSWORD, password);

            mSharedPreferenceHelper.putString(Constants.USER_ADDRESS, serverAddress);

//            mSharedPreferenceHelper.putInt(Constants.LOGIN_STATUS, 1);
        }

        // Getting the current saved details.
        UserInfo info = UserInfo.getInstance();
        info.readCash(view.getAppContext());

        // saving user details.here there is no data is currently saving.
        // Repository class will save the details.
        info.saveCash(view.getAppContext());

//        no need to show home page immediately, instead wait for the register success notification
//        at IotaModule class.
//        this.view.showHome();

    }

    private void handleLoginFailed(String message) {
        if (!Strings.isNullOrEmpty(message))
            this.view.showLoginFailureMessage(message);
    }

    @Override
    public void onViewReady() {

        Log.Debug("mvp_log", "View is ready");

//        readConfiguration();
    }

    private void readConfiguration() {
        installFiles();
//        view.setUserName("1234567890");
//        view.setPassword("111111");
//        view.setIpAddress("192.123.1.0");


        Configuration cfg;

        try {

            cfg = ConfigurationLoader.fromPreset(view.getAppContext(), "user/local.yaml");

        } catch (ParseException | IOException e) {

            e.printStackTrace();

            cfg = new Configuration();
        }

        Log.Debug("mvp_log", "The file path is  " + cfg);


    }


    String[] asset_files = {"config/local.yaml"};
    String[] file_name = {"local.yaml"};

    private synchronized void installFiles() {
        File filesDir = AppBaseApplication.context().getFilesDir();
        String filePath = filesDir.getAbsolutePath();
        try {
            long startTime = System.currentTimeMillis();
            File file = new File(filePath);
            Log.Debug(Log.TAG_IOTA_LOG, "The file path is  " + filePath);
            if (!file.exists()) {
                boolean mkdir = file.mkdir();
            }
            File configFolder = new File(filePath + "/" + "config");
            if (!configFolder.exists()) {
                boolean mkdir = configFolder.mkdir();
            }

            for (int i = 0; i < asset_files.length; i++) {
                file = new File(configFolder + "/" + file_name[i]);
                InputStream is = AppBaseApplication.context().getAssets().open(asset_files[i]);
                FileOutputStream fout = new FileOutputStream(file);
                int c = -1;

                while ((c = is.read()) != -1)
                    fout.write(c);

                fout.flush();
                fout.close();
                is.close();
            }
            Log.Debug(Log.TAG_IOTA_LOG, "Completion time  "
                    + (System.currentTimeMillis() - startTime));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}