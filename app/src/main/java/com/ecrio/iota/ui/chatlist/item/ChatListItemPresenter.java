package com.ecrio.iota.ui.chatlist.item;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.ui.chatlist.model.ChatListItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class ChatListItemPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int color;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2(TAG,"called onCreateViewHolder");

        ChatListItemView movieCardView = new ChatListItemView(parent.getContext());

        color = ContextCompat.getColor(parent.getContext(), R.color.grey);

        updateCardBackgroundColor(movieCardView);

        movieCardView.setFocusableInTouchMode(false);

        return new ViewHolder(movieCardView);
    }

    private void updateCardBackgroundColor(ChatListItemView view) {

        view.setMainBackgroundColor(color);

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        ChatListItem chatListItem = (ChatListItem) item;

        viewHolder.tag = ""+chatListItem.conversationID;

        ChatListItemView movieCardView = (ChatListItemView)viewHolder.view;

        // setting the contact name
        movieCardView.setContactNameText(UserCache.getUserName(chatListItem.id) + ": ");

        // setting the last message
        movieCardView.setLastMessageText(chatListItem.mSG);

        // setting the last message
        movieCardView.setTime(chatListItem.msgTime);

        movieCardView.setAvatarImage(chatListItem.mPhone);

        // setting the last message
        movieCardView.setMessageCount(chatListItem.msgCount);

//        Log.d2(TAG,viewHolder.tag+" called onBindViewHolder");

    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG,viewHolder.tag+" called onUnbindViewHolder");

    }

}