package com.ecrio.iota.ui.splash;

/**
 * Created by Mfluid on 5/15/2018.
 */

public class SplashPresenter implements SplashContract.Presenter {

    private SplashContract.View view;

    public SplashPresenter(SplashContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void openMain() {
        view.showLoginFragment();
    }

    @Override
    public void openLogin() {

    }
}