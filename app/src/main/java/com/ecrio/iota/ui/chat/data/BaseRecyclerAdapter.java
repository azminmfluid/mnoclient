package com.ecrio.iota.ui.chat.data;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 19-Sep-17.
 */

public abstract class BaseRecyclerAdapter<E extends Entity, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements OnRecyclerItemClickListener {


    protected OnRecyclerItemClickListener recyclerItemClickListener;

    protected List<E> data = new ArrayList<>();

    public void setRecyclerItemClickListener(OnRecyclerItemClickListener recyclerItemClickListener) {

        this.recyclerItemClickListener = recyclerItemClickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<E> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public List<E> getData() {
        return data;
    }

    public E getItem(int position) {
        if (data.size() > position) {
            return data.get(position);
        }
        return null;
    }

}
