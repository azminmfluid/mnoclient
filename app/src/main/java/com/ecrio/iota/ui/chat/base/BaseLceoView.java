package com.ecrio.iota.ui.chat.base;

import android.support.annotation.UiThread;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;

/**
 * Created by user on 2018.
 */

public interface BaseLceoView<P extends BasePresenter> extends BaseView<P> {

    @UiThread
    void loadData(boolean needProgress);

    @UiThread
    void showLoading();

    @UiThread
    void showContent();

    @UiThread
    void showError(String errorMsg, boolean isToast);

    @UiThread
    void showOffline();

    @UiThread
    void showOffline(String offlineText);

}
