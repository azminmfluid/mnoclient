package com.ecrio.iota.ui.chatlist.model;

import android.database.Cursor;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.ui.list_base.CursorMapper;
import com.ecrio.iota.utility.Log;

public final class ChatListItemCursorMapper extends CursorMapper {

    private static int conversationIdIndex;
    private static int userIdIndex;
    private static int messageIndex;
    private static int messageTimeIndex;
    private static int TypeIndex;

    @Override
    protected void bindColumns(Cursor cursor) {

        userIdIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID);

        conversationIdIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID);

        messageIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG);

        messageTimeIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME);
        TypeIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE);
    }


    @Override
    protected Object bind(Cursor cursor) {

        // user id
        int userID = cursor.getInt(userIdIndex);

        // phoneNumber correspond to userID
        String userName = UserCache.getPhoneNumber(userID);

        // conversation id
        int conversationID = cursor.getInt(conversationIdIndex);

        int unReadMessages = AppBaseApplication.mDb.getUnReadMessages(conversationID, userID);

        // conversation id
        double msgTimeMillis = cursor.getDouble(messageTimeIndex);
        boolean isRich = cursor.getInt(TypeIndex) == 3;
        String mSG = "";
        if(!isRich) {
            // message
            mSG = cursor.getString(messageIndex);
        }else {
            // rich message
            mSG = "New Message";
        }
        Boolean isComposing = ComposingCache.getIsComposing(conversationID, userID);

        // if the person is typing then display message as typing.
        if (isComposing) mSG = "Typing...";

        Log.d2("last_message", mSG);
        // build a "ChatListItem" object to be processed.
        return new ChatListItem.ChatItemBuilder()
                .id(userID)
                .phone(userName)
                .conversationId(conversationID)
                .message(mSG)
                .messageTime(msgTimeMillis)
                .unreadCount(unReadMessages)
                .build();
    }
}