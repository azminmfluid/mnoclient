package com.ecrio.iota.ui.chat.data;

import android.view.View;

import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellItem;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellView;

public interface AdapterListner {

    void onSelected(CarousalCellItem cell);

    void openFile(String path);

    void onProgressClick(View view, CarousalCellView cell);

    void onOpenLink(String link);

    void onOpencalendar(CreateCalendarEvent event);

    void makeCall(String number);

    void makeReply(String data);
}