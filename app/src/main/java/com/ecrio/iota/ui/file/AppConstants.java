package com.ecrio.iota.ui.file;

/**
 * Created by Mfluid on 9/28/2016.
 */

public class AppConstants {

    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE2 = 2;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 4;
    public static final int GALLERY = 1;
}