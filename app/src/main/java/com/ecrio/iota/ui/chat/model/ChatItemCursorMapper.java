package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.data.cache.MessageStatusCache;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.ui.list_base.CursorMapper;

public final class ChatItemCursorMapper extends CursorMapper {

    private static int idIndex;
    private static int msgIdIndex;
    private static int msgSelectedIndex;
    private static int msgReplyIdIndex;
    private static int userIdIndex;
    private static int movieNameIndex;
    private static int statusIndex;
    private static int TypeIndex;
    private static int isFileIdIndex;
    private static int fileProgressIndex;
    private static int deliveryProgressIndex;
    private static int fileThumbIndex;
    private int timeIndex;
    private static int richIDIndex;
    private static int richTypeIndex;

    @Override
    protected void bindColumns(Cursor cursor) {
        idIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID);
        msgIdIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID);
        msgSelectedIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_SELECTED);
        msgReplyIdIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_REPLY_ID);
        userIdIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID);
        movieNameIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG);
        statusIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS);
        TypeIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE);
        isFileIdIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID);
        fileProgressIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS);
        deliveryProgressIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE);
        fileThumbIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_THUMB);
        timeIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME);
        richIDIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_ID);
        richTypeIndex = cursor.getColumnIndex(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_RICH_TYPE);
    }

    @Override
    protected Object bind(Cursor cursor) {

        // Get the values of the video.
        int id = cursor.getInt(idIndex);
        int msgID = cursor.getInt(msgIdIndex);
        int msgSelected = cursor.getInt(msgSelectedIndex);
        int msgReplyID = cursor.getInt(msgReplyIdIndex);
        int userid = cursor.getInt(userIdIndex);
        int fileID = cursor.getInt(isFileIdIndex);
        Double time = cursor.getDouble(timeIndex);
        // phoneNumber correspond to userID
        String phoneNumber = UserCache.getPhoneNumber(userid);

        String chatMessage = cursor.getString(movieNameIndex);
        String fileProgress = cursor.getString(fileProgressIndex);
        int deliveryProgress = cursor.getInt(deliveryProgressIndex);
        boolean isOutGoing = cursor.getInt(statusIndex) == 1;

        boolean isFile = cursor.getInt(TypeIndex) == 1;
        boolean isInfo = cursor.getInt(TypeIndex) == 2;
        boolean isRich = cursor.getInt(TypeIndex) == 3;

        boolean isGeneral = cursor.getInt(richTypeIndex) == 1;
        boolean isCarousal = cursor.getInt(richTypeIndex) == 2;


        String cat = isFile ? "file" : isRich ? "rich" : isInfo ? "info" : "text";
        byte[] fileByte = cursor.getBlob(fileThumbIndex);
        boolean isMsgRead = deliveryProgress == MessageStatusCache.getReadValue().ordinal();
        // Build a Video object to be processed.
        return new ChatItem.VideoBuilder()
                .id(id)
                .msgID(msgID)
                .msgReplyID(msgReplyID)
                .isMsgRead(isMsgRead)
                .userid(userid)
                .phone(phoneNumber)
                .fileid(fileID)
                .category(cat)
                .isOutGouing(isOutGoing)
                .deliveryStatus(deliveryProgress)
                .isFile(isFile)
                .setRich(isRich)
                .setRichGeneral(isGeneral)
                .setRichCarousal(isCarousal)
                .isSelected(msgSelected)
                .thumb(fileByte)
                .isInfo(isInfo)
                .progress(fileProgress)
                .chatMessage(chatMessage)
                .Time(time)
                .build();
    }
}