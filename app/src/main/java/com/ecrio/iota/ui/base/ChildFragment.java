package com.ecrio.iota.ui.base;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class ChildFragment extends Fragment implements View.OnClickListener {

    public String TAG;
    protected ParentFragment parentFragment;
    private boolean hasMenu = false;

    public void setParent(ParentFragment parentFragment) {
        this.parentFragment = parentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * The time between two consecutive click events, in milliseconds.
     */
    private static final long MIN_CLICK_INTERVAL = 800;

    private long mLastClickTime = 0;

    /**
     * Custom {@link View.OnClickListener} preventing unwanted double click.
     *
     * @param view View where double click is undesirable
     */
    protected void onSingleClick(View view) {
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.uptimeMillis() - mLastClickTime < MIN_CLICK_INTERVAL)
            return;
        mLastClickTime = SystemClock.uptimeMillis();

        onSingleClick(v);
     }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.Debug("Visibility", "ChildFragment Menu is visible");
        } else {
            Log.Debug("Visibility", "ChildFragment Menu is not visible");
        }
    }
}
