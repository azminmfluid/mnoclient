package com.ecrio.iota.ui.login;

public class MyApiUser {

    private String mUsername;
    private String mPassword;
    private String mToken;

    public void setCredentials(String username, String password) {

        mUsername = username;
        mPassword = password;

    }

    public void setToken(String token) {

        mToken = token;

    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getToken() {
        return mToken;
    }
}
