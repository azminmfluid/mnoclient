package com.ecrio.iota.ui.all.bots;


import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.ChatProvider;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.ui.all.base.BaseMvpListFragment;
import com.ecrio.iota.ui.all.bots.item.ContactItemPresenter;
import com.ecrio.iota.ui.all.bots.model.ContactsCursorMapper;
import com.ecrio.iota.ui.all.bots.model.PhoneContact;
import com.ecrio.iota.ui.all.listner.OnItemViewClickedListener;
import com.ecrio.iota.ui.list_base.CursorObjectAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.utility.Log;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BotsFragment extends BaseMvpListFragment<FrameLayout, BotsContract.View, BotsContract.Presenter> implements BotsContract.View, LoaderManager.LoaderCallbacks<Cursor> {

    private static String searchText = "";
    private View mRoot;

    @BindView(R.id.id_lv_contacts)
    ListView mListView;
    private BotsContract.Presenter.ViewHolder mRecyclerViewHolder;
    private final CursorObjectAdapter mPhoneContactCursorAdapter = new CursorObjectAdapter(new ContactItemPresenter());
    private ObjectAdapter mAdapter;
    Cursor mCursor;
    private ContentObserver observer;
    private BotsFragment callback;

    private static final int ALL_BOTS_LOADER = 9;

    public BotsFragment() {
        // Required empty public constructor
    }

    public static BotsFragment newInstance() {

        Bundle args = new Bundle();

        BotsFragment fragment = new BotsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRoot = LayoutInflater.from(getActivity()).inflate(R.layout.updated_contact_list_fragment_layout, null);

        mListView = ((ListView) mRoot.findViewById(R.id.id_lv_contacts));

//        getLoaderManager().initLoader(PhoneContactFragment.ContactsReQuery.ID, null, callback);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }

        });
        callback = this;
        return mRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup listContainer = (ViewGroup) view.findViewById(R.id.chat_container);

        mRecyclerViewHolder = presenter.onCreateViewHolder(listContainer);

        listContainer.addView(mRecyclerViewHolder.view);

        mPhoneContactCursorAdapter.setMapper(new ContactsCursorMapper());

        setAdapter(mPhoneContactCursorAdapter);

    }

    private void updateAdapter() {

        if (mRecyclerViewHolder != null) {

            presenter.onBindViewHolder(mRecyclerViewHolder, mAdapter);

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            getParent().showSearch();
        }
    }

    @NonNull
    @Override
    public BotsContract.Presenter createPresenter() {
        return new BotsPresenter(this, new ItemViewClickedListener());
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {

        Uri contentUri;

        contentUri = TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS;

        Log.d2("onCreateLoader", searchText);

        String[] projection = null;

        String selection = null;

        String[] selectionArgs = null;

        String sortOrder = TableBotUsersContract.UserEntry.COLUMN_NAME;
        if ("".equalsIgnoreCase(searchText)) {

            selection = (TableBotUsersContract.UserEntry.COLUMN_NAME) + "<>''";

        } else {

            selection = (TableBotUsersContract.UserEntry.COLUMN_NAME) + "<>''" + " AND " + (TableBotUsersContract.UserEntry.COLUMN_NAME) + " like '%" + searchText + "%'";

        }
        return new CursorLoader(AppBaseApplication.context(), contentUri, projection, selection, selectionArgs, sortOrder);

    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        Log.d2("cursor", String.valueOf(cursor.getCount()));

        // set adapter here
        showError("No contacts", false);
        mCursor = cursor;

        int count = cursor.getCount();
//        String s = DatabaseUtils.dumpCursorToString(cursor);
//        Log.d2("Log_Size_", s);
        Log.d2("cursor", String.valueOf(count));

        if (mAdapter == null) {

            // set adapter
            setAdapter(mPhoneContactCursorAdapter);

        }
        // change the cursor
        mPhoneContactCursorAdapter.changeCursor(cursor);

        if (count == 0) {
            // set error message in view
            showError("No contacts", false);

        } else {

            showContent();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

        // set adapter empty
        if (loader.getId() == ALL_BOTS_LOADER) {
            mPhoneContactCursorAdapter.changeCursor(null);
        }
    }

    @Override
    public void showDeleteMenu() {
        ((MainActivity) getActivity()).showBotToolBarDeleteMenu();
    }

    @Override
    public void showDeleteMenu(int size, String id) {

        if ((getActivity()) != null) {
            if(size == 1)
            ((MainActivity) getActivity()).showBotToolBarDeleteMenu(id);
            else
                ((MainActivity) getActivity()).showBotToolBarDeleteMenu(null);
        }
    }

    @Override
    public void hideDeleteMenu() {

        if ((getActivity()) != null) {
            ((MainActivity) getActivity()).showBotToolBarMenu();
        }
        mPhoneContactCursorAdapter.refreshCursor();
    }

    @Override
    public void goBack(boolean b) {
        if (b) {
            if (getActivity() != null)
                ((MainActivity) getActivity()).backPressed(true);
        }
    }

    @Override
    public void updateDeleteCount(int size) {
        if (getActivity() != null)
        ((MainActivity) getActivity()).updateCount(size);
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, int position) {

            if (item instanceof PhoneContact) {

                PhoneContact chatListItem = (PhoneContact) item;

                getParent().updateBotContactDetailsFragment(getContactCursor(), position);

            }
        }
    }

    public Cursor getContactCursor() {

        return mCursor;
    }

    private final Handler mHandler = new Handler();
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            getLoaderManager().restartLoader(ALL_BOTS_LOADER, null, callback);
        }
    };

    public void updateSearchText(String searchText2) {

        showLoading();

        searchText = searchText2;

        mHandler.removeCallbacks(mRunnable);

        mHandler.postDelayed(mRunnable, 100);

//        getLoaderManager().restartLoader(0, null, contactReQueryCallBack);

    }

    public void updateSearchText2(String queryText) {
        searchText = queryText;
        // restart loader
        getLoaderManager().restartLoader(ALL_BOTS_LOADER, null, callback);
    }

    LoaderManager.LoaderCallbacks<Cursor> contactReQueryCallBacke = new LoaderManager.LoaderCallbacks<Cursor>() {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {

            Uri contentUri;

            contentUri = TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS;

            Log.d2("onCreateLoader", searchText);

            String[] projection = null;

            String selection = null;

            String[] selectionArgs = null;

            String sortOrder = null;
            if ("".equalsIgnoreCase(searchText)) {

                selection = (TableBotUsersContract.UserEntry.COLUMN_NAME) + "<>''";

            } else {

                selection = (TableBotUsersContract.UserEntry.COLUMN_NAME) + "<>''" + " AND " + (TableBotUsersContract.UserEntry.COLUMN_NAME) + " like '%" + searchText + "%'";

            }
            return new CursorLoader(AppBaseApplication.context(), contentUri, projection, selection, selectionArgs, sortOrder);

        }

        @Override
        public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {

            Log.d2("cursor", String.valueOf(cursor.getCount()));

            // set adapter here
            showError("No contacts", false);
            mCursor = cursor;

            int count = cursor.getCount();

            Log.d2("cursor", String.valueOf(count));

            if (mAdapter == null) {

                // set adapter
                setAdapter(mPhoneContactCursorAdapter);

            }
            // change the cursor
            mPhoneContactCursorAdapter.changeCursor(cursor);

            if (count == ALL_BOTS_LOADER) {
                // set error message in view
                showError("No contacts", false);

            } else {

                showContent();
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

            // set adapter empty
            if (loader.getId() == ALL_BOTS_LOADER) {
                mPhoneContactCursorAdapter.changeCursor(null);
            }
        }

    };

    /**
     * Sets the object adapter for the fragment.
     */
    public void setAdapter(ObjectAdapter adapter) {

        mAdapter = adapter;

        updateAdapter();
    }

    @Override
    public void loadData(boolean needProgress) {

        if (needProgress)
            showLoading();

        observer = new ContentObserver(new Handler(Looper.getMainLooper())) {
            @Override
            public void onChange(boolean selfChange) {
                android.util.Log.d("Insert/Delete", "onClick");
                if (callback.isAdded()) {
                    getLoaderManager().restartLoader(ALL_BOTS_LOADER, null, callback); // With appropriate loader args here
                } else {
                    android.util.Log.d("ContentObserver", "---------------------------------not added");
                }
            }
        };
        if (getActivity() != null)
            getActivity().getContentResolver().registerContentObserver(TableBotUsersContract.UserEntry.CONTENT_URI_ALL_USERS, true, observer);
//        getLoaderManager().initLoader(0, null, callback);
        getLoaderManager().initLoader(ALL_BOTS_LOADER, null, callback);
    }

    boolean isPause = false;

    @Override
    public void onPause() {
        isPause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Loader<?> loa;
        try {
            loa = getLoaderManager().getLoader(ALL_BOTS_LOADER);
        } catch (Exception e) {
            loa = null;
        }
        if (loa == null) {
            getLoaderManager().initLoader(ALL_BOTS_LOADER, null, callback);
        } else {
            getLoaderManager().restartLoader(ALL_BOTS_LOADER, null, callback);
        }
        isPause = false;
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
//                ChatProvider.testInsertBots();
            }
        }, 0);
        getPresenter().checkSelection();

    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (observer != null) {
                getActivity().getContentResolver().unregisterContentObserver(observer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (observer != null) {
                getActivity().getContentResolver().unregisterContentObserver(observer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void canGoBack() {
        getPresenter().canGoBack();
    }

    @Override
    public void deleteMessages() {
        getPresenter().deleteMessages();
    }

    @Override
    public void editContact() {
        getPresenter().performEditBotContact();
    }

    @Override
    public void clearSelection() {
        getPresenter().clearSelection();
    }

}