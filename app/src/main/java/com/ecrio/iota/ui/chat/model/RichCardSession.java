package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableInternalSession;
import com.ecrio.iota.db.tables.TableRichCardGroupContract;

public class RichCardSession {

    private int internalID;

    private int userID;

    private int sessionID;

    private String status;


    public int getInternalID() {
        return internalID;
    }

    public void setInternalID(int internalID) {
        this.internalID = internalID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getSessionID() {
        return sessionID;
    }

    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }

    public static RichCardSession parseDetails(Cursor cursor) {

        RichCardSession conversationUser = null;

        if (cursor.moveToFirst()) {

            conversationUser = new RichCardSession();

            conversationUser.internalID = cursor.getInt(cursor.getColumnIndex(TableRichCardGroupContract.GroupEntry.COLUMN_GROUP_ID));

            conversationUser.sessionID = cursor.getInt(cursor.getColumnIndex(TableRichCardGroupContract.GroupEntry.COLUMN_CONVERSATION_ID));

            conversationUser.userID = cursor.getInt(cursor.getColumnIndex(TableRichCardGroupContract.GroupEntry.COLUMN_USER_ID));

        }
        return conversationUser;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}