package com.ecrio.iota.ui.chat.base;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.view.View;

import com.ecrio.iota.R;

/**
 * Little helper class for animating content, error and loading view
 *
 * @author Hannes Dorfmann
 * @since 1.0.0
 */
public class BaseLceoAnimator {

    private BaseLceoAnimator() {
    }

    /**
     * Show the loading view. No animations, because sometimes loading things is pretty fast (i.e.
     * retrieve data from memory cache).
     */
    public static void showLoading(@NonNull View loadingView,
                                   @NonNull View contentView,
                                   @NonNull View errorView,
                                   @NonNull View offlineView) {
        if (contentView != null)
        contentView.setVisibility(View.GONE);
        if (errorView != null)
        errorView.setVisibility(View.GONE);
        if (offlineView != null)
        offlineView.setVisibility(View.GONE);
        if (loadingView != null)
        loadingView.setVisibility(View.VISIBLE);
    }

    /**
     * Shows the error view instead of the loading view
     */
    public static void showErrorView(@NonNull final View loadingView,
                                     @NonNull final View contentView,
                                     final View errorView,
                                     final View offlineView) {

        contentView.setVisibility(View.GONE);

        offlineView.setVisibility(View.GONE);

        final Resources resources = loadingView.getResources();

        // Not visible yet, so animate the view in
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator in = ObjectAnimator.ofFloat(errorView, View.ALPHA, 1f);

        ObjectAnimator loadingOut = ObjectAnimator.ofFloat(loadingView, View.ALPHA, 0f);

        set.playTogether(in, loadingOut);

        set.setDuration(resources.getInteger(R.integer.lce_error_view_show_animation_time));

        set.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

                errorView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                loadingView.setVisibility(View.GONE);

                loadingView.setAlpha(1f); // For future showLoading calls

            }
        });

        set.start();
    }

    /**
     * Shows the offline view instead of the loading view
     */
    public static void showOfflineView(@NonNull final View loadingView,
                                       @NonNull final View contentView,
                                       @NonNull final View errorView,
                                       final View offlineView) {

        contentView.setVisibility(View.GONE);

        errorView.setVisibility(View.GONE);

        final Resources resources = loadingView.getResources();
        // Not visible yet, so animate the view in
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator in = ObjectAnimator.ofFloat(offlineView, View.ALPHA, 1f);

        ObjectAnimator loadingOut = ObjectAnimator.ofFloat(loadingView, View.ALPHA, 0f);

        set.playTogether(in, loadingOut);

        set.setDuration(resources.getInteger(R.integer.lce_error_view_show_animation_time));

        set.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

                offlineView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                loadingView.setVisibility(View.GONE);

                loadingView.setAlpha(1f); // For future showLoading calls

            }
        });

        set.start();
    }

    /**
     * Display the content instead of the loadingView
     */
    public static void showContent(@NonNull final View loadingView,
                                   @NonNull final View contentView,
                                   @NonNull final View errorView,
                                   @NonNull final View offlineView) {

        if (contentView.getVisibility() == View.VISIBLE) {

            // No Changing needed, because contentView is already visible
            errorView.setVisibility(View.GONE);

            loadingView.setVisibility(View.GONE);

            offlineView.setVisibility(View.GONE);

        } else {

            errorView.setVisibility(View.GONE);

            if(offlineView.getVisibility() == View.VISIBLE) {

                final Resources resources = loadingView.getResources();

                final int translateInPixels = resources.getDimensionPixelSize(R.dimen.lce_content_view_animation_translate_y);
                // Not visible yet, so animate the view in
                AnimatorSet set = new AnimatorSet();

                ObjectAnimator contentFadeIn = ObjectAnimator.ofFloat(contentView, View.ALPHA, 0f, 1f);

                ObjectAnimator contentTranslateIn = ObjectAnimator.ofFloat(contentView, View.TRANSLATION_Y, translateInPixels, 0);

                ObjectAnimator loadingFadeOut = ObjectAnimator.ofFloat(loadingView, View.ALPHA, 1f, 0f);

                ObjectAnimator loadingTranslateOut = ObjectAnimator.ofFloat(loadingView, View.TRANSLATION_Y, 0, -translateInPixels);

                ObjectAnimator offlineFadeOut = ObjectAnimator.ofFloat(offlineView, View.ALPHA, 1f, 0f);

                ObjectAnimator offlineTranslateOut = ObjectAnimator.ofFloat(offlineView, View.TRANSLATION_Y, 0, -translateInPixels);

                set.playTogether(contentFadeIn, contentTranslateIn, loadingFadeOut, loadingTranslateOut, offlineFadeOut, offlineTranslateOut);

                set.setDuration(resources.getInteger(R.integer.lce_content_view_show_animation_time));

                set.addListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                        contentView.setTranslationY(0);

                        loadingView.setTranslationY(0);

                        offlineView.setTranslationY(0);

                        contentView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        offlineView.setVisibility(View.GONE);

                        offlineView.setAlpha(1f);

                        loadingView.setVisibility(View.GONE);

                        loadingView.setAlpha(1f); // For future showLoading calls

                        contentView.setTranslationY(0);

                        loadingView.setTranslationY(0);

                        offlineView.setTranslationY(0);

                    }
                });

                set.start();
            }
            else {

                final Resources resources = loadingView.getResources();

                final int translateInPixels = resources.getDimensionPixelSize(R.dimen.lce_content_view_animation_translate_y);
                // Not visible yet, so animate the view in
                AnimatorSet set = new AnimatorSet();

                ObjectAnimator contentFadeIn = ObjectAnimator.ofFloat(contentView, View.ALPHA, 0f, 1f);

                ObjectAnimator contentTranslateIn = ObjectAnimator.ofFloat(contentView, View.TRANSLATION_Y, translateInPixels, 0);

                ObjectAnimator loadingFadeOut = ObjectAnimator.ofFloat(loadingView, View.ALPHA, 1f, 0f);

                ObjectAnimator loadingTranslateOut = ObjectAnimator.ofFloat(loadingView, View.TRANSLATION_Y, 0, -translateInPixels);

                set.playTogether(contentFadeIn, contentTranslateIn, loadingFadeOut, loadingTranslateOut);

                set.setDuration(resources.getInteger(R.integer.lce_content_view_show_animation_time));

                set.addListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                        contentView.setTranslationY(0);

                        loadingView.setTranslationY(0);

                        contentView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        loadingView.setVisibility(View.GONE);

                        loadingView.setAlpha(1f); // For future showLoading calls

                        contentView.setTranslationY(0);

                        loadingView.setTranslationY(0);

                    }
                });

                set.start();
            }
        }
    }
}
