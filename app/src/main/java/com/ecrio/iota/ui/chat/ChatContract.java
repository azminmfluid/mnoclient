package com.ecrio.iota.ui.chat;

import android.content.Context;
import android.view.ViewGroup;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.OnItemViewSelectedListener;
import com.ecrio.iota.ui.chat.base.BaseLceoView;

public interface ChatContract {

    interface View extends BaseLceoView<Presenter> {

        String getMessageText();

        void showError(int message);

        void clearMessageText();

        int getConversationId();

        int getConversationUserId();

        String getConversationPhNo();

        String getFilePath();

        Context context();

        void goBack(boolean b);

        void showDeleteMenu();

        void hideDeleteMenu();

        void performDelete();

        void showFilePickerDialog();

        void handleGallery();

        void handleCamera();

        void takePicture();

        boolean isConnected();

        boolean hasQueuedMessage();

        void showEmogiView();

        void fetchQueuedMessage();

        void makeCall(String number);

        void showNumberError(int error_msg);

        void showLinkError(int error_msg);

        void openLink(String url);

        void showToast(String msg);

        void setReplyData(String data);

        String getReplyData();

        void refreshChat(String phoneNumber);

        void initiateSession();

        void showInfoDetail();
    }

    interface Presenter extends BasePresenter {

        void performSendMessage();

        void onDestroy();

        /**
         * Creates a new {@link android.view.View}.
         */
        ViewHolder onCreateViewHolder(ViewGroup parent);

        /**
         * Binds a {@link android.view.View} to an item.
         */
        void onBindViewHolder(ViewHolder viewHolder, Object item);

        /**
         * Unbinds a {@link android.view.View} from an item. Any expensive references may be
         * released here, and any fields that are not bound for every item should be
         * cleared here.
         */
        void onUnbindViewHolder(ViewHolder viewHolder);

        void startComposing();

        void stopComposing();

        void attemptFileSend();

        void startIMSession(String mNumber);

        /**
         * This will send the read status of the messages received.
         *
         * @param sessionID session ID of the chat
         * @param msgID     ID of the message
         */
        void performReadMessage(int sessionID, int msgID);

        void canGoBack();

        void deleteMessages();

        void performSendFile();

        void attemptQueuedMessageSend(int msgID, String msgText);

        void attemptQueuedReplySend(int msgID, String msgText);

        void attemptQueuedFileSend(int msgID, int fileid, String msgText, byte[] thumb);

        void clearSession(String mNumber);

        void performImogiView();

        void performSendReply();

        void setOnItemViewSelectedListener(OnItemViewSelectedListener viewSelectedListener);

        /**
         * ViewHolder can be subclassed and used to cache any view accessors needed
         * to improve binding performance (for example, results of findViewById)
         * without needing to subclass a View.
         */
        class ViewHolder {

            public String tag;

            public final android.view.View view;

            public ViewHolder(android.view.View view) {

                this.view = view;
            }

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }
        }


    }

}