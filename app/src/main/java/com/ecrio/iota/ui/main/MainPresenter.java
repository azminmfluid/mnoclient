package com.ecrio.iota.ui.main;

import com.ecrio.iota.data.main.DataSource;
import com.ecrio.iota.data.main.MainRepository;

/**
 * Created by Mfluid on 5/15/2018.
 */

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private final MainRepository mRepository;

    public MainPresenter(MainContract.View view, MainRepository repository) {
        this.view = view;
        mRepository = repository;
    }

    @Override
    public void onViewReady() {
        mRepository.getNewChat(new DataSource.GetNewChatCallback() {
            @Override
            public void onNewChat() {

            }

            @Override
            public void onFailed(String s) {

            }
        });
    }

    @Override
    public void openContactList() {

        // Tell view to show dashboard screen.
        view.showAllContactsFragment();
    }

    @Override
    public void openSplash() {

        // Tell view to show splash screen.
        view.showSplashFragment();
    }

    @Override
    public void openChatList() {

        // Tell view to show all chats screen.
        view.showChatListFragment();
    }

    @Override
    public void openPrevPage() {
        view.showPrevFragment();
    }

    @Override
    public void openDeletePage() {

        view.showDeleteDialog();
    }

    @Override
    public void openEditContactPage() {
        view.showEditContactFragment();
    }
    @Override
    public void openAddPage() {
        view.showAddContactFragment();
    }

    @Override
    public void listenNewSession() {
        mRepository.getNewChat(new DataSource.GetNewChatCallback() {
            @Override
            public void onNewChat() {

            }

            @Override
            public void onFailed(String s) {

            }
        });
    }

    @Override
    public void openSettingsPage() {
        view.showSettingsDialog();
    }

}