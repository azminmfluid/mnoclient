package com.ecrio.iota.ui.list_base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ecrio.iota.utility.Log;

import java.util.ArrayList;


public class ItemBridgeAdapter extends RecyclerView.Adapter {

    private ObjectAdapter mAdapter;

    private ArrayList<Presenter> mPresenters = new ArrayList<Presenter>();

    private ObjectAdapter.DataObserver mDataObserver = new ObjectAdapter.DataObserver() {
        @Override
        public void onChanged() {

            ItemBridgeAdapter.this.notifyDataSetChanged();

        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart,itemCount);
        }
    };

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Presenter.ViewHolder presenterVh;

        View view;

        Presenter presenter = mPresenters.get(viewType);

        presenterVh = presenter.onCreateViewHolder(parent);

        view = presenterVh.view;

        ViewHolder viewHolder = new ViewHolder(presenter, view, presenterVh);

        onCreate(viewHolder);

        View presenterView = viewHolder.mHolder.view;

        if (presenterView != null) {

            viewHolder.mFocusChangeListener.mChainedListener = presenterView.getOnFocusChangeListener();

            presenterView.setOnFocusChangeListener(viewHolder.mFocusChangeListener);

        }
        return viewHolder;
    }

    protected void onCreate(ViewHolder viewHolder) {
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.mItem = mAdapter.get(position);

        viewHolder.mPresenter.onBindViewHolder(viewHolder.mHolder, viewHolder.mItem);

        onBind(viewHolder, position);

    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {

        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.mPresenter.onUnbindViewHolder(viewHolder.mHolder);
        onViewDetached(viewHolder);

    }

    @Override
    public int getItemCount() {

        int count = mAdapter != null ? mAdapter.size() : 0;

//        Log.DebugAdapter("Item Count == " + count);

        return count;
    }

    @Override
    public long getItemId(int position) {

        return mAdapter.getId(position);

    }

    public void setAdapter(ObjectAdapter objectAdapter) {

        if (objectAdapter == mAdapter) {

            return;
        }

        if (mAdapter != null) {

            mAdapter.unregisterObserver(mDataObserver);
        }

        mAdapter = objectAdapter;

        if (mAdapter == null) {

            notifyDataSetChanged();

            return;
        }

        mAdapter.registerObserver(mDataObserver);

        notifyDataSetChanged();


    }

    @Override
    public int getItemViewType(int position) {

        PresenterSelector presenterSelector = mAdapter.getPresenterSelector();

        Object item = mAdapter.get(position);

        Presenter presenter = presenterSelector.getPresenter(item);

        int type = mPresenters.indexOf(presenter);

//        Log.DebugAdapter("item == " + item + "& type == " + type);

        if (type < 0) {

            mPresenters.add(presenter);

            type = mPresenters.indexOf(presenter);

//            Log.v(TAG, "getItemViewType added presenter " + presenter + " type " + type);

//            onAddPresenter(presenter, type);

        }

        return type;
    }

    /**
     * ViewHolder for the ItemBridgeAdapter.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public final Presenter mPresenter;

        final OnFocusChangeListener mFocusChangeListener = new OnFocusChangeListener();

        public final Presenter.ViewHolder mHolder;

        public Object mItem;

        ViewHolder(Presenter presenter, View view, Presenter.ViewHolder holder) {
            super(view);

            mPresenter = presenter;

            mHolder = holder;

        }

    }

    final class OnFocusChangeListener implements View.OnFocusChangeListener {
        View.OnFocusChangeListener mChainedListener;

        @Override
        public void onFocusChange(View view, boolean hasFocus) {

            if (mChainedListener != null) {
                mChainedListener.onFocusChange(view, hasFocus);
            }
        }
    }

    /**
     * Called when ViewHolder has been bound to data.
     */
    protected void onBind(ViewHolder viewHolder, int position) {
    }

    protected void onViewDetached(ViewHolder viewHolder) {
    }

    public int getItemsSize() {

        int count = mAdapter != null ? mAdapter.size() : 0;

        return count;
    }

    public void notifyChanged() {

        this.notifyItemChanged(getItemsSize());

    }

    public void notifyAllChanged() {

        this.notifyItemRangeChanged(0, getItemsSize());

    }

}