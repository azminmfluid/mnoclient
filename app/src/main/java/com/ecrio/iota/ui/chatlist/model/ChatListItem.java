package com.ecrio.iota.ui.chatlist.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Video is an immutable object that holds the various metadata associated with a single video.
 */
public final class ChatListItem implements Parcelable {
    public final int id;
    public final int conversationID;
    public final String mSG;
    public final int msgCount;
    public final String mPhone;
    public final Double msgTime;

    private ChatListItem(
            final int userID,
            final String mPhone,
            final int conversationID,
            final String message,
            final Double msgTime, final int msgCount) {
        this.id = userID;
        this.mPhone = mPhone;
        this.conversationID = conversationID;
        this.mSG = message;
        this.msgTime = msgTime;
        this.msgCount = msgCount;
    }

    protected ChatListItem(Parcel in) {
        id = in.readInt();
        conversationID = in.readInt();
        mSG = in.readString();
        mPhone = in.readString();
        msgTime = in.readDouble();
        msgCount = in.readInt();
    }

    public static final Creator<ChatListItem> CREATOR = new Creator<ChatListItem>() {
        @Override
        public ChatListItem createFromParcel(Parcel in) {
            return new ChatListItem(in);
        }

        @Override
        public ChatListItem[] newArray(int size) {
            return new ChatListItem[size];
        }
    };

    @Override
    public String toString() {
        String s = "Video{";
        s += "userID=" + id;
        s += ", conversationID='" + conversationID + "'";
        s += ", message='" + mSG + "'";
        s += "}";
        return s;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(conversationID);
        dest.writeString(mSG);
        dest.writeDouble(msgTime);
        dest.writeInt(msgCount);
    }

    // Builder for Video object.
    public static class ChatItemBuilder {
        private int userid;
        private int conversationid;
        private String message;
        private Double messageTime;
        private String userPhone;
        private int msgCount;

        public ChatItemBuilder id(int id) {
            this.userid = id;
            return this;
        }

        public ChatItemBuilder conversationId(int conversationid) {
            this.conversationid = conversationid;
            return this;
        }


        public ChatItemBuilder message(String message) {
            this.message = message;
            return this;
        }


        public ChatItemBuilder messageTime(Double messageTime) {
            this.messageTime = messageTime;
            return this;
        }

        public ChatListItem build() {
            return new ChatListItem(
                    userid,
                    userPhone,
                    conversationid,
                    message,
                    messageTime,
                    msgCount
            );
        }

        public ChatItemBuilder phone(String userPhone) {
            this.userPhone = userPhone;
            return this;
        }

        public ChatItemBuilder unreadCount(int msgCount) {
            this.msgCount = msgCount;
            return this;
        }
    }
}