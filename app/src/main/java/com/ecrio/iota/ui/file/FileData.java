package com.ecrio.iota.ui.file;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mfluid on 7/19/2016.
 */
public class FileData {
    private int Id;
    private String completed;
    private String sessionId;
    private int userId;
    private int sender;
    private String fileName;
    private String filePath;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileData() {
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public static List<Integer> parse(Cursor query) {
        List<Integer> longList = new ArrayList<>();
        int anInt = -1;
        while (query.moveToNext())
        {
            anInt = query.getInt(query.getColumnIndex(FileTable.FOLDER_ID));
            longList.add(anInt);
        }
        return longList;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public int getSender() {
        return sender;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }
}