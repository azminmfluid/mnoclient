package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableConversationMainContract;

public class Conversation {

    public int getConversationID() {
        return ConversationID;
    }

    public void setConversationID(int conversationID) {
        ConversationID = conversationID;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    private int ConversationID;
    private int status;
    private double time;


    public static Conversation parseConversation(Cursor cursor) {

        Conversation conversation = null;

        if (cursor.moveToFirst()) {

            conversation = new Conversation();

            conversation.ConversationID = cursor.getInt(cursor.getColumnIndex(TableConversationMainContract.ConversationEntry.COLUMN_CONVERSATION_ID));

            conversation.status = cursor.getInt(cursor.getColumnIndex(TableConversationMainContract.ConversationEntry.COLUMN_STATUS));

            conversation.time = cursor.getDouble(cursor.getColumnIndex(TableConversationMainContract.ConversationEntry.COLUMN_TIME));

        }
        return conversation;
    }


}
