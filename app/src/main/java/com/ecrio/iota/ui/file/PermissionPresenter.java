package com.ecrio.iota.ui.file;

import android.content.pm.PackageManager;

import com.ecrio.iota.model.Action;
import com.ecrio.iota.model.PermissionAction;
import com.ecrio.iota.utility.Log;

import java.util.ArrayList;

/**
 * Created by Mfluid on 5/24/2018.
 */

public class PermissionPresenter {
    private PermissionAction permissionAction;
    private PermissionCallbacks permissionCallbacks;

    public PermissionPresenter(PermissionAction permissionAction, PermissionCallbacks permissionCallbacks) {
        this.permissionAction = permissionAction;
        this.permissionCallbacks = permissionCallbacks;
    }

    public void requestReadContactsPermission() {
        checkAndRequestPermission(Action.READ_CONTACTS);
    }

    public void requestAllPermission() {
        checkAndRequestPermission(Action.READ_CONTACTS, Action.SAVE_IMAGE);
    }

    public void requestCallingPermission() {
        checkAndRequestPermission(Action.CALL_PHONE);
    }

    public void requestReadContactsPermissionAfterRationale() {
        requestPermission(Action.READ_CONTACTS);
    }

    public void requestWriteExternalStorangePermission() {
        checkAndRequestPermission(Action.SAVE_IMAGE);
    }

    public void requestWriteExternalStorangePermissionAfterRationale() {
        requestPermission(Action.SAVE_IMAGE);
    }

    public void requestCameraPermission() {
        checkAndRequestPermission(Action.TAKE_PHOTO);
    }

    public void requestCameraPermissionAfterRationale() {
        requestPermission(Action.TAKE_PHOTO);
    }

    public void requestCallingPermissionAfterRationale() {
        requestPermission(Action.CALL_PHONE);
    }

    private void checkAndRequestPermission(Action action) {
        if (permissionAction.hasSelfPermission(action.getPermission())) {
            permissionCallbacks.permissionAccepted(action.getCode());
        } else {
            if (permissionAction.shouldShowRequestPermissionRationale(action.getPermission())) {
                Log.d2("PermissionPresenter", "called showRationale for " + action.getPermission());
                permissionCallbacks.showRationale(action.getCode());
            } else {
                Log.d2("PermissionPresenter", "called requestPermission for " + action.getPermission());
                permissionAction.requestPermission(action.getPermission(), action.getCode());
            }
        }
    }

    private void checkAndRequestPermission(Action... actions) {
        boolean needPermissions = false;
        ArrayList<String> permissions = new ArrayList<>();
        for (Action action : actions) {

            if (permissionAction.hasSelfPermission(action.getPermission())) {

            } else {
                needPermissions = true;
                permissions.add(action.getPermission());
            }
        }
        if (!needPermissions) {
            permissionCallbacks.permissionAccepted(Action.ACTION_CODE_ALL);
        } else {
            permissionAction.requestPermission(permissions.toArray(new String[0]), Action.ACTION_CODE_ALL);
        }
    }

    private void requestPermission(Action action) {
        permissionAction.requestPermission(action.getPermission(), action.getCode());
    }

    public void checkGrantedPermission(int[] grantResults, int requestCode) {
        if (verifyGrantedPermission(grantResults)) {
            permissionCallbacks.permissionAccepted(requestCode);
        } else {
            permissionCallbacks.permissionDenied(requestCode);
        }
    }

    private boolean verifyGrantedPermission(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public interface PermissionCallbacks {
        void permissionAccepted(@Action.ActionCode int actionCode);

        void permissionDenied(@Action.ActionCode int actionCode);

        void showRationale(@Action.ActionCode int actionCode);
    }
}