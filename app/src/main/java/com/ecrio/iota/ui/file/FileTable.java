package com.ecrio.iota.ui.file;
import android.provider.BaseColumns;

public final class FileTable implements BaseColumns {

	public static final String TAG = "FileContract";
	public static final String TABLE_NAME = "filetable";
	public static final String FOLDER_ID = "fileID";
	public static final String FIELD_PROGRESS = "progress";
	public static final String FIELD_SESSION_ID = "session_id";
	public static final String FIELD_USER_ID = "user_id";
	public static final String FIELD_FILE_NAME = "f_name";
	public static final String FIELD_FILE_PATH = "f_path";
	public static final String FIELD_WHO = "who";
	public static final String FIELD_COMPLETED = "completed";

	public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
			+ " ("
			+ _ID + " integer primary key on conflict replace, "
			+ FIELD_COMPLETED + " text, "
			+ FOLDER_ID + " integer, "
			+ FIELD_FILE_NAME + " text, "
			+ FIELD_FILE_PATH + " text, "
			+ FIELD_SESSION_ID + " text, "
			+ FIELD_USER_ID + " text, "
			+ FIELD_WHO + " text, "
			+ FIELD_PROGRESS + " text)";

}