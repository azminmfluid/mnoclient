package com.ecrio.iota.ui.chat.item.rc_carousal.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ecrio.iota.ui.chat.item.rc_carousal.OnFocusChangeListener;

/**
 * Created by user on 19-Sep-17.
 */
public class CarousalCellVH extends RecyclerView.ViewHolder {

    final View.OnFocusChangeListener mFocusChangeListener = new OnFocusChangeListener();

    CarousalCellView view;

    public CarousalCellVH(View itemView) {
        super(itemView);

        view = (CarousalCellView)itemView;

//        mFocusChangeListener.mChainedListener = view.getOnFocusChangeListener();

        view.setOnFocusChangeListener(mFocusChangeListener);
    }
}