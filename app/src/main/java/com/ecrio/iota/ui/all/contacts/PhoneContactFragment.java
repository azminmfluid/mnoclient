package com.ecrio.iota.ui.all.contacts;

import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract.Contacts;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.all.base.BaseMvpListFragment;
import com.ecrio.iota.ui.all.contacts.item.ContactItemPresenter;
import com.ecrio.iota.ui.all.contacts.model.ContactsCursorMapper;
import com.ecrio.iota.ui.all.contacts.model.PhoneContact;
import com.ecrio.iota.ui.all.listner.OnItemViewClickedListener;
import com.ecrio.iota.ui.list_base.CursorObjectAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

import butterknife.ButterKnife;

public class PhoneContactFragment extends BaseMvpListFragment<FrameLayout, PhoneContactContract.View, PhoneContactContract.Presenter> implements PhoneContactContract.View {

    View mRoot;
    private static String searchText = "";

    Cursor mCursor;

    private ObjectAdapter mAdapter;
    private final CursorObjectAdapter mPhoneContactCursorAdapter = new CursorObjectAdapter(new ContactItemPresenter());
    private PhoneContactContract.Presenter.ViewHolder mRecyclerViewHolder;

    private final Handler mHandler = new Handler();

    public static PhoneContactFragment newInstance() {

        PhoneContactFragment mPhoneContactFragment = new PhoneContactFragment();

        return mPhoneContactFragment;

    }

    @NonNull
    @Override
    public PhoneContactContract.Presenter createPresenter() {
        return new PhoneContactPresenter(this, new ItemViewClickedListener());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        searchText = "";
        super.onPause();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRoot = inflater.inflate(R.layout.updated_contact_list_fragment_layout, container, false);

        ButterKnife.bind(this, mRoot);

        return mRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewGroup listContainer = (ViewGroup) view.findViewById(R.id.chat_container);

        mRecyclerViewHolder = presenter.onCreateViewHolder(listContainer);

        listContainer.addView(mRecyclerViewHolder.view);

        mPhoneContactCursorAdapter.setMapper(new ContactsCursorMapper());

        updateAdapter();

    }

    @Override
    public void canGoBack() {

    }

    @Override
    public void deleteMessages() {

    }

    /**
     * Sets the object adapter for the fragment.
     */
    public void setAdapter(ObjectAdapter adapter) {

        mAdapter = adapter;

        updateAdapter();
    }

    private void updateAdapter() {

        if (mRecyclerViewHolder != null) {

            presenter.onBindViewHolder(mRecyclerViewHolder, mAdapter);

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getParent() != null)
                try {
                    getParent().hideSearch();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    public Cursor getContactCursor() {

        return mCursor;
    }

    public interface ContactsQuery {

        final static int QUERY_ID = 1;

        final static Uri CONTENT_URI = Contacts.CONTENT_URI;

        final static Uri FILTER_URI = Contacts.CONTENT_FILTER_URI;

        final static String SELECTION = (Contacts.DISPLAY_NAME) + "<>''"
                + " AND " + Contacts.IN_VISIBLE_GROUP + "=1" + " AND "
                + Contacts.HAS_PHONE_NUMBER;

        final static String SORT_ORDER = Contacts.DISPLAY_NAME;

        final static String[] PROJECTION = {

                Contacts._ID,

                Contacts.LOOKUP_KEY,

                Contacts.DISPLAY_NAME,

                Contacts._ID,

                SORT_ORDER};

        final static int ID = 0;
        final static int LOOKUP_KEY = 1;
        final static int DISPLAY_NAME = 2;
        final static int PHOTO_THUMBNAIL_DATA = 3;
        final static int SORT_KEY = 4;
    }

    LoaderManager.LoaderCallbacks<Cursor> contactReQueryCallBack2 = new LoaderManager.LoaderCallbacks<Cursor>() {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {

            Uri contentUri;

            contentUri = ContactsQuery.CONTENT_URI;

            Log.d2("onCreateLoader", searchText);

            String SELECTION = "";

            if ("".equalsIgnoreCase(searchText)) {

                SELECTION = (Contacts.DISPLAY_NAME) + "<>''" + " AND " + Contacts.IN_VISIBLE_GROUP + "=1" + " AND " + Contacts.HAS_PHONE_NUMBER;

            } else {

                SELECTION = (Contacts.DISPLAY_NAME) + "<>''" + " AND " + (Contacts.DISPLAY_NAME) + " like '%" + searchText + "%'" + " AND " + Contacts.IN_VISIBLE_GROUP + "=1" + " AND " + Contacts.HAS_PHONE_NUMBER;

            }

            if (getActivity() != null)
                return new CursorLoader(getActivity(), contentUri, ContactsReQuery.PROJECTION, SELECTION, null, ContactsReQuery.SORT_ORDER);
            else return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {

            mCursor = cursor;

            int count = cursor.getCount();

            Log.d2("cursor", String.valueOf(count));

            if (mAdapter == null) {

                // set adapter
                setAdapter(mPhoneContactCursorAdapter);

            }
            // change the cursor
            mPhoneContactCursorAdapter.changeCursor(cursor);

            if (count == 0) {
                // set error message in view
                showError("No contacts", false);

            } else {

                showContent();
            }

        }

        @Override
        public void onLoaderReset(Loader<Cursor> arg0) {

            mPhoneContactCursorAdapter.changeCursor(null);

        }

    };

    @Override
    protected boolean requestDataIfViewCreated() {

        if (null == mAdapter) {

            return true;

        } else {

            return false;

        }

    }

    @Override
    public void loadData(boolean needProgress) {

        if (needProgress)
            showLoading();

        getLoaderManager().initLoader(ContactsReQuery.ID, null, contactReQueryCallBack2);

    }

    public interface ContactsReQuery {

        final static int QUERY_ID = 2;

        final static Uri CONTENT_URI = Contacts.CONTENT_URI;

        final static Uri FILTER_URI = Contacts.CONTENT_FILTER_URI;

        final static String SELECTION = (Contacts.DISPLAY_NAME) + "<>''" + " AND " + (Contacts.DISPLAY_NAME) + " like '%" + searchText + "%'" + " AND " + Contacts.IN_VISIBLE_GROUP + "=1" + " AND " + Contacts.HAS_PHONE_NUMBER;

        final static String SORT_ORDER = Contacts.DISPLAY_NAME;

        final static String[] PROJECTION = {Contacts._ID, Contacts.LOOKUP_KEY, Contacts.DISPLAY_NAME, Contacts.PHOTO_URI, Contacts._ID, SORT_ORDER};

        final static int ID = 0;
        final static int LOOKUP_KEY = 1;
        final static int DISPLAY_NAME = 2;
        final static int PHOTO_THUMBNAIL_DATA = 3;
        final static int SORT_KEY = 4;
    }

    public class PhoneCustomCursorAdapter extends CursorAdapter implements SectionIndexer {

        Context mContext;
        ViewGroup parent;
        View mmView;
        ViewHolder viewHolder;
        Cursor cursor;

        public int selectedPostion = -1;

        public PhoneCustomCursorAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
            mContext = context;
            cursor = c;
        }

        @Override
        public boolean hasStableIds() {

            return true;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            this.parent = parent;
            mmView = LayoutInflater.from(mContext).inflate(R.layout.contact_custom_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgContactImage = ((ImageView) mmView.findViewById(R.id.img_calltype));
            viewHolder.txtContactName = ((TextView) mmView.findViewById(R.id.txt_contact_name));
            viewHolder.contactLayout = ((RelativeLayout) mmView.findViewById(R.id.contactLayout));
            mmView.setTag(viewHolder);
            return mmView;

        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            viewHolder = (ViewHolder) view.getTag();

            viewHolder.txtContactName.setText(cursor.getString(ContactsReQuery.DISPLAY_NAME));

            viewHolder.imgContactImage.setMaxWidth(50);

            String uri = cursor.getString(ContactsReQuery.PHOTO_THUMBNAIL_DATA);

//            com.ecrio.util.Log.Debug("Contact_Log", cursor.getString(ContactsReQuery.DISPLAY_NAME) + "\n uri==" + uri);

            if (cursor.getPosition() == selectedPostion) {

                viewHolder.contactLayout.setBackgroundColor(context.getResources().getColor(R.color.red));

            } else {

                viewHolder.contactLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
            }

            viewHolder.contactLayout.setTag(cursor.getString(ContactsReQuery.ID));

            if (uri != null) {

                viewHolder.imgContactImage.setImageURI(Uri.parse(uri));

            } else {

                viewHolder.imgContactImage.setImageURI(null);

            }

            viewHolder.imgContactImage.setBackground(getActivity().getResources().getDrawable(R.drawable.logo_loader));

        }

        @Override
        public Cursor getCursor() {

            return cursor;
        }

        @Override
        public Cursor swapCursor(Cursor newCursor) {

            cursor = newCursor;

            return super.swapCursor(newCursor);
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        private class ViewHolder {

            TextView txtContactName;

            ImageView imgContactImage;

            RelativeLayout contactLayout;

        }

        @Override
        public Object[] getSections() {

            return null;
        }

        @Override
        public int getPositionForSection(int section) {

            return section;
        }

        @Override
        public int getSectionForPosition(int position) {

            return position;
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
    }

    public void updateSearchText(String searchText2) {

        showLoading();

        searchText = searchText2;

        mHandler.removeCallbacks(mRunnable);

        mHandler.postDelayed(mRunnable, 100);

//        getLoaderManager().restartLoader(ContactsReQuery.ID, null, contactReQueryCallBack);

    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {

            try {
                if (contactReQueryCallBack2 != null)
                    getLoaderManager().restartLoader(ContactsReQuery.ID, null, contactReQueryCallBack2);
            } catch (Exception e) {
                if (e.getMessage() != null)
                    Log.Error(e.getMessage());
//                e.printStackTrace();
            }

        }
    };

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, int position) {

            if (item instanceof PhoneContact) {

                PhoneContact chatListItem = (PhoneContact) item;

                getParent().updateContactDetailsFragment(getContactCursor(), position);

            }
        }
    }

}