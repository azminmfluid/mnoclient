package com.ecrio.iota.ui.all;


import android.support.v4.app.Fragment;

import com.ecrio.iota.utility.Log;

/**
 * Created by Mfluid on 8/2/2016.
 */
public class AllContactsFragmentChild extends Fragment {


    public AllContactsFragmentParent parentFragment;

    public void setParentFragment(AllContactsFragmentParent parentFragment) {
        this.parentFragment = parentFragment;
    }

    public AllContactsFragmentParent getParent() {
        return parentFragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            Log.Debug("Visibility", "AllContactsFragmentChild Menu is visible:      " + this.getClass().getSimpleName());
        } else {
//            Log.Debug("Visibility", "AllContactsFragmentChild Menu is not visible:  " + this.getClass().getSimpleName());
        }
    }
}
