package com.ecrio.iota.ui.chat.item.txt_send;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.utility.StringUtils;
import com.github.siyamed.shapeimageview.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class OutgoingMsgView extends FrameLayout {

    @BindView(R.id.TVtime)
    public TextView mTVTime;
    @BindView(R.id.IMGtickStatus)
    public ImageView mIVStatus;
    @BindView(R.id.TVtext)
    public TextView mTVMessage;
    @BindView(R.id.IMGpropic)
    public CircularImageView mIVPic;
    private String mMessage = "";
    private String mPhone = "";

    private View mRoot;

    public OutgoingMsgView(Context context) {
        this(context, null);
    }

    public OutgoingMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public OutgoingMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(false);

        setFocusableInTouchMode(false);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        mRoot = inflater.inflate(R.layout.item_chat_outgoing, this);

        ButterKnife.bind(this, mRoot);

    }

    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setMessageText(String message) {
        if (mMessage.contentEquals(message)) return;
        mMessage = message;
        mTVMessage.setText(message);

    }

    public void setUserNameText(String username) {

    }

    public void setStatus(int status) {

        mIVStatus.setImageBitmap(DrawableCache.getTypeface(status, getContext()));

    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }

    public void setTime(Double time) {
        mTVTime.setText("" + StringUtils.getDateToString(time));
    }

    public void setAvatarImage(String phone) {
        mIVPic.setVisibility(GONE);
//        getAvatar();
    }

    // set default profile icon.
    private void getAvatar() {
        Drawable drawable = DrawableCache.mOutAvatar;
        mIVPic.setImageDrawable(drawable);
        if (null == mIVPic.getDrawable()) {
            mIVPic.setBackground(DrawableCache.getProPicBack(1, getContext()));
        }
    }

}