package com.ecrio.iota.ui.chat.item.rc_general;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.ecrio.iota.R;
import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.GeneralPurposeCard;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.ui.chat.item.rc_reply.RcReplyMsgView;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;
import com.google.gson.Gson;

/**
 * Created by user on 2018.
 */

public class RcGeneralMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2("Log_View", "----------onCreateViewHolder-----------------");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        RcGeneralMsgView rcGeneralMsgView = new RcGeneralMsgView(parent.getContext());
        Log.d2("Final_RC", ""+rcGeneralMsgView);

//        updateCardBackgroundColor(incomingMsgView, false);

        return new ViewHolder(rcGeneralMsgView);
    }

    private void updateCardBackgroundColor(RcGeneralMsgView view, boolean selected) {

        Log.d2("Log_View", "selected is " + selected);
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ChatItem chatItem = (ChatItem) item;
        Log.d2("Log_Selected", "selected is " + chatItem.isSelected);
        viewHolder.tag = chatItem.msgText;
        RcGeneralMsgView rcGeneralMsgView = (RcGeneralMsgView) viewHolder.view;
        updateCardBackgroundColor(rcGeneralMsgView, (chatItem.isSelected != 0));
        rcGeneralMsgView.setMessageText(chatItem.msgText, String.valueOf(chatItem.msgID));
        rcGeneralMsgView.setAvatarImage(chatItem.mPhone);
        rcGeneralMsgView.setUserNameText("" + chatItem.userid);
        rcGeneralMsgView.setStatus(chatItem.deliveryStatus);
        Log.d2(TAG, chatItem.msgText + " called onBindViewHolder");
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        Log.DebugChat(" onUnbind "+viewHolder.tag);
        RcGeneralMsgView rcGeneralMsgView = (RcGeneralMsgView) viewHolder.view;
        rcGeneralMsgView.callback = null;
        rcGeneralMsgView.clear();
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder) {
        String msgText = viewHolder.tag;
        Log.DebugChat(" onViewAttached "+msgText);
//        RcGeneralMsgView rcGeneralMsgView = (RcGeneralMsgView) viewHolder.view;
//        rcGeneralMsgView.setMessageText(msgText);
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder viewHolder) {
        String msgText = viewHolder.tag;
        Log.DebugChat(" onViewAttached "+msgText);
        RcGeneralMsgView rcGeneralMsgView = (RcGeneralMsgView) viewHolder.view;
        rcGeneralMsgView.clear();
    }
}