package com.ecrio.iota.ui.base;

import com.ecrio.iota.ui.list_base.Presenter;

/**
 * Interface for receiving notification when a row or item becomes selected.
 */
public interface OnItemViewSelectedListener {
    /**
     * Called when a row or a new item becomes selected.
     * @param itemViewHolder The view holder of the item that is currently selected.
     */
    public void onItemSelected(Presenter.ViewHolder itemViewHolder);
}
