package com.ecrio.iota.ui.home;

import com.ecrio.iota.ui.base.BasePresenter;
import com.ecrio.iota.ui.base.BaseView;
import com.ecrio.iota.ui.chatlist.model.ChatListItem;

/**
 * Created by user on 2018.
 */

public interface MainContract {

    interface View extends BaseView<Presenter> {

        void showAllContactsFragment();

        void showAllChatsFragment();

        void showSettingsFragment();

        void showChatFragment(ChatListItem item);

        void showChatFragment(String number);

        void showChatFragment(String number, String name);

        boolean onBackPressed();


        void showDefaultView();
    }

    interface Presenter extends BasePresenter {

        void openChat();

        void openAllContacts();

        void openAllChats();

        boolean onBackPressed();

    }
}
