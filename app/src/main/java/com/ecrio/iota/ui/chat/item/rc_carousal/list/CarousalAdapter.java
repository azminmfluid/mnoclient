package com.ecrio.iota.ui.chat.item.rc_carousal.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.ui.chat.data.AdapterListner;
import com.ecrio.iota.ui.chat.data.BaseRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 19-Sep-17.
 */

public class CarousalAdapter extends BaseRecyclerAdapter<CarousalCellItem, CarousalCellVH> {

    private final Context mContext;
    private AdapterListner carousalListner;
    private final List<CarousalCellItem> selectedCells = new ArrayList<CarousalCellItem>();

    public CarousalAdapter(Context context) {

        mContext = context;

    }

    @NonNull
    @Override
    public CarousalCellVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CarousalCellView carousalCellView = new CarousalCellView(parent.getContext());

        carousalCellView.setFocusable(false);

        carousalCellView.setFocusableInTouchMode(false);

        carousalCellView.setListener(listener);

        return new CarousalCellVH(carousalCellView);
    }

    @Override
    public void onBindViewHolder(@NonNull CarousalCellVH holder, int position) {

        CarousalCellItem week = getItem(position);

        holder.view.init(week);

    }

    @Override
    public void onItemClick(int pos) {

    }

    public void setListner(AdapterListner listner) {
        this.carousalListner = listner;
    }

    public interface Listener {
        void handleClick(CarousalCellItem cell);

        void openFile(String path);

        void onProgressClick(View view, CarousalCellView cellView);

        void doOpenLink(String link);

        void doOpenCalendar(CreateCalendarEvent event);

        void doMakeCall(String number);

        void doReply(String data);
    }

    private final Listener listener = new CellClickedListener();

    private class CellClickedListener implements Listener {
        @Override
        public void handleClick(CarousalCellItem cell) {
            Content content_ = cell.getDate();
            boolean wasSelected = doSelectItem(content_, cell);
            if (carousalListner != null) {
                if (wasSelected) {
                    carousalListner.onSelected(cell);
                }
            }
        }

        @Override
        public void openFile(String path) {
            // open the media with external app.
            carousalListner.openFile(path);
        }

        @Override
        public void onProgressClick(View view, CarousalCellView cellView) {
            // download the media in background.
            carousalListner.onProgressClick(view,cellView);
        }

        @Override
        public void doOpenLink(String link) {
            // open the link with browser.
            carousalListner.onOpenLink(link);
        }

        @Override
        public void doOpenCalendar(CreateCalendarEvent event) {
            // add event to calendar.
            carousalListner.onOpencalendar(event);
        }

        @Override
        public void doMakeCall(String number) {
            // make call with the number.
            carousalListner.makeCall(number);
        }

        @Override
        public void doReply(String data) {
            // call reply API.
            carousalListner.makeReply(data);
        }
    }

    private boolean doSelectItem(Content content, CarousalCellItem cell) {

        clearOldSelections();

        if (content != null) {
            // Select a new cell.
            if (selectedCells.size() == 0 || !selectedCells.contains(cell)) {

                selectedCells.add(cell);

                cell.setSelected(true);
            }
        }
        // Update the adapter.
        validateAndUpdate();
        return content != null;
    }

    private void clearOldSelections() {

        for (CarousalCellItem selectedCell : selectedCells) {

            // De-select the currently-selected cell.
            selectedCell.setSelected(false);

        }

        selectedCells.clear();

    }

    private void validateAndUpdate() {
        notifyDataSetChanged();
    }

}
