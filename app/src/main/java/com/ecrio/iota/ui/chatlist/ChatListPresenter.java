package com.ecrio.iota.ui.chatlist;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.chatlist.listner.OnItemViewClickedListener;
import com.ecrio.iota.ui.chatlist.model.ChatListItem;
import com.ecrio.iota.ui.list_base.ItemBridgeAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.utility.Log;

import java.util.ArrayList;
import java.util.List;

public class ChatListPresenter implements ChatListContract.Presenter {

    private ChatListContract.View mView;

    private OnItemViewClickedListener onItemViewClickedListener;
    List<String> selectedUserIDs = new ArrayList<String>();

    public ChatListPresenter(ChatListContract.View mView, OnItemViewClickedListener listener) {

        this.mView = mView;

        onItemViewClickedListener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        ViewHolder vh = createGridViewHolder(parent);

        vh.mAdapter = new GridItemBridgeAdapter();

        initializeGridViewHolder(vh);

        return vh;
    }

    @Override
    public void onBindViewHolder(ChatListContract.Presenter.ViewHolder viewHolder, Object item) {

        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter((ObjectAdapter) item);

        vh.getGridView().setAdapter(vh.mAdapter);

    }

    @Override
    public void onUnbindViewHolder(ChatListContract.Presenter.ViewHolder viewHolder) {

        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter(null);

        vh.getGridView().setAdapter(null);

    }

    /**
     * inflate list layout.
     */
    protected ViewHolder createGridViewHolder(ViewGroup parent) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_grid, parent, false);

        return new ViewHolder((RecyclerView) root.findViewById(R.id.browse_grid));

    }

    /**
     * Called after a {@link ViewHolder} is created.     *
     *
     * @param vh The ViewHolder to initialize for the vertical grid.
     */
    protected void initializeGridViewHolder(ViewHolder vh) {

        Context context = vh.mRecyclerView.getContext();

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);

        vh.getGridView().setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, layoutManager.getOrientation());

        vh.getGridView().addItemDecoration(dividerItemDecoration);

    }

    public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        onItemViewClickedListener = listener;
    }

    public final OnItemViewClickedListener getOnItemViewClickedListener() {
        return this.onItemViewClickedListener;
    }

    @Override
    public void onViewReady() {

    }

    /**
     * ViewHolder for the VerticalGridPresenter.
     */
    public static class ViewHolder extends ChatListContract.Presenter.ViewHolder {

        final RecyclerView mRecyclerView;

        ItemBridgeAdapter mAdapter;

        public ViewHolder(RecyclerView view) {
            super(view);

            mRecyclerView = view;

        }

        public RecyclerView getGridView() {

            return mRecyclerView;
        }

    }

    class GridItemBridgeAdapter extends ItemBridgeAdapter {

        @Override
        protected void onBind(final ViewHolder viewHolder, int position) {

            View itemView = viewHolder.mHolder.view;

            itemView.setOnClickListener(new View.OnClickListener() {

                public void onClick(View view) {

                    if (getOnItemViewClickedListener() != null) {
                        getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, viewHolder.mItem);
                    }

                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    ChatListItem mItem = (ChatListItem) viewHolder.mItem;

                    int conversationID = mItem.conversationID;

                    int userID = mItem.id;

                    userID = applyMultiSelect(userID, conversationID, userID);

                    if (userID != -1) {
                        // select a new cell.
                        if (selectedUserIDs.size() == 0 || !selectedUserIDs.contains(String.valueOf(userID))) {

                            mView.showDeleteActionMenu();

                            selectedUserIDs.add("" + userID);

                            Log.Debug("Log_Update_", "called update ..........." + 1);

                        }
                    }
                    if (selectedUserIDs.size() == 0) {

                        mView.hideDeleteActionMenu();

                    }
                    Log.Debug("Log_Size_", "size is ..........." + selectedUserIDs);
                    return false;
                }
            });

        }

        @Override
        protected void onCreate(ViewHolder viewHolder) {


        }
        private int applyMultiSelect(int userID, int conversationID, int msgID) {

            for (String selectedUserID : selectedUserIDs) {

                if (selectedUserID.contentEquals(String.valueOf(msgID))) {

                    selectedUserIDs.remove(selectedUserID);

                    Log.Debug("Log_Update_", "called update ..........." + 0);

                    msgID = -1;

                    break;
                }
            }
            return msgID;
        }
    }
}