package com.ecrio.iota.ui.all.bots;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.RemoteException;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.db.tables.TableConversationGroupContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.db.tables.TableUsersContract;
import com.ecrio.iota.ui.all.bots.model.PhoneContact;
import com.ecrio.iota.ui.all.contacts.PhoneContactContract;
import com.ecrio.iota.ui.all.listner.OnItemViewClickedListener;
import com.ecrio.iota.ui.list_base.ItemBridgeAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.utility.Log;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mfluid on 5/15/2018.
 */

public class BotsPresenter implements BotsContract.Presenter {

    private BotsContract.View mView;
    private OnItemViewClickedListener onItemViewClickedListener;
    List<String> selectedMsgIDs = new ArrayList<String>();

    public BotsPresenter(BotsContract.View view, OnItemViewClickedListener listener) {
        this.mView = view;
        onItemViewClickedListener = listener;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        // view holder object
        ViewHolder vh = createRecyclerViewHolder(parent);

        vh.mAdapter = new GridItemBridgeAdapter();

        initializeRecyclerViewHolder(vh);

        return vh;
    }

    @Override
    public void onBindViewHolder(BotsContract.Presenter.ViewHolder viewHolder, Object item) {

        // view holder object
        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter((ObjectAdapter) item);

        vh.getGridView().setAdapter(vh.mAdapter);

    }

    @Override
    public void onUnbindViewHolder(BotsContract.Presenter.ViewHolder viewHolder) {

        ViewHolder vh = (ViewHolder) viewHolder;

        vh.mAdapter.setAdapter(null);

        vh.getGridView().setAdapter(null);

    }

    @Override
    public void canGoBack() {
        Log.Debug("Log_Size_", "size is ..........." + selectedMsgIDs);
        if (selectedMsgIDs.size() == 0) {
            mView.goBack(true);
        } else {
            selectedMsgIDs.clear();
            mView.hideDeleteMenu();
            mView.goBack(false);
        }
    }

    @Override
    public void deleteMessages() {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ArrayList<String> strings = new ArrayList<>(selectedMsgIDs);
        for (String id : strings) {
            selectedMsgIDs.remove(id);
            UserCache.resetUser(id);
//            Cursor cursor = AppBaseApplication.context().getContentResolver().query(TableBotUsersContract.UserEntry.CONTENT_URI_INDIVIDUAL, new String[]{TableBotUsersContract.UserEntry.COLUMN_PHONE}, TableBotUsersContract.UserEntry.COLUMN_USER_ID+"=?", new String[]{id}, null);
//            cursor.close();

            final String[] numberProjection = new String[]{TableBotUsersContract.UserEntry.COLUMN_PHONE};
            Cursor phone = new CursorLoader(AppBaseApplication.context(), TableBotUsersContract.UserEntry.CONTENT_URI_INDIVIDUAL,
                    numberProjection, TableBotUsersContract.UserEntry.COLUMN_USER_ID + "= ?",
                    new String[]{String.valueOf(id)}, null).loadInBackground();
            if (phone != null && phone.moveToFirst()) {
                final int contactNumberColumnIndex = phone.getColumnIndex(TableBotUsersContract.UserEntry.COLUMN_PHONE);
                String bot_number = phone.getString(contactNumberColumnIndex);
                Log.Debug("Log_Size_", bot_number + " number is " + bot_number);
                phone.close();
                if (!Strings.isNullOrEmpty(bot_number)) {
                    final String[] idProjection = new String[]{TableUsersContract.UserEntry.COLUMN_USER_ID};
                    Cursor group_user = new CursorLoader(AppBaseApplication.context(), TableUsersContract.UserEntry.CONTENT_URI_INDIVIDUAL,
                            idProjection, TableUsersContract.UserEntry.COLUMN_PHONE + "= ?",
                            new String[]{String.valueOf(bot_number)}, null).loadInBackground();
                    if (group_user != null && group_user.moveToFirst()) {
                        final int userIDColumnIndex = group_user.getColumnIndex(TableUsersContract.UserEntry.COLUMN_USER_ID);
                        int group_user_id = group_user.getInt(userIDColumnIndex);
                        Log.Debug("Log_Size_", bot_number + " group_user_id is " + group_user_id);
                        if (group_user_id != -1) {
                            ContentProviderOperation.Builder chats_builder = ContentProviderOperation.newDelete(TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(Long.valueOf(id)));
                            chats_builder.withSelection(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID + "=?", new String[]{String.valueOf(group_user_id)});
                            ops.add(chats_builder.build());
                            group_user.close();

                            ContentProviderOperation.Builder reply_builder = ContentProviderOperation.newDelete(TableReplyContract.CompositionEntry.CONTENT_URI_ALL_REPLIES);
                            reply_builder.withSelection(TableReplyContract.CompositionEntry.COLUMN_USER_ID + "=?", new String[]{String.valueOf(group_user_id)});
                            ops.add(reply_builder.build());

//                            ContentProviderOperation.Builder chatgroup_builder = ContentProviderOperation.newDelete(TableConversationGroupContract.GroupEntry.buildUserUri(Integer.valueOf(id)));
//                            chatgroup_builder.withSelection(TableConversationGroupContract.GroupEntry.COLUMN_USER_ID + "=?", new String[]{String.valueOf(group_user_id)});
//                            ops.add(chatgroup_builder.build());

                        }
                    }
                }

                // Name
                ContentProviderOperation.Builder name_builder = ContentProviderOperation.newDelete(TableBotUsersContract.UserEntry.buildUserUri2(Long.valueOf(id)));
                name_builder.withSelection(TableBotUsersContract.UserEntry.COLUMN_USER_ID + "=?", new String[]{String.valueOf(id)});
                // Adding update operation to operations list
                // to insert display name in the table ContactsContract.Data
                ops.add(name_builder.build());
            }

        }
        updateBotContact(ops);
        mView.hideDeleteMenu();
    }

    @Override
    public void checkSelection() {
        if (selectedMsgIDs.size() > 0) {
            mView.showDeleteMenu();
            if (selectedMsgIDs.size() == 1) {
                mView.showDeleteMenu(selectedMsgIDs.size(), selectedMsgIDs.get(0));
            }
        }
    }

    @Override
    public void clearSelection() {
        selectedMsgIDs.clear();
    }

    @Override
    public void performEditBotContact() {
        if (selectedMsgIDs.size() == 1) {
            String id = selectedMsgIDs.get(0);
        }
    }

    public boolean updateBotContact(ArrayList<ContentProviderOperation> ops) {

        try {
            ContentProviderResult[] contentProviderResults = AppBaseApplication.context().getContentResolver().applyBatch(TableBotUsersContract.CONTENT_AUTHORITY, ops);
            String s = contentProviderResults.toString();
            Log.d2("error", "error whule inserting image:  " + s + "");
        } catch (RemoteException e) {
            e.printStackTrace();
//                    Toast.makeText(getActivity(), "" + "Remote Exception while adding contact", Toast.LENGTH_LONG).show();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
//                    Toast.makeText(getActivity(), "Operation Exception While adding contact", Toast.LENGTH_LONG).show();
        }
//                Toast.makeText(getActivity(), "Contact updated", Toast.LENGTH_LONG).show();
        return true;

//        Toast.makeText(getActivity(), "Contact adding failed", Toast.LENGTH_LONG).show();
    }

    /**
     * inflate list layout.
     */
    private ViewHolder createRecyclerViewHolder(ViewGroup parent) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_grid, parent, false);

        return new ViewHolder((RecyclerView) root.findViewById(R.id.browse_grid));

    }

    /**
     * Called after a {@link ViewHolder} is created.
     *
     * @param vh The ViewHolder to initialize for the vertical grid.
     */
    private void initializeRecyclerViewHolder(ViewHolder vh) {

        Context context = vh.mRecyclerView.getContext();

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);

        vh.getGridView().setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, layoutManager.getOrientation());

        vh.getGridView().addItemDecoration(dividerItemDecoration);

    }

    /**
     * ViewHolder for the VerticalGridPresenter.
     */
    public static class ViewHolder extends BotsContract.Presenter.ViewHolder {

        public ViewHolder(RecyclerView view) {
            super(view);
            mRecyclerView = view;
        }

        public RecyclerView getGridView() {

            return mRecyclerView;
        }

    }

    class GridItemBridgeAdapter extends ItemBridgeAdapter {

        @Override
        protected void onBind(final ViewHolder viewHolder, int position) {


            View itemView = viewHolder.mHolder.view;
            PhoneContact mItem = (PhoneContact) viewHolder.mItem;
            if (selectedMsgIDs.contains(mItem.getContactID())) {
                Log.d("-----" + mItem.getContactID() + "[true]");
            } else {
                Log.d("------" + mItem.getContactID() + "[false]");
            }
            itemView.setOnClickListener(new View.OnClickListener() {

                public void onClick(View view) {

//                    mView.showError("on click", true);


                    String msgID = mItem.getContactID();

                    if (selectedMsgIDs.size() != 0) {

                        msgID = applyMultiSelect(msgID);

                        if (msgID != null) {
                            // select a new cell.
                            if (selectedMsgIDs.size() == 0 || !selectedMsgIDs.contains(String.valueOf(msgID))) {

                                selectedMsgIDs.add("" + msgID);

                                mView.showDeleteMenu();

                                Log.Debug("Bot_Contact", msgID + "[true]");

                                itemView.setSelected(true);
                            }
                        } else {
                            Log.Debug("Bot_Contact", msgID + "[false]");
                            itemView.setSelected(false);
                        }
                    } else {
                        if (getOnItemViewClickedListener() != null) {
                            getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, viewHolder.mItem, position);
                        }
                    }

                    if (selectedMsgIDs.size() >0) {
                        mView.showDeleteMenu(selectedMsgIDs.size(), selectedMsgIDs.get(0));
                    }
                    if (selectedMsgIDs.size() == 0) {
                        mView.hideDeleteMenu();
                    }
                    mView.updateDeleteCount(selectedMsgIDs.size());

                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
//                    mView.showError("on long click", true);


                    String msgID = mItem.getContactID();

                    msgID = applyMultiSelect(msgID);

                    if (msgID != null) {
                        // select a new cell.
                        if (selectedMsgIDs.size() == 0 || !selectedMsgIDs.contains(String.valueOf(msgID))) {

                            selectedMsgIDs.add("" + msgID);

                            mView.showDeleteMenu();
                            Log.Debug("Bot_Contact", msgID + "[true]");
                            itemView.setSelected(true);

                        }
                    } else {
                        Log.Debug("Bot_Contact", msgID + "[false]");
                        itemView.setSelected(false);
                    }

                    if (selectedMsgIDs.size() >0) {
                        mView.showDeleteMenu(selectedMsgIDs.size(), selectedMsgIDs.get(0));
                    }
                    if (selectedMsgIDs.size() == 0) {
                        mView.hideDeleteMenu();
                    }
                    mView.updateDeleteCount(selectedMsgIDs.size());
                    Log.Debug("Log_Size_", "size is ..........." + selectedMsgIDs);
                    return true;
                }
            });
            if (selectedMsgIDs.contains(mItem.getContactID())) {
                Log.Debug("Bot_Contact", mItem.getContactID() + "[true]");
                itemView.setSelected(true);
            } else {
                Log.Debug("Bot_Contact", mItem.getContactID() + "[false]");
                itemView.setSelected(false);
            }
        }

        @Override
        protected void onCreate(ViewHolder viewHolder) {


        }

        private String applyMultiSelect(String msgID) {

            for (String selectedMsgID : selectedMsgIDs) {

                if (selectedMsgID.contentEquals(String.valueOf(msgID))) {

                    selectedMsgIDs.remove(selectedMsgID);
                    Log.Debug("Log_Update_", "called update ..........." + 0);

                    msgID = null;

                    break;
                }
            }
            return msgID;
        }

    }

    private OnItemViewClickedListener getOnItemViewClickedListener() {
        return this.onItemViewClickedListener;
    }

}