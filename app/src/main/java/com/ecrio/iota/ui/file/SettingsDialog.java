package com.ecrio.iota.ui.file;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.ecrio.iota.ui.chat.ChatFragment;

/**
 * Created by mfluiduser on 11/25/2016.
 */

public class SettingsDialog extends DialogFragment {

    private View root;
    private LinearLayout camera, gallery;
//    private MainActivity bookingFragment;
    private ChatFragment fragment;
    private String tag = "ProfilePic";
    private String picturePath = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

//        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.full_screen_login);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//        root = inflater.inflate(R.layout.fragment_settings_dialog, container, false);
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        SettingsFragment settingsFragment = SettingsFragment.newInstance();
//        settingsFragment.hasParent(true);
//        settingsFragment.setParent(this);
//        transaction.replace(R.id.container, settingsFragment, "");
//        transaction.commit();
        return root;
    }

    public void onCancelClick() {

        SettingsDialog.this.dismiss();

    }

    public void onSaveClick() {

        SettingsDialog.this.dismiss();


    }
}