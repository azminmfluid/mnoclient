package com.ecrio.iota.ui.chatlist;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.tables.TableCompositionContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.ui.base.BaseMvpChatListFragment;
import com.ecrio.iota.ui.chat.ChatFragment;
import com.ecrio.iota.ui.chatlist.item.ChatListItemPresenter;
import com.ecrio.iota.ui.chatlist.listner.OnItemViewClickedListener;
import com.ecrio.iota.ui.chatlist.model.ChatListItem;
import com.ecrio.iota.ui.chatlist.model.ChatListItemCursorMapper;
import com.ecrio.iota.ui.chatlist.model.ComposingCache;
import com.ecrio.iota.ui.list_base.CursorObjectAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.utility.Log;
import com.google.common.base.Strings;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends BaseMvpChatListFragment<FrameLayout, ChatListContract.View, ChatListContract.Presenter> implements ChatListContract.View, LoaderManager.LoaderCallbacks<Cursor> {


    private View mRoot;

    private ImageView mChatList;

    private static final int ALL_CHATS_LOADER = 2;

    private static final int ALL_COMPOSITIONS = 3;

    private final CursorObjectAdapter mChatListCursorAdapter = new CursorObjectAdapter(new ChatListItemPresenter());

    private TextView mErrorView;
    //    private ChatListPresenter mPresenter;
    private ChatListContract.Presenter.ViewHolder mGridViewHolder;
    private ObjectAdapter mAdapter;


    public ChatListFragment() {
        // Required empty public constructor
    }

    public static ChatListFragment newInstance() {

        Bundle args = new Bundle();

        ChatListFragment fragment = new ChatListFragment();

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getLoaderManager().initLoader(ALL_CHATS_LOADER, null, this);

        getLoaderManager().initLoader(ALL_COMPOSITIONS, null, this);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();

        if ((getActivity()) != null) {
            ((MainActivity) getActivity()).showChatListToolBarMenu();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mRoot = inflater.inflate(R.layout.fragment_chat_list, container, false);

        mErrorView = (TextView) mRoot.findViewById(R.id.errorView);

        // The mapper is used to build the object from the cursor.
        mChatListCursorAdapter.setMapper(new ChatListItemCursorMapper());

        // set adapter
        setAdapter(mChatListCursorAdapter);

        return mRoot;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewGroup container = (ViewGroup) view.findViewById(R.id.chat_container);

        mGridViewHolder = presenter.onCreateViewHolder(container);

        container.addView(mGridViewHolder.view);

        updateAdapter();

    }

    /**
     * Sets the object adapter for the fragment.
     */
    public void setAdapter(ObjectAdapter adapter) {

        mAdapter = adapter;

        updateAdapter();
    }

    private void updateAdapter() {

        if (mGridViewHolder != null) {

            presenter.onBindViewHolder(mGridViewHolder, mAdapter);

        }
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (id == ALL_CHATS_LOADER) {

            return new CursorLoader(AppBaseApplication.context(), TableConversationsContract.ChatEntry.CONTENT_CHATLIST_URI,
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        } else {
            return new CursorLoader(AppBaseApplication.context(), TableCompositionContract.CompositionEntry.CONTENT_URI_ALL_COMPOSITIONS,
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {

        if (loader.getId() == ALL_CHATS_LOADER) {

            if (cursor != null && cursor.moveToFirst()) {

                mErrorView.setVisibility(View.GONE);

                mErrorView.setText("");

                // change the cursor
                mChatListCursorAdapter.changeCursor(cursor);

                showContent();

            } else {

                mErrorView.setVisibility(View.VISIBLE);

                mErrorView.setText("No Chats");

            }
        } else if (loader.getId() == ALL_COMPOSITIONS) {

            if (cursor != null && cursor.moveToFirst()) {

                // clear all the composition status inorder to refresh the status in the list.
                ComposingCache.clear();

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        mChatListCursorAdapter.refreshCursor();
//                        mGridViewHolder.mAdapter.notifyAllChanged();
                    }
                });
//                Toast.makeText(getActivity(), "composition changed", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

        mChatListCursorAdapter.changeCursor(null);

    }

    @NonNull
    @Override
    public ChatListContract.Presenter createPresenter() {

        return new ChatListPresenter(this, new ItemViewClickedListener());
    }

    @Override
    public void loadData(boolean needProgress) {

    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item) {
            if (item instanceof ChatListItem) {
                Log.Debug("ItemClick", "" + item);
                ChatListItem chatListItem = (ChatListItem) item;
                String my_phone = AppBaseApplication.mPreferences.getString("my_phone", "");
                if (chatListItem.id != -1) {
                    if (!Strings.isNullOrEmpty(chatListItem.mPhone)) {
                        if(!my_phone.contentEquals(chatListItem.mPhone)) {
                            ChatFragment chatFragment = new ChatFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("chat", chatListItem);
                            chatFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).loadChatThread(chatFragment);
                        }else{
                            Log.Debug("ItemClick", "unable to start session with your own number");
                            try {
                                Toast.makeText(getActivity(), "unable to create session with your own number", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void showDeleteActionMenu() {

        if ((getActivity()) != null) {
//            ((MainActivity) getActivity()).showChatToolBarDeleteMenu();
        }
    }

    @Override
    public void hideDeleteActionMenu() {

        if ((getActivity()) != null) {
//            ((MainActivity) getActivity()).showChatListToolBarMenu();
        }
    }
}