package com.ecrio.iota.ui.all.bots.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.ui.list_base.CursorMapper;

public final class ContactsCursorMapper extends CursorMapper {

    private static int contactIdIndex;
    private static int displayNameIndex;

    @Override
    protected void bindColumns(Cursor cursor) {
        contactIdIndex = cursor.getColumnIndex(TableBotUsersContract.UserEntry.COLUMN_USER_ID);
        displayNameIndex = cursor.getColumnIndex(TableBotUsersContract.UserEntry.COLUMN_NAME);
    }


    @Override
    protected Object bind(Cursor cursor) {

        // contact id
        String contactID = cursor.getString(contactIdIndex);

        // contact name
        String displayName = cursor.getString(displayNameIndex);

        // contact name
        String thumbnailPhoto = null;

        // build a "ChatListItem" object to be processed.
        return new PhoneContact()
                .setContactID(contactID)
                .setContactName(displayName)
                .setThumbNailPhoto(thumbnailPhoto);
    }
}