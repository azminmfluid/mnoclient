package com.ecrio.iota.ui.login;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ecrio.iota.Injection;
import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.ui.base.BaseMvpFragment;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.ui.main.MainContract;
import com.ecrio.iota.ui.settings.SettingsDialog;
import com.ecrio.iota.utility.ProgressDialogFeedBack;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginFragment extends BaseMvpFragment<LoginContract.View, LoginContract.Presenter> implements LoginContract.View, View.OnClickListener {

    private SettingsDialog parent;
    private boolean hasParent;
    private HandlerThread handlerThread;
    private boolean autoFill = true;

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((getActivity()) != null) {
            if (!hasParent) {
                ((MainActivity) getActivity()).showLoginToolBarMenu();
            }
        }
        checkSDCardPermission();
    }

    @BindView(R.id.BTNsave)
    public Button mLogin;

    @BindView(R.id.BTNreset)
    public Button mReset;

    @BindView(R.id.EDTname)
    public EditText mUsername;

    @BindView(R.id.EDTpassword)
    public EditText mPassword;

    @BindView(R.id.EDTserverAddress)
    public EditText mServerAddress;

    @BindView(R.id.EDTserverPort)
    public EditText mServerPort;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.activity_settings, container, false);

        ButterKnife.bind(this, root);

        mLogin.setOnClickListener(this);

        mReset.setOnClickListener(this);

        if (hasParent) {
            mLogin.setVisibility(View.INVISIBLE);
            mReset.setVisibility(View.INVISIBLE);
            mUsername.setEnabled(false);
            mPassword.setEnabled(false);
            mServerAddress.setEnabled(false);
            mServerPort.setEnabled(false);
        }
        mPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (mPassword.getRight() - mPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if(mPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD){
                            mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                            mPassword.setSelection(mPassword.getText().length());
                        }else{
                            mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            mPassword.setSelection(mPassword.getText().length());
                        }
                        return true;
                    }
                }
                return false;
            }
        });
//        checkSDCardPermission();
        handlerThread = new HandlerThread("MyHandlerThread");

        handlerThread.start();


        return root;
    }

    private void checkSDCardPermission() {
        if (getActivity() != null) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                if(autoFill)
                presenter.getCurrentUser();
            } else {
                if(autoFill)
                presenter.getCurrentUser();
//                ((MainContract.View) getActivity()).requestReadImage();
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewReady();
//        checkSDCardPermission();
    }

    @Override
    public String getUsername() {
        return mUsername.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPassword.getText().toString();
    }

    @Override
    public String getServerAddress() {
        return mServerAddress.getText().toString();
    }

    @Override
    public String showEmptyFields() {
        mUsername.setText("");
        mUsername.setError(null);
        mPassword.setText("");
        mPassword.setError(null);
        mServerAddress.setText("");
        mServerAddress.setError(null);
        return null;
    }

    @Override
    public void showUserNameError(int error) {
        mUsername.setError(getResources().getString(error));
        mUsername.requestFocus();
    }

    @Override
    public void showPasswordError(int error) {
        mPassword.setError(getResources().getString(error));
        mPassword.requestFocus();
    }

    @Override
    public void showServerAddressError(int error) {
        mServerAddress.setError(getResources().getString(error));
        mServerAddress.requestFocus();
    }

    @Override
    public void showHome() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).showAllContactsFragment();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setLoadingIndicator(false);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            new Handler(handlerThread.getLooper()).post(new Runnable() {
                @Override
                public void run() {
                    ProgressDialogFeedBack.getInstance(getActivity()).start("Registering...");
                }
            });

        } else {
            new Handler(handlerThread.getLooper()).post(new Runnable() {
                @Override
                public void run() {
                    ProgressDialogFeedBack.getInstance(getActivity()).cancel();
                }
            });
        }
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void setUserName(String name) {
        if(hasParent)
        mUsername.setText(name);
    }

    @Override
    public void setPassword(String password) {
        if(hasParent)
        mPassword.setText(password);
    }

    @Override
    public void setIpAddress(String address) {
        mServerAddress.setText(address);
    }

    @Override
    public String getPort() {
        return mServerPort.getText().toString().trim();
    }

    @Override
    public void showPortError(int message) {

        mServerPort.setError(getResources().getString(message));
        mServerPort.requestFocus();
    }

    @Override
    public void setPort(int remotePort) {
        mServerPort.setText(String.valueOf(remotePort));
    }

    @Override
    public void showLoginFailureMessage(String message) {
        String msg = message;
        if (msg.equals(""))
            msg = getResources().getString(R.string.api_default_error);
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.alert_title_for_api_error);
            builder.setMessage(msg);
            builder.setPositiveButton("Ok", null);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public void setParent(SettingsDialog parent) {
        this.parent = parent;
    }

    public void hasParent(boolean hasParent) {
        this.hasParent = hasParent;
    }

    @Override
    public void showNetworkFailureMessage(String message) {
        showError(message);
    }

    private void showError(String message) {
        String msg = message;
        if (msg.equals(""))
            msg = getResources().getString(R.string.api_default_error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.alert_title_for_network_error);
        builder.setMessage(msg);
        builder.setPositiveButton("Ok", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @NonNull
    @Override
    public LoginContract.Presenter createPresenter() {

        return new LoginPresenter(this, Injection.provideRepository(AppBaseApplication.context()));

    }

    @Override
    public void onClick(View view) {

        if (view == mLogin) {
            if (getActivity() != null) {
                presenter.checkFields();
            }
        } else if (view == mReset) {
            presenter.performClearFields();
        }
    }

    @Override
    public void contactPermissionSuccess() {
        presenter.performRegister();
    }

    @Override
    public void checkPermissionAndRegister() {
        if (getActivity() != null) {
            ((MainActivity) getActivity()).requestReadContacts();
        }
    }

    @Override
    public void setAutoFill(boolean autoFill) {

        this.autoFill = autoFill;
    }

    public void handleSDCard() {
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
        if(autoFill)
        presenter.getCurrentUser();
//        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().onViewDestroyed();
    }
}