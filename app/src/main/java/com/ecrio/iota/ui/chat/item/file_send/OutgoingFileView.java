package com.ecrio.iota.ui.chat.item.file_send;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.AvatarHelper;
import com.ecrio.iota.data.cache.DrawableCache;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.StringUtils;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 2018.
 */

public class OutgoingFileView extends FrameLayout {

    @BindView(R.id.TVtime)
    public TextView mTVTime;
    @BindView(R.id.IMGtickStatus)
    public ImageView mIVStatus;
    @BindView(R.id.IMGpropic)
    public CircularImageView mIVPic;
    @BindView(R.id.IVAttachment)
    ImageView mAttachment;

    private View mRoot;

    public OutgoingFileView(Context context) {
        this(context, null);
    }

    public OutgoingFileView(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.Widget_Leanback_ImageCardView);
    }

    public OutgoingFileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Make sure the ImageCardView is focusable.

        setFocusable(false);

        setFocusableInTouchMode(false);

        LayoutInflater inflater = LayoutInflater.from(getContext());

        mRoot = inflater.inflate(R.layout.item_chat_file_outgoing, this);

        ButterKnife.bind(this, mRoot);

    }

    public void setTextBackgroundColor(int color) {

//        root.setBackgroundColor(color);

    }

    public void setMessageText(String message) {

        Log.DebugFile(" file path is " + message);
    }
    public void setThumbnailImage(byte[] bytes) {
        if (bytes != null) {
            try {
                final Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                mAttachment.setImageBitmap(bitmap);
                mAttachment.setAlpha(Float.parseFloat("1"));
            } catch (Exception e) {
                android.util.Log.e("TAG", "Exception", e);
//                mAttachment.setImageResource(R.drawable.file_img);
                mAttachment.setAlpha(Float.parseFloat("0.15"));
            }
        } else {
            android.util.Log.e("TAG", "IMAGE NOT FOUND");
//            mAttachment.setImageResource(R.drawable.file_img);
            mAttachment.setAlpha(Float.parseFloat("0.15"));
        }

    }
    public void setUserNameText(String username) {

    }

    public void setStatus(int status) {

        mIVStatus.setImageBitmap(DrawableCache.getTypeface(status, getContext()));

    }

    public void setMainBackgroundColor(int color) {
        mRoot.setBackgroundColor(color);
    }

    public void setTime(Double time) {
        mTVTime.setText("" + StringUtils.getDateToString(time));
    }

    public void setAvatarImage(String phone) {

        getAvatar(phone);
    }

    // Looks to see if an image is in the file system.
    private void getAvatar(String phoneNumber) {

        Drawable contactPic = DrawableCache.getContactPic(phoneNumber);

        if (contactPic == null) {

            String iD = AvatarHelper.contactIdByPhoneNumber(AppBaseApplication.getContext(), phoneNumber);

            Log.Debug("PhoneToBitmap", "iD : " + iD);

            if (iD == null) {

                Drawable drawable = DrawableCache.mOutAvatar;

                mIVPic.setImageDrawable(drawable);

                DrawableCache.putContactPic(phoneNumber, drawable);

                return;
            }
            Uri thumbImageUri = getThumbImageUri(Integer.parseInt(iD));
            Log.Debug("PhoneToBitmap", "URI : " + thumbImageUri);

            if (thumbImageUri != null) {
                Drawable drawable = null;
                try {
                    drawable = Drawable.createFromStream(AppBaseApplication.getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);

                } catch (FileNotFoundException e) {

                    drawable = DrawableCache.mOutAvatar;

                    mIVPic.setImageDrawable(drawable);

                    DrawableCache.putContactPic(phoneNumber, drawable);
                }
            } else {
                mIVPic.setImageDrawable(null);
                mIVPic.setImageURI(null);

            }
        } else {
            mIVPic.setImageDrawable(contactPic);

        }

        if (null == mIVPic.getDrawable()) {

            mIVPic.setBackground(DrawableCache.getProPicBack(0, getContext()));
        }

    }

    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {

            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

}