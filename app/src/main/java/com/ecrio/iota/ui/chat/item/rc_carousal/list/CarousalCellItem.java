package com.ecrio.iota.ui.chat.item.rc_carousal.list;

import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.ui.chat.data.Entity;

/**
 * Describes the state of a particular rich card cell.
 */
public class CarousalCellItem extends Entity {
    private final Content data;
    private boolean isSelected;
    private final boolean isSelectable;
    private final String value;

    public CarousalCellItem(Content data, boolean selectable, boolean selected, String value) {
        this.data = data;
        isSelectable = selectable;
        isSelected = selected;
        this.value = value;
    }

    public Content getDate() {
        return data;
    }

    public boolean isSelectable() {
        return isSelectable;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "MonthCellDescriptor{"
                + "date="
                + data
                + ", value="
                + value
                + ", isSelected="
                + isSelected
                + ", isSelectable="
                + isSelectable
                + '}';
    }
}