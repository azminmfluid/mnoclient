
package com.ecrio.iota.ui.base;

import android.support.annotation.NonNull;

public interface BaseCallback<V extends BaseView<P>, P extends BasePresenter> {

  /**
   * Creates the presenter instance
   *
   * @return the created presenter instance
   */
  @NonNull
  P createPresenter();

  /**
   * Sets the presenter instance
   *
   * @param presenter The presenter instance
   */
  void setPresenter(P presenter);

  /**
   * Get the presenter.
   *
   * @return the presenter instance. can be null.
   */
  P getPresenter();

  /**
   * Get the MvpView for the presenter
   *
   * @return The view associated with the presenter
   */
  V getMvpView();
}

