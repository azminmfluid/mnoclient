package com.ecrio.iota.ui.list_base;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.chat.model.ReplyItem;
import com.ecrio.iota.utility.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ChatItemBridgeAdapter extends RecyclerView.Adapter {

    private ObjectAdapter mAdapter;

    private int lastPosition = -1;

    private ArrayList<Presenter> mPresenters = new ArrayList<Presenter>();
//    private HashMap<Presenter,Presenter> mPresenters = new HashMap<Presenter,Presenter>();

    private ObjectAdapter.DataObserver mDataObserver = new ObjectAdapter.DataObserver() {
        @Override
        public void onChanged() {

            ChatItemBridgeAdapter.this.notifyDataSetChanged();

        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            ChatItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            ChatItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            ChatItemBridgeAdapter.this.notifyItemRemoved(itemCount);
            Log.d2("Final_RC", "notifyItemRemoved[" + itemCount + "]");
        }
    };

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        Log.d2("Final_RC","onCreateViewHolder["+viewType+"]");
        Presenter.ViewHolder presenterVh;
        View view;
        Object item = (viewType == -100) ? new ReplyItem() : mAdapter.get(viewType);
        Log.d2("Final_RC", "onCreateViewHolder[" + viewType + "]" + (item instanceof ReplyItem ? "is reply" : "is chat"));
        PresenterSelector presenterSelector = mAdapter.getPresenterSelector();
        Presenter presenter = presenterSelector.getPresenter(item);

//        Presenter presenter = mPresenters.get(viewType);

        presenterVh = presenter.onCreateViewHolder(parent);

        view = presenterVh.view;

        ViewHolder viewHolder = new ViewHolder(presenter, view, presenterVh);

        onCreate(viewHolder);

        View presenterView = viewHolder.mHolder.view;

        if (presenterView != null) {

            viewHolder.mFocusChangeListener.mChainedListener = presenterView.getOnFocusChangeListener();

            presenterView.setOnFocusChangeListener(viewHolder.mFocusChangeListener);

        }
//        Log.d2("Final_RC","onCreateViewHolder["+viewHolder.getItemId()+"]");

        return viewHolder;
    }

    protected void onCreate(ViewHolder viewHolder) {
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.mItem = mAdapter.get(position);
        Log.d2("Final_RC", "onBindViewHolder[" + holder.getLayoutPosition() + "]" + (viewHolder.mItem instanceof ReplyItem ? "is reply" : "is chat"));
        setAnimation(viewHolder.mHolder.view.getContext(), viewHolder.mHolder.view, position);
        viewHolder.mPresenter.onBindViewHolder(viewHolder.mHolder, viewHolder.mItem);

        onBind(viewHolder, position);

    }

    /**
     * Here is the key method to apply the animation
     */
    public void setAnimation(Context context, View viewToAnimate, int position) {
        try {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Log.d2("Final_RC", "onViewRecycled[" + holder.getLayoutPosition() + "]");
        Log.d2("Final_RC", "onViewRecycled[old:" + (viewHolder.mItem instanceof ReplyItem ? "reply" : "chat") + "]");
        Log.d2("Final_RC", "onViewRecycled[new:" + (mAdapter.get(holder.getLayoutPosition()) instanceof ReplyItem ? "reply" : "chat") + "]");
        viewHolder.mPresenter.onUnbindViewHolder(viewHolder.mHolder);
        viewHolder.mItem = null;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Log.d2("Final_RC", "onViewAttachedToWindow[" + holder.getLayoutPosition() + "]");
        viewHolder.mPresenter.onViewAttachedToWindow(viewHolder.mHolder);
        onAttachedToWindow(viewHolder);
        try {
            viewHolder.itemView.clearAnimation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Log.d2("Final_RC", "onViewDetachedFromWindow[" + holder.getLayoutPosition() + "]");
        viewHolder.mPresenter.onViewAttachedToWindow(viewHolder.mHolder);
        onViewDetached(viewHolder);
    }

    @Override
    public int getItemCount() {

        int count = mAdapter != null ? mAdapter.size() : 0;

//        Log.DebugAdapter("Item Count == " + count);

        return count;
    }

    @Override
    public long getItemId(int position) {

        return mAdapter.getId(position);

    }

    public void setAdapter(ObjectAdapter objectAdapter) {

        if (objectAdapter == mAdapter) {

            return;
        }

        if (mAdapter != null) {

            mAdapter.unregisterObserver(mDataObserver);
        }

        mAdapter = objectAdapter;

        if (mAdapter == null) {

            notifyDataSetChanged();

            return;
        }

        mAdapter.registerObserver(mDataObserver);

        notifyDataSetChanged();


    }

    @Override
    public int getItemViewType(int position) {
        Object item = mAdapter.get(position);
        // return a negative value for viewType(-100) to avoid future change in viewHolder in the same position.
        if (item instanceof ReplyItem) return -100;
        // return the item position as viewType to avoid reusing the same viewHolder and causing flickering.
        return position;
    }

    /**
     * ViewHolder for the ItemBridgeAdapter.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public final Presenter mPresenter;

        final OnFocusChangeListener mFocusChangeListener = new OnFocusChangeListener();

        public final Presenter.ViewHolder mHolder;

        public Object mItem;

        ViewHolder(Presenter presenter, View view, Presenter.ViewHolder holder) {
            super(view);

            mPresenter = presenter;

            mHolder = holder;

        }

    }

    final class OnFocusChangeListener implements View.OnFocusChangeListener {
        View.OnFocusChangeListener mChainedListener;

        @Override
        public void onFocusChange(View view, boolean hasFocus) {

            if (mChainedListener != null) {
                mChainedListener.onFocusChange(view, hasFocus);
            }
        }
    }

    /**
     * Called when ViewHolder has been bound to data.
     */
    protected void onBind(ViewHolder viewHolder, int position) {
    }

    /**
     * Called when ViewHolder has been attached to window.
     */
    protected void onAttachedToWindow(ViewHolder viewHolder) {
    }

    protected void onViewDetached(ViewHolder viewHolder) {
    }

    public int getItemsSize() {

        int count = mAdapter != null ? mAdapter.size() : 0;

//        Log.d("state size "+count);
        return count;
    }

    public void notifyChanged() {

        this.notifyItemChanged(getItemsSize());

    }

    public void notifyAllChanged() {

        this.notifyItemRangeChanged(0, getItemsSize());

    }

}