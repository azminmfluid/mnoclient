package com.ecrio.iota.ui.chatlist;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.base.ChildFragment;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.utility.Log;

/**
 * Created by Mfluid on 8/2/2016.
 */
public class ChatListBaseFragment extends ChildFragment {

    private View root;
    private boolean isVisibleToUser;
    private Handler mHandler;
    private FloatingActionButton mAddNew;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_chat_base, container, false);

        mAddNew = (FloatingActionButton) root.findViewById(R.id.fab);

        mAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) getActivity()).loadPagerMessageComposePage();

            }
        });

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

        ChatListFragment fragment = ChatListFragment.newInstance();
        transaction.replace(R.id.chatListContainer, fragment);
        transaction.addToBackStack("ChatListFragment");
        transaction.commit();
        return root;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        this.isVisibleToUser = isVisibleToUser;
        super.setUserVisibleHint(isVisibleToUser);
        Log.Debug("Visibility", "ChatListBaseFragment ---> isVisibleToUser: " + isVisibleToUser);
        if (isVisibleToUser) {

            mHandler = new Handler(Looper.getMainLooper());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    parentFragment.hideLoading();
                }
            }, 1500);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupChatView(MenuItem chatItem) {

        ActionBar supportActionBar = ((MainActivity) getActivity()).getSupportActionBar();

        final View view = supportActionBar.getCustomView();
        TextView nameView = (TextView) view.findViewById(R.id.id_top_Name);
        nameView.setMaxWidth(Integer.MAX_VALUE);
        nameView.setText("All Chats");
        TextView phoneView = (TextView) view.findViewById(R.id.id_top_Phone);
        phoneView.setVisibility(View.GONE);

//        final CartActionMenu view = (CartActionMenu) MenuItemCompat.getActionView(chatItem);
//        TextView nameView = (TextView) view.findViewById(R.id.id_top_Name);
//        nameView.setText("All Chats");
//        TextView phoneView = (TextView) view.findViewById(R.id.id_top_Phone);
//        phoneView.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {

            default:
                Log.Debug("Visibility", "ChatListBaseFragment ---> calling super");
                return super.onOptionsItemSelected(item);
        }
    }

    public static ChatListBaseFragment newInstance() {

        Bundle args = new Bundle();

        ChatListBaseFragment fragment = new ChatListBaseFragment();
        fragment.setArguments(args);
        return fragment;
    }
}