package com.ecrio.iota.ui.base;

public interface BasePresenter {
    /**
     * to be called when view is ready.
     */
    void onViewReady();
}
