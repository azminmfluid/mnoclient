package com.ecrio.iota.ui.home;

public class MainPresenter implements MainContract.Presenter {

    private final MainContract.View mView;

    public MainPresenter(MainContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void openChat() {
        mView.showChatFragment("");
    }

    @Override
    public void openAllContacts() {
        mView.showAllContactsFragment();
    }

    @Override
    public void openAllChats() {
        mView.showAllChatsFragment();
    }

    @Override
    public boolean onBackPressed() {
        return mView.onBackPressed();
    }

    @Override
    public void onViewReady() {

        mView.showDefaultView();
    }

}