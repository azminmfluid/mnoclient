package com.ecrio.iota.ui.chat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ecrio.iota.BuildConfig;
import com.ecrio.iota.Injection;
import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.ChatProvider;
import com.ecrio.iota.db.tables.TableCompositionContract;
import com.ecrio.iota.db.tables.TableConversationGroupContract;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.db.tables.TableUsersContract;
import com.ecrio.iota.enums.ChatStatus;
import com.ecrio.iota.model.ContactDetail;
import com.ecrio.iota.ui.base.BaseMvpChatFragment;
import com.ecrio.iota.ui.base.OnItemViewSelectedListener;
import com.ecrio.iota.ui.chat.base.MEditText;
import com.ecrio.iota.ui.chat.data.ChatPresenterSelector;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.chat.model.ChatItemCursorMapper;
import com.ecrio.iota.ui.chat.model.ReplyItemCursorMapper;
import com.ecrio.iota.ui.chatlist.model.ChatListItem;
import com.ecrio.iota.ui.file.FilePic;
import com.ecrio.iota.ui.list_base.ChatCursorObjectAdapter;
import com.ecrio.iota.ui.list_base.ObjectAdapter;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.ui.main.MainActivity;
import com.ecrio.iota.ui.main.MainContract;
import com.ecrio.iota.ui.settings.InfoDialog;
import com.ecrio.iota.utility.GenricFunction;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.RequestCode;
import com.google.common.base.Strings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChatFragment extends BaseMvpChatFragment<LinearLayout, ChatContract.View, ChatContract.Presenter> implements ChatContract.View, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int SESSION_UNAVAILABLE = 0;
    private static final int SESSION_AVAILABLE = 1;
    private static final int SESSION_FAILED = 2;
    private View mRoot;

    @BindView(R.id.EDTmessage)
    public MEditText mMessageET;

    @BindView(R.id.BTNsend)
    public Button mSendMsgBT;

    @BindView(R.id.BTNattachment)
    public Button mSendFileBT;

    @BindView(R.id.BTNsmily)
    public Button mSmiley;

    @BindView(R.id.id_tv_is_composing)
    public TextView mIsComposing;

    private int mSelectedPosition = -1;
    int wordCount;

    private static final int ALL_CHATS_LOADER = 1;

    private static final int USER_LOADER = 2;

    private static final int CHAT_GROUP_LOADER = 3;

    private static final int CHAT_COMPOSITION_LOADER = 4;

    private static final int ALL_QUEUED_CHATS_LOADER = 5;

    private static final int CHAT_SUGGESTION_LOADER = 6;

    private final ChatCursorObjectAdapter mChatCursorAdapter = new ChatCursorObjectAdapter(new ChatPresenterSelector());

    private int conversationUserID;

    private int conversationID;
    private String mNumber;
    private Button mSendFiled;
    private TextView txtNumber;
    private TextView txtName;
    private boolean firstTime;
    private ChatStatus chatStatus;
    private int composingStatus;
    private int replyStatus;
    private boolean scrollToPosition = true;
    private boolean replyAdded = false;
    //    private boolean requestingNewSession = false;
    private FilePic filePic;
    private boolean hasQueuedChats;
    private HashMap<String, Integer> queuedMsgHashMap;
    private ChatItemCursorMapper chatItemMapper;
    private String replyData;
    private ChatItem chatItem;

    public static ChatFragment newInstance() {

        Bundle args = new Bundle();

        ChatFragment fragment = new ChatFragment();

        fragment.setArguments(args);

        return fragment;

    }

    @NonNull
    @Override
    public ChatContract.Presenter createPresenter() {
        return new ChatPresenter(this, Injection.provideChatRepository(AppBaseApplication.context()));
//        return mPresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        VideoProvider.testBulkInsert();
        firstTime = true;
        if (getArguments().containsKey("chat")) {

            ChatListItem chat = (ChatListItem) getArguments().getParcelable("chat");

            if (chat != null) {

                Log.Debug("ChatFragment", "=========ChatScreen=========" + chat.conversationID);

                Log.Debug("ChatFragment", "chat.conversationID == " + chat.conversationID);

                Log.Debug("ChatFragment", "chat.id == " + chat.id);

                Log.Debug("ChatFragment", "chat.mSG == " + chat.mSG);

                mNumber = AppBaseApplication.mDb.getConversationPhoneNumber(chat.id);

                Log.Debug("ChatFragment", "mNumber:" + mNumber);

                conversationUserID = AppBaseApplication.mDb.getConversationUserID(mNumber);

                Log.Debug("ChatFragment", "conversationUserID == " + conversationUserID);

//                conversationID = YtsApplication.mDb.getConversationGroupSessionID(conversationUserID);

//                Log.Debug("ChatFragment", "conversationID == " + conversationID);

                Log.Debug("ChatFragment", "=========ChatScreen=========" + chat.conversationID);

                AppBaseApplication.mDb.updatedAllMessageSelectedWithMsgIDToMsgTable(conversationUserID);
            }
        } else if (getArguments().containsKey("number")) {

            Log.Debug("ChatFragment", "=========ChatScreen=========");


            mNumber = getArguments().getString("number");

            mNumber = mNumber.replace("+", "");

            Log.Debug("ChatFragment", "mNumber:" + mNumber);

            conversationUserID = AppBaseApplication.mDb.getConversationUserID(mNumber);

            Log.Debug("ChatFragment", "conversationUserID:" + conversationUserID);


//            conversationID = YtsApplication.mDb.getConversationIDNew(conversationUserID);

//            Log.Debug("ChatFragment", "conversationID:" + conversationID);
        } else if (getArguments().containsKey("number2")) {
            Log.Debug("ChatFragment", "=========ChatScreen=========");

            mNumber = getArguments().getString("number2");
            mNumber = mNumber.replace("+", "");
            Log.Debug("ChatFragment", "mNumber:" + mNumber);

            conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(mNumber);

            Log.Debug("ChatFragment", "conversationUserID:" + conversationUserID);
//            conversationID = YtsApplication.mDb.getConversationIDNew(conversationUserID);
//            conversationID = YtsApplication.mDb.insertOrUpdateNewConversationTempID(conversationUserID, conversationID);
//            Log.Debug("ChatFragment", "conversationID:" + conversationID);

            Log.Debug("ChatFragment", "*=========ChatScreen=========*");
            AppBaseApplication.mDb.updatedAllMessageSelectedWithMsgIDToMsgTable(conversationUserID);
//            ChatProvider.testNewUserMessageInsert(mNumber);

        }
    }

    @Override
    public void refreshChat(String phoneNumber) {
        if (Strings.isNullOrEmpty(mNumber) || !mNumber.contentEquals(phoneNumber)) {
            mNumber = phoneNumber.replace("+", "").replace("[^0-9]", "");
            Log.Debug("ChatFragment", "mNumber:" + mNumber);
            conversationUserID = AppBaseApplication.mDb.getUpdatedConversationUserID(mNumber);
            Log.Debug("ChatFragment", "conversationUserID:" + conversationUserID);
            AppBaseApplication.mDb.updatedAllMessageSelectedWithMsgIDToMsgTable(conversationUserID);
            getLoaderManager().destroyLoader(CHAT_GROUP_LOADER);
            getLoaderManager().destroyLoader(ALL_CHATS_LOADER);
            getLoaderManager().destroyLoader(USER_LOADER);
            isSessionInitiated = false;
            loadData(true);
        }
    }

    boolean isSessionInitiated = false;

    @Override
    public void initiateSession() {
        if (!isSessionInitiated) {
            // perform start session.
            runStartSessionInBackground();
            isSessionInitiated = true;
        }
        if (chatStatus == ChatStatus.SESSION_FAILED) {
            showError("Message saved as offline.", true);
        }
    }

    InfoDialog infoDialog;

    @Override
    public void showInfoDetail() {
        if (infoDialog == null)
            infoDialog = new InfoDialog();
        if (infoDialog.getDialog() != null && infoDialog.getDialog().isShowing())
            return;
        infoDialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onResume() {
        super.onResume();
        if ((getActivity()) != null) {
            ((MainActivity) getActivity()).showChatToolBarMenu();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRoot = inflater.inflate(R.layout.activity_chatdetail, container, false);

        queuedMsgHashMap = new HashMap<>();

        ButterKnife.bind(this, mRoot);

        String userName = UserCache.getUserName(conversationUserID);
        updateTitle(userName);
//        ContactDetail contactDetail = GenricFunction.getContactInfo(getActivity(), mNumber);
//
//        if (null != contactDetail.getContactName()) {
//
//            updateTitle(contactDetail.getContactName());
//
//        } else {
//
//            updateTitle(mNumber);
//
//        }
        mMessageET.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                if (s.toString().length() == wordCount) {

//                    Toast.makeText(getActivity(), "No more than " + wordCount + " characters allowed", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length() > 0) {

//                    mSendMsgBT.setBackgroundResource(R.drawable.text_field_active);

                    mSendMsgBT.setEnabled(true);

                } else {

//                    mSendMsgBT.setBackgroundResource(R.drawable.text_field_nill);

                    mSendMsgBT.setEnabled(false);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (mMessageET.getText().toString().trim().length() == 0) {

//                    Log.Debug("ChatFragment", "composing stop.....");
                    getPresenter().stopComposing();
                } else {

//                    Log.Debug("ChatFragment", "composing start.....");
                    getPresenter().startComposing();
                }

            }


        });

        mMessageET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int DRAWABLE_LEFT = 0;
                int DRAWABLE_TOP = 1;
                int DRAWABLE_RIGHT = 2;
                int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    int right = mMessageET.getRight();

//                    if (event.getRawX() >= (right - mMessageET.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
////
////                                FilePic filePic = new FilePic();
////
////                                filePic.setParent(ChatFragment.this);
////
////                                filePic.show(getChildFragmentManager(), "");
//
////                        mWebView.loadUrl("javascript:fileUploadCall('Hello World!')");
//
//                            }
//                        });
//                        return true;
//
//                    }

                    if (mMessageET.getText().toString().trim().length() == 0) {

//                        Log.Debug("ChatFragment", "composing stop.....");
                        getPresenter().stopComposing();
                    }

                } else if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (mMessageET.getText().toString().trim().length() > 0) {

//                        Log.Debug("ChatFragment", "composing start.....");
                        getPresenter().startComposing();

                    } else {

//                        Log.Debug("ChatFragment", "composing stop.....");
                        getPresenter().stopComposing();
                    }

                }

                return false;

            }
        });


        mSendMsgBT.setOnClickListener(this);

        mSendFileBT.setOnClickListener(this);

        mSmiley.setOnClickListener(this);

//        presenter = new ChatPresenter(this, Injection.provideChatRepository(AppBaseApplication.context()));

        mChatCursorAdapter.setMapper(new ChatItemCursorMapper());

        mChatCursorAdapter.setReplyMapper(new ReplyItemCursorMapper());

        setAdapter(mChatCursorAdapter);

        return mRoot;
    }

    private ObjectAdapter mAdapter;

    private ChatPresenter.ViewHolder mGridViewHolder;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ChatProvider.testBulkInsert();

        ViewGroup chatContainer = (ViewGroup) view.findViewById(R.id.chat_container);

        mGridViewHolder = (ChatPresenter.ViewHolder) getPresenter().onCreateViewHolder(chatContainer);

        chatContainer.addView(mGridViewHolder.view);

        updateAdapter();

//        showLoading();

    }

    @Override
    protected void onSingleClick(View view) {

        if (view == mSendMsgBT) {

            getPresenter().performSendMessage();

        } else if (view == mSendFileBT) {

            getPresenter().performSendFile();

        } else if (view == mSmiley) {

            getPresenter().performImogiView();

        }
    }

    @Override
    public void canGoBack() {
        getPresenter().canGoBack();
    }

    /**
     * Returns the object adapter.
     */
    public ObjectAdapter getAdapter() {

        return mAdapter;
    }

    /**
     * Sets the object adapter for the fragment.
     */
    public void setAdapter(ObjectAdapter adapter) {

        mAdapter = adapter;

        updateAdapter();
    }

    private void updateAdapter() {

        if (mGridViewHolder != null) {

            getPresenter().onBindViewHolder(mGridViewHolder, mAdapter);

            getPresenter().setOnItemViewSelectedListener(mViewSelectedListener);
        }
//        VideoProvider.testBulkInsert();
    }

    @Override
    public String getMessageText() {

        return mMessageET.getText().toString();

    }

    @Override
    public void showError(int message) {

        Toast.makeText(getActivity(), "" + getResources().getString(message), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void clearMessageText() {

        mMessageET.setText("");

    }

    @Override
    public int getConversationId() {

        return conversationID;

    }

    @Override
    public int getConversationUserId() {

        return conversationUserID;

    }

    @Override
    public String getConversationPhNo() {
        return mNumber;
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();

        getPresenter().onDestroy();

    }

    public volatile Queue<Runnable> runReStartIMInBackground = new LinkedList<Runnable>();

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (id == ALL_CHATS_LOADER) {
            return new CursorLoader(AppBaseApplication.context(), TableConversationsContract.ChatEntry.buildChatsWithUserIDUri(conversationUserID),
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        } else if (id == ALL_QUEUED_CHATS_LOADER) {
            return new CursorLoader(getActivity(), TableConversationsContract.ChatEntry.buildQueuedChatsWithUserIDUri(conversationUserID),
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        } else if (id == CHAT_GROUP_LOADER) {
            return new CursorLoader(AppBaseApplication.context(), TableConversationGroupContract.GroupEntry.buildUserUri(conversationUserID),
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        } else if (id == CHAT_COMPOSITION_LOADER) {
            return new CursorLoader(AppBaseApplication.context(), TableCompositionContract.CompositionEntry.buildSessionUri(conversationID),
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        } else if (id == CHAT_SUGGESTION_LOADER) {
            return new CursorLoader(AppBaseApplication.context(), TableReplyContract.CompositionEntry.buildSessionUri(conversationID),
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );
        } else {
            return new CursorLoader(AppBaseApplication.context(), TableUsersContract.UserEntry.buildUserUri(conversationUserID),
                    null, // projection
                    null, // selection
                    null, // selection clause
                    null  // sort order
            );

        }

//        return new CursorLoader(getActivity(), TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION,
//                null, // projection
//                null, // selection
//                null, // selection clause
//                null  // sort order
//        );


    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {

        if (loader.getId() == ALL_CHATS_LOADER) {
            Log.Debug("IOTA_LOG", "onLoadFinished for all chats");
            if (cursor != null && cursor.moveToFirst()) {
                Log.d("Refresh_Occurred_In_Chat");
                mChatCursorAdapter.changeCursor(cursor);
                if (scrollToPosition) {
//                    scrollToPosition = false;
                    if (mSelectedPosition != -1) {
                        Log.DebugChat("scrolling to last position " + mSelectedPosition);
                        try {
                            mGridViewHolder.mGridView.smoothScrollToPosition(mSelectedPosition);
                        } catch (Exception e) {
                            Log.Error("exception occurred in scrolling to last selected view");
                        }
                        Log.DebugChat("mSelectedPosition is -1");
                        mSelectedPosition = -1;
                    } else {
                        Log.DebugChat("scrolling to bottom message");
                        mGridViewHolder.mGridView.scrollToPosition(mChatCursorAdapter.size() - 1);
                    }
                }
                // hide progress and show chat screen.
                showContent();
            } else {
                Log.d("Refresh_Occurred_In_Chat");
                mChatCursorAdapter.changeCursor(cursor);
                // showing error message as Toast
                showError("Type a message to start", true);
                showContent();
                Log.Debug("IOTA_LOG", ";;;;;;;;;;;;no chats;;;;;;;;;;;;;;");
            }
        } else if (loader.getId() == ALL_QUEUED_CHATS_LOADER) {

            if (cursor != null && cursor.moveToFirst()) {
                Log.d("Refresh_Occurred_In_QueuedChat");
                chatItemMapper = new ChatItemCursorMapper();

                chatItem = (ChatItem) chatItemMapper.convert(cursor);

                Log.DebugQueue("Queued Message :- queued item is " + chatItem.msgText);

                hasQueuedChats = true;

                if (queuedMsgHashMap.containsValue(chatItem.msgID)) {

                    Log.DebugQueue("Queued Message :- queued item is already processed" + chatItem);

                    return;
                }

                queuedMsgHashMap.put("msgID", chatItem.msgID);

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (chatItem != null) {

                            Log.DebugQueue("Queued Message :- sending.." + chatItem.msgText);

                            saveMessageAsDelivered(chatItem);
                        } else {
                            Log.DebugQueue("Queued Message :- is null");
                        }
                    }
                }, 500);


            } else {

                Log.Debug("Queued Message :- queued chats not found");

                hasQueuedChats = false;

            }
        } else if (loader.getId() == CHAT_GROUP_LOADER) {
            Log.Debug(Log.TAG_IOTA_LOG, "onLoadFinished for group");
            if (cursor != null && cursor.moveToFirst()) {
                conversationID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID));
                Log.Debug(Log.TAG_IOTA_LOG, "session id from chat group is " + conversationID);

//                Log.Debug(Log.TAG_IOTA_LOG, "called restartLoader of chat compositions");
                getLoaderManager().restartLoader(CHAT_COMPOSITION_LOADER, null, this);
                getLoaderManager().restartLoader(CHAT_SUGGESTION_LOADER, null, this);

            }
        } else if (loader.getId() == CHAT_COMPOSITION_LOADER) {
            // Log.Debug(Log.TAG_IOTA_LOG, "onLoadFinished for chat compositions");
            if (cursor != null && cursor.moveToFirst()) {
                // setting composing status.
                composingStatus = cursor.getInt(cursor.getColumnIndex(TableCompositionContract.CompositionEntry.COLUMN_STATUS));
                // Log.Debug(Log.TAG_IOTA_LOG, "composing status from chat composition table is " + composingStatus);
                if (composingStatus == 1) {
                    mIsComposing.setVisibility(View.VISIBLE);
                    mIsComposing.setAlpha(1);
                    // add state to typing.
                    // mChatCursorAdapter.setState(ChatCursorObjectAdapter.STATE_TYPING);
                    // Log.d("state set to typing");
                    // Log.d("called notify list");
                    // mGridViewHolder.mAdapter.notifyChanged();
                } else {
                    mIsComposing.setVisibility(View.INVISIBLE);
                    mIsComposing.setAlpha(0);
                    // add state to not typing.
                    // mChatCursorAdapter.setState(ChatCursorObjectAdapter.STATE_NOT_TYPING);
                    // Log.d("state set to no typing");
                    // Log.d("called notify list");
                    // mGridViewHolder.mAdapter.notifyChanged();
                }
                // Log.d("scrolling to last position");
                // scroll chat list to bottom.
                // mGridViewHolder.mGridView.scrollToPosition(mChatCursorAdapter.size() - 1);

            } else {
                mIsComposing.setVisibility(View.INVISIBLE);
                mIsComposing.setAlpha(0);
                // set composing status to zero.
                // composingStatus = 0;
                // set state as not-typing in adapter.
                // mChatCursorAdapter.setState(ChatCursorObjectAdapter.STATE_NOT_TYPING);
                // notify the change in adapter.
                // mGridViewHolder.mAdapter.notifyChanged();
            }

        } else if (loader.getId() == CHAT_SUGGESTION_LOADER) {
//            Log.Debug(Log.TAG_IOTA_LOG, "onLoadFinished for chat compositions");
            if (cursor != null && cursor.moveToFirst()) {
                Log.DebugChat("Refresh_Occurred_In_Suggested_Reply");
                // setting composing status.
                replyStatus = cursor.getInt(cursor.getColumnIndex(TableReplyContract.CompositionEntry.COLUMN_STATUS));
//                Log.Debug(Log.TAG_IOTA_LOG, "composing status from chat composition table is " + composingStatus);
                if (replyStatus == 1) {
                    Log.DebugChat("set state to have suggestions");
                    // add state to have suggested replies.
                    mChatCursorAdapter.setState(ChatCursorObjectAdapter.STATE_SUGGESTIONS);
                    Log.DebugChat("reply cursor updated");
                    mChatCursorAdapter.changeReplyCursor(cursor);
//                    Log.DebugChat("called notify chat list");
//                    mGridViewHolder.mAdapter.notifyChanged();
                    Log.DebugChat("replyAdded is " + replyAdded);
                    if (!replyAdded) {
                        Log.DebugChat("sets replyAdded to true");
                        replyAdded = true;
                        // scroll chat list to bottom.
                        Log.DebugChat("scrolling to last position");
                        mGridViewHolder.mGridView.scrollToPosition(mChatCursorAdapter.size() - 1);
                    }
                } else {
                    Log.DebugChat("set state to no suggestions");
                    // add state to have suggested replies.
                    mChatCursorAdapter.setState(ChatCursorObjectAdapter.STATE_NO_SUGGESTIONS);
                    Log.DebugChat("reply cursor updated");
                    mChatCursorAdapter.changeReplyCursor(cursor);
//                    Log.DebugChat("called notify chat list");
//                    mGridViewHolder.mAdapter.notifyChanged();
                    Log.DebugChat("sets replyAdded to false");
                    replyAdded = false;
                }
            } else {
                Log.DebugChat("Refresh_Occurred_In_Suggested_Reply");
                // set composing status to zero.
                Log.DebugChat("sets replyStatus to 0");
                replyStatus = 0;
                // set state as not-typing in adapter.
                Log.DebugChat("set state to no suggestions");
                mChatCursorAdapter.setState(ChatCursorObjectAdapter.STATE_NO_SUGGESTIONS);
                Log.DebugChat("reply cursor changed to null");
                mChatCursorAdapter.changeReplyCursor(null);
                Log.DebugChat("calling notify chat list");
                // notify the change in adapter.
//                mGridViewHolder.mAdapter.notifyChanged();
                if (replyAdded) {
                    Log.DebugChat("replies added previously");
                    Log.DebugChat("sets replyAdded to false");
                    replyAdded = false;
//                    Log.d("scrolling to last position");
//                    // scroll chat list to bottom.
//                    mGridViewHolder.mGridView.scrollToPosition(mChatCursorAdapter.size() - 1);
                }
            }

        } else if (loader.getId() == USER_LOADER) {

            Log.Debug("onLoadFinished for user");

            if (cursor != null && cursor.moveToFirst()) {

                String sessionStatus = cursor.getString(cursor.getColumnIndex(TableUsersContract.UserEntry.COLUMN_STATUS));
                Log.Debug("session status from table is " + sessionStatus);

                if (!sessionStatus.isEmpty()) {

                    if (sessionStatus.contentEquals("0")) {

                        chatStatus = ChatStatus.SESSION_UNAVAILABLE;
                        Log.DebugQueue("status set to unavailable");
//                        firstTime = false;
//                        if (!isSessionInitiated) {
//                            // show progress
//                            // showLoading();
//                            isSessionInitiated = true;
//                            // perform start session.
//                            runStartSessionInBackground();
//                        }

                    } else if (sessionStatus.contentEquals("1")) {
                        isSessionInitiated = false;
                        if (chatStatus == ChatStatus.NOT_INITIATED
                                || chatStatus == ChatStatus.SESSION_FAILED
                                || chatStatus == ChatStatus.SESSION_AVAILABLE
                                || chatStatus == ChatStatus.SESSION_UNAVAILABLE) {
                            chatStatus = ChatStatus.SESSION_AVAILABLE;
                            Log.DebugQueue("status set to Established");
                            Log.DebugQueue("called init Loader of queue");
                            queuedMsgHashMap.clear();
                            getLoaderManager().initLoader(ALL_QUEUED_CHATS_LOADER, null, this);
                        } else {
                            Log.DebugQueue("status set to not initiated");
                            chatStatus = ChatStatus.NOT_INITIATED;
                        }
//                        chatStatus = ChatStatus.SESSION_UNAVAILABLE;
//                        if (!isSessionInitiated) {
//                            chatStatus = ChatStatus.SESSION_UNAVAILABLE;
//                            isSessionInitiated = true;
//                            // perform start session.
//                            runStartSessionInBackground();
//                        } else {
//                            chatStatus = ChatStatus.SESSION_AVAILABLE;
//
//                            getLoaderManager().initLoader(ALL_QUEUED_CHATS_LOADER, null, this);
//                            // remove task from queue.
//                            runReStartIMInBackground.clear();
//                        }
                        // show chat screen.
                        showContent();

//                    if (firstTime) {
//
//                        firstTime = false;
//
//                        showLoading();
//
//                        mPresenter.startIMSession(mNumber);
//
//                    } else {
//
//                        showContent();
//
//                    }

                    } else if (sessionStatus.contentEquals("2")) {
                        // session initiation called. then show toast.
                        if (isSessionInitiated) {
                            String errorMsg = "Session Terminated!";
                            showError(errorMsg, true);
                        }
                        isSessionInitiated = false;
                        // set status to failed.
                        chatStatus = ChatStatus.SESSION_FAILED;
                        Log.DebugQueue("status set to failed");
                        if (firstTime) {
                            // set true to avoid duplicate calls.
                            firstTime = false;
                        } else {
                            // show error message
//                            String errorMsg = "Session Terminated!";
//                            showError(errorMsg, true);
                        }
                    } else {

                        Log.Debug("IOTA_LOG", "Loading......");

                        showLoading();
                    }

                } else {

                    Log.Debug("IOTA_LOG", "Loading......");

                    showLoading();
                }

            }
//            Log.Debug("ESBIMALCClientService", "received onLoadFinished for user");
        }

    }

    private void refreshQueue() {

        if (queuedMsgHashMap == null) {

            queuedMsgHashMap = new HashMap<>();
        } else {
            queuedMsgHashMap.clear();
        }
        chatStatus = ChatStatus.SESSION_AVAILABLE;
        getLoaderManager().restartLoader(ALL_QUEUED_CHATS_LOADER, null, this);

    }

    private void saveMessageAsDelivered(ChatItem item) {
        if (item.isFile)
            getPresenter().attemptQueuedFileSend(item.msgID, item.fileid, item.msgText, item.thumb);
        else if (item.isRich)
            getPresenter().attemptQueuedReplySend(item.msgID, item.msgText);
        else
            getPresenter().attemptQueuedMessageSend(item.msgID, item.msgText);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

        Toast.makeText(getActivity(), "onLoaderReset", Toast.LENGTH_SHORT).show();
        if (loader.getId() == ALL_CHATS_LOADER) {
            mChatCursorAdapter.changeCursor(null);
        } else {

            Log.Debug("ESBIMALCClientService", "received onLoaderReset for user");

        }

    }

    private void runStartSessionInBackground() {
        // only perform start session if there is not added previously.
        if (runReStartIMInBackground.isEmpty()) {

            runReStartIMInBackground.add(new Runnable() {
                @Override
                public void run() {

                    //call start session, if session is not started or failed previously.
                    if (chatStatus == ChatStatus.SESSION_UNAVAILABLE || chatStatus == ChatStatus.SESSION_FAILED || chatStatus == ChatStatus.SESSION_AVAILABLE || chatStatus == ChatStatus.NOT_INITIATED) {

                        Log.DebugChat("calling startIMSession");
                        isSessionInitiated = true;
                        if (isVisible())
                            getPresenter().startIMSession(mNumber);
                    }
                }
            });

            new Handler().postDelayed(runReStartIMInBackground.remove(), 2000);
        }
    }

    @Override
    public void loadData(boolean needProgress) {

        showLoading();

//        getLoaderManager().initLoader(USER_LOADER, null, this);

        Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of user");
        getLoaderManager().initLoader(USER_LOADER, null, this);

        Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of all chats");
        getLoaderManager().initLoader(ALL_CHATS_LOADER, null, this);

        Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of group");
        getLoaderManager().initLoader(CHAT_GROUP_LOADER, null, this);

        Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of queue");
//        getLoaderManager().initLoader(ALL_QUEUED_CHATS_LOADER, null, this);

    }

    @Override
    public ChatContract.Presenter getBasePresenter() {
        return null;
    }

    public void makeCall() {

    }

    enum TaskResult {
        SUCCESS, FAILED, ERROR
    }

    @Override
    public String getFilePath() {
        return picturePath;
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void goBack(boolean b) {
        if (b) {
            ((MainActivity) getActivity()).backPressed(true);
        } else {

            AppBaseApplication.mDb.updatedAllMessageSelectedWithMsgIDToMsgTable(conversationUserID);
        }
    }

    @Override
    public void showDeleteMenu() {

        if ((getActivity()) != null) {
            ((MainActivity) getActivity()).showChatToolBarDeleteMenu();
        }
    }

    @Override
    public void hideDeleteMenu() {

        if ((getActivity()) != null) {
            ((MainActivity) getActivity()).showChatToolBarMenu();
        }
    }

    @Override
    public void performDelete() {
        getPresenter().deleteMessages();
    }

    @Override
    public void showFilePickerDialog() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (filePic != null && (filePic.getDialog() != null && filePic.getDialog().isShowing()))
                        return;
                    filePic = new FilePic();
                    filePic.setParent(ChatFragment.this);
                    filePic.show(getChildFragmentManager(), "");
                }
            });
        }
    }

    @Override
    public void handleGallery() {
        featureInDevelopment();
    }

    @Override
    public void handleCamera() {
        featureInDevelopment();
//        if (getActivity() != null) {
//            if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
//                Toast.makeText(getActivity(), getString(R.string.error_camera_not_available), Toast.LENGTH_SHORT).show();
//            }
//            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                ((MainContract.View) getActivity()).requestReadImage();
//                return;
//            }
//            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                ((MainContract.View) getActivity()).requestTakePhoto();
//                return;
//            }
//            takePicture();
//        }
    }

    @Override
    public void takePicture() {
        if (getActivity() == null)
            return;
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String path = Environment.getExternalStorageDirectory().toString();
            File file = new File(path, "temp.jpg");
            Uri uri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, RequestCode.CODE_CAMERA);
//            this.picturePath = file.getAbsolutePath();
//            getPresenter().attemptFileSend();
        } catch (Exception e) {
            e.printStackTrace();
            String path = Environment.getExternalStorageDirectory().toString();
            File file = new File(path, "temp.jpg");
            Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, RequestCode.CODE_CAMERA);
        }
    }

    @Override
    public boolean isConnected() {
        if (BuildConfig.FLAVOR.equals("mock")) {
            return (chatStatus == ChatStatus.SESSION_AVAILABLE);
        }
        return (chatStatus == ChatStatus.SESSION_AVAILABLE);
    }

    @Override
    public boolean hasQueuedMessage() {
        return hasQueuedChats;
    }

    @Override
    public void showEmogiView() {
        featureInDevelopment();
    }

    @Override
    public void fetchQueuedMessage() {
//        refreshQueue();
    }

    @Override
    public void makeCall(String number) {
        if (getActivity() != null) {
            Intent makePhoneCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
            if (makePhoneCall.resolveActivity(getActivity().getPackageManager()) == null) {
                showError(R.string.error_head_msg_not_allowed);
                return;
            }
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ((MainContract.View) getActivity()).requestCallPermission(number);
                return;
            }
            dispatchCallIntent(number);
        }

    }

    private void dispatchCallIntent(String number) {
        try {
            Intent makePhoneCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
            makePhoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(makePhoneCall);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showNumberError(int error_msg) {
        showError(error_msg);
    }

    @Override
    public void showLinkError(int error_msg) {
        showError(error_msg);
    }

    @Override
    public void openLink(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setReplyData(String data) {
        replyData = data;
        presenter.performSendReply();
    }

    @Override
    public String getReplyData() {
        return replyData;
    }

    private void featureInDevelopment() {
        Toast.makeText(getActivity(), R.string.message_under_dev, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();

        try {
            getPresenter().stopComposing();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getPresenter().stopComposing();
            getLoaderManager().destroyLoader(USER_LOADER);
            Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of all chats");
            getLoaderManager().destroyLoader(ALL_CHATS_LOADER);
            Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of group");
            getLoaderManager().destroyLoader(CHAT_GROUP_LOADER);
            Log.Debug(Log.TAG_IOTA_LOG, "called initLoader of queue");
            getLoaderManager().destroyLoader(ALL_QUEUED_CHATS_LOADER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode.CODE_GALLERY && resultCode != 0) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();
                String picturePath = getRealPathFromURI(selectedImage);
                File photo = new File(picturePath);
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath(), bitmapOptions);
                    bitmap = scaleBitmap(bitmap);
                    String path = Environment.getExternalStorageDirectory().toString() + File.separator + "VoltaireSBIM" + File.separator + "default";
                    File folderDefault = new File(path);
                    boolean folderExist = true;
                    if (!folderDefault.exists()) {
                        folderExist = folderDefault.mkdirs();
                    }
                    if (!folderExist) {
                        return;
                    }
                    File finalFile = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        OutputStream outFile = new FileOutputStream(finalFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String filePath = finalFile.getAbsolutePath();
                    handleNewPic(filePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == RequestCode.CODE_CAMERA && resultCode != 0) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                File photo = null;
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        photo = temp;
                        break;
                    }
                }
                if (photo == null)
                    return;
                try {

                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath(), bitmapOptions);

                    String realpath = photo.getAbsolutePath();
                    int rotateImage = getCameraPhotoOrientation(getActivity(), Uri.fromFile(photo), realpath);
                    System.out.println("rotate image" + rotateImage);
                    if (rotateImage != 0) {

                        Matrix mat = new Matrix();
                        mat.postRotate(rotateImage);
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);
                    }
                    bitmap = scaleBitmap(bitmap);
                    boolean delete = photo.delete();
                    String path = Environment.getExternalStorageDirectory().toString() + File.separator + "VoltaireSBIM" + File.separator + "default";
                    File folderDefault = new File(path);
                    boolean folderExist = true;
                    if (!folderDefault.exists()) {
                        folderExist = folderDefault.mkdirs();
                    }
                    if (!folderExist) {
                        return;
                    }
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String picturePath = file.getAbsolutePath();
                    handleNewPic(picturePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            android.util.Log.i("RotateImage", "Exif orientation: " + orientation);
            android.util.Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    private String picturePath = "";

    public void handleNewPic(String picturePath) {

        this.picturePath = picturePath;

        handleResult(true);

    }

    private boolean picStatus;

    private void handleResult(boolean hasimage) {

        picStatus = hasimage;

        try {
            uploadProfile(this.picturePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void uploadProfile(String filepath) throws IOException {

        featureInDevelopment();
        if (picStatus) {

            getPresenter().attemptFileSend();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public Bitmap scaleBitmap(Bitmap bitmap) {
        // Get this image width and height
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        // Define the pre- converted image width and height
        int newWidth = 240;
        int newHeight = 240;
        //Scaling rate calculation , the new size than the original size
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        //Create a picture of the operation matrix objects
        Matrix matrix = new Matrix();
        //Zoom motion picture
        matrix.postScale(scaleWidth, scaleHeight);
        // Rotary motion picture
        //matrix.postRotate(45);
        //Create a new image
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        return resizedBitmap;
    }

    private OnItemViewSelectedListener mViewSelectedListener =
            new OnItemViewSelectedListener() {
                @Override
                public void onItemSelected(Presenter.ViewHolder itemViewHolder) {
                    int position = mGridViewHolder.getGridView().getChildLayoutPosition(itemViewHolder.view);
//                    Log.d2(TAG, "grid selected position " + position);
//                    showToast("" + position);
                    ItemSelected(position);
                }
            };

    void ItemSelected(int position) {

        if (position != mSelectedPosition) {
            Log.DebugChat("mSelectedPosition is " + position);
            mSelectedPosition = position;
        }

    }

}