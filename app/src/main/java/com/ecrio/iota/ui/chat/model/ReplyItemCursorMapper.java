package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.data.cache.MessageStatusCache;
import com.ecrio.iota.data.cache.UserCache;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.ui.list_base.CursorMapper;

public final class ReplyItemCursorMapper extends CursorMapper {

    private static int idIndex;
    private static int conversionIdIndex;
    private static int userIdIndex;
    private static int statusIndex;
    private static int messageIndex;

    @Override
    protected void bindColumns(Cursor cursor) {
        idIndex = cursor.getColumnIndex(TableReplyContract.CompositionEntry.COLUMN_ID);
        conversionIdIndex = cursor.getColumnIndex(TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID);
        userIdIndex = cursor.getColumnIndex(TableReplyContract.CompositionEntry.COLUMN_USER_ID);
        statusIndex = cursor.getColumnIndex(TableReplyContract.CompositionEntry.COLUMN_STATUS);
        messageIndex = cursor.getColumnIndex(TableReplyContract.CompositionEntry.COLUMN_MESSAGE);
    }

    @Override
    protected Object bind(Cursor cursor) {
        // Get the values of the video.
        int id = cursor.getInt(idIndex);
        String msg = cursor.getString(messageIndex);
        int status = cursor.getInt(statusIndex);
        int userid = cursor.getInt(userIdIndex);
        int conversationID = cursor.getInt(conversionIdIndex);
        // Build a Video object to be processed.
        return new ReplyItem(conversationID, userid, status, msg);
    }
}