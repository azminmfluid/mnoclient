package com.ecrio.iota.ui.chat.item.rc_carousal;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;
import com.google.gson.Gson;

/**
 * Created by user on 2018.
 */

public class RcCarousalMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2("Log_View", "----------onCreateViewHolder-----------------");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        RcCarousalMsgView rcCarousalMsgView = new RcCarousalMsgView(parent.getContext());

//        updateCardBackgroundColor(incomingMsgView, false);

        return new ViewHolder(rcCarousalMsgView);
    }

    private void updateCardBackgroundColor(RcCarousalMsgView view, boolean selected) {

        Log.d2("Log_View", "selected is " + selected);
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        ChatItem chatItem = (ChatItem) item;
        Log.d2("Log_Selected", "item is " + chatItem);
        viewHolder.tag = chatItem.msgText;
        Log.d2("Rich_onBind", viewHolder.tag);
        RcCarousalMsgView rcCarousalMsgView = (RcCarousalMsgView) viewHolder.view;
        updateCardBackgroundColor(rcCarousalMsgView, (chatItem.isSelected != 0));
        rcCarousalMsgView.setMessageText(chatItem.msgText, chatItem.msgID);

        rcCarousalMsgView.setAvatarImage(chatItem.mPhone);
        rcCarousalMsgView.setUserNameText("" + chatItem.userid);
        rcCarousalMsgView.setStatus(chatItem.deliveryStatus);
//        rcCarousalMsgView.initTest();
        Log.d2(TAG, chatItem.msgText + " called onBindViewHolder");
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        String msgText = viewHolder.tag;
//        Log.d2("Rich_onUnbind", msgText);
//        RichCardEntity richCard = new Gson().fromJson(msgText, RichCardEntity.class);
//        Log.d2("Rich_onViewAttach", richCard.getMessage().getGeneralPurposeCardCarousel().getContent().get(0).getMedia().getMediaUrl());
        RcCarousalMsgView rcCarousalMsgView = (RcCarousalMsgView) viewHolder.view;
//        rcCarousalMsgView.setSelected(false);
        rcCarousalMsgView.clear();
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        RcCarousalMsgView rcCarousalMsgView = (RcCarousalMsgView) holder.view;
        String msgText = holder.tag;
//        Log.d2("Rich_onViewDettach", msgText);
//        RichCardEntity richCard = new Gson().fromJson(msgText, RichCardEntity.class);
//        Log.d2("Rich_onViewAttach", richCard.getMessage().getGeneralPurposeCardCarousel().getContent().get(0).getMedia().getMediaUrl());
//        rcCarousalMsgView.setMessageText(msgText);
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        RcCarousalMsgView rcCarousalMsgView = (RcCarousalMsgView) holder.view;
        String msgText = holder.tag;
//        Log.d2("Rich_onViewDettach", msgText);
//        RichCardEntity richCard = new Gson().fromJson(msgText, RichCardEntity.class);
//        Log.d2("Rich_onViewAttach", richCard.getMessage().getGeneralPurposeCardCarousel().getContent().get(0).getMedia().getMediaUrl());
        rcCarousalMsgView.clear();
    }
}