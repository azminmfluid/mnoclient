package com.ecrio.iota.ui.chat.model;

import android.database.Cursor;

import com.ecrio.iota.db.tables.TableConversationGroupContract;
import com.ecrio.iota.utility.Log;

public class ConversationGroup {

    private int groupID;

    private int conversationID;

    private int userID;

    public int getGroupID() {

        return groupID;
    }

    public void setGroupID(int groupID) {

        this.groupID = groupID;
    }

    public int getConversationID() {
        return conversationID;
    }

    public void setConversationID(int conversationID) {
        this.conversationID = conversationID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public static ConversationGroup parseDetails(Cursor cursor) {

        ConversationGroup conversationUser = null;
int i = 0;
        while (cursor.moveToNext()) {

            i ++;
            Log.d2("logg", "conversationGroupUserID---"+i);
            conversationUser = new ConversationGroup();

            conversationUser.groupID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID));

            conversationUser.conversationID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID));

            conversationUser.userID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_USER_ID));

        }

        return conversationUser;
    }

    public static ConversationGroup parseDetails2(Cursor cursor) {

        ConversationGroup conversationUser = null;
        int i = 0;
        if (cursor.moveToFirst()) {

            i ++;
            Log.d2("logg", "conversationGroupUserID---"+i);
            conversationUser = new ConversationGroup();

            conversationUser.groupID = cursor.getInt(0);

        }

        return conversationUser;
    }


    public static ConversationGroup parseDetails3(Cursor cursor) {

        ConversationGroup conversationUser = null;
        int i = 0;
        if (cursor.moveToFirst()) {

            i ++;
            Log.d2("logg", "conversationGroupUserID---"+i);
            conversationUser = new ConversationGroup();

            conversationUser.groupID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_GROUP_ID));

            conversationUser.conversationID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_CONVERSATION_ID));

            conversationUser.userID = cursor.getInt(cursor.getColumnIndex(TableConversationGroupContract.GroupEntry.COLUMN_USER_ID));

        }

        return conversationUser;
    }
}