package com.ecrio.iota.ui.chat.item.txt_recv;

import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.chat.model.ChatItem;
import com.ecrio.iota.ui.list_base.Presenter;
import com.ecrio.iota.utility.Log;

/**
 * Created by user on 2018.
 */

public class IncomingMsgPresenter extends Presenter {

    private String TAG = "CardPresenter";

    private int mDefaultBackgroundColor;
    private int mSelectedBackgroundColor;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        Log.d2("Log_View", "----------onCreateViewHolder-----------------");

        mDefaultBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.white);

        mSelectedBackgroundColor = ContextCompat.getColor(parent.getContext(), R.color.gray);

        IncomingMsgView incomingMsgView = new IncomingMsgView(parent.getContext());

//        updateCardBackgroundColor(incomingMsgView, false);

        return new ViewHolder(incomingMsgView);
    }

    private void updateCardBackgroundColor(IncomingMsgView view, boolean selected) {

        Log.d2("Log_View", "selected is " + selected);
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

        view.setMainBackgroundColor(color);

//        view.findViewById(R.id.main_image).setBackgroundColor(view.getResources().getColor(android.R.color.white));

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {

        ChatItem video = (ChatItem) item;

        Log.d2("Log_Selected", "selected is " + video.isSelected);
        viewHolder.tag = video.msgText;

        IncomingMsgView incomingMsgView = (IncomingMsgView) viewHolder.view;

        updateCardBackgroundColor(incomingMsgView, (video.isSelected != 0));

        incomingMsgView.setMessageText(video.msgText);

        incomingMsgView.setAvatarImage(video.mPhone);

        incomingMsgView.setUserNameText("" + video.userid);

        incomingMsgView.setStatus(video.deliveryStatus);

        incomingMsgView.setTime(video.sendTime);

        Log.d2(TAG, video.msgText + " called onBindViewHolder");
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

        Log.d2(TAG, viewHolder.tag + " called onUnbindViewHolder");

        IncomingMsgView incomingMsgView = (IncomingMsgView) viewHolder.view;

        incomingMsgView.setSelected(false);

    }

}
