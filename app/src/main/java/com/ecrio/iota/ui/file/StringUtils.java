package com.ecrio.iota.ui.file;

import java.io.File;

public class StringUtils {



    @Deprecated
    public static boolean isEmpty(String input) {
        if (input == null || "".equals(input))
            return true;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                return false;
            }
        }
        return true;
    }
    public static boolean hasNoSeperator(String input) {
        if (input.contains(File.separator))
            return false;
        return true;
    }
    public static boolean hasNoDot(String input) {
        if (input.contains("."))
            return false;
        return true;
    }
}
