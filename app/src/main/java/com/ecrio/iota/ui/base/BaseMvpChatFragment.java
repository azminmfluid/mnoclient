package com.ecrio.iota.ui.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ecrio.iota.R;
import com.ecrio.iota.ui.chat.base.BaseFragment;
import com.ecrio.iota.ui.chat.base.BaseLceoView;
import com.ecrio.iota.ui.main.MainActivity;

/**
 * Created by user on 2018.
 */

public abstract class BaseMvpChatFragment<CV extends View, V extends BaseLceoView<P>, P extends BasePresenter> extends BaseFragment<V, P> implements BaseCallback<V, P>,BaseLceoView<P> {

    protected View loadingView;
    protected CV contentView;
    protected TextView errorTextView;


    protected boolean requestDataIfViewCreated() {
        return true;
    }

    protected P presenter;

    @NonNull
    public abstract P createPresenter();

    public void updateTitle(String contactName) {

        if ((getActivity()) != null) {

            ((MainActivity) getActivity()).updateToolbarTitle(contactName);

        }
    }

    @Override
    public void setPresenter(@NonNull P presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public P getBasePresenter() {
        return presenter;
    }

    @NonNull
    @Override
    public V getMvpView() {
        return (V) this;
    }

    protected int getLayoutId() {
        return 0;
    }

    protected void onReloadViewClicked() {

        loadData(true);

    }

    /**
     * Called if the error view has been clicked. To disable clicking on the errorTextView use
     * <code>errorTextView.setClickable(false)</code>
     */
    protected void onErrorViewClicked() {

        loadData(true);

    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadingView = createLoadingView(view);

        contentView = createContentView(view);

        errorTextView = createErrorView(view);

        errorTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onErrorViewClicked();

            }
        });

        if(requestDataIfViewCreated()) {

            loadData(true);

        }
    }

    /**
     * Create the loading view. Default is {@code findViewById(R.id.loadingView)}
     *
     * @return the loading view
     */
    @NonNull
    protected View createLoadingView(View view) {

        return view.findViewById(R.id.loadingView);

    }

    /**
     * Create the content view. Default is {@code findViewById(R.id.contentView)}
     *
     * @return the content view
     */
    @NonNull
    protected CV createContentView(View view) {

        return (CV) view.findViewById(R.id.contentView);

    }

    @NonNull
    protected TextView createErrorView(View view) {

        return (TextView) view.findViewById(R.id.errorView);

    }

    @NonNull
    protected View createReloadView(View view) {

        return view.findViewById(R.id.offlineReloadView);

    }

    @NonNull
    protected View createOfflineView(View view) {

        return view.findViewById(R.id.offlineView);

    }

    @Override
    public void showContent() {

        animateContentViewIn();

    }

    @Override
    public void showOffline() {
        //not needed in chat screen
    }

    @Override
    public void showOffline(String offlineText) {
        //not needed in chat screen
    }

    @Override
    public void showError(String errorMsg, boolean isToast) {

        if (isToast) {

            showLightError(errorMsg);

        } else {

            errorTextView.setText(errorMsg);

            animateErrorViewIn();

        }
    }

    @Override
    public void showLoading() {

        animateLoadingViewIn();

    }

    /**
     * The default behaviour is to display a toast message as light error (i.e. pull-to-refresh
     * error).
     * Override this method if you want to display the light error in another way (like crouton).
     */
    protected void showLightError(String msg) {

        if (getActivity() != null) {

            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

        }

    }

    /**
     * Animates the error view in (instead of displaying content view / loading view)
     */
    protected void animateErrorViewIn() {

        BaseMvpChatAnimator.showErrorView(loadingView, contentView, errorTextView);

    }

    /**
     * Override this method if you want to provide your own animation for showing the loading view
     */
    protected void animateLoadingViewIn() {

        BaseMvpChatAnimator.showLoading(loadingView, contentView, errorTextView);

    }

    /**
     * Called to animate from loading view to content view
     */
    protected void animateContentViewIn() {

        BaseMvpChatAnimator.showContent(loadingView, contentView, errorTextView);

    }

    public abstract void canGoBack();
}