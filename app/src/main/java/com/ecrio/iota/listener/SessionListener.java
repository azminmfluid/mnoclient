package com.ecrio.iota.listener;

import android.widget.Toast;

import com.ecrio.iota.Session;
import com.ecrio.iota.SessionFile;
import com.ecrio.iota.SessionMessage;
import com.ecrio.iota.SessionRichCard;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.utility.Log;

/**
 * Created by Mfluid on 7/4/2018.
 */

public class SessionListener implements Session.Listener {

    @Override
    public void onSessionState(Session session, Session.State state) {
        Log.DebugApi("Received onSessionState...." + state);
        try {
            Toast.makeText(AppBaseApplication.context(), "" + state, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String userNumber = null;
        int userID = -1;
        String conversationID = null;
        String contributionId = null;
        switch (state) {
            case Established:
                String peer = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                Log.DebugApi("peer:-" + peer);
                userNumber = peer.replace("+", "");
                conversationID = session.getConversation().getId();
                Log.DebugApi("conversation ID:-" + conversationID);
                contributionId = session.getContributionId();
                Log.DebugApi("contribution ID:-" + contributionId);
                Log.DebugApi("Looking for user id of " + userNumber);
                userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                Log.DebugApi("User id : " + userID);
                int updatedInternalSessionID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, conversationID);
                break;
            case Ended:
                String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                Log.DebugApi("Received peer : " + peer1);
                userNumber = peer1.replace("+", "");
                conversationID = session.getConversation().getId();
                Log.DebugApi("conversation ID:-" + conversationID);
                contributionId = session.getContributionId();
                Log.DebugApi("contribution ID:-" + contributionId);
                Log.DebugApi("Looking for user id of " + userNumber);
                userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "2");
                Log.DebugApi("User id : " + userID);
                break;
            case ConnectFailed:
                String peer2 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                Log.DebugApi("Received peer : " + peer2);
                userNumber = peer2.replace("+", "");
                conversationID = session.getConversation().getId();
                Log.DebugApi("conversation ID:-" + conversationID);
                contributionId = session.getContributionId();
                Log.DebugApi("contribution ID:-" + contributionId);
                Log.DebugApi("Looking for user id of " + userNumber);
                userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "2");
                Log.DebugApi("User id : " + userID);
            case Ringing:
                String peer3 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                Log.DebugApi("Received peer : " + peer3);
                userNumber = peer3.replace("+", "");
                conversationID = session.getConversation().getId();
                Log.DebugApi("conversation ID:-" + conversationID);
                contributionId = session.getContributionId();
                Log.DebugApi("contribution ID:-" + contributionId);
                Log.DebugApi("Looking for user id of " + userNumber);
                userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "0");
                Log.DebugApi("User id : " + userID);
        }
    }

    @Override
    public void onSessionEvent(Session session, SessionMessage message) {

    }

    @Override
    public void onSessionEvent(Session session, SessionFile file) {

    }

    @Override
    public void onSessionEvent(Session session, SessionRichCard card) {

    }
}