
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsAction {

    @SerializedName("disableAnonymization")
    @Expose
    private DisableAnonymization disableAnonymization;
    @SerializedName("enableDisplayedNotifications")
    @Expose
    private EnableDisplayedNotifications enableDisplayedNotifications;

    public DisableAnonymization getDisableAnonymization() {
        return disableAnonymization;
    }

    public void setDisableAnonymization(DisableAnonymization disableAnonymization) {
        this.disableAnonymization = disableAnonymization;
    }

    public EnableDisplayedNotifications getEnableDisplayedNotifications() {
        return enableDisplayedNotifications;
    }

    public void setEnableDisplayedNotifications(EnableDisplayedNotifications enableDisplayedNotifications) {
        this.enableDisplayedNotifications = enableDisplayedNotifications;
    }

}
