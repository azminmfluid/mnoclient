package com.ecrio.iota.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("Address")
    @Expose
    private String Address;

    public User(String userName, String password, String address) {
        this.userName = userName;
        this.password = password;
        Address = address;
    }
}