
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceAction {

    @SerializedName("requestDeviceSpecifics")
    @Expose
    private RequestDeviceSpecifics requestDeviceSpecifics;

    public RequestDeviceSpecifics getRequestDeviceSpecifics() {
        return requestDeviceSpecifics;
    }

    public void setRequestDeviceSpecifics(RequestDeviceSpecifics requestDeviceSpecifics) {
        this.requestDeviceSpecifics = requestDeviceSpecifics;
    }

}
