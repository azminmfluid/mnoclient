
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("generalPurposeCard")
    @Expose
    private GeneralPurposeCard generalPurposeCard;
    @SerializedName("generalPurposeCardCarousel")
    @Expose
    private GeneralPurposeCardCarousel generalPurposeCardCarousel;

    public GeneralPurposeCard getGeneralPurposeCard() {
        return generalPurposeCard;
    }

    public void setGeneralPurposeCard(GeneralPurposeCard generalPurposeCard) {
        this.generalPurposeCard = generalPurposeCard;
    }

    public GeneralPurposeCardCarousel getGeneralPurposeCardCarousel() {
        return generalPurposeCardCarousel;
    }

    public void setGeneralPurposeCardCarousel(GeneralPurposeCardCarousel generalPurposeCardCarousel) {
        this.generalPurposeCardCarousel = generalPurposeCardCarousel;
    }

    public boolean isGeneralPurpose() {
        return generalPurposeCard != null;
    }

    public boolean isCarousal() {
        return generalPurposeCardCarousel != null;
    }
}
