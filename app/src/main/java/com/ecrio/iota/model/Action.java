package com.ecrio.iota.model;

import android.Manifest;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Mfluid on 5/24/2018.
 */

public class Action {

    @IntDef({ ACTION_CODE_READ_CONTACTS, ACTION_CODE_READ_IMAGE, ACTION_CODE_TAKE_PICTURE, ACTION_CODE_CALL_PHONE, ACTION_CODE_ALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ActionCode {
    }

    public static final int ACTION_CODE_READ_CONTACTS = 0;
    public static final int ACTION_CODE_READ_IMAGE = 11;
    public static final int ACTION_CODE_TAKE_PICTURE = 22;
    public static final int ACTION_CODE_CALL_PHONE = 33;
    public static final int ACTION_CODE_ALL = 44;

    public static final Action READ_CONTACTS = new Action(ACTION_CODE_READ_CONTACTS, Manifest.permission.READ_CONTACTS);
    public static final Action SAVE_IMAGE = new Action(ACTION_CODE_READ_IMAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    public static final Action TAKE_PHOTO = new Action(ACTION_CODE_TAKE_PICTURE, Manifest.permission.CAMERA);
    public static final Action CALL_PHONE = new Action(ACTION_CODE_CALL_PHONE, Manifest.permission.CALL_PHONE);

    private int code;
    private String permission;

    private Action(@ActionCode int value, String name) {
        this.code = value;
        this.permission = name;
    }

    @ActionCode
    public int getCode() {
        return code;
    }

    public String getPermission() {
        return permission;
    }
}