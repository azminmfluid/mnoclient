
package com.ecrio.iota.model.rich_new;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewResponse {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("display_text")
    @Expose
    private String displayText;
    @SerializedName("postback")
    @Expose
    private Postback postback;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public Postback getPostback() {
        return postback;
    }

    public void setPostback(Postback postback) {
        this.postback = postback;
    }

}
