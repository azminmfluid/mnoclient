
package com.ecrio.iota.model.rich_new;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Postback {

    @SerializedName("data")
    @Expose
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
