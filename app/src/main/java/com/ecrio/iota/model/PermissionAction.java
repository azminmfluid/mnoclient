package com.ecrio.iota.model;

/**
 * Created by Mfluid on 5/24/2018.
 */

public interface  PermissionAction {

    boolean hasSelfPermission(String permission);

    void requestPermission(String permission, int requestCode);

    void requestPermission(String[] permission, int requestCode);

    boolean shouldShowRequestPermissionRationale(String permission);
}