
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrlAction {

    @SerializedName("openUrl")
    @Expose
    private OpenUrl openUrl;

    public OpenUrl getOpenUrl() {
        return openUrl;
    }

    public void setOpenUrl(OpenUrl openUrl) {
        this.openUrl = openUrl;
    }

}
