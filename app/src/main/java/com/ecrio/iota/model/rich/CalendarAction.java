
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarAction {

    @SerializedName("createCalendarEvent")
    @Expose
    private CreateCalendarEvent createCalendarEvent;

    public CreateCalendarEvent getCreateCalendarEvent() {
        return createCalendarEvent;
    }

    public void setCreateCalendarEvent(CreateCalendarEvent createCalendarEvent) {
        this.createCalendarEvent = createCalendarEvent;
    }

    public boolean hasCreateCalendarEvent() {
        return createCalendarEvent != null;
    }
}