package com.ecrio.iota.model.impl.permission;

import android.os.Build;

/**
 * Created by Cesar on 24/09/15.
 */
public class CommonUtils {

    public static boolean isMarshmallowOrHigher() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        } else {
            return true;
        }
    }
}
