
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DialerAction {

    @SerializedName("dialEnrichedCall")
    @Expose
    private DialEnrichedCall dialEnrichedCall;
    @SerializedName("dialPhoneNumber")
    @Expose
    private DialPhoneNumber dialPhoneNumber;
    @SerializedName("dialVideoCall")
    @Expose
    private DialVideoCall dialVideoCall;

    public DialEnrichedCall getDialEnrichedCall() {
        return dialEnrichedCall;
    }

    public void setDialEnrichedCall(DialEnrichedCall dialEnrichedCall) {
        this.dialEnrichedCall = dialEnrichedCall;
    }

    public DialPhoneNumber getDialPhoneNumber() {
        return dialPhoneNumber;
    }

    public void setDialPhoneNumber(DialPhoneNumber dialPhoneNumber) {
        this.dialPhoneNumber = dialPhoneNumber;
    }

    public DialVideoCall getDialVideoCall() {
        return dialVideoCall;
    }

    public void setDialVideoCall(DialVideoCall dialVideoCall) {
        this.dialVideoCall = dialVideoCall;
    }

    public boolean hasDialPhone() {
        return dialPhoneNumber!=null;
    }

    public boolean hasDialEnrichedCall() {
        return dialEnrichedCall!=null;
    }

    public boolean hasDialVideoCall() {
        return dialVideoCall!=null;
    }
}