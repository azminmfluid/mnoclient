
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComposeAction {

    @SerializedName("composeTextMessage")
    @Expose
    private ComposeTextMessage composeTextMessage;
    @SerializedName("composeCameraRecordingMessage")
    @Expose
    private ComposeCameraRecordingMessage composeCameraRecordingMessage;

    public ComposeTextMessage getComposeTextMessage() {
        return composeTextMessage;
    }

    public void setComposeTextMessage(ComposeTextMessage composeTextMessage) {
        this.composeTextMessage = composeTextMessage;
    }

    public ComposeCameraRecordingMessage getComposeCameraRecordingMessage() {
        return composeCameraRecordingMessage;
    }

    public void setComposeCameraRecordingMessage(ComposeCameraRecordingMessage composeCameraRecordingMessage) {
        this.composeCameraRecordingMessage = composeCameraRecordingMessage;
    }

}
