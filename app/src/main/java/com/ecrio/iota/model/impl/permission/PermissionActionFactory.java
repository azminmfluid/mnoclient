package com.ecrio.iota.model.impl.permission;

import android.app.Activity;

import com.ecrio.iota.model.PermissionAction;

/**
 * Created by Cesar on 27/09/15.
 */
public class PermissionActionFactory {

    private Activity activity;

    public PermissionActionFactory(Activity activity) {
        this.activity = activity;
    }

    public PermissionAction getPermissionAction() {
        return new ActivityPermissionActionImpl(activity);
    }
}
