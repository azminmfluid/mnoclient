
package com.ecrio.iota.model.scl;

import com.ecrio.iota.model.rich.Suggestion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Content {

    @Expose
    private String description;
    @SerializedName("suggestions")
    @Expose
    private List<Suggestion> suggestions = null;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Suggestion> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<Suggestion> suggestions) {
        this.suggestions = suggestions;
    }

}
