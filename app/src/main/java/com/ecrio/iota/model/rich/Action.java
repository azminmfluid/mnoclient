
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Action {

    @SerializedName("urlAction")
    @Expose
    private UrlAction urlAction;
    @SerializedName("settingsAction")
    @Expose
    private SettingsAction settingsAction;
    @SerializedName("mapAction")
    @Expose
    private MapAction mapAction;
    @SerializedName("dialerAction")
    @Expose
    private DialerAction dialerAction;
    @SerializedName("deviceAction")
    @Expose
    private DeviceAction deviceAction;
    @SerializedName("composeAction")
    @Expose
    private ComposeAction composeAction;
    @SerializedName("calendarAction")
    @Expose
    private CalendarAction calendarAction;
    @SerializedName("displayText")
    @Expose
    private String displayText;
    @SerializedName("postback")
    @Expose
    private Postback postback;

    public UrlAction getUrlAction() {
        return urlAction;
    }

    public void setUrlAction(UrlAction urlAction) {
        this.urlAction = urlAction;
    }

    public SettingsAction getSettingsAction() {
        return settingsAction;
    }

    public void setSettingsAction(SettingsAction settingsAction) {
        this.settingsAction = settingsAction;
    }

    public MapAction getMapAction() {
        return mapAction;
    }

    public void setMapAction(MapAction mapAction) {
        this.mapAction = mapAction;
    }

    public DialerAction getDialerAction() {
        return dialerAction;
    }

    public void setDialerAction(DialerAction dialerAction) {
        this.dialerAction = dialerAction;
    }

    public DeviceAction getDeviceAction() {
        return deviceAction;
    }

    public void setDeviceAction(DeviceAction deviceAction) {
        this.deviceAction = deviceAction;
    }

    public ComposeAction getComposeAction() {
        return composeAction;
    }

    public void setComposeAction(ComposeAction composeAction) {
        this.composeAction = composeAction;
    }

    public CalendarAction getCalendarAction() {
        return calendarAction;
    }

    public void setCalendarAction(CalendarAction calendarAction) {
        this.calendarAction = calendarAction;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public Postback getPostback() {
        return postback;
    }

    public void setPostback(Postback postback) {
        this.postback = postback;
    }
    public boolean hasDialerAction() {
        return dialerAction!=null;
    }

    public boolean hasURLAction() {
        return urlAction!=null;
    }

    public boolean hasMapAction() {
        return mapAction!=null;
    }

    public boolean hasCalendarAction() {
        return calendarAction!=null;
    }
}