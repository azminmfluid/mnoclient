
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Suggestion {

    @SerializedName("reply")
    @Expose
    private Reply reply;
    @SerializedName("action")
    @Expose
    private Action action;

    public Reply getReply() {
        return reply;
    }

    public void setReply(Reply reply) {
        this.reply = reply;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public boolean isReply() {
        return reply!=null;
    }

    public boolean isAction() {
        return action!=null;
    }
}