
package com.ecrio.iota.model;

public class LoginRequest {

    private String mUsername;
    private String mPassword;
    private String mAddress;
    private String mPort;

    public String getPassword() {
        return mPassword;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getmAddress() {
        return mAddress;
    }

    public String getmPort() {
        return mPort;
    }

    public static class Builder {

        private String mPassword;
        private String mUsername;
        private String mAddress;
        private String port;

        public Builder withPassword(String Password) {
            this.mPassword = Password;
            return this;
        }

        public Builder withUsername(String Username) {
            this.mUsername = Username;
            return this;
        }

        public Builder withAddress(String mAddress) {
            this.mAddress = mAddress;
            return this;
        }

        public LoginRequest build() {
            LoginRequest LoginInfo = new LoginRequest();
            LoginInfo.mUsername = mUsername;
            LoginInfo.mPassword = mPassword;
            LoginInfo.mAddress = mAddress;
            LoginInfo.mPort = port;
            return LoginInfo;
        }

        public Builder withPort(String port) {
            this.port = port;
            return this;
        }
    }

}