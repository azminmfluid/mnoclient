package com.ecrio.iota.model;

public class ContactDetail {
	
	String contactName;
	String PhotoUri;
	
	String ContactID;
	
	
	public String getContactID() {
		return ContactID;
	}
	public void setContactID(String contactID) {
		ContactID = contactID;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getPhotoUri() {
		return PhotoUri;
	}
	public void setPhotoUri(String photoUri) {
		PhotoUri = photoUri;
	}
	public ContactDetail(String contactName, String photoUri) {
		super();
		this.contactName = contactName;
		PhotoUri = photoUri;
	}
	public ContactDetail() {
		
	}
	
	
}