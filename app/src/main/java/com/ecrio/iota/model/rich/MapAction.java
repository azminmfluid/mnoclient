
package com.ecrio.iota.model.rich;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapAction {

    @SerializedName("showLocation")
    @Expose
    private ShowLocation showLocation;
    @SerializedName("requestLocationPush")
    @Expose
    private RequestLocationPush requestLocationPush;

    public ShowLocation getShowLocation() {
        return showLocation;
    }

    public void setShowLocation(ShowLocation showLocation) {
        this.showLocation = showLocation;
    }

    public RequestLocationPush getRequestLocationPush() {
        return requestLocationPush;
    }

    public void setRequestLocationPush(RequestLocationPush requestLocationPush) {
        this.requestLocationPush = requestLocationPush;
    }

    public boolean hasShowLocation() {
        return showLocation != null;
    }
}
