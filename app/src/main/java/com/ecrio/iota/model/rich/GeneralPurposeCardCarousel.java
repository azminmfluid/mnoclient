
package com.ecrio.iota.model.rich;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeneralPurposeCardCarousel {

    @SerializedName("layout")
    @Expose
    private Layout layout;
    @SerializedName("content")
    @Expose
    private List<Content> content = null;

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

}
