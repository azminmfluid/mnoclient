package com.ecrio.iota.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by azmin on 25/11/16.
 */

public class Utilities {


    public static final String API_PUBLIC_KEY = "zoccer";
    public static final String REQ_FORMAT = "yyyy-MM-dd";//2017-12-20
    public static final String CURENT_FORMAT = "yyyy-MM-dd";//2017-12-20
    public static final String SERVER_DATE_FORMAT = "dd MMM yyyy hh:mm a";//2017-12-20 12:00 PM
    String epocTime = generateEpochTime();
    String md5Value = generateMD5Hash(API_PUBLIC_KEY + epocTime);


    /**
     * Check for intenet connectivity
     *
     * @param context
     * @return
     */
    public static boolean isInternet(Context context) {
        ConnectivityManager zConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo zNetworkInfo = zConnectivityManager.getActiveNetworkInfo();
        return zNetworkInfo != null && zNetworkInfo.isConnectedOrConnecting();

    }

    /**
     * Hide keyboard from anywhere in the app
     *
     * @param context
     */
    public static void hideKeyboard(Activity context) {
        try {
            InputMethodManager inputManager = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(context.getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate MD5 hash value for the string parameter passed
     *
     * @param key API_PUBLIC_KEY + epocTime
     * @return
     */
    public static String generateMD5Hash(String key) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(key.getBytes(), 0, key.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }

    /**
     * Generate Unix/Epoch time
     *
     * @return
     */
    public static String generateEpochTime() {
        String epochtime = null;

        try {
            epochtime = String.valueOf(System.currentTimeMillis() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return epochtime;
    }

    /**
     * Get Application Version Number
     *
     * @param context
     * @return
     */
    public static String getAppVersionName(Context context) {

        String versionName = null;

        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return versionName;
    }

    /**
     * Get Application Version Code
     *
     * @param context
     * @return
     */
    public static int getAppVersionCode(Context context) {

        int versionCode = 0;
        try {
            versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return versionCode;
    }

    /**
     * Get OS Version
     *
     * @return
     */
    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * Check whether email is valid
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Matcher matcher;
        Pattern pattern = Pattern.compile(emailPattern);

        matcher = pattern.matcher(email);

        if (matcher != null)
            return matcher.matches();
        else
            return false;
    }


    public static String getTodaysDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = new Date();
        return sdf.format(date);
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    public static String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String hourString = "";
        if (hours < 10)
            hourString = "0" + hours;
        else
            hourString = String.valueOf(hours);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hourString).append(":")
                .append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    public static String convertTo24hr(String _12HourTime) {
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm:a");

        Date _12HourDt = null;
        try {
            _12HourDt = _12HourSDF.parse(_12HourTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return _24HourSDF.format(_12HourDt);
    }


    public static Dialog showProgressBar(Context mContext, String message) {
        Dialog progress = new Dialog(mContext);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.progress_dialouge_asynck_task);
        TextView TVmessage = (TextView) progress.findViewById(R.id.TVmessage);
        progress.setCancelable(true);

        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return progress;
    }

    public static Dialog showNointernetConnection(Context mContext) {
        Dialog progress = new Dialog(mContext);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.layout_no_internet_connection);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return progress;
    }


    public static void openPlayStoreForApp(Context context) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context
                            .getResources()
                            .getString(R.string.app_market_link) + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context
                            .getResources()
                            .getString(R.string.app_google_play_store_link) + appPackageName)));
        }
    }

    public static String getFormatedDate(String parse_date, String reqFormat, String curentFormat) {
        String formattedDate = "";
        try {
            DateFormat originalFormat = new SimpleDateFormat(curentFormat, Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat(reqFormat);
            Date date = originalFormat.parse(parse_date);
            formattedDate = targetFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.v("date", "getFormatedDate : " + formattedDate + "parse_date : " + parse_date + "reqFormat : " + curentFormat + "reqFormat : " + curentFormat);
        return formattedDate;
    }
    public static int dpToPx2(Context context, int dp) {
        return Math.round(dp * getPixelScaleFactor(context));
    }
    public static int dpToPx(Context context,int dp) {
        float density = context.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    private static float getPixelScaleFactor(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT;
    }
    /**
     * Add specific parsing to gson
     *
     * @return new instance of {@link Gson}
     */

    public static JSONObject readJson(String fileName) throws JSONException {

        String json = "{}";
        try {
            InputStream inputStream = AppBaseApplication.context().getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject(json);
    }
    public static String readString(String fileName) {

        String json = "{}";
        try {
            InputStream inputStream = AppBaseApplication.context().getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }
}