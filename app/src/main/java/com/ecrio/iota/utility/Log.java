package com.ecrio.iota.utility;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.ecrio.iota.base.AppBaseApplication;

import java.util.Hashtable;

public class Log {

    public static final String TAG_IOTA_LOG = "IOTA_LOG";
    public static final String TXT_CHAT = "API     :- ";
    public static final String TXT_DB = "DB      :- ";
    public static final String TXT_CHATS = "Chats   :- ";
    public static final String TXT_RICH = "RichCard:- ";
    public static final String TXT_CHATS_FILE = "File    :- ";
    public static final String TXT_CHATS_QUEUE = "Queue   :- ";
    public static final String TXT_CHATS_ADAPTER = "Adapter :- ";
    public static final String TAG_CAROUSAL = "IOTA_CAROUSAL";
    public static boolean D = true;

    static {
        rtr = new ReportLogResult(AppBaseApplication.context(), "RcsClient");
    }

    public static void Debug(String Message) {

        if (D) {
            android.util.Log.d(TAG_IOTA_LOG, Message);
            writeLog(TAG_IOTA_LOG, Message);
        }

    }

    public static void DebugApi(String Message) {

        if (D) {
            String msg = TXT_CHAT + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugMessage(String veryLongString) {
        veryLongString = TXT_CHAT + veryLongString;
        if (D) {
            int maxLogSize = 1000;
            for (int i = 0; i <= veryLongString.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > veryLongString.length() ? veryLongString.length() : end;
                android.util.Log.d(TAG_IOTA_LOG, veryLongString.substring(start, end));
            }
            writeLog(TAG_IOTA_LOG, veryLongString);
        }
    }

    public static void DebugDB(String Message) {

        if (D) {
            String msg = TXT_DB + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugChat(String Message) {

        if (D) {
            String msg = TXT_CHATS + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugRich(String Message) {

        if (D) {
            String msg = TXT_CHAT + TXT_RICH + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugChatE(String Message) {

        if (D) {
            String msg = TXT_CHATS + Message;
            android.util.Log.e(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugFile(String Message) {

        if (D) {
            String msg = TXT_CHATS_FILE + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugQueue(String Message) {

        if (D) {
            String msg = TXT_CHATS_QUEUE + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void DebugAdapter(String Message) {

        if (D) {
            String msg = TXT_CHATS_ADAPTER + Message;
            android.util.Log.d(TAG_IOTA_LOG, msg);
//            writeLog(TAG_IOTA_LOG, msg);
        }

    }

    public static void Debug(String tag, String Message) {

        if (D) {
            android.util.Log.d(tag, Message);
            writeLog(tag, Message);
        }

    }

    public static void d2(String tag, String Message) {

        if (D) {
            android.util.Log.d(tag, Message);
            writeLog(tag, Message);
        }

    }

    public static void d(String Message) {

        if (D) {
            android.util.Log.d(TAG_IOTA_LOG, Message);
            writeLog(TAG_IOTA_LOG, Message);
        }

    }

    public static void Debug(String tag, String message1, String message2) {

        if (D) {
            android.util.Log.d(tag, String.format(message1, message2));
            writeLog(tag, String.format(message1, message2));
        }

    }

    public static void Error(String Message) {

        android.util.Log.e(TAG_IOTA_LOG, Message);
        writeLog(TAG_IOTA_LOG, Message);
    }
    private static ReportLogResult rtr;
    private static void writeLog(String funcName, String notifyData) {
        try {
            String result = "{";
                result += notifyData + "\t";
            result += "}";

            Hashtable<String, String> testResults = new Hashtable();
            testResults.put(funcName, result);
//        this.rtr.addTestResult(funcName, true, result);
            if (ContextCompat.checkSelfPermission(AppBaseApplication.context(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                rtr.reportResults(testResults);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}