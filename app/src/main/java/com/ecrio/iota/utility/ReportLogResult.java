package com.ecrio.iota.utility;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

public class ReportLogResult {
    private Context context;
    private String testName;
    private Hashtable<String, String> testResults = new Hashtable();

    public ReportLogResult(Context context, String testName) {
        this.context = context;
        this.testName = testName;
    }

    public void addTestResult(String funcName, boolean pass, String hint) {
        String result = pass ? "PASS" : "FAIL";
        String error = "";
        if (hint != null) {
            if (pass) {
                error = " Details: " + hint;
            } else {
                error = " Error: " + hint;
            }
        }
        this.testResults.put(funcName, result + error);
    }

    public void reportResults() {
        try {
//            File f = new File(this.context.getExternalFilesDir(null), "testresult.txt");
            File f = new File(this.context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "testresult.txt");
            Log.v("ReportTestResults", "results: " + f.getAbsolutePath());
            if (!f.exists()) {
                f.createNewFile();
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
            bw.write(this.testName);
            bw.newLine();
            bw.write("Execution date: " + DateFormat.getInstance().format(Calendar.getInstance().getTime(), new StringBuffer(), new FieldPosition(1)).toString());
            bw.newLine();
            Enumeration<String> keys = this.testResults.keys();
            while (keys.hasMoreElements()) {
                String testCase = (String) keys.nextElement();
                bw.write("   " + testCase + ": " + ((String) this.testResults.get(testCase)));
                bw.newLine();
            }
            bw.write("==================");
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reportResults(Hashtable<String, String> testResults) {
        try {
//            File f = new File(this.context.getExternalFilesDir(null), "testresult.txt");
            File f = new File(this.context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "logresult.txt");
//            Log.v("ReportTestResults", "results: " + f.getAbsolutePath());
            if (!f.exists()) {
                f.createNewFile();
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
            bw.write(this.testName);
            bw.newLine();
            bw.write("Execution time: " + DateFormat.getInstance().format(Calendar.getInstance().getTime(), new StringBuffer(), new FieldPosition(1)).toString());
            bw.newLine();
            Enumeration<String> keys = testResults.keys();
            while (keys.hasMoreElements()) {
                String testCase = (String) keys.nextElement();
                bw.write("   " + testCase + ": " + ((String) testResults.get(testCase)));
                bw.newLine();
            }
            bw.write("==================");
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}