package com.ecrio.iota.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

public final class Imageutils {

    private Imageutils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        Bitmap resizedBitmap = bm;

        int width = bm.getWidth();

        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;

        float scaleHeight = ((float) newHeight) / height;

        // CREATE A MATRIX FOR THE MANIPULATION

        Matrix matrix = new Matrix();

        // RESIZE THE BIT MAP

        matrix.postScale(scaleWidth, scaleHeight);

        // RECREATE THE NEW BITMAP
        try {
            resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        } catch (Throwable t) {
            android.util.Log.d("out of memory exception", t + "");
        }

        return resizedBitmap;
    }

    public static Bitmap getMiddlePictureInTimeLineGif(String filePath, int reqWidth, int reqHeight) {

        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            options.inJustDecodeBounds = false;
            options.inPurgeable = true;
            options.inInputShareable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            if (bitmap == null) {
                return null;
            }

            int height = options.outHeight;
            int width = options.outWidth;

            int cutHeight = 0;
            int cutWidth = 0;

            if (height >= reqHeight && width >= reqWidth) {
                cutHeight = reqHeight;
                cutWidth = reqWidth;

            } else if (height < reqHeight && width >= reqWidth) {

                cutHeight = height;
                cutWidth = (reqWidth * cutHeight) / reqHeight;

            } else if (height >= reqHeight && width < reqWidth) {

                cutWidth = width;
                cutHeight = (reqHeight * cutWidth) / reqWidth;

            } else if (height < reqHeight && width < reqWidth) {

                float betweenWidth = ((float) reqWidth - (float) width) / (float) width;
                float betweenHeight = ((float) reqHeight - (float) height) / (float) height;

                if (betweenWidth > betweenHeight) {
                    cutWidth = width;
                    cutHeight = (reqHeight * cutWidth) / reqWidth;

                } else {
                    cutHeight = height;
                    cutWidth = (reqWidth * cutHeight) / reqHeight;

                }

            }

            if (cutWidth <= 0 || cutHeight <= 0) {
                return null;
            }

            Bitmap region = Bitmap.createBitmap(bitmap, 0, 0, cutWidth, cutHeight);

            if (region != bitmap) {
                bitmap.recycle();
                bitmap = region;
            }

            if (bitmap.getHeight() < reqHeight && bitmap.getWidth() < reqWidth) {
                Bitmap scale = Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, true);
                if (scale != bitmap) {
                    bitmap.recycle();
                    bitmap = scale;
                }
            }

            return bitmap;

        } catch (OutOfMemoryError ignored) {
            return null;
        }
    }
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (height > reqHeight && reqHeight != 0) {
                inSampleSize = (int) Math.floor((double) height / (double) reqHeight);
            }

            int tmp = 0;

            if (width > reqWidth && reqWidth != 0) {
                tmp = (int) Math.floor((double) width / (double) reqWidth);
            }

            inSampleSize = Math.max(inSampleSize, tmp);

        }
        int roundedSize;
        if (inSampleSize <= 8) {
            roundedSize = 1;
            while (roundedSize < inSampleSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (inSampleSize + 7) / 8 * 8;
        }

        return roundedSize;
    }
}