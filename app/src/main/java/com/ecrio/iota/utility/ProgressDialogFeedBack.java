package com.ecrio.iota.utility;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Mfluid on 5/29/2018.
 */

public class ProgressDialogFeedBack {
    private static Context context;
    private ProgressDialog dialog = null;
    private static ProgressDialogFeedBack instance;

    public static ProgressDialogFeedBack getInstance(Context context) {
        ProgressDialogFeedBack.context = context;

        if(instance == null){
            instance = new ProgressDialogFeedBack();
        }

        return instance;
    }

    public void start(String message) {
        if(dialog != null)dialog.cancel();
        dialog = ProgressDialog.show(context, "", message, true, false);
    }

    public void update(String progress) {
        if(dialog != null){
            dialog.setMessage(progress);
        }
    }

    public void cancel() {
        if(dialog != null){
            dialog.dismiss();
        }
    }

    public void dismiss() {
    }

}