package com.ecrio.iota.utility;

import android.graphics.drawable.Drawable;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.cache.AvatarHelper;

/**
 * Created by user on 17-Jul-16.
 */
public interface ImageCache {

    public static Drawable mOutAvatar = AvatarHelper.VectorToDrawable(AppBaseApplication.getContext(), R.drawable.ic_chat_user_in);

    public static Drawable mInAvatar = AvatarHelper.VectorToDrawable(AppBaseApplication.getContext(), R.drawable.ic_chat_user);

}