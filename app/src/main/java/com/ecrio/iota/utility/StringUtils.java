package com.ecrio.iota.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by s.s on 2018.
 */
public class StringUtils {

    private final static ThreadLocal<SimpleDateFormat> dateFormater2 = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("hh:mm aa");
        }
    };

    private final static ThreadLocal<SimpleDateFormat> calendarEventFormater = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        }
    };
    public static Calendar getStringToDate(String time){
        Date date = null;
        try {
            date = calendarEventFormater.get().parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String getDateToString(Double time){

        String timeToString;

        Calendar inCalendar = Calendar.getInstance();
        inCalendar.setTimeInMillis(time.longValue());

        timeToString = dateFormater2.get().format(inCalendar.getTimeInMillis());

        return timeToString;
    }

    public static String getContactToPhoneNumber(String number){
        String finalNumber;
        finalNumber = number.replace("+", "").replaceAll("[^0-9]", "");
        return finalNumber;
    }

    public static String getTelToPhoneNumber(String number){
        String finalNumber;
        finalNumber = number.replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
        return finalNumber;
    }
}