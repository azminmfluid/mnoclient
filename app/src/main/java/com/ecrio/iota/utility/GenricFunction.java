package com.ecrio.iota.utility;

import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract.PhoneLookup;
import android.text.format.DateFormat;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.tables.TableBotUsersContract;
import com.ecrio.iota.model.ContactDetail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GenricFunction {

    public static String getEmijoByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }


    public static ContactDetail getContactInfo(Context context, String phoneNumber) {
        String contactName = null;
        String photoUri = null;
        String ContactId = null;
        ContactDetail contactDetail = new ContactDetail();
        Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber.replace("+", "")));
        Cursor cs = context.getContentResolver().query(
                uri,
                new String[]{PhoneLookup._ID, PhoneLookup.DISPLAY_NAME, PhoneLookup.PHOTO_THUMBNAIL_URI}, null, null, null);
        if (cs.getCount() > 0) {
            cs.moveToFirst();
            contactName = cs.getString(cs.getColumnIndex(PhoneLookup.DISPLAY_NAME));
            photoUri = cs.getString(cs.getColumnIndex(PhoneLookup.PHOTO_THUMBNAIL_URI));
            ContactId = cs.getString(cs.getColumnIndex(PhoneLookup._ID));
        }
        contactDetail.setContactName(contactName);
        contactDetail.setPhotoUri(photoUri);
        contactDetail.setContactID(ContactId);
        cs.close();
        return contactDetail;

    }
    public static ContactDetail getBotContactInfo(Context context, String phoneNumber) {
        String contactName = null;
        String ContactId = null;
        ContactDetail contactDetail = new ContactDetail();
        Uri uri = Uri.withAppendedPath(TableBotUsersContract.UserEntry.CONTENT_URI_INDIVIDUAL, Uri.encode(phoneNumber));
        String[] projection = {TableBotUsersContract.UserEntry.COLUMN_USER_ID, TableBotUsersContract.UserEntry.COLUMN_NAME};
        Cursor cs = context.getContentResolver().query(
                uri,
                projection, null, null, null);
        if (cs!= null && cs.getCount() > 0) {
            cs.moveToFirst();
            contactName = cs.getString(cs.getColumnIndex(TableBotUsersContract.UserEntry.COLUMN_NAME));
            ContactId = cs.getString(cs.getColumnIndex(TableBotUsersContract.UserEntry.COLUMN_USER_ID));
        }
        contactDetail.setContactName(contactName);
        contactDetail.setContactID(ContactId);
        cs.close();
        return contactDetail;

    }

    public static String getDate(long milliSeconds, String dateFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    public static String getFormattedDate(Context context, long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d2, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return (String) DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return "Yesterday";
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else
            return DateFormat.format("MM/dd/yyyy, h:mm aa", smsTime).toString();
    }


    public static void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + AppBaseApplication.context().getPackageName() + "//databases//" + databaseName + "";
                String backupDBPath = "iota_db_new"+System.currentTimeMillis()+".sqlite";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
                MediaScannerConnection.scanFile(AppBaseApplication.context(), new String[] { backupDB.getPath() }, new String[] { "application/x-sqlite3" }, null);
            }

        } catch (Exception e) {

        }
    }

    //	//code to export the db to sdcard
    public static void exportDatabse2(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//com.ecrio.mccs//databases//" + databaseName + "";
                String backupDBPath = "stratus.sqlite";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    Log.d2("SQLITE_CURSOR", "success");
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }else{
                Log.d2("SQLITE_CURSOR", "Exception");
            }
        } catch (Exception e) {
            Log.d2("SQLITE_CURSOR", "Exception");
            e.printStackTrace();
        }
    }


    public static void playNotificationTone(Context context) {

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
