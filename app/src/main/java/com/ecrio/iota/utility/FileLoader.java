package com.ecrio.iota.utility;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.ecrio.iota.ui.file.FileHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 17-Jul-16.
 */
public class FileLoader {
    private static final int DOWNLOAD_UPDATE = 2;
    private static final int DOWNLOAD_COMPLETED = 3;
    private static final int NETWORK_ERROR = 4;
    private static final int TIME_OUT_ERROR = 5;
    private static final int URL_ERROR = 6;
    public static final String EXTRA_FILE_URL = "extra_image_url";

    private CallbackManager mCallbackManager = new CallbackManager();
    private GetVideosTask mTask = new GetVideosTask();
    private BlockingQueue<String> mUrlList = new ArrayBlockingQueue<String>(50);


    public void inteceptdIfDownloading(String url, boolean interceptFlag) {
        mCallbackManager.putInterceptFlag(url, interceptFlag);
    }

    public void get(String url, String contentType, VideoLoaderCallback callback) {
        Log.d("Download", "url is "+url);
        if (FileHelper.checkFile(url, contentType)) {
            Log.d("Download", "File already exist");
            // do nothing as the file already exists.
            callback.refresh(url,true);
            return;

        }

        mCallbackManager.put(url, FileHelper.getFileFormatFromUrlAndContentType(url, contentType), callback);

        startDownloadThread(url);

    }

    private void startDownloadThread(String url) {
        if (url != null) {
            addUrlToDownloadQueue(url);
        }
        // Start Thread
        Thread.State state = mTask.getState();
        if (Thread.State.NEW == state) {
            mTask.start(); // first start
        } else if (Thread.State.TERMINATED == state) {
            mTask = new GetVideosTask(); // restart
            mTask.start();
        }
    }

    private void addUrlToDownloadQueue(String url) {
        if (!mUrlList.contains(url)) {
            try {
                mUrlList.put(url);
                Log.d("Download", "URL added " + url);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    class GetVideosTask extends Thread {
        private volatile boolean mTaskTerminated = false;
        private static final int TIMEOUT = 3 * 60;
        private boolean isPermanent = true;

        @Override
        public void run() {
            try {
                while (!mTaskTerminated) {
                    String url = null;
                    if (isPermanent) {
                        url = mUrlList.take();
                    } else {
                        url = mUrlList.poll(TIMEOUT, TimeUnit.SECONDS); // waiting
                        if (null == url) {
                            break;
                        } // no more, shutdown
                    }
                    Log.d("Download", "URL is " + url);
                    boolean b = safeGet(url);
                    Log.d("Download", "Result is " + b);
                    // use handler to process callback
                }
            } catch (Exception e) {
                Log.d("Download", "Exception occured");
                e.printStackTrace();
            } finally {
                Log.d("Download", "Exception occured");
                mTaskTerminated = true;
            }

        }

        @SuppressWarnings("unused")
        public boolean isPermanent() {
            return isPermanent;
        }

        @SuppressWarnings("unused")
        public void setPermanent(boolean isPermanent) {
            this.isPermanent = isPermanent;
        }

        @SuppressWarnings("unused")
        public void shutDown() throws InterruptedException {
            mTaskTerminated = true;
        }
    }

    private String tmpFileSize;
    private int progress;
    private String tmpFilePath;
    private String fileSize;
    private String targetFilePath;
    private boolean interceptFlag;

    public boolean safeGet(String fileUrl) {
        String fileUrlWithExtension =  fileUrl;
        Log.d("download_progress", "-------------safeGet");
        try {
//
//            String tmpdownloadingFile = FileHelper.getFileNameFromUrl("http://127.0.0.1:8080/592d829f_75be9")+".tmp";
//            tmpFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + tmpdownloadingFile;
//            String downloadingFile = FileHelper.getFileNameFromUrl("http://127.0.0.1:8080/592d829f_75be9")+FileHelper.getFileFormatFromUrl("http://127.0.0.1:8080/592d829f_75be9");
//            targetFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + downloadingFile;
//            File targetFile = new File(targetFilePath);

            String tmpdownloadingFile = FileHelper.getFileNameFromUrl(fileUrl) + ".tmp";
            tmpFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + tmpdownloadingFile;
//            String downloadingFile = FileHelper.getFileNameFromUrl(fileUrl) + FileHelper.getFileFormatFromUrl(fileUrl);
            String downloadingFile = FileHelper.getFileNameFromUrl(fileUrl) + mCallbackManager.getFileExtension(fileUrl);
            targetFilePath = FileHelper.getRootDir() + "/Downloads/Sessions/" + downloadingFile;
            File targetFile = new File(targetFilePath);
            if (targetFile.exists()) {
                Log.d("Download", fileUrl + "\nfile is already downloaded");
                return false;
            }
            File tmpFile = new File(tmpFilePath);
            FileOutputStream fos = new FileOutputStream(tmpFile);
//            fileUrlWithExtension += mCallbackManager.getFileExtension(fileUrl);
            fileUrlWithExtension = FileHelper.getFileURLFromUrlAndContentType(fileUrl, mCallbackManager.getFileExtension(fileUrl));
            com.ecrio.iota.utility.Log.DebugAdapter("Downloading URL before formatting :-"+fileUrlWithExtension);
            com.ecrio.iota.utility.Log.d2("Final_RC", "Media with extension :-\n"+fileUrlWithExtension);
            String replace = fileUrlWithExtension.replace(" ", "%20");
            URL url = new URL(replace);
            com.ecrio.iota.utility.Log.DebugAdapter("Downloading URL is "+replace);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(6000);
            // optional default is GET
            conn.setRequestMethod("GET");
            conn.connect();
            int length = conn.getContentLength();
            InputStream is = conn.getInputStream();

            DecimalFormat df = new DecimalFormat("0.00");
            fileSize = df.format((float) length / 1024 / 1024) + "MB";
            byte buffer[] = new byte[1024];
            int count = 0;
            do {
                int byteCount = is.read(buffer);
                int byteOffset = 0;
                count += byteCount;
                if (byteCount > 0) {
                    tmpFileSize = df.format((float) count / 1024 / 1024) + "MB";
                    progress = (int) (((float) count / length) * 100);
//                    Log.d("download_progress", fileUrl + "\nin progress---" + tmpFileSize);
//                    Log.d("log_progress", fileUrl + "\nin progress---" + progress);
//                    Log.d("log_progress", "count---" + count);
//                    Log.d("log_progress", "length---" + length);
//                handler.sendEmptyMessage(DOWNLOAD_UPDATE);
                    final Message m1 = handler.obtainMessage(DOWNLOAD_UPDATE);
                    Bundle bundle1 = m1.getData();
                    bundle1.putString(EXTRA_FILE_URL, fileUrl);
                    handler.sendMessage(m1);
                } else if (byteCount <= 0) {
                    Log.d("log_progress", "count---" + count);
                    Log.d("log_progress", "length---" + length);
                    Log.d("log_progress", fileUrl + "\ncompleted---" + progress);
                    if (tmpFile.renameTo(targetFile)) {
                        final Message m2 = handler.obtainMessage(DOWNLOAD_COMPLETED);
                        Bundle bundle2 = m2.getData();
                        bundle2.putString(EXTRA_FILE_URL, fileUrl);
                        handler.sendMessage(m2);
                    }
                    break;
                }


                fos.write(buffer, byteOffset, byteCount);
            } while (!mCallbackManager.isInterceptFlag(fileUrl));
            fos.close();
            is.close();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            final Message m2 = handler.obtainMessage(URL_ERROR);
            Bundle bundle2 = m2.getData();
            bundle2.putString(EXTRA_FILE_URL, fileUrl);
            handler.sendMessage(m2);
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            final Message m2 = handler.obtainMessage(TIME_OUT_ERROR);
            Bundle bundle2 = m2.getData();
            bundle2.putString(EXTRA_FILE_URL, fileUrl);
            handler.sendMessage(m2);
        } catch (ConnectException e) {
            e.printStackTrace();
            final Message m2 = handler.obtainMessage(NETWORK_ERROR);
            Bundle bundle2 = m2.getData();
            bundle2.putString(EXTRA_FILE_URL, fileUrl);
            handler.sendMessage(m2);
        } catch (Exception e) {
            e.printStackTrace();
            final Message m2 = handler.obtainMessage(TIME_OUT_ERROR);
            Bundle bundle2 = m2.getData();
            bundle2.putString(EXTRA_FILE_URL, fileUrl);
            handler.sendMessage(m2);
        }
        return false;
    }

    private Handler handler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case DOWNLOAD_UPDATE:
                    Bundle bundle1 = msg.getData();
                    String url1 = bundle1.getString(EXTRA_FILE_URL);
                    mCallbackManager.call(url1, progress, progress, false);
                    break;
                case DOWNLOAD_COMPLETED:
                    Bundle bundle2 = msg.getData();
                    String url2 = bundle2.getString(EXTRA_FILE_URL);
                    Log.d("Download", url2 + "\tcompleted successfully");
                    com.ecrio.iota.utility.Log.DebugChat(url2 + "\tcompleted successfully");
                    mCallbackManager.call(url2, true);
                    break;
                case NETWORK_ERROR:
                    Bundle bundle3 = msg.getData();
                    String url3 = bundle3.getString(EXTRA_FILE_URL);
                    Log.d("Download", url3 + "\tnot completed \tnetwork error");
                    com.ecrio.iota.utility.Log.DebugChat(url3 + "\tnot completed \tnetwork error");
                    mCallbackManager.failed(url3);
                    break;
                case TIME_OUT_ERROR:
                    Bundle bundle4 = msg.getData();
                    String url4 = bundle4.getString(EXTRA_FILE_URL);
                    Log.d("Download", url4 + "\tnot completed \ttimeout error");
                    com.ecrio.iota.utility.Log.DebugChat(url4 + "\tnot completed \ttimeout error");
                    mCallbackManager.failed(url4);
                    break;
                case URL_ERROR:
                    Bundle bundle5 = msg.getData();
                    String url5= bundle5.getString(EXTRA_FILE_URL);
                    Log.d("Download", url5 + "\tnot completed \turl error");
                    com.ecrio.iota.utility.Log.DebugChat(url5 + "\tnot completed \turl error");
                    mCallbackManager.failed(url5);
                    break;
                default:
            }

        }
    };

    public interface VideoLoaderCallback {
        void refresh(String url, boolean status);

        void failed(String url);

        void refresh(String url, int progress, int percentage, boolean status);
    }

    private class CallbackManager {
        private static final String TAG = "ProgressBarMap";
        private boolean interceptFlag;
        private ConcurrentHashMap<String, List<VideoLoaderCallback>> mCallbackMap;
        private HashMap<String, Boolean> booleanHashMap;
        private HashMap<String, String> extensionHashMap;

        public CallbackManager() {
            mCallbackMap = new ConcurrentHashMap<String, List<VideoLoaderCallback>>();
            booleanHashMap = new HashMap<String, Boolean>();
            extensionHashMap = new HashMap<String, String>();
        }

        public boolean isInterceptFlag(String url) {
            if (booleanHashMap.containsKey(url)) {
                Log.d("booleanHashMap", "contains " + url);
                interceptFlag = booleanHashMap.get(url);
                Log.d("booleanHashMap", "return " + interceptFlag);
            } else {
                Log.d("booleanHashMap", "not contains " + url);
                interceptFlag = false;
            }

            return interceptFlag;
        }


        public void putInterceptFlag(String url, boolean interceptFlag) {
            booleanHashMap.put(url, interceptFlag);
        }

        public void put(String url, String fileExtension, VideoLoaderCallback callback) {
            Log.v(TAG, "url=" + url + extensionHashMap.get(url));
            if (!mCallbackMap.containsKey(url)) {
                Log.v(TAG, "url does not exist, add list to map");
                mCallbackMap.put(url, new ArrayList<VideoLoaderCallback>());
                // mCallbackMap.put(url, Collections.synchronizedList(new
                // ArrayList<VideoLoaderCallback>()));
            }
            booleanHashMap.put(url, false);
            extensionHashMap.put(url, fileExtension);
            mCallbackMap.get(url).add(callback);
            Log.v(TAG, "Add callback to list, count(url)=" + mCallbackMap.get(url).size());
        }

        public void call(String url, boolean status) {
            Log.v(TAG, "call url=" + url + extensionHashMap.get(url));
            List<VideoLoaderCallback> callbackList = mCallbackMap.get(url);
            if (callbackList == null) {
                Log.e(TAG, "callbackList=null");
                return;
            }
            mCallbackMap.remove(url);
            for (VideoLoaderCallback callback : callbackList) {
                if (callback != null) {
                    callback.refresh(url, status);
                }
            }

            callbackList.clear();
        }

        public void failed(String url) {
            Log.v(TAG, "call url=" + url + extensionHashMap.get(url));
            List<VideoLoaderCallback> callbackList = mCallbackMap.get(url);
            if (callbackList == null) {
                Log.e(TAG, "callbackList=null");
                return;
            }
            for (VideoLoaderCallback callback : callbackList) {
                if (callback != null) {
                    callback.failed(url);
                }
            }

            callbackList.clear();
            mCallbackMap.remove(url);
        }

        public void call(String url, int progress, int percentage, boolean status) {
            Log.v(TAG, "call url=" + url + extensionHashMap.get(url));
            List<VideoLoaderCallback> callbackList = mCallbackMap.get(url);
            if (callbackList == null) {
                Log.e(TAG, "callbackList=null");
                return;
            }
            for (VideoLoaderCallback callback : callbackList) {
                if (callback != null) {
                    callback.refresh(url, progress, percentage, status);
                }
            }

            if (status) {
                callbackList.clear();
                mCallbackMap.remove(url);
            }
        }

        public String getFileExtension(String url) {
            return extensionHashMap.get(url);
        }
    }
}
