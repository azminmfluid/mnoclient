package com.ecrio.iota.utility;

/**
 * Created by Mfluid on 5/24/2018.
 */

public class RequestCode {
    public static final int CODE_GALLERY = 11;
    public static final int CODE_CAMERA = 22;
    public static final int CODE_CALL = 33;
}