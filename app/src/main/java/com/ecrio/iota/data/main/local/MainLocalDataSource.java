package com.ecrio.iota.data.main.local;

import android.content.Context;
import android.support.annotation.NonNull;

import com.ecrio.iota.data.main.DataSource;

/**
 * Created by Mfluid on 5/28/2018.
 */

public class MainLocalDataSource implements DataSource {

    private static MainLocalDataSource INSTANCE;

    public static MainLocalDataSource getInstance(Context context) {
        if (INSTANCE == null) {

            INSTANCE = new MainLocalDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void getNewChat(@NonNull GetNewChatCallback callback) {

    }
}