package com.ecrio.iota.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TasksDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "iota.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String BOOLEAN_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + LoginPersistenceContract.TaskEntry.TABLE_NAME + " (" +
                    LoginPersistenceContract.TaskEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
                    LoginPersistenceContract.TaskEntry.COLUMN_NAME_USERNAME + TEXT_TYPE + COMMA_SEP +
                    LoginPersistenceContract.TaskEntry.COLUMN_NAME_PASSWORD + TEXT_TYPE + COMMA_SEP +
                    LoginPersistenceContract.TaskEntry.COLUMN_NAME_SERVER_ADDRESS + TEXT_TYPE +
                    " )";

    public TasksDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
        if (newVersion > oldVersion) {

            if (newVersion == 2) {

                handleVersion2Changes();

            } else if (newVersion == 3) {

                if (oldVersion == 1) {

                    handleVersion2Changes();
                }

                handleVersion3Changes();
            } else if (newVersion == 4) {

                if (oldVersion == 1) {

                    handleVersion2Changes();
                    handleVersion3Changes();

                }

                if (oldVersion == 2) {

                    handleVersion3Changes();
                }

                handleVersion4Changes();
            }

        }
    }

    private void handleVersion4Changes() {

    }

    private void handleVersion3Changes() {

    }

    private void handleVersion2Changes() {

    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Not required as at version 1
    }
}
