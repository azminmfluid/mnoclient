package com.ecrio.iota.data.db;

import android.provider.BaseColumns;

/**
 * The contract used for the db to save the tasks locally.
 */
public final class LoginPersistenceContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    private LoginPersistenceContract() {
    }

    /* Inner class that defines the table contents */
    public static abstract class TaskEntry implements BaseColumns {
        public static final String TABLE_NAME = "task";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_SERVER_ADDRESS = "server_addr";
    }

}
