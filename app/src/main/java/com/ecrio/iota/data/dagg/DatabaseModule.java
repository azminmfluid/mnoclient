package com.ecrio.iota.data.dagg;

//import com.ecrio.iota.Iota;

import com.ecrio.iota.db.MyDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class DatabaseModule {

    private MyDatabase mDatabase;

    public DatabaseModule(MyDatabase database) {
        this.mDatabase = database;
    }

    @Provides
    @Singleton
    public MyDatabase provideIota() {
        return mDatabase;
    }

}
