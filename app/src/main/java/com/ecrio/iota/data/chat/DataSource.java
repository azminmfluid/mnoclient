package com.ecrio.iota.data.chat;

/**
 * Main class for accessing iota API.
 * <p>
 * onSuccess() and onFailed() callbacks are used to inform the user of errors or successful operations.
 */
public interface DataSource {


    void startIMSession(String[] strings, int sessionType_oneToOne, int uriType_tel, int i, GetChatCallback callback);

    void saveMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback);

    /**
     * Send a new line of text to the server.
     *
     * @param conversationID
     * @param conversationPhNo
     * @param conversationUserID
     * @param newMessage         The text to add to the chat.
     */
    void sendMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetChatCallback callback);

    void saveReplyMessage(int conversationID, int conversationUserID, String newMessage, GetReplySaveCallback callback);

    void sendReplyMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetReplyCallback callback);

    void sendComposing(int conversationID, int type, GetChatCallback callback);

    void saveMessageReadStatus(int conversationID, int conversationUserID, String msgID, GetChatCallback callback);

    void sendMessageReadStatus(String conversationID, int status, int msgID, GetChatCallback callback);

    void endIMSession(int conversationID, int conversationUserID, String conversationPhNo, GetChatCallback callback);

    void saveNewSession(String phoneNumber, int sessionID);

    void resetSession(String phoneNumber, int sessionID);

    void saveEstablishedSession(int sessionID);

    void deleteMessage(int conversationID, int conversationUserID, DeleteCallback callback);

    void sendFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, SendFileCallback callback);

    void queueFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback);

    void queueFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback);

    void saveFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback);

    void queueMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback);

    void queueReplyMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback);

    void updateQueuedMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback);

    void updateQueuedReplyMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback);

    void updateFailedMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback callback);

    void updateQueuedFile(int conversationID, int conversationUserID, int fileID, int msgID, UpdateQueueFileCallback callback);

    void sendQueuedFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback);

    void updateFailedFile(int conversationID, int conversationUserID, int msgID, UpdateQueueFileCallback callback);

    void updateInternalSession(String phoneNumber, String sessionID);

    void updateDeliveredMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback updateMsgCallback);

    void resetReply(int conversationID, int userID);

    interface GetChatCallback {

        void onSuccess(String conversationID);

        void onFailed();

    }
    interface GetChatCallback2 {

        void onSuccess(String conversationID);

        void onFailed(String number);

    }
    interface GetReplyCallback {

        void onSuccess(String conversationID);

        void onFailed();

    }

    interface UpdateMsgCallback {

        void onSuccess();

        void onFailed();

    }

    interface GetFileCallback {

        void onSuccess(int value);

        void onFailed();

    }

    interface UpdateQueueCallback {

        void onSuccess(int value);

        void onFailed();

    }

    interface SendFileCallback {

        void onSuccess(int value);

        void onFailed();

    }

    interface DeleteCallback {

        void onSuccess();

        void onFailed();

    }

    interface SaveFileCallback {

        void onSuccess(String msgID);

        void onFailed();

    }

    interface UpdateQueueFileCallback {

        void onSuccess();

        void onFailed();

    }

    interface GetSaveCallback {

        void onSuccess(String msgID);

        void onFailed();

    }

    interface GetReplySaveCallback {

        void onSuccess(String msgID);

        void onFailed();

    }

}