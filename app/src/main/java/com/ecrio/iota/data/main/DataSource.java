package com.ecrio.iota.data.main;

import android.support.annotation.NonNull;

/**
 * Main class for accessing iota API.
 * <p>
 * onSuccess() and onFailed() callbacks are used to inform the user of errors or successful operations.
 */
public interface DataSource {

    void getNewChat(@NonNull GetNewChatCallback callback);

    interface GetNewChatCallback {

        void onNewChat();

        void onFailed(String s);

    }

}