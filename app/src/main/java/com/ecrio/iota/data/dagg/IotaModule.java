package com.ecrio.iota.data.dagg;

//import com.ecrio.iota.Iota;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.ecrio.iota.ContactInfo;
import com.ecrio.iota.ContentType;
import com.ecrio.iota.Conversation;
import com.ecrio.iota.Direction;
import com.ecrio.iota.Iota;
import com.ecrio.iota.PagerMessage;
import com.ecrio.iota.R;
import com.ecrio.iota.ResourceState;
import com.ecrio.iota.ResourceType;
import com.ecrio.iota.Response;
import com.ecrio.iota.Session;
import com.ecrio.iota.SessionFile;
import com.ecrio.iota.SessionMessage;
import com.ecrio.iota.SessionRichCard;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.enums.ConversationSender;
import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.enums.NetworkStatus;
import com.ecrio.iota.enums.RegistrationStatus;
import com.ecrio.iota.enums.ResourceStatus;
import com.ecrio.iota.enums.RichType;
import com.ecrio.iota.enums.SubscriptionStatus;
import com.ecrio.iota.model.ContactDetail;
import com.ecrio.iota.model.rich.Action;
import com.ecrio.iota.model.rich.CalendarAction;
import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.model.rich.DialPhoneNumber;
import com.ecrio.iota.model.rich.DialerAction;
import com.ecrio.iota.model.rich.GeneralPurposeCard;
import com.ecrio.iota.model.rich.GeneralPurposeCardCarousel;
import com.ecrio.iota.model.rich.MapAction;
import com.ecrio.iota.model.rich.Message;
import com.ecrio.iota.model.rich.Reply;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.model.rich.ShowLocation;
import com.ecrio.iota.model.rich.Suggestion;
import com.ecrio.iota.model.rich.UrlAction;
import com.ecrio.iota.pref.Constants;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.ui.chat.item.file_send.FileData;
import com.ecrio.iota.ui.chat.item.file_send.MFiles;
import com.ecrio.iota.ui.chat.model.CompositionMsg;
import com.ecrio.iota.ui.chat.model.ConversationMsg;
import com.ecrio.iota.ui.file.FileHelper;
import com.ecrio.iota.ui.file.FileUtils;
import com.ecrio.iota.ui.login.MyApi;
import com.ecrio.iota.utility.GenricFunction;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.Utilities;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class IotaModule {

    private NetworkStatus NetworkStatus = new NetworkStatus(ResourceStatus.STATUS_NETWORK);
    private RegistrationStatus RegistrationStatus = new RegistrationStatus(ResourceStatus.STATUS_REGISTRATION);
    private SubscriptionStatus SubscriptionStatus = new SubscriptionStatus(ResourceStatus.STATUS_SUBSCRIPTION);
    private SubscriptionStatus CapabilityStatus = new SubscriptionStatus(ResourceStatus.STATUS_CAPABILITIES);
    public volatile Queue<Runnable> runReStartIMInBackground = new LinkedList<Runnable>();

    private Iota mIota;
    private MyApi mApi;
    private final Context mContext;
    private ArrayList<Iota.ResourceListener> mListeners = new ArrayList<>();
    private ArrayList<Iota.ContactListener> mContactListeners = new ArrayList<>();
    private ArrayList<Iota.NewConversationListener> mConversationListeners = new ArrayList<>();

    private HashMap<String, Session.Listener> sessionListenerHashMap = new HashMap<>();
    private HashMap<String, Session> sessionHashMap = new HashMap<>();
    ArrayList<Integer> arrlist = new ArrayList<Integer>(Collections.nCopies(4, 0));


    public IotaModule(Iota mIota, MyApi mapi, Context context) {
        this.mIota = mIota;
        this.mApi = mapi;
        mContext = context;
        this.mIota.addResourceListener(getResourceListener(context));
        this.mIota.addContactListener(getContactListener(context));
        this.mIota.addNewConversationListener(getNewConversationListener(context));
    }

    @NonNull
    private Iota.NewConversationListener getNewConversationListener(Context context) {
        return (iota, conversation) -> {
            Log.DebugApi("Received onConversationCreated notification");
            for (int i = 0; i < mConversationListeners.size(); i++) {
                mConversationListeners.get(i).onConversationCreated(iota, conversation);
            }
            // Adding listener for incoming sessions.
            conversation.addListener(new Conversation.Listener() {
                @Override
                public void onPagerMessageReceived(Conversation conversation, PagerMessage pagerMessage) {
                    Log.Error("Received onPagerMessageReceived");
                }

                @Override
                public void onIncomingSession(Conversation conversation, Session session) {
                    String peer = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                    Log.DebugApi("Received peer : " + peer);
                    String contributionId = session.getContributionId();
                    Log.DebugApi("Received contributionId : " + contributionId);
//                    int conversationUserID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(peer, "1");
//                    Log.DebugApi("User ID : " + conversationUserID);
//                    // update/insert the chat details with the new conversation id generated with this user/phoneNo.
//                    int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(conversationUserID, contributionId);
//                    Log.DebugApi("Internal session ID : " + conversationInternalID);
//                    int conversationID = AppBaseApplication.mDb.insertOrUpdateNewConversationID(conversationUserID, conversationInternalID);
//                    Log.DebugApi("Session ID : " + conversationID);
                    Log.DebugApi("Responding session with accept");

                        Log.DebugApi("Saving new session");
                        sessionHashMap.put(peer, session);
                        // Looking for listener added previously for the same user.
                        Session.Listener listener = sessionListenerHashMap.get(peer);
                        if (listener == null)
                            sessionListenerHashMap.put(peer, new Session.Listener() {

                                @Override
                                public void onSessionState(Session session, Session.State state) {
                                    Log.DebugApi("Received onSessionState...." + state);
                                    try {
                                        Toast.makeText(context, "" + state, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (state == Session.State.Ended) {
                                        String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("Received peer : " + peer1);
                                        String userNumber = peer1.replace("+", "");
                                        sessionHashMap.remove(userNumber);
                                        Log.DebugApi("Looking for user id of " + userNumber);
                                        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "2");
                                        Log.DebugApi("User id : " + userID);
                                    } else if (state == Session.State.ConnectFailed) {
                                        String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("Received peer : " + peer1);
                                        String userNumber = peer1.replace("+", "");
                                        sessionHashMap.remove(userNumber);
                                        Log.DebugApi("Looking for user id of " + userNumber);
                                        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "2");
                                        Log.DebugApi("User id : " + userID);
                                    } else if (state == Session.State.Established) {
                                        String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("Received peer : " + peer1);
                                        String userNumber = peer1.replace("+", "");
                                        String conversationID = session.getConversation().getId();
                                        Log.DebugApi("conversation ID :-" + conversationID);
                                        int conversationUserID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(peer1, "1");
                                        int updatedInternalSessionID = AppBaseApplication.mDb.getUpdatedInternalSessionID(conversationUserID, conversationID);
                                        int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(updatedInternalSessionID);
                                        Log.DebugDB("user ID of session" + updatedInternalSessionID + " is : " + conversationGroupUserID);
                                        Log.DebugDB("updating user status to 1");
                                        // updating the conversation status with the user to 1.
                                        AppBaseApplication.mDb.updateConversationUserStatus(conversationGroupUserID, "1");
                                        Log.DebugDB("inserting session started info message");
//                                        insertInfoMessage(conversationGroupUserID, updatedInternalSessionID, "");
                                    }
                                }

                                @Override
                                public void onSessionEvent(Session session, SessionMessage sessionMessage) {
                                    Log.DebugApi("Received onSessionEvent....");
                                    Direction direction = sessionMessage.getDirection();
                                    if (direction == Direction.Incoming) {
                                        // ==================== Incoming message handling here ==========================
                                        Log.DebugApi("-----------*session receiving progress*-----------");
                                        String contributionId = session.getContributionId();
                                        Log.DebugApi("Received contributionID : " + contributionId);
                                        String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("Received peer : " + peer1);
                                        String txtMessage = new String(sessionMessage.getContent().getData(), StandardCharsets.UTF_8);
                                        Log.DebugApi("Received message : " + txtMessage);

                                        String suggested_chip_list = sessionMessage.getBotSuggestion().getJson();
                                        Log.d2("SPLITTED_JSOn", "Bot suggestion :- " + suggested_chip_list);

                                        String richReply = null;
                                        boolean hasSuggestions = false;
                                        if (!Strings.isNullOrEmpty(suggested_chip_list)) {
                                            try {
                                                JSONObject entity = new JSONObject(suggested_chip_list);
                                                hasSuggestions = true;
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        Log.DebugMessage("RichCard:- Received scl : " + suggested_chip_list);
                                        RichCardEntity entity = null;
                                        if (hasSuggestions) {
//                                        if(!richMessage.endsWith("}}}"))richMessage+="}";
                                            try {
                                                String richMessage = String.valueOf(Utilities.readString("richmessage.json"));
                                                entity = new Gson().fromJson(richMessage, RichCardEntity.class);
                                            } catch (JsonSyntaxException e) {
                                                e.printStackTrace();
                                            }
                                            if(entity == null){
                                                Toast.makeText(context, "Invalid Data!", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            Message message = entity.getMessage();
                                            if (message != null) {
                                                if (message.isGeneralPurpose()) {
                                                    Content scl_content = new Gson().fromJson(suggested_chip_list, Content.class);
                                                    List<Suggestion> scl_suggestions = scl_content.getSuggestions();
                                                    Log.DebugApi((scl_suggestions !=null && !scl_suggestions.isEmpty())?("has global suggestions "+scl_suggestions.size()):"no global suggestions");
                                                    Content card_content = message.getGeneralPurposeCard().getContent();
                                                    Log.DebugApi((card_content == null)?"no content":"has content");
                                                    // No need to combine individual suggestions to global one.
                                                    if (card_content != null) {
                                                        if (card_content.getSuggestions() == null || card_content.getSuggestions().size() == 0) {
                                                            card_content.setSuggestions((scl_suggestions==null)?new ArrayList<>():scl_suggestions);
                                                        }
                                                        else if(card_content.getSuggestions() != null) {
                                                            card_content.getSuggestions().clear();
                                                            card_content.setSuggestions((scl_suggestions==null)?new ArrayList<>():scl_suggestions);
                                                        }
                                                    }else{
                                                        // to avoid not displaying suggestions if content is not there in JSON
                                                        Content contentnew = new Content();
                                                        contentnew.setSuggestions((scl_suggestions==null)?new ArrayList<>():scl_suggestions);
                                                        message.getGeneralPurposeCard().setContent(contentnew);
                                                    }
                                                }
                                            }
                                            try {
                                                String combinedRichMessage = new Gson().toJson(entity);
                                                if (combinedRichMessage != null)
                                                    richReply = combinedRichMessage;
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        String userNumber = peer1.replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("Looking for user id of " + userNumber);
                                        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                                        Log.DebugApi("User id : " + userID);
                                        int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                                        Log.DebugApi("Internal session id : " + conversationInternalID);
                                        int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                                        Log.DebugApi("Conversation group user id : " + conversationGroupUserID);
                                        Log.DebugApi("Inserting new message details to local db");
                                        // creating new object for saving data to conversation table.
                                        insertMessageToDB(txtMessage, conversationInternalID, conversationGroupUserID);
                                        insertRichReplyToDB(richReply, conversationInternalID, conversationGroupUserID);
                                        // showing notification in status bar.
                                        String action = "android.intent.action.MAIN";
                                        showNotification(conversationInternalID, AppBaseApplication.context(), txtMessage, action, userNumber);

                                    } else if (direction == Direction.Outgoing) {
                                        // ==================== Outgoing message handling here ==========================
                                        Log.DebugApi("-----------*file sending progress*-----------");
                                        String contributionId = session.getContributionId();
                                        Log.DebugApi("Received contributionID : " + contributionId);
                                        String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("Received peer : " + peer1);
                                        String message = new String(sessionMessage.getContent().getData(), StandardCharsets.UTF_8);
                                        Log.DebugApi("Received message : " + message);
                                        String userNumber = peer1.replace("+", "");
                                        Log.DebugApi("Looking for user id of " + userNumber);
                                        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                                        Log.DebugApi("User id : " + userID);
                                        int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                                        Log.DebugApi("Internal session id : " + conversationInternalID);
                                        int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                                        Log.DebugApi("Conversation group user id : " + conversationGroupUserID);

                                    }
                                }

                                @Override
                                public void onSessionEvent(Session session, SessionFile sessionFile) {
                                    Direction direction = sessionFile.getDirection();
                                    if (direction == Direction.Incoming) {
                                        Log.DebugApi("Received onSessionEvent....");
                                        Log.DebugApi("FileSession:- Received onSessionEvent...." + sessionFile);
                                        String contributionId = session.getContributionId();
                                        Log.DebugApi("FileSession:- Received contributionID : " + contributionId);
                                        String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                        Log.DebugApi("FileSession:- Received peer : " + peer1);
                                        String message = new String(sessionFile.getContent().getData(), StandardCharsets.UTF_8);
                                        Log.DebugApi("FileSession:- Received message : " + message);
                                        String userNumber = peer1.replace("+", "");
                                        Log.DebugApi("FileSession:- Looking for user id " + userNumber);
                                        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                                        Log.DebugApi("FileSession:- User id : " + userID);
                                        int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                                        Log.DebugApi("FileSession:- Internal session id : " + conversationInternalID);
                                        int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                                        Log.DebugApi("FileSession:- Conversation group user id : " + conversationGroupUserID);

                                        int conversationFileSessionID = conversationInternalID;// need to update with unique ID(from the API itself)
                                        MFiles fileDetailNotCompleted = AppBaseApplication.mDb.getIncomingFileSessionDetails(conversationInternalID);

                                        // no record found in the table with the session ID.
                                        if (fileDetailNotCompleted == null) {
                                            int[] newID = createNewID(conversationGroupUserID, conversationFileSessionID);
                                            int FileFolderId = newID[0];
                                            int FileSessionInternalID = newID[1];
                                            String folder_name = "" + FileFolderId;
                                            Log.DebugApi("FileSession:- defined new folder name-" + folder_name);
                                            String FilePath = Environment.getExternalStorageDirectory().toString() + File.separator + FileHelper.FOLDER_NAME + File.separator + folder_name;
                                            Log.DebugApi("FileSession:- defined new folder path-" + FilePath);
                                            FileUtils.createFolder(FilePath);
                                            Log.DebugApi("FileSession:- created new folder-" + FilePath);
                                            String path = FilePath + File.separator + "session_files";
                                            File file = FileUtils.createFile(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                                            Log.DebugApi("FileSession:- created new file-" + file.getAbsolutePath());
                                            Log.DebugApi("FileSession:- inserting file table with values(" + conversationGroupUserID + "," + conversationFileSessionID + "," + FileFolderId + "," + file.getAbsolutePath() + "," + "0)");
                                            AppBaseApplication.mDb.updateFilePath(conversationGroupUserID, conversationFileSessionID, FileFolderId, file.getAbsolutePath(), "0");
                                            Log.DebugApi("FileSession:- updating file path to the db-" + file.getAbsolutePath());
                                            insertFileMessageToDB(conversationInternalID, conversationGroupUserID, conversationFileSessionID, FileSessionInternalID, FileFolderId, file);
                                        } else {
                                            int[] newID = createNewID(conversationGroupUserID, conversationFileSessionID);

                                            int FileFolderId = newID[0];
                                            int FileSessionInternalID = newID[1];
                                            Log.DebugApi("FileSession:- folder and file is already created for the session ID-" + conversationFileSessionID);
                                            Log.DebugApi("FileSession:- created new internal ID-" + FileFolderId);
                                            String folder_name = "" + FileFolderId;
                                            Log.DebugApi("FileSession:- defined new folder name-" + folder_name);
                                            String FilePath = Environment.getExternalStorageDirectory().toString() + File.separator + "Nimbus" + File.separator + folder_name;
                                            Log.DebugApi("FileSession:- defined new folder path-" + FilePath);
                                            FileUtils.createFolder(FilePath);
                                            Log.DebugApi("FileSession:- created new folder-" + FilePath);
                                            String path = FilePath + File.separator + "session_files";
                                            Log.DebugApi("FileSession:- defined new path for saving files-" + folder_name);
                                            File file = FileUtils.createFile(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                                            Log.DebugApi("FileSession:- created new file-" + file.getAbsolutePath());
                                            Log.DebugApi("FileSession:- inserting file table with values(" + conversationGroupUserID + "," + conversationFileSessionID + "," + FileFolderId + "," + file.getAbsolutePath() + "," + "0)");
                                            AppBaseApplication.mDb.updateFilePath(conversationGroupUserID, conversationFileSessionID, FileFolderId, file.getAbsolutePath(), "0");
                                            Log.DebugApi("FileSession:- updating file path to the db-" + file.getAbsolutePath());
                                            updateFileMessageToDB(conversationInternalID, conversationGroupUserID, conversationFileSessionID, FileSessionInternalID, FileFolderId, file);

                                        }
                                    } else if (direction == Direction.Outgoing) {

                                    }

                                }

                                @Override
                                public void onSessionEvent(Session session, SessionRichCard sessionRichCard) {
                                    Log.DebugApi("RichCard:- Received onSessionEvent...." + sessionRichCard);
                                    String contributionId = session.getContributionId();
                                    Log.DebugApi("RichCard:- Received contributionID : " + contributionId);
                                    String peer1 = session.getPeer().replace("tel:", "").replace("+", "").replaceAll("[^0-9]", "");
                                    Log.DebugApi("RichCard:- Received peer : " + peer1);
                                    String richMessage = new String(sessionRichCard.getContent().getData(), StandardCharsets.UTF_8);
                                    String richReply = null;
                                    String suggestedchiplist = sessionRichCard.getBotSuggestion().getJson();
                                    Log.d2("SPLITTED_JSOn", "Bot suggestion :- " + suggestedchiplist);

                                    boolean isSuggestionInside = true;
                                    if (!Strings.isNullOrEmpty(suggestedchiplist)) {
                                        try {
                                            JSONObject entity = new JSONObject(suggestedchiplist);
                                            isSuggestionInside = false;
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    Log.DebugMessage("RichCard:- Received message : " + richMessage);
                                    Log.DebugMessage("RichCard:- Received scl : " + suggestedchiplist);
                                    RichCardEntity entity = null;
                                    if (!isSuggestionInside) {
//                                        if(!richMessage.endsWith("}}}"))richMessage+="}";
                                        try {
                                            entity = new Gson().fromJson(richMessage, RichCardEntity.class);
                                        } catch (JsonSyntaxException e) {
                                            e.printStackTrace();
                                        }
                                        if(entity == null){
                                            Toast.makeText(context, "Invalid Data!", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        Message message = entity.getMessage();
                                        if (message != null) {
                                            if (message.isGeneralPurpose()) {
                                                com.ecrio.iota.model.scl.Content suggestion = new Gson().fromJson(suggestedchiplist, com.ecrio.iota.model.scl.Content.class);
                                                List<Suggestion> suggestions = suggestion.getSuggestions();
                                                Log.DebugApi((suggestions !=null && !suggestions.isEmpty())?("has global suggestions "+suggestions.size()):"no global suggestions");
                                                Content content = message.getGeneralPurposeCard().getContent();
                                                Log.DebugApi((content == null)?"no content":"has content");
                                                // No need to combine individual suggestions to global one.
                                                if (content != null) {
                                                    if (content.getSuggestions() == null || content.getSuggestions().size() == 0) {
                                                        content.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                                                    }
                                                    else if(content.getSuggestions() != null) {
                                                        content.getSuggestions().clear();
                                                        content.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                                                    }
                                                }else{
                                                    // to avoid not displaying suggestions if content is not there in JSON
                                                    Content contentnew = new Content();
                                                    contentnew.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                                                    message.getGeneralPurposeCard().setContent(contentnew);
                                                }
                                            } else if (message.isCarousal()) {
                                                com.ecrio.iota.model.scl.Content suggestion = new Gson().fromJson(suggestedchiplist, com.ecrio.iota.model.scl.Content.class);
                                                List<Suggestion> suggestions = suggestion.getSuggestions();
                                                Log.DebugApi((suggestions !=null && !suggestions.isEmpty())?("has global suggestions "+suggestions.size()):"no global suggestions");
                                                List<Content> content = message.getGeneralPurposeCardCarousel().getContent();
                                                Log.DebugApi((content != null && !content.isEmpty()) ? "has content" : "no content");
//                                                if (content != null) {
//
//                                                    for (Content content_ : content) {
//                                                        // No need to combine individual suggestions to global one.
//                                                        if (content_ != null) {
//                                                            if (content_.getSuggestions() == null || content_.getSuggestions().size() == 0) {
//                                                                content_.setSuggestions((suggestions == null) ? new ArrayList<>() : suggestions);
//                                                            } else if (content_.getSuggestions() != null) {
//                                                                // content_.getSuggestions().addAll(suggestions);// no need to combine all suggestions
//                                                                content_.getSuggestions().clear();
//                                                                content_.setSuggestions((suggestions == null) ? new ArrayList<>() : suggestions);
//                                                            }
//                                                        }else{
//                                                            // to avoid not displaying suggestions if content is not there in JSON
//                                                            content_ = new Content();
//                                                            content_.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
//                                                        }
//                                                        break;
//                                                    }
//                                                }else{
                                                    // to avoid not displaying suggestions if content is not there in JSON
                                                    Content contentNew = new Content();
                                                    contentNew.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                                                    ArrayList<Content> contents = new ArrayList<>();
                                                    contents.add(contentNew);
                                                    message.getGeneralPurposeCardCarousel().setContent(contents);
//                                                }
                                            }
                                        }
                                        try {
                                            String combinedRichMessage = new Gson().toJson(entity);
                                            if (combinedRichMessage != null)
                                                richReply = combinedRichMessage;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    try {
                                        entity = new Gson().fromJson(richMessage, RichCardEntity.class);
                                    } catch (JsonSyntaxException e) {
                                        e.printStackTrace();
                                    }
                                    if(entity == null){
                                        Toast.makeText(context, "Invalid Data!", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    boolean hasData = false;
                                    Log.DebugRich("===PARSING===");
                                    int richTYPE = -1;
                                    if (entity == null) {
                                        Log.DebugRich("rich card object is null");
                                    } else {
                                        Message richMsg = entity.getMessage();
                                        if (richMsg != null) {
                                            Log.d2("Final_RC","rich card object is \n"+new Gson().toJson(richMsg));
                                            if (richMsg.isGeneralPurpose()) {
                                                richTYPE = RichType.GENERAL.ordinal();
                                                GeneralPurposeCard card = richMsg.getGeneralPurposeCard();
                                                if (card != null) {
                                                    Content content = card.getContent();
                                                    String title = content.getTitle();
                                                    Log.DebugRich("title is " + title);
                                                    String description = content.getDescription();
                                                    Log.DebugRich("description is " + description);
                                                    List<Suggestion> suggestions = content.getSuggestions();
                                                    if (suggestions != null && !suggestions.isEmpty()) {
                                                        for (Suggestion suggestion : suggestions) {
                                                            if (suggestion != null) {
                                                                if (suggestion.isReply()) {
                                                                    Reply reply = suggestion.getReply();
                                                                    Log.DebugRich(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                                    hasData = true;
                                                                } else if (suggestion.isAction()) {
                                                                    Action action = suggestion.getAction();
                                                                    Log.DebugRich(String.format("action data found \"%s\"", action.getDisplayText()));
                                                                    if (action.hasDialerAction()) {
                                                                        DialerAction dialerAction = action.getDialerAction();
                                                                        if (dialerAction.hasDialPhone()) {
                                                                            Log.DebugRich("action is dialer");
                                                                            DialPhoneNumber dialPhoneNumber = dialerAction.getDialPhoneNumber();
                                                                            if (dialPhoneNumber != null) {
                                                                                Log.DebugRich("dialing number is " + dialPhoneNumber.getPhoneNumber());
                                                                            }
                                                                        }
                                                                    } else if (action.hasURLAction()) {
                                                                        Log.DebugRich("action is URL");
                                                                        UrlAction urlAction = action.getUrlAction();
                                                                        if (urlAction != null) {
                                                                            Log.DebugRich("URL is " + urlAction.getOpenUrl().getUrl());
                                                                        }
                                                                    } else if (action.hasMapAction()) {
                                                                        MapAction mapAction = action.getMapAction();
                                                                        if (mapAction.hasShowLocation()) {
                                                                            ShowLocation showLocation = mapAction.getShowLocation();
                                                                            if (showLocation != null) {
                                                                                Log.DebugRich("Location is " + showLocation.getLocation());
                                                                                Log.DebugRich("URL is " + showLocation.getFallbackUrl());
                                                                            } else {
                                                                                Log.DebugRich("showLocation is " + showLocation);
                                                                            }
                                                                        }
                                                                    } else if (action.hasCalendarAction()) {
                                                                        Log.DebugRich("action is Calendar");
                                                                        CalendarAction calendarAction = action.getCalendarAction();
                                                                        CreateCalendarEvent createCalendarEvent = calendarAction.getCreateCalendarEvent();
                                                                        if (createCalendarEvent != null) {
                                                                            Log.DebugRich("Title is " + createCalendarEvent.getTitle());
                                                                            Log.DebugRich("Description is " + calendarAction.getCreateCalendarEvent().getDescription());
                                                                        }
                                                                    }
//                                                                    hasData = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    hasData = true;
                                                }
                                            } else if (richMsg.isCarousal()) {
                                                richTYPE = RichType.CAROUSAL.ordinal();
                                                GeneralPurposeCardCarousel cardCarousel = richMsg.getGeneralPurposeCardCarousel();
                                                if (cardCarousel != null) {
                                                    List<Content> content_s = cardCarousel.getContent();
                                                    if (content_s != null && !content_s.isEmpty()) {
                                                        for (Content content_ : content_s) {
                                                            String title = content_.getTitle();
                                                            Log.DebugRich("title is " + title);
                                                            String description = content_.getDescription();
                                                            Log.DebugRich("description is " + description);
                                                            List<Suggestion> suggestion_s = content_.getSuggestions();
                                                            if (suggestion_s != null) {
                                                                for (Suggestion suggestion_ : suggestion_s) {
                                                                    if (suggestion_ != null) {
                                                                        if (suggestion_.isReply()) {
                                                                            Reply reply = suggestion_.getReply();
                                                                            Log.DebugRich(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                                            hasData = true;
                                                                        } else if (suggestion_.isAction()) {
                                                                            Action action = suggestion_.getAction();
                                                                            Log.DebugRich(String.format("action data found \"%s\"", action.getDisplayText()));
                                                                            if (action.hasDialerAction()) {
                                                                                DialerAction dialerAction = action.getDialerAction();
                                                                                if (dialerAction.hasDialPhone()) {
                                                                                    Log.DebugRich("action is dialer");
                                                                                    DialPhoneNumber dialPhoneNumber = dialerAction.getDialPhoneNumber();
                                                                                    if (dialPhoneNumber != null) {
                                                                                        Log.DebugRich("dialing number is " + dialPhoneNumber.getPhoneNumber());
                                                                                    }
                                                                                }
                                                                            } else if (action.hasURLAction()) {
                                                                                Log.DebugRich("action is URL");
                                                                                Log.DebugRich("URL is " + action.getUrlAction().getOpenUrl().getUrl());
                                                                            } else if (action.hasMapAction()) {
                                                                                Log.DebugRich("action is Map");
                                                                                MapAction mapAction = action.getMapAction();
                                                                                if (mapAction.hasShowLocation()) {
                                                                                    ShowLocation showLocation = mapAction.getShowLocation();
                                                                                    if (showLocation != null) {
                                                                                        Log.DebugRich("Location is " + showLocation.getLocation());
                                                                                        Log.DebugRich("URL is " + showLocation.getFallbackUrl());
                                                                                    } else {
                                                                                        Log.DebugRich("showLocation is " + showLocation);
                                                                                    }
                                                                                } else {
                                                                                    Log.DebugRich("mapAction is " + mapAction);
                                                                                }
                                                                            } else if (action.hasCalendarAction()) {
                                                                                Log.DebugRich("action is Calendar");
                                                                                Log.DebugRich("Title is " + action.getCalendarAction().getCreateCalendarEvent().getTitle());
                                                                                Log.DebugRich("Description is " + action.getCalendarAction().getCreateCalendarEvent().getDescription());
                                                                            }
//                                                                            hasData = true;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            hasData = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    Log.DebugRich("===COMPLETED===" + hasData);

                                    if (hasData) {
                                        String userNumber = peer1.replace("+", "");
                                        Log.DebugApi("RichCard:- Looking for user id " + userNumber);
                                        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                                        Log.DebugApi("RichCard:- User id : " + userID);
                                        int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                                        Log.DebugApi("RichCard:- Internal session id : " + conversationInternalID);
                                        int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                                        Log.DebugApi("RichCard:- Conversation group user id : " + conversationGroupUserID);

                                        int RichCardInternalID = AppBaseApplication.mDb.getUpdatedInternalRichCardID(userID, conversationInternalID);
                                        Log.DebugApi("RichCard:- Conversation rich card id : " + RichCardInternalID);
                                        // creating new object for saving data to conversation table.
                                        insertRichMessageToDB(richMessage, conversationInternalID, conversationGroupUserID, RichCardInternalID, richTYPE);
                                        insertRichReplyToDB(richReply, conversationInternalID, conversationGroupUserID);
                                        Log.DebugMessage("Debug:Rich:"+richMessage);
                                        Log.DebugMessage("Debug:SCL:"+richReply);
                                    }
                                }

                            });
                        // Adding listener for incoming messages.
                        session.addListener(sessionListenerHashMap.get(peer));
                        if (session.getContentTypes() == null || session.getContentTypes().size() == 0) {
//                            try {
//                                Toast.makeText(context, "no content types", Toast.LENGTH_SHORT).show();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                            session.respond(Response.Accept,
                                    ContentType.Text,
                                    ContentType.IMDN,
                                    ContentType.FileTransfer,
                                    ContentType.Composing,
                                    ContentType.RichCard,
                                    ContentType.SuggestedChipList,
                                    ContentType.SuggestionResponse);
                        } else {
//                            try {
//                                Toast.makeText(context, "has " + session.getContentTypes().size() + " content types", Toast.LENGTH_SHORT).show();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                            // Responding to incoming session.
                            session.respond(Response.Accept,
                                    ContentType.Text,
                                    ContentType.IMDN,
                                    ContentType.FileTransfer,
                                    ContentType.Composing,
                                    ContentType.RichCard,
                                    ContentType.SuggestedChipList,
                                    ContentType.SuggestionResponse);
                        }
                        int conversationUserID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(peer, "1");
                        Log.DebugApi("User ID : " + conversationUserID);
                        // update/insert the chat details with the new conversation id generated with this user/phoneNo.
                        int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(conversationUserID, contributionId);
                        Log.DebugApi("Internal session ID : " + conversationInternalID);
                        int conversationID = AppBaseApplication.mDb.insertOrUpdateNewConversationID(conversationUserID, conversationInternalID);
                        Log.DebugApi("Session ID : " + conversationID);

                }
            });
        };
    }

    @NonNull
    private Iota.ContactListener getContactListener(Context context) {
        return (iota, contactInfo) -> {
            Log.DebugApi("Received onContactUpdate " + contactInfo.getEvent());
            Log.DebugApi(contactInfo.getEvent().name() + " " + contactInfo.getState().toString());
            for (int i = 0; i < mContactListeners.size(); i++) {
                mContactListeners.get(i).onContactUpdate(iota, contactInfo);
            }

            ContactInfo.Event event = contactInfo.getEvent();
            ContactInfo.State state = contactInfo.getState();
            if (state == ContactInfo.State.Active) {

            } else {

            }
            if (event == ContactInfo.Event.Registered) {
                if (state == ContactInfo.State.Terminated) {
                    arrlist.set(ResourceStatus.STATUS_REGISTRATION.ordinal(), 0);
//                    arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 0);
                    arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 0);
                    // check all the resources are available
                    checkRegistration(context);
                } else if (state == ContactInfo.State.Active) {
                    arrlist.set(ResourceStatus.STATUS_REGISTRATION.ordinal(), 1);
                    arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 1);
                    arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 1);
                    // check all the resources are available
                    checkRegistration2(context);
                } else if (state == ContactInfo.State.None) {
                    arrlist.set(ResourceStatus.STATUS_REGISTRATION.ordinal(), 0);
//                    arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 0);
                    arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 0);
                    // check all the resources are available
                    checkRegistration(context);
                }
            }


        };
    }

    @NonNull
    private Iota.ResourceListener getResourceListener(Context context) {
        return (instance, resource, status) -> {
            arrlist.set(ResourceStatus.STATUS_NETWORK.ordinal(), 1);
            arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 1);
            arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 1);
            Log.DebugApi("Received onResourceStateUpdate " + resource.toString() + " " + status.toString());
            if (resource == ResourceType.Network) {
                NetworkStatus.getStatus().setState(status);
                if (status == ResourceState.Available) {
//                    arrlist.set(ResourceStatus.STATUS_NETWORK.ordinal(), 1);
                } else if (status == ResourceState.Unavailable) {
//                    arrlist.set(ResourceStatus.STATUS_NETWORK.ordinal(), 0);
                } else if (status == ResourceState.Unknown) {
//                    arrlist.set(ResourceStatus.STATUS_NETWORK.ordinal(), 0);
                }
            }
            if (resource == ResourceType.Registration) {
                RegistrationStatus.getStatus().setState(status);
                if (status == ResourceState.Available) {
                    arrlist.set(ResourceStatus.STATUS_REGISTRATION.ordinal(), 1);
                    checkRegistration2(context);
                } else if (status == ResourceState.Unavailable) {
                    arrlist.set(ResourceStatus.STATUS_REGISTRATION.ordinal(), 0);
                    checkRegistration(context);
                } else if (status == ResourceState.Unknown) {
                    arrlist.set(ResourceStatus.STATUS_REGISTRATION.ordinal(), 0);
                    checkRegistration(context);
                } else {
                    checkRegistration(context);
                }
                // check all the resources are available
//                checkRegistration(context);

            }
            if (resource == ResourceType.Subscription) {
                SubscriptionStatus.getStatus().setState(status);
                if (status == ResourceState.Available) {
                    arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 1);
                } else if (status == ResourceState.Unavailable) {
//                    arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 0);
                } else if (status == ResourceState.Unknown) {
//                    arrlist.set(ResourceStatus.STATUS_SUBSCRIPTION.ordinal(), 0);
                }
                // check all the resources are available
//                checkRegistration(context);

            }
            if (resource == ResourceType.Capabilities) {
                CapabilityStatus.getStatus().setState(status);
                if (status == ResourceState.Available) {
                    arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 1);
                    checkRegistration2(context);
                } else if (status == ResourceState.Unavailable) {
//                    arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 0);
                    checkRegistration(context);
                } else if (status == ResourceState.Unknown) {
//                    arrlist.set(ResourceStatus.STATUS_CAPABILITIES.ordinal(), 0);
                    checkRegistration(context);
                } else {
                    checkRegistration(context);
                }
                // check all the resources are available
//                checkRegistration(context);

            }
            for (int i = 0; i < mListeners.size(); i++) {
                mListeners.get(i).onResourceStateUpdate(instance, resource, status);
            }
        };
    }

    private void insertInfoMessage(int conversationGroupUserID, int conversationID, String requested) {
        // creating new object for saving data to conversation table.
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        // set status of this user(phone no) to 0(requested) and return the user id.
        conversationMsg.setUserID(conversationGroupUserID);
        // setting conversationID.
        conversationMsg.setConversationID(conversationID);
        // setting the message.
        conversationMsg.setMessage(requested);
        conversationMsg.setReplyId("-1");
        // setting conversationFileID to -1.
        conversationMsg.setConversationFileID(-1);
        // setting progress to 0.
        conversationMsg.setConversationFileProgress("0%");
        // setting isOutGouing value to 0
        // 0-attendee
        // 1-myself
        conversationMsg.setConversationSender(ConversationSender.OUT.ordinal());
        // setting status value to 0
        // 0 - not delivered
        // 1 - delivered
        // 2 -composing
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // 0 - text message
        // 1 - file message
        // 2 - info message
        conversationMsg.setConversationType(ConversationType.INFO.ordinal());
        // Insert new conversation to "conversation reply" table
        AppBaseApplication.mDb.insertNewInfoMessage(conversationMsg);
    }

    Handler handler = new Handler(Looper.getMainLooper());

    private void checkRegistration(Context context) {
        Log.d2("Register_", "checkRegistration");
        if (!runReStartIMInBackground.isEmpty()) {
            Log.d2("Register_", "clearing...");
            runReStartIMInBackground.clear();
        }
        runReStartIMInBackground.add(getRegisterRunnable(context));
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(runReStartIMInBackground.remove(), 5000);
    }

    private void checkRegistration2(Context context) {
        Log.d2("Register_", "checkRegistration");
        if (!runReStartIMInBackground.isEmpty()) {
            Log.d2("Register_", "clearing...");
            runReStartIMInBackground.clear();
        }
        runReStartIMInBackground.add(getRegisterRunnable(context));
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(runReStartIMInBackground.remove(), 0);
    }

    @NonNull
    private Runnable getRegisterRunnable(Context context) {
        return new Runnable() {
            @Override
            public void run() {
                int indexOf = arrlist.indexOf(0);
                Log.d2("Register_", "status is " + indexOf);
                if (indexOf == -1) {
                    UserInfo info = UserInfo.getInstance();
                    info.readCash(context);
                    info.setStatus(1);
                    info.saveCash(context);
                    Log.d("setting credentials");
                    Log.d("username is " + info.getUser_name());
                    Log.d("password is " + info.getUser_password());
                    mApi.setCredentials(info.getUser_name(), info.getUser_password());
                    AppBaseApplication.mPreferences.edit().putString("my_phone", info.getUser_name()).apply();
                    //registration successful.
                    //notify the status using broadcast.
                    Log.d("sending broadcast");

                    AppBaseApplication.mDb.clearData();
                    info.readCash(mContext);
                    String user_name = info.getUser_name();
                    info.setPrevUser(user_name);
                    info.saveCash(context);
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
                    // Create intent with action
                    Intent localIntent = new Intent(Constants.ACTION_REGISTER);
                    //already registered user.
                    localIntent.putExtra(Constants.KEY_NAME_STATUS, false);
                    // Send local broadcast
                    localBroadcastManager.sendBroadcast(localIntent);
                } else {
                    Log.d("clearing credentials");
                    UserInfo info = UserInfo.getInstance();
                    info.readCash(mContext);


                    // create intent with action
                    Intent localIntent = new Intent(Constants.ACTION_REGISTER);
                    int login_status = info.getLogin_status();
                    if (!RegistrationStatus.getStatus().unknown()) {
                        Log.DebugApi("Registration status is unknown.");

                    }


                    if (login_status == 1) {
                        try {
                            if (!RegistrationStatus.getStatus().unknown()) {
                                Log.DebugApi("calling Iota deregister()");
                                IotaModule.this.mIota.deregister();
                                Log.DebugApi("clearing session list");
                                sessionHashMap.clear();
                                Log.DebugApi("calling Iota close()");
                                IotaModule.this.mIota.close();
                                Log.DebugApi("calling new Iota instance");
                                IotaModule.this.mIota = Iota.newInstance(mContext);
                                IotaModule.this.mIota.addResourceListener(IotaModule.this.getResourceListener(context));
                                IotaModule.this.mIota.addContactListener(IotaModule.this.getContactListener(context));
                                IotaModule.this.mIota.addNewConversationListener(IotaModule.this.getNewConversationListener(context));
                                Log.DebugApi("updating module");
                                AppBaseApplication.getInstance().updateSettings(IotaModule.this);
                                Log.DebugApi("_instance created: " + mIota + "");
                                for (int i = 0; i < mListeners.size(); i++) {
                                    mListeners.get(i).onResourceStateUpdate(mIota, ResourceType.Registration, ResourceState.Unavailable);
                                }
                            }
                        }catch (Exception e) {
                            Log.DebugApi("Exception occured");
                            e.printStackTrace();
                        }
                        //already registered user.
                        localIntent.putExtra(Constants.KEY_NAME_STATUS, true);

                    } else {
                        try {
                            if (!RegistrationStatus.getStatus().unknown()) {
                                Log.DebugApi("clearing session list");
                                sessionHashMap.clear();
                                Log.DebugApi("calling Iota close()");
                                IotaModule.this.mIota.close();
                                Log.DebugApi("calling new Iota instance");
                                IotaModule.this.mIota = Iota.newInstance(mContext);
                                IotaModule.this.mIota.addResourceListener(IotaModule.this.getResourceListener(context));
                                IotaModule.this.mIota.addContactListener(IotaModule.this.getContactListener(context));
                                IotaModule.this.mIota.addNewConversationListener(IotaModule.this.getNewConversationListener(context));
                                Log.DebugApi("updating module");
                                AppBaseApplication.getInstance().updateSettings(IotaModule.this);
                                Log.DebugApi("_instance created: " + mIota + "");
                                for (int i = 0; i < mListeners.size(); i++) {
                                    mListeners.get(i).onResourceStateUpdate(mIota, ResourceType.Registration, ResourceState.Unavailable);
                                }
                            }
                        }catch (Exception e) {
                            Log.DebugApi("Exception occurred");
                            e.printStackTrace();
                        }
                        //not registered user.
                        localIntent.putExtra(Constants.KEY_NAME_STATUS, false);
                    }
                    // set status to not registered.
                    info.setStatus(-1);
                    info.saveCash(mContext);
                    mApi.setCredentials("", "");

                    // notify the status using broadcast.
                    Log.d("sending broadcast");
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
                    // Send local broadcast
                    localBroadcastManager.sendBroadcast(localIntent);
                }
            }
        };
    }

    private void updateFileMessageToDB(int conversationInternalID, int conversationGroupUserID, int conversationFileSessionID, int fileSessionInternalID, int fileFolderId, File file) {
        // creating new object for saving data to conversation table.
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        conversationMsg.setUserID(conversationGroupUserID);
        // setting conversationID.
        conversationMsg.setConversationID(conversationInternalID);
        // setting conversationID.
        conversationMsg.setConversationFileID(fileSessionInternalID);
        // setting conversationID.
        conversationMsg.setConversationFileProgress("0%");
        conversationMsg.setConversationFileThumb(new byte[]{});
        // setting reply message.
        conversationMsg.setMessage(file.getName());
        conversationMsg.setReplyId("-1");
        // setting isOutGouing value to 0
        // 0 - isOutGouing
        // 1 - myself
        conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
        // setting status value to 0
        // 0 - not delivered
        // 1 - delivered
        // 2 - composing
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // 0 - text message
        // 1 - file message
        // 2 - info
        conversationMsg.setConversationType(ConversationType.FILE.ordinal());
        // Insert new conversation to "conversation reply" table
        AppBaseApplication.mDb.insertNewFileMessage(conversationMsg);

        Log.DebugApi("inserting new message to the db-" + conversationMsg);
        Log.DebugApi("updating file table with values(" + conversationGroupUserID + "," + String.valueOf(conversationFileSessionID) + "," + fileFolderId + "," + "0)");
        int fileTableUpdateID = AppBaseApplication.mDb.updateIncomingFileProgress(conversationGroupUserID, conversationFileSessionID, fileFolderId, "0");
        Log.DebugApi("updated file sending progress to file table row == " + fileTableUpdateID);
        Log.DebugApi("updating progress to the db-" + "0");
    }

    private void insertFileMessageToDB(int conversationInternalID, int conversationGroupUserID, int conversationFileSessionID, int fileSessionInternalId, int fileFolderId, File file) {
        // creating new object for saving data to conversation table.
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        conversationMsg.setUserID(conversationGroupUserID);
        // setting conversationID.
        conversationMsg.setConversationID(conversationInternalID);
        // setting conversationID.
        conversationMsg.setConversationFileID(fileSessionInternalId);
        // setting conversationID.
        conversationMsg.setConversationFileProgress("0%");
        conversationMsg.setConversationFileThumb(new byte[]{});
        // setting reply message.
        conversationMsg.setMessage(file.getName());
        // setting reply message.
        conversationMsg.setReplyId("-1");
        // setting isOutGouing value to 0
        // 0 - isOutGouing
        // 1 - myself
        conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
        // setting status value to 0
        // 0 - not delivered
        // 1 - delivered
        // 2 - composing
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // 0 - text message
        // 1 - file message
        // 2 - info
        conversationMsg.setConversationType(ConversationType.FILE.ordinal());
        // Insert new conversation to "conversation reply" table
        AppBaseApplication.mDb.insertNewFileMessage(conversationMsg);
        Log.DebugApi("inserting new message to the db-" + conversationMsg);
        Log.DebugApi("updating file table with values(" + conversationGroupUserID + "," + String.valueOf(conversationFileSessionID) + "," + fileFolderId + "," + "0)");
        int fileTableUpdateID = AppBaseApplication.mDb.updateIncomingFileProgress(conversationGroupUserID, conversationFileSessionID, fileFolderId, "0");
        Log.DebugApi("updated file sending progress to file table row == " + fileTableUpdateID);
        Log.DebugApi("updated progress to the db - " + "0");
    }

    private int[] createNewID(int userID, int conversationFileSessionID) {

        int attachmentInternalId;

        File newPath1 = new File(android.os.Environment.getExternalStorageDirectory().toString() + File.separator + FileHelper.FOLDER_NAME + File.separator);

        FileUtils.createFolder(newPath1);

        File[] files = newPath1.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.matches(("[0-9]+"));
            }
        });

        ArrayList<String> arrayList = new ArrayList<>();
        for (File file : files) {
            arrayList.add(file.getName());
        }

        String lastFile = "1000";

        if (arrayList.size() > 0) {

            Collections.sort(arrayList);

            lastFile = arrayList.get(arrayList.size() - 1);

        }
        int folderNameID = Integer.parseInt(lastFile);

        MFiles fileDetail = AppBaseApplication.mDb.getIFileSessionDetailsNotCompleted(userID, conversationFileSessionID, folderNameID);

        if (fileDetail != null) {

            Log.Debug("items found in the file table against userID==" + userID + " Incomming == " + true + " completed == " + false + " folderNameID==" + folderNameID);

            String attachmentPath = Environment.getExternalStorageDirectory().toString() + File.separator + FileHelper.FOLDER_NAME + File.separator + folderNameID;

            boolean exists = FileUtils.checkFolderExists(attachmentPath);

            if (exists) {
                FileUtils.deleteDirectory(new File(attachmentPath));
            }

            attachmentInternalId = folderNameID;

        } else {

            Log.Debug("no item found in the file table against userID==" + userID + " Incomming == " + true + " completed == " + false + " folderNameID==" + folderNameID);

            attachmentInternalId = folderNameID + 1;

        }

        FileData data = new FileData();
        data.setUserId(userID);
        data.setSessionId("" + conversationFileSessionID);
        data.setId(attachmentInternalId);
        data.setSender(ConversationSender.IN.ordinal());
        data.setCompleted("0");

        Log.Debug("created new item in the file table for userID==" + userID + " FileSessionID == " + conversationFileSessionID + " AttachmentInternalId == " + attachmentInternalId + " Completed==" + 0);

        AppBaseApplication.mDb.clearDetail(attachmentInternalId);

        Long id = AppBaseApplication.mDb.insertFileSession(data);

        return new int[]{attachmentInternalId, id.intValue()};
    }

    private void insertMessageToDB(String message, int conversationInternalID, int conversationGroupUserID) {
        ConversationMsg conversationMsg = new ConversationMsg();
        // userID.
        conversationMsg.setUserID(conversationGroupUserID);
        // conversationInternalID.
        conversationMsg.setConversationID(conversationInternalID);
        // message.
        conversationMsg.setMessage(message);
        // id of message from api.
        conversationMsg.setReplyId("0");
        // conversationFileID.
        conversationMsg.setConversationFileID(-1);
        // file progress.
        conversationMsg.setConversationFileProgress("0%");
        // incoming
        conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
        // status value to delivered
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // TEXT(0) - text message
        // FILE(1) - file message
        // INFO(2) - info
        // IGNORE(3) - ignore
        conversationMsg.setConversationType(ConversationType.TEXT.ordinal());
        // Insert new conversation to "conversation reply" table
        Uri uri = AppBaseApplication.mDb.insertNewTextMessages(conversationMsg);
        Log.DebugDB("URI for the new message " + uri);
    }

    private void insertRichMessageToDB(String message, int conversationInternalID, int conversationGroupUserID, int RichCardInternalID, int richType) {
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        conversationMsg.setUserID(conversationGroupUserID);
        // setting conversationInternalID.
        conversationMsg.setConversationID(conversationInternalID);
        // setting message.
        conversationMsg.setMessage(message);
        // setting id of message.
        conversationMsg.setReplyId("0");
        // setting conversationFileID.
        conversationMsg.setConversationFileID(-1);
        // setting file progress.
        conversationMsg.setConversationFileProgress("0%");
        // setting conversationRichCardID
        conversationMsg.setConversationRichCardID(RichCardInternalID);
        conversationMsg.setRichType(richType);
        // setting message as incoming
        conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
        // setting status value to delivered
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // TEXT(0) - text message
        // FILE(1) - file message
        // INFO(2) - info
        // IGNORE(3) - ignore
        conversationMsg.setConversationType(ConversationType.RICH.ordinal());
        // Insert new conversation to "conversation reply" table
        Uri uri = AppBaseApplication.mDb.insertNewTextMessages(conversationMsg);
        Log.Debug("URI for the message " + uri);
    }

    private void insertRichReplyToDB(String message, int conversationInternalID, int conversationGroupUserID) {
        if (Strings.isNullOrEmpty(message)) return;
        // The message that the user send.
        String Message = String.valueOf(message);
        // unique id of the this user.
        int conversationUserID = conversationGroupUserID;
        // unique conversation id of the chat occurred with the user
        int ConversationID = conversationInternalID;
        // Insert new conversation to "conversation reply" table

        CompositionMsg compositionMsg = new CompositionMsg();
        // setting userID.
        compositionMsg.setUserID(conversationUserID);
        // setting conversationID.
        compositionMsg.setConversationID(ConversationID);
        // setting status.
        compositionMsg.setCompositionState(Integer.parseInt("1"));
        // Insert new conversation to "conversation reply" table
//        YtsApplication.mDb.insertNewCompositionMessage(compositionMsg);

        final ContentResolver cr = AppBaseApplication.context().getContentResolver();

        ContentValues values = new ContentValues();

        values.put(TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID, compositionMsg.getConversationID());

        values.put(TableReplyContract.CompositionEntry.COLUMN_STATUS, compositionMsg.getCompositionState());

        values.put(TableReplyContract.CompositionEntry.COLUMN_USER_ID, compositionMsg.getUserID());

        values.put(TableReplyContract.CompositionEntry.COLUMN_MESSAGE, Message);

        Uri insert = cr.insert(TableReplyContract.CompositionEntry.CONTENT_URI_ALL_REPLIES, values);

    }

    private void showNotification(int conversationInternalID, Context context, String message, String action, String phoneNumber) {

        //intent used to launch MainActivity
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (launchIntent != null) {
            launchIntent.putExtra("PhoneNumber", phoneNumber);
            if (action != null) {
                launchIntent.setAction(action);
            }
        }

        // use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(AppBaseApplication.context(), (int) System.currentTimeMillis(), launchIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // build notification

        // fetch contact details
        ContactDetail contactinfo = GenricFunction.getContactInfo(context, phoneNumber);

        RemoteViews remoteView = new RemoteViews(AppBaseApplication.context().getPackageName(), R.layout.chat_notification_layout);

        remoteView.setTextViewText(R.id.txt_ContactNumber, message);

//        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Uri soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification);

        remoteView.setTextViewText(R.id.txt_TimeStamp, GenricFunction.getDate(System.currentTimeMillis(), "hh:mm aa"));

        remoteView.setImageViewResource(R.id.imgVolte, R.mipmap.icon_notification);

        Notification n;

        // checking contact details
        if (null != contactinfo) {
            // fetching contact name.
            if (null != contactinfo.getContactName()) {
                //setting contact name.
                remoteView.setTextViewText(R.id.txt_contactName, contactinfo.getContactName());
            } else {
                // setting phone number if no name found.
                remoteView.setTextViewText(R.id.txt_contactName, phoneNumber);
            }
            // fetching profile icon.
            if (null != contactinfo.getPhotoUri()) {
                // setting contact icon.
                remoteView.setImageViewUri(R.id.imgContactImage, Uri.parse(contactinfo.getPhotoUri()));
            } else {
                // setting default icon.
                remoteView.setImageViewResource(R.id.imgContactImage, R.mipmap.icon_user_notify);
            }
        } else {
            // if contact details are empty then,
            // set phone number instead of contact name for display.
            remoteView.setTextViewText(R.id.txt_contactName, phoneNumber);
            // set default profile icon.
            remoteView.setImageViewResource(R.id.imgContactImage, R.mipmap.icon_user_notify);
        }
        n = new Notification.Builder(AppBaseApplication.context())
                .setContentTitle("New message")
                .setContentText(message)
                .setSmallIcon(getNotificationIcon())
                .setContent(remoteView)
                .setSound(soundUri)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pIntent)
                .setAutoCancel(true).build();
        NotificationManager nm = (NotificationManager) AppBaseApplication.context().getSystemService(Context.NOTIFICATION_SERVICE);

        if (nm != null)
            nm.notify(conversationInternalID, n);
        else
            Log.Error("Notification manager is null.");
    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT);
        return whiteIcon ? R.mipmap.icon_notification : R.mipmap.icon_notification;
    }

    @Provides
    @Singleton
    public Iota provideIota() {
        Log.DebugApi("_instance returned " + mIota + "");
        return mIota;
    }

    @Provides
    @Singleton
    public MyApi provideMyApi() {
        return mApi;
    }

    @Provides
    @Singleton
    public NetworkStatus provideNetworkStatus() {
        return new NetworkStatus(ResourceStatus.STATUS_NETWORK);
    }

    @Provides
    @Singleton
    public RegistrationStatus provideRegistrationStatus() {
        return new RegistrationStatus(ResourceStatus.STATUS_REGISTRATION);
    }

    @Provides
    @Singleton
    public SubscriptionStatus provideSubscriptionStatus() {
        return new SubscriptionStatus(ResourceStatus.STATUS_SUBSCRIPTION);
    }

    @Provides
    @Singleton
    public ArrayList<Iota.ResourceListener> provideResourceListener() {
        return mListeners;
    }

    @Provides
    @Singleton
    public ArrayList<Iota.ContactListener> provideContactListener() {
        return mContactListeners;
    }

    @Provides
    @Singleton
    public HashMap<String, Session> provideSessions() {
        return sessionHashMap;
    }

    @Provides
    @Singleton
    public HashMap<String, Session.Listener> provideSessionListners() {
        return sessionListenerHashMap;
    }

    @Provides
    @Singleton
    public ArrayList<Iota.NewConversationListener> provideConversationListener() {
        return mConversationListeners;
    }

}