package com.ecrio.iota.data.cache;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;

import java.io.FileNotFoundException;
import java.lang.ref.SoftReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * Created by user on 2018.
 */
public class AvatarHelper{

    public static final int DEFAULT_COMPRESS_QUALITY = 90;
    private final Context mContext;
    private final HashMap<String, SoftReference<Bitmap>> mCache;
    private final MessageDigest mDigest;

    public AvatarHelper(Context context) {
        mContext = context;
        mCache = new HashMap<String, SoftReference<Bitmap>>();

        try {
            mDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            // This shouldn't happen.
            throw new RuntimeException("No MD5 algorithm.");
        }
    }

    public Bitmap downloadImage2(String url) throws Exception {
        return getAvatar(url);
    }

    // Looks to see if an image is in the file system.
    private Bitmap getAvatar(String phoneNumber) {

        Bitmap bitmap = null;

        String iD = AvatarHelper.contactIdByPhoneNumber(AppBaseApplication.getContext(), phoneNumber);
        Log.v("PhoneToBitmap", "iD : " + iD);

        Uri thumbImageUri = getThumbImageUri(Integer.parseInt(iD));
        Log.v("PhoneToBitmap", "URI : " + thumbImageUri);

        if (thumbImageUri != null) {

            Drawable photo = getContactPhoto(thumbImageUri);
            Log.v("PhoneToBitmap", "photo : " + photo);

            bitmap = drawableToBitmap(photo);
            Log.v("PhoneToBitmap", "bitmap : " + bitmap);

        }

        if (bitmap == null) {
            bitmap = drawableToBitmap(AppBaseApplication.getContext(), R.drawable.user_icons);
            Log.v("PhoneToBitmap", "bitmap default: " + bitmap);
        }
        return bitmap;

    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        // Convert Drawable to Bitmap first:
        Bitmap bitmap = (Bitmap) ((BitmapDrawable) drawable).getBitmap();
        return bitmap;
    }

    public Drawable getContactPhoto(Uri thumbImageUri) {

        Drawable drawable = null;
        try {

            drawable = Drawable.createFromStream(AppBaseApplication.getContext().getContentResolver().openInputStream(thumbImageUri), thumbImageUri.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return drawable;
    }


    public Uri getThumbImageUri(int contactId) {

        Uri photoUri = null;
        try {
            //content://com.android.contacts/display_photo/3
            //content://com.android.contacts/contacts/732/display_photo
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        } catch (Exception e) {

            return null;
        }

        return photoUri;
    }

    public static final String contactIdByPhoneNumber(Context ctx, String phoneNumber) {
        String contactId = null;
        if (phoneNumber != null && phoneNumber.length() > 0) {
            ContentResolver contentResolver = ctx.getContentResolver();

            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

            String[] projection = new String[]{ContactsContract.PhoneLookup._ID};

            Cursor cursor = contentResolver.query(uri, projection, null, null, null);

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
                cursor.close();
            }
        }
        return contactId;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {

        Drawable drawable = AppBaseApplication.context().getResources().getDrawable(R.drawable.user_icons);
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap bitmap2 = zoomBitmap(bitmap, width, height);
        Bitmap roundedCornerBitmap;
        roundedCornerBitmap = getRoundedCornerBitmap(bitmap2, width / 2);
        return roundedCornerBitmap;
    }

    public static Bitmap zoomBitmap(Bitmap bitmap, int w, int h) {
        Bitmap newbmp = null;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Matrix matrix = new Matrix();
            float scaleWidht = ((float) w / width);
            float scaleHeight = ((float) h / height);
            matrix.postScale(scaleWidht, scaleHeight);
            newbmp = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        }
        return newbmp;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float roundPx) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff000000;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap getBitmapByUri2(Uri uri, Context c) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            bitmap = null;
        }
        return bitmap;
    }

    public static Bitmap VectorToBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Drawable VectorToDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        return drawable;
    }

    public static Bitmap drawableToBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap.Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
        Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return bitmap;
    }

}