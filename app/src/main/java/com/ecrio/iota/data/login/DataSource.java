package com.ecrio.iota.data.login;

import android.support.annotation.NonNull;

import com.ecrio.iota.model.LoginRequest;

/**
 * Main class for accessing iota API.
 * <p>
 * onSuccess() and onFailed() callbacks are used to inform the user of errors or successful operations.
 */
public interface DataSource {

    void register(LoginRequest loginRequest, @NonNull GetRegisterCallback callback);

    void getCurrentUser(@NonNull GetUserCallback callback);

    void setRegistrationInvoked(boolean value);

    void quit();

    interface GetRegisterCallback {

        void onSuccess();

        void onFailed(String s);

    }

    interface GetUserCallback {

        void onSuccess(String username, String password, String address, int remotePort);

        void onFailed(String s);

    }

}