package com.ecrio.iota.data.login;

import android.support.annotation.NonNull;

import com.ecrio.iota.model.LoginRequest;

import static com.google.common.base.Preconditions.checkNotNull;

public class IotaRepository implements DataSource {

    private static IotaRepository INSTANCE = null;

    private final DataSource mTasksRemoteDataSource;

    private final DataSource mTasksLocalDataSource;

    // Prevent direct instantiation.
    private IotaRepository(@NonNull DataSource tasksRemoteDataSource, @NonNull DataSource tasksLocalDataSource) {
        mTasksRemoteDataSource = checkNotNull(tasksRemoteDataSource);
        mTasksLocalDataSource = checkNotNull(tasksLocalDataSource);

    }


    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param tasksRemoteDataSource the backend data source
     * @param tasksLocalDataSource  the device storage data source
     * @return the {@link IotaRepository} instance
     */
    public static IotaRepository getInstance(DataSource tasksRemoteDataSource, DataSource tasksLocalDataSource) {

        if (INSTANCE == null) {

            INSTANCE = new IotaRepository(tasksRemoteDataSource, tasksLocalDataSource);

        }
        return INSTANCE;
    }

    @Override
    public void register(LoginRequest loginRequest, @NonNull final GetRegisterCallback callback) {

        checkNotNull(callback);

        // If the app is not registered we need to call register API.
        mTasksRemoteDataSource.register(loginRequest, new GetRegisterCallback() {

            @Override
            public void onSuccess() {

                callback.onSuccess();

            }

            @Override
            public void onFailed(String s) {

                callback.onFailed("Error in application register.");

            }

        });

    }

    @Override
    public void getCurrentUser(@NonNull GetUserCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.getCurrentUser(new GetUserCallback() {

            @Override
            public void onSuccess(String username, String password, String address, int remotePort) {

                callback.onSuccess(username, password, address, remotePort);

            }

            @Override
            public void onFailed(String s) {

                callback.onFailed(null);
//                callback.onSuccess("1234567891", "12345", "192.168.196.2", 7070);

            }

        });

    }

    @Override
    public void setRegistrationInvoked(boolean value) {
        mTasksRemoteDataSource.setRegistrationInvoked(false);
    }

    @Override
    public void quit() {
        mTasksRemoteDataSource.quit();
    }

}