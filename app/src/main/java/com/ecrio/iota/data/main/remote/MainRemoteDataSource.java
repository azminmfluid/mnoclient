package com.ecrio.iota.data.main.remote;

import android.content.Context;
import android.support.annotation.NonNull;

import com.ecrio.iota.Conversation;
import com.ecrio.iota.Iota;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.main.DataSource;
import com.ecrio.iota.utility.Log;

import javax.inject.Inject;

/**
 * Created by Mfluid on 5/28/2018.
 */

public class MainRemoteDataSource implements DataSource {

    @Inject
    public Iota mIota;

    public MainRemoteDataSource() {
        AppBaseApplication.getInstance().getComponent().inject(this);
        Log.Debug("_instance from iota model", mIota + "");
    }

    private static MainRemoteDataSource INSTANCE;

    public static MainRemoteDataSource getInstance(Context context) {
        if (INSTANCE == null) {

            INSTANCE = new MainRemoteDataSource();
        }
        return INSTANCE;
    }

    @Override
    public void getNewChat(@NonNull GetNewChatCallback callback) {
        Log.DebugApi("Added listener for new conversation");
        mIota.addNewConversationListener(new Iota.NewConversationListener() {
            @Override
            public void onConversationCreated(Iota iota, Conversation conversation) {

                Log.Debug(" Received \"onConversationCreated\"");
            }
        });
    }

}