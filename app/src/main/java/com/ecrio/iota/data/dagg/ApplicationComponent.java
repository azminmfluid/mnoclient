package com.ecrio.iota.data.dagg;

import com.ecrio.iota.data.chat.local.ChatLocalDataSource;
import com.ecrio.iota.data.chat.remote.ChatRemoteDataSource;
import com.ecrio.iota.data.login.remote.IotaRemoteDataSource;
import com.ecrio.iota.data.main.remote.MainRemoteDataSource;
import com.ecrio.iota.ui.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Mfluid on 5/17/2018.
 */
@Singleton
@Component(modules = {ApplicationModule.class, IotaModule.class, DatabaseModule.class})
public interface ApplicationComponent {

    void inject(MainActivity target);
    void inject(IotaRemoteDataSource target);
    void inject(ChatRemoteDataSource target);
    void inject(ChatLocalDataSource target);
    void inject(MainRemoteDataSource target);
}