package com.ecrio.iota.data.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.ecrio.iota.R;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.utility.ImageCache;

import java.util.HashMap;

/**
 * Created by user on 2018.
 */

public class DrawableCache implements ImageCache {
    private static HashMap<Integer, Bitmap> drawableCache = new HashMap<>();
    private static HashMap<Integer, Drawable> proPicCache = new HashMap<>();
    private static HashMap<String, Drawable> contactPicCache = new HashMap<>();

    public static Bitmap getTypeface(int status, Context context) {

        Bitmap bitmap;

        bitmap = drawableCache.get(status);

        if (bitmap == null) {

            try {
                bitmap = AvatarHelper.VectorToBitmap(context, R.drawable.message_status_send);
                if (status == DeliveryStatus.NOT_DELIVERED.ordinal())
                    bitmap = AvatarHelper.VectorToBitmap(context, R.drawable.message_status_send);
                else if (status == DeliveryStatus.DELIVERED.ordinal())
                    bitmap = AvatarHelper.VectorToBitmap(context, R.drawable.message_status_delivered);
                else if (status == DeliveryStatus.READ.ordinal())
                    bitmap = AvatarHelper.VectorToBitmap(context, R.drawable.message_status_read);
                else if (status == DeliveryStatus.FAILED.ordinal())
                    bitmap = AvatarHelper.VectorToBitmap(context, R.drawable.message_status_failed);
                else if (status == DeliveryStatus.QUEUED.ordinal())
                    bitmap = AvatarHelper.VectorToBitmap(context, R.drawable.message_status_queue);
//                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.message_status_failed);
            } catch (Exception e) {
                return null;
            }

            drawableCache.put(status, bitmap);
        }

        return bitmap;
    }

    public static Drawable getProPicBack(int status, Context context) {

        Drawable drawable;

        drawable = proPicCache.get(status);

        if (drawable == null) {
            try {
//                drawable = context.getDrawable(R.drawable.placeholder_user);
                if (status == 0)
                    drawable = AvatarHelper.VectorToDrawable(context, R.drawable.ic_chat_user);
                else
                    drawable = AvatarHelper.VectorToDrawable(context, R.drawable.ic_chat_user_in);
            } catch (Exception e) {
                return null;
            }
            proPicCache.put(status, drawable);
        }
        return drawable;
    }

    public static Drawable getContactPic(String status) {

        Drawable drawable;

        drawable = contactPicCache.get(status);

        return drawable;
    }

    public static Drawable putContactPic(String status, Drawable drawable) {

        Drawable drawable1 = contactPicCache.put(status, drawable);

        return drawable;
    }
}