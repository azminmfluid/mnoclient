package com.ecrio.iota.data.chat.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.base.BaseApplication;
import com.ecrio.iota.data.chat.DataSource;
import com.ecrio.iota.data.db.TasksDbHelper;
import com.ecrio.iota.db.MyDatabase;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.enums.ConversationSender;
import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.model.rich_response.Reply;
import com.ecrio.iota.model.rich_response.ReplyData;
import com.ecrio.iota.model.rich_response.Response;
import com.ecrio.iota.ui.chat.item.file_send.FileData;
import com.ecrio.iota.ui.chat.item.file_send.MFiles;
import com.ecrio.iota.ui.chat.model.CompositionMsg;
import com.ecrio.iota.ui.chat.model.ConversationMsg;
import com.ecrio.iota.ui.file.FileHelper;
import com.ecrio.iota.utility.Log;
import com.google.gson.Gson;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;


public class ChatLocalDataSource implements DataSource {

    private TasksDbHelper mDbHelper;

    @Inject
    MyDatabase mDb;

    // Prevent direct instantiation.
    private ChatLocalDataSource(@NonNull Context context) {
        mDbHelper = new TasksDbHelper(context);

        checkNotNull(context);


        AppBaseApplication.getInstance().getComponent().inject(this);
    }


    private static ChatLocalDataSource INSTANCE;

    public static ChatLocalDataSource getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ChatLocalDataSource(context);
        }
        return INSTANCE;
    }

    @Override
    public void saveMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {
        Log.DebugDB("userID=" + conversationUserID + " conversationInternalID=" + conversationID + " message='" + newMessage + "' progress=not delivered");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, newMessage);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.NOT_DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "-1");
        Uri insert = BaseApplication.context().getContentResolver().insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues);
        String msgID;
        if (insert != null) {
            msgID = insert.getLastPathSegment();
            callback.onSuccess(msgID);
        } else {
            callback.onFailed();
        }
    }

    @Override
    public void saveReplyMessage(int conversationID, int conversationUserID, String newMessage, GetReplySaveCallback callback) {
        Log.DebugDB("userID=" + conversationUserID + " conversationInternalID=" + conversationID + " message='" + newMessage + "' progress=not delivered");
        ReplyData replyData = new ReplyData();
        Response response = new Response();
        Reply reply = new Gson().fromJson(newMessage, Reply.class);
        response.setReply(reply);
        replyData.setResponse(response);
        String responseJson = new Gson().toJson(replyData);
        Log.DebugApi("Reply data " + responseJson);

        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, reply.getDisplayText());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.NOT_DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "-1");
        Uri insert = BaseApplication.context().getContentResolver().insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues);
        String msgID;
        if (insert != null) {
            msgID = insert.getLastPathSegment();
            callback.onSuccess(msgID);
        } else {
            callback.onFailed();
        }
        // For now we do not showing any reply messages in the UI, so it is not inserting to local db. sending only response to server.
//        callback.onSuccess("1");
    }

    @Override
    public void saveMessageReadStatus(int conversationID, int conversationUserID, String msgID, GetChatCallback callback) {
        // updating conversation message as read.
        final ContentResolver cr = BaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.READ.ordinal());
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        values.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] selectionArgs = new String[]{"" + msgID};
        int update = cr.update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION_READ, values, where, selectionArgs);
        if (update < 0) {
            callback.onFailed();
        } else {
            callback.onSuccess("");
        }
    }

    @Override
    public void startIMSession(String[] strings, int sessionType_oneToOne, int uriType_tel, int i, GetChatCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void sendComposing(int conversationID, int type, GetChatCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void sendMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetChatCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void sendReplyMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetReplyCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void sendMessageReadStatus(String conversationID, int status, int msgID, GetChatCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void endIMSession(int conversationID, int conversationUserID, String conversationPhNo, GetChatCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void resetSession(String phoneNumber, int sessionID) {

        // set status of this user(phone no) to 0(requested) and return the user id.
        int userID = mDb.getNewOrUpdatedConversationUserID(phoneNumber, "0");
        Log.DebugDB("user ID of " + phoneNumber + " is : " + userID);

        // update/insert the chat details with the new conversation id generated with this user/phoneNo.
        int ConversationID = mDb.insertOrUpdateNewConversationID(userID, sessionID);
        Log.DebugDB("session ID of " + phoneNumber + " now is :" + ConversationID);

    }

    @Override
    public void saveNewSession(String phoneNumber, int sessionID) {

        Log.Debug(Log.TAG_IOTA_LOG, ".............................");
        // set status of this user(phone no) to 0(requested) and return the user id.
        int conversationUserID = mDb.getNewOrUpdatedConversationUserID(phoneNumber, "1");
        Log.Debug(Log.TAG_IOTA_LOG, "ID of " + phoneNumber + " is : " + conversationUserID);

        // update/insert the chat details with the new conversation id generated with this user/phoneNo.
        int ConversationID = mDb.insertOrUpdateNewConversationID(conversationUserID, sessionID);
        Log.Debug(Log.TAG_IOTA_LOG, "conversation ID of " + phoneNumber + " is :" + ConversationID);

        Log.Debug(Log.TAG_IOTA_LOG, "-----------inserting new message--------------");
        // creating new object for saving data to conversation table.
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        // set status of this user(phone no) to 0(requested) and return the user id.
        conversationMsg.setUserID(conversationUserID);
        // setting conversationID.
        conversationMsg.setConversationID(ConversationID);
        // setting the message.
        conversationMsg.setMessage("");
        conversationMsg.setReplyId("-1");
        // setting conversationFileID to -1.
        conversationMsg.setConversationFileID(-1);
        // setting progress to 0.
        conversationMsg.setConversationFileProgress("0%");
        // setting isOutGouing value to 0
        // 0-attendee
        // 1-myself
        conversationMsg.setConversationSender(ConversationSender.OUT.ordinal());
        // setting status value to 0
        // 0 - not delivered
        // 1 - delivered
        // 2 -composing
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // 0 - text message
        // 1 - file message
        // 2 - info message
        conversationMsg.setConversationType(ConversationType.IGNORE.ordinal());
        // Insert new conversation to "conversation reply" table
        mDb.insertNewInfoMessage(conversationMsg);
    }

    @Override
    public void saveEstablishedSession(int sessionID) {
        int conversationGroupUserID = mDb.getConversationGroupUserIDLatest(sessionID);
        Log.DebugDB("user ID of session" + sessionID + " is : " + conversationGroupUserID);
        Log.DebugDB("updating user status to 1");
        // updating the conversation status with the user to 1.
        mDb.updateConversationUserStatus(conversationGroupUserID, "1");
        Log.DebugDB("inserting session started info message");
        insertInfoMessage(conversationGroupUserID, sessionID, "");//session started
    }

    @Override
    public void deleteMessage(int conversationID, int conversationUserID, DeleteCallback callback) {
        int i = mDb.deleteConversationMessages(conversationID, conversationUserID);
        callback.onSuccess();
    }

    @Override
    public void sendFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, SendFileCallback callback) {
        // do nothing
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.close();
    }

    @Override
    public void queueFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {
        Log.DebugQueue("inserting file details to file table userID=" + conversationUserID + " sessionID=" + conversationID + " filePath=" + filePath);
        int ConversationFID = -1;
        Log.DebugFile("temporary file SessionID is " + ConversationFID);
        MFiles outgoingFileSessionDetailsNotCompleted = mDb.getOutgoingFileSessionDetailsNotCompleted(ConversationFID);
        // do nothing if there is no file session saved.
        if (outgoingFileSessionDetailsNotCompleted == null) {
            Log.DebugFile("no records found in file table with conversationFileSessionID = " + ConversationFID);
        } else {
            Log.DebugFile("records found in file table with conversationFileSessionID = " + ConversationFID);
        }

        FileData response = new FileData();
        response.setUserId(mDb.getConversationGroupUserIDLatest(conversationID));
        response.setSessionId("" + ConversationFID);
        response.setSender(ConversationSender.OUT.ordinal());
        response.setId(0);
        response.setFileName(FileHelper.getFileName(filePath));
        response.setFilePath(filePath);
        response.setCompleted("0");
//            YtsApplication.mDb.clearDetail(0);
        Long internalFileSessionID = mDb.insertFileSession(response);
        int value = internalFileSessionID.intValue();
        Log.DebugFile("    ");
        callback.onSuccess(value);
    }

    @Override
    public void queueFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] thumb, SaveFileCallback callback) {
        Log.DebugQueue("inserting file message to queue userID=" + conversationUserID + " sessionID=" + conversationID + " fileSessionID=" + fileSessionID + " filePath=" + filePath + " state=queued");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, fileSessionID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.FILE.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, filePath);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.QUEUED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "-1");
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "0%");
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_THUMB, thumb);
        Uri insert = BaseApplication.context().getContentResolver().insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues);
        if (insert != null) {
            String lastPathSegment = insert.getLastPathSegment();
            callback.onSuccess(lastPathSegment);
        } else {
            callback.onFailed();
        }
    }

    @Override
    public void saveFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {
        Log.DebugQueue("inserting file message to db userID=" + conversationUserID + " sessionID=" + conversationID + " fileSessionID=" + fileSessionID + " filePath=" + filePath + " state=queued");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, fileSessionID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.FILE.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, filePath);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.NOT_DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "-1");
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_PROGRESS, "0%");
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_THUMB, bytes);
        Uri insert = BaseApplication.context().getContentResolver().insert(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues);
        if (insert != null) {
            String lastPathSegment = insert.getLastPathSegment();
            callback.onSuccess(lastPathSegment);
        } else {
            callback.onFailed();
        }
    }

    @Override
    public void queueMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {
        Log.DebugQueue("inserting message to queue UserID=" + conversationUserID + " sessionID=" + conversationID + " message=" + newMessage + " state=queued");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, newMessage);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.QUEUED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "-1");
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        Uri insert = AppBaseApplication.context().getContentResolver().insert(TableConversationsContract.ChatEntry.CONTENT_URI_QUEUED_CONVERSATIONS, contentValues);
        String lastPathSegment;
        if (insert != null) {
            lastPathSegment = insert.getLastPathSegment();
            callback.onSuccess(lastPathSegment);
        } else {
            callback.onFailed();
        }
    }

    @Override
    public void queueReplyMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {
        Log.DebugQueue("inserting reply to queue UserID=" + conversationUserID + " sessionID=" + conversationID + " message=" + newMessage + " state=queued");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.RICH.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG, newMessage);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.QUEUED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_TIME, System.currentTimeMillis());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_STATUS, "-1");
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, -1);
        Uri insert = AppBaseApplication.context().getContentResolver().insert(TableConversationsContract.ChatEntry.CONTENT_URI_QUEUED_CONVERSATIONS, contentValues);
        String lastPathSegment;
        if (insert != null) {
            lastPathSegment = insert.getLastPathSegment();
            callback.onSuccess(lastPathSegment);
        } else {
            callback.onFailed();
        }
    }

    @Override
    public void updateQueuedMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {
        Log.DebugQueue("updating message in the queue UserID=" + conversationUserID + " sessionID=" + conversationID + " msgID=" + msgID +" status=not delivered");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.NOT_DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + msgID};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_QUEUED_CONVERSATION_BY_USER, contentValues, where, whereArgs);
        callback.onSuccess(update);
    }

    @Override
    public void updateQueuedReplyMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {
        Log.DebugQueue("updating reply in the queue UserID=" + conversationUserID + " sessionID=" + conversationID + " msgID=" + msgID +" status=not delivered");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.NOT_DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.RICH.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + msgID};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_QUEUED_CONVERSATION_BY_USER, contentValues, where, whereArgs);
        callback.onSuccess(update);
    }

    @Override
    public void updateFailedMessage(int conversationID, int conversationUserID, String msgID, UpdateMsgCallback callback) {
        Log.DebugDB("Message :- updating failed message UserID=" + conversationUserID + " conversationID=" + conversationID + " msgID=" + msgID + " progress=failed");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.FAILED.ordinal());
//        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + msgID};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues, where, whereArgs);
    }

    @Override
    public void updateDeliveredMessage(int conversationID, int conversationUserID, String msgID, UpdateMsgCallback updateMsgCallback) {
        Log.Debug("Message :- updating message UserID=" + conversationUserID + " conversationID=" + conversationID + " msgID=" + msgID + " progress=delivered");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.DELIVERED.ordinal());
//        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + msgID};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues, where, whereArgs);
    }

    @Override
    public void resetReply(int conversationID, int userID) {
        CompositionMsg compositionMsg = new CompositionMsg();
        // setting userID.
        compositionMsg.setUserID(userID);
        // setting conversationID.
        compositionMsg.setConversationID(conversationID);
        // setting status.
        compositionMsg.setCompositionState(Integer.parseInt("0"));
        // Insert new conversation to "conversation reply" table
//        YtsApplication.mDb.insertNewCompositionMessage(compositionMsg);

        final ContentResolver cr = AppBaseApplication.context().getContentResolver();
        ContentValues values = new ContentValues();

        values.put(TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID, compositionMsg.getConversationID());

        values.put(TableReplyContract.CompositionEntry.COLUMN_STATUS, compositionMsg.getCompositionState());

        values.put(TableReplyContract.CompositionEntry.COLUMN_USER_ID, compositionMsg.getUserID());

        values.put(TableReplyContract.CompositionEntry.COLUMN_MESSAGE, "null");

        Uri insert = cr.insert(TableReplyContract.CompositionEntry.CONTENT_URI_ALL_REPLIES, values);

    }

    @Override
    public void updateQueuedFile(int conversationID, int conversationUserID, int fileID, int msgID, UpdateQueueFileCallback callback) {
        Log.DebugFile("updating file in the queue userID=" + conversationUserID + " conversationID=" + conversationID + " msgID=" + msgID + " status=sending");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_FILE_ID, fileID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.NOT_DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.FILE.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + msgID};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_QUEUED_CONVERSATION_BY_USER, contentValues, where, whereArgs);
    }

    @Override
    public void updateFailedFile(int conversationID, int conversationUserID, int msgID, UpdateQueueFileCallback callback) {
        Log.DebugFile("updating failed file in the queue UserID=" + conversationUserID + " conversationID=" + conversationID + " msgID=" + msgID + " status=failed");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.FILE.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.FAILED.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + msgID};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues, where, whereArgs);
    }

    @Override
    public void updateInternalSession(String phoneNumber, String sessionID) {
        int conversationUserID = mDb.getNewOrUpdatedConversationUserID(phoneNumber, "0");
        Log.Debug(Log.TAG_IOTA_LOG, "ID of " + phoneNumber + " is : " + conversationUserID);
        int updatedInternalSessionID = mDb.getUpdatedInternalSessionID(conversationUserID, sessionID);

    }

    private void insertInfoMessage(int conversationGroupUserID, int conversationID, String requested) {
        // creating new object for saving data to conversation table.
        ConversationMsg conversationMsg = new ConversationMsg();
        // setting userID.
        // set status of this user(phone no) to 0(requested) and return the user id.
        conversationMsg.setUserID(conversationGroupUserID);
        // setting conversationID.
        conversationMsg.setConversationID(conversationID);
        // setting the message.
        conversationMsg.setMessage(requested);
        conversationMsg.setReplyId("-1");
        // setting conversationFileID to -1.
        conversationMsg.setConversationFileID(-1);
        // setting progress to 0.
        conversationMsg.setConversationFileProgress("0%");
        // setting isOutGouing value to 0
        // 0-attendee
        // 1-myself
        conversationMsg.setConversationSender(ConversationSender.OUT.ordinal());
        // setting status value to 0
        // 0 - not delivered
        // 1 - delivered
        // 2 -composing
        conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
        // set time to current time
        conversationMsg.setTime(System.currentTimeMillis());
        // 0 - text message
        // 1 - file message
        // 2 - info message
        conversationMsg.setConversationType(ConversationType.INFO.ordinal());
        // Insert new conversation to "conversation reply" table
//        mDb.insertNewInfoMessage(conversationMsg);
    }

    @Override
    public void sendQueuedFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {

    }

}