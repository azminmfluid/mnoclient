package com.ecrio.iota.data.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.utility.ImageCache;
import com.ecrio.iota.utility.Utilities;

import java.util.HashMap;

/**
 * Created by user on 2018.
 */

public class DpToPxCache implements ImageCache {
    private static HashMap<Integer, Integer> drawableCache = new HashMap<>();

    public static Integer getMediaHeightPixel(int dp) {

        Integer pixel;

        pixel = drawableCache.get(dp);

        if (pixel == null) {

            try {
                Float dimension = AppBaseApplication.context().getResources().getDimension(dp);
                pixel = Utilities.dpToPx(AppBaseApplication.context(), dimension.intValue());
            } catch (Exception e) {
                return Utilities.dpToPx(AppBaseApplication.context(), 200);
            }

            drawableCache.put(dp, pixel);
        }

        return pixel;
    }
    public static Integer getMediaWidthPixel(int dp) {

        Integer pixel;

        pixel = drawableCache.get(dp);

        if (pixel == null) {

                int dimension = AppBaseApplication.context().getResources().getDimensionPixelSize(dp);
                pixel = Utilities.dpToPx(AppBaseApplication.context(), dimension);

            drawableCache.put(dp, pixel);
        }

        return pixel;
    }
}