package com.ecrio.iota.data.chat;

import android.support.annotation.NonNull;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.utility.Log;

import static com.google.common.base.Preconditions.checkNotNull;

public class ChatRepository implements DataSource {

    private static ChatRepository INSTANCE = null;

    private final DataSource mTasksRemoteDataSource;

    private final DataSource mTasksLocalDataSource;

    // Prevent direct instantiation.
    private ChatRepository(@NonNull DataSource tasksRemoteDataSource, @NonNull DataSource tasksLocalDataSource) {
        mTasksRemoteDataSource = checkNotNull(tasksRemoteDataSource);
        mTasksLocalDataSource = checkNotNull(tasksLocalDataSource);

    }


    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param tasksRemoteDataSource the backend data source
     * @param tasksLocalDataSource  the device storage data source
     * @return the {@link ChatRepository} instance
     */
    public static ChatRepository getInstance(DataSource tasksRemoteDataSource, DataSource tasksLocalDataSource) {

        if (INSTANCE == null) {

            INSTANCE = new ChatRepository(tasksRemoteDataSource, tasksLocalDataSource);

        }
        return INSTANCE;
    }

    @Override
    public void startIMSession(String[] phNOs, int sessionType, int uriType, int sessionID, GetChatCallback callback) {

        checkNotNull(callback);

        // set session id to -1
        resetIMSessionDetail(phNOs[0], -1);

        // If the app is not registered we need to call register API.
        mTasksRemoteDataSource.startIMSession(phNOs, sessionType, uriType, sessionID, new GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {

                int conversationUserID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(phNOs[0], "1");

                int updatedInternalSessionID = AppBaseApplication.mDb.getUpdatedInternalSessionID(conversationUserID, conversationID);

                // set new session id
                saveIMSessionDetail(phNOs[0], updatedInternalSessionID);
                // set status to established
                saveEstablishedSession(updatedInternalSessionID);
                callback.onSuccess(conversationID);
            }

            /**
             * if any failure occurred, store the details to the database and perform later.
             */
            @Override
            public void onFailed() {
                int userID2 = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(phNOs[0], "2");
                mTasksLocalDataSource.startIMSession(phNOs, sessionType, uriType, sessionID, new GetChatCallback() {
                    @Override
                    public void onSuccess(String conversationID) {
                        callback.onSuccess(conversationID);
                    }

                    @Override
                    public void onFailed() {
                        callback.onFailed();
                    }
                });
            }
        });
    }

    private void saveIMSessionDetail(String phoneNumber, int sessionID) {

        mTasksLocalDataSource.saveNewSession(phoneNumber, sessionID);

    }

    public void resetIMSessionDetail(String phoneNumber, int sessionID) {

        mTasksLocalDataSource.resetSession(phoneNumber, sessionID);

    }

    @Override
    public void saveMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

        checkNotNull(callback);

        mTasksLocalDataSource.resetReply(conversationID, conversationUserID);

        mTasksLocalDataSource.saveMessage(conversationID, conversationUserID, newMessage, new GetSaveCallback() {
            @Override
            public void onSuccess(String msgID) {
                callback.onSuccess(msgID);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });

    }

    @Override
    public void sendMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetChatCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.sendMessage(conversationID, conversationPhNo, conversationUserID, newMessage, messageId, new GetChatCallback() {
            @Override
            public void onSuccess(String conversationId) {
                mTasksLocalDataSource.resetReply(conversationID, conversationUserID);
                Log.DebugChat(" message send success[\t"+newMessage+"\t]");
                callback.onSuccess(conversationId);
            }

            @Override
            public void onFailed() {
                Log.DebugChat(" message send failed[\t"+newMessage+"\t]");
                mTasksLocalDataSource.updateFailedMessage(conversationID, conversationUserID, messageId, new UpdateMsgCallback() {
                    @Override
                    public void onSuccess() {
                        Log.DebugChat(" message status[failed] updated success");
                        callback.onFailed();
                    }

                    @Override
                    public void onFailed() {
                        Log.DebugChat(" message status[failed] updated failed");
                        callback.onFailed();
                    }
                });

            }
        });
    }

    @Override
    public void saveReplyMessage(int conversationID, int conversationUserID, String newMessage, GetReplySaveCallback callback) {

        checkNotNull(callback);

        // clear suggested replies from UI.
        mTasksLocalDataSource.resetReply(conversationID, conversationUserID);

        mTasksLocalDataSource.saveReplyMessage(conversationID, conversationUserID, newMessage, new GetReplySaveCallback() {
            @Override
            public void onSuccess(String msgID) {
                callback.onSuccess(msgID);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });

    }

    @Override
    public void sendReplyMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetReplyCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.sendReplyMessage(conversationID, conversationPhNo, conversationUserID, newMessage, messageId, new GetReplyCallback() {
            @Override
            public void onSuccess(String conversationIDString) {
                mTasksLocalDataSource.updateDeliveredMessage(conversationID, conversationUserID, messageId, null);
                callback.onSuccess(conversationIDString);
            }

            @Override
            public void onFailed() {
                Log.DebugChat(" reply message send failed[\t"+newMessage+"\t]");
                mTasksLocalDataSource.updateFailedMessage(conversationID, conversationUserID, messageId, new UpdateMsgCallback() {
                    @Override
                    public void onSuccess() {
                        Log.DebugChat(" message status[failed] updated success");
                    }

                    @Override
                    public void onFailed() {
                        Log.DebugChat(" message status[failed] updated failed");
                    }
                });
                callback.onFailed();
            }
        });
    }

    @Override
    public void sendFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, SendFileCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.sendFile(conversationID, conversationPhNo, conversationUserID, filePath, fileID, new SendFileCallback() {

            @Override
            public void onSuccess(int value) {
                callback.onSuccess(value);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void queueFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {
        // saving file details to file table.
        mTasksLocalDataSource.queueFile(conversationID, conversationPhNo, conversationUserID, filePath, fileID, new GetFileCallback() {
            @Override
            public void onSuccess(int value) {
                Log.DebugFile("success saving file details to file table.");
                Log.DebugFile("file internal id is " + value);
                callback.onSuccess(value);
            }

            @Override
            public void onFailed() {
                Log.DebugFile("success saving file details to file table.");
                callback.onFailed();
            }
        });
    }

    @Override
    public void queueFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {

        mTasksLocalDataSource.queueFileMessage(conversationID, conversationUserID, fileSessionID, filePath, bytes, new SaveFileCallback() {
            @Override
            public void onSuccess(String msgID) {
                Log.DebugFile("success saving file message to main chat table.");
                callback.onSuccess(msgID);
            }

            @Override
            public void onFailed() {
                Log.DebugFile("failure saving file message to main chat table.");
                callback.onFailed();
            }
        });

    }

    @Override
    public void saveFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {
        mTasksLocalDataSource.saveFileMessage(conversationID, conversationUserID, fileSessionID, filePath, bytes, new SaveFileCallback() {
            @Override
            public void onSuccess(String msgID) {
                Log.DebugFile("success saving file message to main chat table.");
                callback.onSuccess(msgID);
            }

            @Override
            public void onFailed() {
                Log.DebugFile("failure saving file message to main chat table.");
                callback.onFailed();
            }
        });
    }

    @Override
    public void queueMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

        // clear suggested replies from UI.
        mTasksLocalDataSource.resetReply(conversationID, conversationUserID);

        mTasksLocalDataSource.queueMessage(conversationID, conversationUserID, newMessage, new DataSource.GetSaveCallback() {

            @Override
            public void onSuccess(String msgID) {

                callback.onSuccess(msgID);
            }

            @Override
            public void onFailed() {

            }

        });
    }

    @Override
    public void updateQueuedMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {
        mTasksLocalDataSource.updateQueuedMessage(conversationID, conversationUserID, msgID, new UpdateQueueCallback() {
            @Override
            public void onSuccess(int value) {
                callback.onSuccess(value);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void updateQueuedReplyMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {
        mTasksLocalDataSource.updateQueuedReplyMessage(conversationID, conversationUserID, msgID, new UpdateQueueCallback() {
            @Override
            public void onSuccess(int value) {
                callback.onSuccess(value);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void updateFailedMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback callback) {

    }

    @Override
    public void queueReplyMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

        checkNotNull(callback);

        // clear suggested replies from UI.
        mTasksLocalDataSource.resetReply(conversationID, conversationUserID);

        mTasksLocalDataSource.queueReplyMessage(conversationID, conversationUserID, newMessage, new DataSource.GetSaveCallback() {

            @Override
            public void onSuccess(String msgID) {
                callback.onSuccess(msgID);
            }

            @Override
            public void onFailed() {

            }

        });
    }

    @Override
    public void updateQueuedFile(int conversationID, int conversationUserID, int fileID, int msgID, UpdateQueueFileCallback callback) {
        mTasksLocalDataSource.updateQueuedFile(conversationID, conversationUserID, fileID, msgID, new UpdateQueueFileCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailed() {

            }
        });
    }

    @Override
    public void sendQueuedFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {
        mTasksRemoteDataSource.sendQueuedFile(conversationID, conversationPhNo, conversationUserID, filePath, fileID, new GetFileCallback() {
            @Override
            public void onSuccess(int internalFileSessionID) {
                if (internalFileSessionID >= 0) {
                    callback.onSuccess(internalFileSessionID);
                } else {
                    callback.onFailed();
                }
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void updateFailedFile(int conversationID, int conversationUserID, int msgID, UpdateQueueFileCallback callback) {
        mTasksLocalDataSource.updateFailedFile(conversationID, conversationUserID, msgID, new UpdateQueueFileCallback() {
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void updateInternalSession(String phoneNumber, String sessionID) {
        mTasksLocalDataSource.updateInternalSession(phoneNumber, sessionID);
    }

    @Override
    public void updateDeliveredMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback updateMsgCallback) {

    }

    @Override
    public void resetReply(int conversationID, int userID) {

    }

    @Override
    public void sendComposing(int conversationID, int type, GetChatCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.sendComposing(conversationID, type, new GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {
                callback.onSuccess(conversationID);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void saveMessageReadStatus(int conversationID, int conversationUserID, String msgID, GetChatCallback callback) {

        checkNotNull(callback);

        mTasksLocalDataSource.saveMessageReadStatus(conversationID, conversationUserID, msgID, new GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {
                callback.onSuccess(conversationID);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void sendMessageReadStatus(String conversationID, int status, int msgID, GetChatCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.sendMessageReadStatus(conversationID, status, msgID, new GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {
                callback.onSuccess(conversationID);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void endIMSession(int conversationID, int conversationUserID, String conversationPhNo, GetChatCallback callback) {

        checkNotNull(callback);

        mTasksRemoteDataSource.endIMSession(conversationID, conversationUserID, conversationPhNo, new GetChatCallback() {
            @Override
            public void onSuccess(String conversationID) {
                callback.onSuccess(conversationID);
            }

            @Override
            public void onFailed() {
                callback.onFailed();
            }
        });
    }

    @Override
    public void saveNewSession(String phoneNumber, int sessionID) {

    }

    @Override
    public void resetSession(String phoneNumber, int sessionID) {

    }

    @Override
    public void saveEstablishedSession(int sessionID) {

        mTasksLocalDataSource.saveEstablishedSession(sessionID);
    }

    @Override
    public void deleteMessage(int conversationID, int conversationUserID, DeleteCallback callback) {
        mTasksLocalDataSource.deleteMessage(conversationID, conversationUserID, callback);
    }

}