package com.ecrio.iota.data.login.remote;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.ecrio.iota.Configuration;
import com.ecrio.iota.ConfigurationLoader;
import com.ecrio.iota.ContactInfo;
import com.ecrio.iota.Conversation;
import com.ecrio.iota.Feature;
import com.ecrio.iota.InterfaceException;
import com.ecrio.iota.Iota;
import com.ecrio.iota.OperationFailedException;
import com.ecrio.iota.ResourceState;
import com.ecrio.iota.ResourceType;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.login.DataSource;
import com.ecrio.iota.model.LoginRequest;
import com.ecrio.iota.pref.Constants;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.utility.Log;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class IotaRemoteDataSource implements DataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 2000;

    static String string = "";

    private static IotaRemoteDataSource INSTANCE;

    private static boolean mRegistrationInvoked = false;

    private static boolean mConfigured = false;
//    private final HandlerThread handlerThread;

    @Inject
    Iota mIota;

    @Inject
    ArrayList<Iota.ResourceListener> mListeners;

    @Inject
    ArrayList<Iota.ContactListener> mContactListeners;

    @Inject
    ArrayList<Iota.NewConversationListener> mConversationListeners;

    private final ResourceListener resourceListener = new ResourceListener();
    private boolean isFromSDCard;

    public static IotaRemoteDataSource getInstance(@NonNull Context context) {
        if (INSTANCE == null) {

            INSTANCE = new IotaRemoteDataSource();
        }
        return INSTANCE;
    }

    public IotaRemoteDataSource() {

        AppBaseApplication.getInstance().getComponent().inject(this);

        Log.Debug("_instance from model", mIota + "");

        mConversationListeners.add(new Iota.NewConversationListener() {
            @Override
            public void onConversationCreated(Iota iota, Conversation conversation) {

                Log.Debug(Log.TAG_IOTA_LOG, "onConversationCreated");

            }
        });

        mContactListeners.add(new Iota.ContactListener() {
            @Override
            public void onContactUpdate(Iota iota, ContactInfo contactInfo) {
                Log.Debug(Log.TAG_IOTA_LOG, "onContactUpdate");
            }
        });

        mListeners.add(new Iota.ResourceListener() {
            @Override
            public void onResourceStateUpdate(Iota iota, ResourceType resource, ResourceState status) {
//                Log.Debug(Log.TAG_IOTA_LOG, "addResourceListener");
                if (resource == ResourceType.Network) {
                    if (status == ResourceState.Unavailable) {

                    }
                }
                if (resource == ResourceType.Registration) {

                    if (status == ResourceState.Unavailable) {
                        AppBaseApplication.getInstance().getComponent().inject(IotaRemoteDataSource.this);
                        Log.DebugApi("_instance refreshed: " + mIota);
                    }

                }
                if (resource == ResourceType.Subscription) {
                    if (status == ResourceState.Unavailable) {
                        GetRegisterCallback callback = resourceListener.callback;
                        if (callback != null) {
                            callback.onFailed("");
                        }
                        resourceListener.setCallback(null);
                    }
                }
            }
        });

//        handlerThread = new HandlerThread("MyHandlerThread");
//
//        handlerThread.start();
    }

    @Override
    public void register(LoginRequest request, @NonNull final GetRegisterCallback callback) {

        try {
            Log.d("Config: copying config file.");
            copyConfigFile(request);
        } catch (IOException e) {
            Log.d("Config: Exception in copying config file.");
            e.printStackTrace();
        }

        if (!mConfigured) {

            Log.d("Config: calling configure()");
            configure();

        }

        if (!mRegistrationInvoked) {
            // Simulate network by delaying the execution. handlerThread.getLooper()
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

//                    try {

                        UserInfo mUserInfo = UserInfo.getInstance();
                        mUserInfo.setUser_name(request.getUsername());
                        mUserInfo.setPassword(request.getPassword());
                        mUserInfo.saveCash(AppBaseApplication.context());
                        resourceListener.setCallback(callback);
                        mIota.register(Feature.CPM_PagerMode, Feature.CPM_Chat);

                        mRegistrationInvoked = true;

                        callback.onSuccess();

//                    } catch (OperationFailedException | InterfaceException e) {
//                        UserInfo mUserInfo = UserInfo.getInstance();
//                        mUserInfo.setUser_name("");
//                        mUserInfo.setPassword("");
//                        mUserInfo.saveCash(AppBaseApplication.context());
//
//                        e.printStackTrace();
//                        try {
//                            mIota.deregister();
//                        } catch (InterfaceException | OperationFailedException e1) {
//                            e1.printStackTrace();
//                        }
//                        Log.Error("Returned error message.");
//                        // return failure message
//                        callback.onFailed("Error in application register.");
//
//                    }
                }
            }, SERVICE_LATENCY_IN_MILLIS);

        } else {
            callback.onSuccess();
        }

    }

    @Override
    public void getCurrentUser(@NonNull GetUserCallback callback) {
        try {
            Log.d("Config: copying config file.");
            copyConfigFile(null);
        } catch (IOException e) {
            Log.d("Config: Exception in copying config file.");
            e.printStackTrace();
        }
        Configuration cfg;
        try {
            if (ContextCompat.checkSelfPermission(AppBaseApplication.context(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                cfg = ConfigurationLoader.fromPreset(AppBaseApplication.context(), "user/local.yaml");
            } else {
                InputStream fileInputStream = null;
                fileInputStream = AppBaseApplication.context().getAssets().open("config/local.yaml");

                cfg = new Configuration();
                cfg.load(fileInputStream);
            }
            String privateIdentity = "";
            String password = "";
            String remoteAddress = "";

            password = cfg.getPassword();

            privateIdentity = cfg.getPrivateIdentity();

            String homeDomain = cfg.getHomeDomain();
            String domain = "@" + homeDomain;
            if (privateIdentity.contains(domain)) {
                privateIdentity = privateIdentity.replace(domain, "");
            }
            if (privateIdentity.contains("+")) {
                privateIdentity = privateIdentity.replace(domain, "");
            }

            List<String> remoteAddresses = cfg.getNetwork().getRemoteAddresses();
            if (remoteAddresses != null && remoteAddresses.size() >= 0) {
                remoteAddress = remoteAddresses.get(0);
            }
            int remotePort = cfg.getNetwork().getRemotePort();

            Log.Debug(Log.TAG_IOTA_LOG, "Loaded default configuration preset");

            callback.onSuccess(privateIdentity, password, remoteAddress, remotePort);
        } catch (ParseException | IOException e) {

            e.printStackTrace();

            callback.onFailed(String.format("Could not load default configuration preset: %s", e.getMessage()));

            Log.Debug(Log.TAG_IOTA_LOG, String.format("Could not load default configuration preset: %s", e.getMessage()));

        }

    }

    @Override
    public void setRegistrationInvoked(boolean value) {
        mConfigured = false;
        mRegistrationInvoked = false;
    }

    @Override
    public void quit() {
        try {
//            handlerThread.quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configure() {

        Configuration cfg;

        try {
            cfg = ConfigurationLoader.fromPreset(AppBaseApplication.context(), "user/local.yaml");
//            cfg.setLogFile("/sdcard/IotaLocalTest_App.txt");
            Log.Debug(Log.TAG_IOTA_LOG, "Loaded default configuration preset");

        } catch (ParseException | IOException e) {

            e.printStackTrace();

            cfg = new Configuration();

            Log.Debug(Log.TAG_IOTA_LOG, String.format("Could not load default configuration preset: %s", e.getMessage()));


        }

        try {
            mIota.configure(cfg);
            mConfigured = true;

        } catch (InterfaceException | OperationFailedException e) {

            e.printStackTrace();

            Log.Debug(Log.TAG_IOTA_LOG, String.format("Configuration failed: %s", e.getMessage()));

        }
    }

    private void updateConfigurationDetails(Map<String, Object> root, String phoneNumber, String password, String address, String port) {

        String publicIdentity, privateIdentity;

        String homeDomain = (String) root.get("home-domain");

        AppBaseApplication.mPreferences.edit().putString(Constants.DOMAIN, homeDomain).apply();
//        if (phoneNumber.startsWith("+")) {
//            publicIdentity = phoneNumber;
//            privateIdentity = phoneNumber.replace("+", "");
//        } else {
//            publicIdentity = "+" + phoneNumber;
//            privateIdentity = "" + phoneNumber;
//        }

        if (phoneNumber.startsWith("+")) {
            publicIdentity = phoneNumber;
            privateIdentity = phoneNumber.replace("+", "");
        } else {
            publicIdentity = "" + phoneNumber;
            privateIdentity = "" + phoneNumber;
        }
        publicIdentity = publicIdentity + "@" + homeDomain;
        privateIdentity = privateIdentity + "@" + homeDomain;

        putIfNotNull(root, "public-identity", "sip:" + publicIdentity, true);
        Log.DebugApi("public-identity is " + "sip:" + publicIdentity);
        putIfNotNull(root, "private-identity", privateIdentity, true);
        Log.DebugApi("private-identity is " + privateIdentity);
        putIfNotNull(root, "password", password, true);
        Map<String, Object> network = (Map) root.get("network");

        if (network.get("remote-addresses") instanceof List) {

            List<String> addresses = (List) network.get("remote-addresses");
            addresses.clear();
            addresses.add(address);
            network.put("remote-addresses", addresses);
        }

        network.put("remote-port", port);

        root.put("network", network);

        Map<String, Object> media = (Map) root.get("media");
        putIfNotNull(media, "user-name", publicIdentity, true);
//        root.put("media", media);
    }

    private void copyConfigFile(LoginRequest loginRequest) throws IOException {
        File assetFile = null;

        isFromSDCard = true;

        File sdCardFile = null;
        if (ContextCompat.checkSelfPermission(AppBaseApplication.context(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            sdCardFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/lims/local.yaml");
        }
        if (sdCardFile != null && sdCardFile.exists()) {
            assetFile = sdCardFile;
            isFromSDCard = true;
        } else {
            assetFile = new File("file:///android_asset/config/local.yaml");
            isFromSDCard = false;
        }

        boolean exists = assetFile.exists();

        File tfile = new File(AppBaseApplication.context().getFilesDir().getAbsolutePath() + "/config/local.yaml");

        if (!tfile.exists()) {

            File filesDir = AppBaseApplication.context().getFilesDir();

            installFiles(assetFile, filesDir.getAbsolutePath());

        }

        Yaml yaml = new Yaml();
        File folder = new File(AppBaseApplication.context().getFilesDir().getAbsolutePath() + "/config");
        if (!folder.exists()) {
            folder.mkdir();
        }
        File file = new File(AppBaseApplication.context().getFilesDir().getAbsolutePath() + "/config/local.yaml");
        if (!file.exists()) {
            try {
                boolean newFile = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        InputStream is = new FileInputStream(file);

        Map<String, Object> root = yaml.load(is);

        if (root != null && loginRequest != null) {
            // update new details.
            updateConfigurationDetails(root, loginRequest.getUsername(), loginRequest.getPassword(), loginRequest.getmAddress(), loginRequest.getmPort());

            FileWriter writer = new FileWriter(file.getAbsolutePath());

            yaml.dump(root, writer);
        }

    }

    private static void putIfNotNull(Map<String, Object> root, String key, Object value, boolean override) {
        if (value != null || override) {
            root.put(key, value);
        }

    }

    String[] asset_files = {"config/local.yaml"};
    String[] file_name = {"local.yaml"};

    private synchronized void installFiles(File fromFile, String filePath) {
        try {
            long startTime = System.currentTimeMillis();
            File file = new File(filePath);
            Log.Debug(Log.TAG_IOTA_LOG, "The file path is  " + filePath);
            if (!file.exists()) {
                boolean mkdir = file.mkdir();
            }
            File configFolder = new File(filePath + "/" + "config");
            if (!configFolder.exists()) {
                boolean mkdir = configFolder.mkdir();
            }

            for (int i = 0; i < asset_files.length; i++) {
                file = new File(configFolder + "/" + file_name[i]);
                if (!file.exists()) {
                    InputStream is = null;
                    if (isFromSDCard)
                        is = new FileInputStream(fromFile);
                    else
                        is = AppBaseApplication.context().getAssets().open(asset_files[i]);
                    FileOutputStream fout = new FileOutputStream(file);
                    int c = -1;

                    while ((c = is.read()) != -1)
                        fout.write(c);

                    fout.flush();
                    fout.close();
                    is.close();
                }
            }
            Log.Debug(Log.TAG_IOTA_LOG, "Completion time  "
                    + (System.currentTimeMillis() - startTime));
        } catch (IOException e) {
            Log.Debug(Log.TAG_IOTA_LOG,"Exception in copy file to app storage");
            e.printStackTrace();
        }
    }

    private static class ResourceListener {

        GetRegisterCallback callback;

        public void setCallback(GetRegisterCallback callback) {
            this.callback = callback;
        }
    }
}