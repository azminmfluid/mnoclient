package com.ecrio.iota.data.login.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.ecrio.iota.data.db.TasksDbHelper;
import com.ecrio.iota.data.login.DataSource;
import com.ecrio.iota.model.LoginRequest;

import static com.google.common.base.Preconditions.checkNotNull;


public class IotaLocalDataSource implements DataSource {

    private TasksDbHelper mDbHelper;

    private static IotaLocalDataSource INSTANCE;

    public static IotaLocalDataSource getInstance(@NonNull Context context) {

        if (INSTANCE == null) {

            INSTANCE = new IotaLocalDataSource(context);

        }
        return INSTANCE;
    }

    // Prevent direct instantiation.
    private IotaLocalDataSource(@NonNull Context context) {

        checkNotNull(context);

        mDbHelper = new TasksDbHelper(context);

    }

    @Override
    public void register(LoginRequest loginRequest, @NonNull final GetRegisterCallback callback) {
        // open a read-only database.
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // close database
        db.close();
        // This will be called if the app is registered.
        callback.onSuccess();
        // This will be called if the app is not registered.
        callback.onFailed("Error in application register.");
    }

    @Override
    public void getCurrentUser(@NonNull GetUserCallback callback) {

        callback.onFailed("");
    }

    @Override
    public void setRegistrationInvoked(boolean value) {

    }

    @Override
    public void quit() {

    }

}