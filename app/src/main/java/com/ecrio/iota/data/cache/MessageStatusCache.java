package com.ecrio.iota.data.cache;

import com.ecrio.iota.enums.DeliveryStatus;

import java.util.HashMap;

/**
 * Created by user on 2018.
 */

public class MessageStatusCache {

    private static HashMap<Integer, DeliveryStatus> drawableCache = new HashMap<>();

    public static DeliveryStatus getReadValue() {

        DeliveryStatus deliveryStatus;

        deliveryStatus = drawableCache.get(0);

        if (deliveryStatus == null) {

            deliveryStatus = DeliveryStatus.READ;
            drawableCache.put(0, DeliveryStatus.READ);
        }

        return deliveryStatus;
    }
}
