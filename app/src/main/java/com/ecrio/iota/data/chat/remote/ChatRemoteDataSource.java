package com.ecrio.iota.data.chat.remote;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.ecrio.iota.BotSuggestion;
import com.ecrio.iota.ContentType;
import com.ecrio.iota.Conversation;
import com.ecrio.iota.Direction;
import com.ecrio.iota.InterfaceException;
import com.ecrio.iota.Iota;
import com.ecrio.iota.OperationFailedException;
import com.ecrio.iota.PagerMessage;
import com.ecrio.iota.R;
import com.ecrio.iota.ResourceState;
import com.ecrio.iota.ResourceType;
import com.ecrio.iota.Sender;
import com.ecrio.iota.Session;
import com.ecrio.iota.SessionFile;
import com.ecrio.iota.SessionMessage;
import com.ecrio.iota.SessionRichCard;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.data.chat.DataSource;
import com.ecrio.iota.db.tables.TableConversationsContract;
import com.ecrio.iota.db.tables.TableReplyContract;
import com.ecrio.iota.enums.ConversationSender;
import com.ecrio.iota.enums.ConversationType;
import com.ecrio.iota.enums.DeliveryStatus;
import com.ecrio.iota.enums.RichType;
import com.ecrio.iota.model.rich.Action;
import com.ecrio.iota.model.rich.CalendarAction;
import com.ecrio.iota.model.rich.Content;
import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.model.rich.DialPhoneNumber;
import com.ecrio.iota.model.rich.DialerAction;
import com.ecrio.iota.model.rich.GeneralPurposeCard;
import com.ecrio.iota.model.rich.GeneralPurposeCardCarousel;
import com.ecrio.iota.model.rich.MapAction;
import com.ecrio.iota.model.rich.Message;
import com.ecrio.iota.model.rich.Reply;
import com.ecrio.iota.model.rich.RichCardEntity;
import com.ecrio.iota.model.rich.ShowLocation;
import com.ecrio.iota.model.rich.Suggestion;
import com.ecrio.iota.model.rich.UrlAction;
import com.ecrio.iota.model.rich_response.ReplyData;
import com.ecrio.iota.model.rich_response.Response;
import com.ecrio.iota.pref.Constants;
import com.ecrio.iota.ui.chat.item.file_send.FileData;
import com.ecrio.iota.ui.chat.item.file_send.MFiles;
import com.ecrio.iota.ui.chat.model.CompositionMsg;
import com.ecrio.iota.ui.chat.model.ConversationMsg;
import com.ecrio.iota.ui.file.FileHelper;
import com.ecrio.iota.ui.file.FileUtils;
import com.ecrio.iota.utility.Log;
import com.ecrio.iota.utility.StringUtils;
import com.ecrio.iota.utility.Utilities;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class ChatRemoteDataSource implements DataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 500;

    static String string = "";

    private static ChatRemoteDataSource INSTANCE;

    private static boolean mRegistrationInvoked = false;
    private final Conversation mConversation;
    private final Sender.Listener<SessionMessage> messageListener = new SessionMessageListener();
    private final Sender.Listener<SessionFile> fileListener = new SessionFileListener();
    private final Sender.Listener<SessionRichCard> richListener = new SessionRichListener(0, null, "0");
    private final SessionListener sessionListener = new SessionListener();
    @Inject
    Iota mIota;
    @Inject
    HashMap<String, Session> sessionHashMap;

    @Inject
    HashMap<String, Session.Listener> sessionListenerHashMap;

    @Inject
    ArrayList<Iota.ResourceListener> mIotaResourceListener;

    public HashMap<String, String> messages = new HashMap<>();
    private Session session1;
    private static MediaPlayer mp;

    public static ChatRemoteDataSource getInstance(@NonNull Context context) {
        if (INSTANCE == null) {

            INSTANCE = new ChatRemoteDataSource();
        }
        return INSTANCE;
    }

    private ChatRemoteDataSource() {

        AppBaseApplication.getInstance().getComponent().inject(this);
        mConversation = mIota.newConversation(new Conversation.Listener() {
            @Override
            public void onPagerMessageReceived(Conversation conversation, PagerMessage pagerMessage) {
                Log.DebugApi("Received onPagerMessageReceived");
            }

            @Override
            public void onIncomingSession(Conversation conversation, Session session) {
                Log.DebugApi("Received onIncomingSession");
            }
        });
        mIotaResourceListener.add(new Iota.ResourceListener() {
            @Override
            public void onResourceStateUpdate(Iota iota, ResourceType resource, ResourceState status) {
                if (resource == ResourceType.Network) {
                    if (status == ResourceState.Unavailable) {

                    }
                }
                if (resource == ResourceType.Registration) {
                    if (status == ResourceState.Unavailable) {
                        GetChatCallback callback = sessionListener.callback;
                        if (callback != null) {
                            callback.onFailed();
                        }
                    }
                }
                if (resource == ResourceType.Subscription) {
                    if (status == ResourceState.Unavailable) {

                    }
                }
            }
        });
    }

    // calling new session API.
    @Override
    public void startIMSession(String[] strings, int sessionType_oneToOne, int uriType_tel, int i, GetChatCallback callback) {
        // phone number
        String phoneNumber = StringUtils.getContactToPhoneNumber(strings[0]);
        // domain
        String domain = AppBaseApplication.mPreferences.getString(Constants.DOMAIN, "");
        Log.DebugChat("Number is "+ phoneNumber);
        // delaying the action
        Handler handler = new Handler(Looper.myLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                session1 = sessionHashMap.get(phoneNumber);
                Log.DebugApi("session : " + ((session1 != null)?"found":"not found"));
                if (session1 == null) {
                    try {
                        Log.DebugApi("creating new session");
                        session1 = mConversation.newSession();
                        sessionHashMap.put(phoneNumber, session1);
                        session1.setPeer("sip:" + phoneNumber + "@" + domain);
                        session1.addListener(sessionListener.setCallback(new GetChatCallback() {
                            @Override
                            public void onSuccess(String conversationID) {
                                Log.DebugApi("successfully session Established");
                                callback.onSuccess(conversationID);
                            }

                            @Override
                            public void onFailed() {
                                Log.DebugApi("Failed to Establish session");
                                sessionHashMap.remove(phoneNumber);
                                callback.onFailed();
                            }
                        }));
                        session1.start(false,
                                ContentType.Text,
                                ContentType.IMDN,
                                ContentType.Composing);
                    } catch (OperationFailedException | InterfaceException e) {
                        Log.Error("Exception occurred in creating new session");
                        e.printStackTrace();
                        sessionHashMap.remove(phoneNumber);
                        callback.onFailed();
                    }
                } else {
                    Session.State state = session1.getState();
                    if (state == Session.State.Established) {
                        Log.DebugApi("session is Established");
                        String conversationID = session1.getConversation().getId();
                        Log.DebugApi("conversation ID :-" + conversationID);
                        callback.onSuccess(conversationID);
                    } else {
                        Log.DebugApi("session is not Established");
                        Log.DebugApi("starting new session");
                        try {
                            session1.setPeer("sip:" + phoneNumber + "@" + domain);
                            session1.addListener(sessionListener.setCallback(new GetChatCallback() {
                                @Override
                                public void onSuccess(String conversationID) {
                                    Log.DebugApi("successfully session Established");
                                    callback.onSuccess(conversationID);
                                }

                                @Override
                                public void onFailed() {
                                    Log.DebugApi("Failed to Establish session");
                                    sessionHashMap.remove(phoneNumber);
                                    callback.onFailed();
                                }
                            }));
                            session1.start(false,
                                    ContentType.Text,
                                    ContentType.IMDN,
                                    ContentType.Composing);
                        } catch (OperationFailedException | InterfaceException e) {
                            Log.Error("Exception occurred in start new session");
                            e.printStackTrace();
                            sessionHashMap.remove(phoneNumber);
                            callback.onFailed();
                        }
                    }
                }
            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

        // delaying the action 
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                // return failure message
                callback.onFailed();

            }
        }, SERVICE_LATENCY_IN_MILLIS);

    }

    @Override
    public void sendMessage(int conversationID, String conversationPhNo, int conversationUserID, String newMessage, String messageId, GetChatCallback callback) {
        Log.DebugApi("called new message API...." + conversationPhNo);
        try {
            session1 = sessionHashMap.get(conversationPhNo);
            Log.DebugApi("session : " + (session1 != null));
            if (session1 != null) {
                messages.put(newMessage, messageId);
                session1.sendTextMessage(newMessage, messageListener);
                callback.onSuccess(String.valueOf(conversationID));
            } else {
                Log.Error("session is null");
                callback.onFailed();
            }
        } catch (OperationFailedException | InterfaceException e) {
            Log.Error("Received Exception");
            e.printStackTrace();
            callback.onFailed();
        }
    }

    @Override
    public void saveReplyMessage(int conversationID, int conversationUserID, String newMessage, GetReplySaveCallback callback) {

    }

    /**
     * @param conversationID     Internal ID of the session
     * @param conversationPhNo   Phone Number of the user
     * @param conversationUserID Internal ID of the user
     * @param replyMessage       Reply message from the suggested reply list
     * @param messageId          Internal ID of the message
     * @param callback           Callback registered by the calling view for notification
     */
    @Override
    public void sendReplyMessage(int conversationID, String conversationPhNo, int conversationUserID, String replyMessage, String messageId, GetReplyCallback callback) {
        Log.DebugApi("called reply message API...." + conversationPhNo);
        try {
            session1 = sessionHashMap.get(conversationPhNo);
            Log.DebugApi("session : " + (session1 != null));
            SessionRichCard sessionRichCard = session1.newRichCard();
            if (sessionRichCard != null) {
                ReplyData replyData = new ReplyData();
                Response response = new Response();
                response.setReply(new Gson().fromJson(replyMessage, com.ecrio.iota.model.rich_response.Reply.class));
                replyData.setResponse(response);
                // JSON data to be send as response
                String responseJson = new Gson().toJson(replyData);
                Log.DebugApi("Reply data " + responseJson);
//                byte[] objAsBytes = responseJson.getBytes(StandardCharsets.UTF_8);
                BotSuggestion botSuggestion = sessionRichCard.getBotSuggestion();
                if (botSuggestion != null) {
                    Log.DebugApi("BotSuggestion not null " + responseJson);
                    // set response JSON
                    botSuggestion.setJson(responseJson);
                    botSuggestion.setResponse(true);
                    sessionRichCard.send(new SessionRichListener(conversationID, callback, messageId));
                }
            }
//            callback.onSuccess(String.valueOf(conversationID));
        } catch (OperationFailedException | InterfaceException e) {
            Log.Error("Received Exception");
            e.printStackTrace();
            callback.onFailed();
        }
    }

    @Override
    public void sendFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, SendFileCallback callback) {

        // delaying the action 
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                session1 = sessionHashMap.get(conversationPhNo);
                Log.DebugApi("session : " + (session1 != null));
                sendRich(callback);
//                sendFile(conversationID,filePath,callback);

            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    public void sendFile(int conversationID, String filePath, SendFileCallback callback) {
        SessionFile sessionFile = session1.newFile();
        if (sessionFile != null) {
            Log.Debug("file Session ID is " + conversationID);
            FileData response = new FileData();
            response.setId(0);
            response.setFileName(FileHelper.getFileName(filePath));
            response.setFilePath(filePath);
            response.setSender(ConversationSender.OUT.ordinal());
            response.setSessionId("" + conversationID);
            response.setUserId(AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationID));
            response.setCompleted("0");
            Long internalFileSessionID = AppBaseApplication.mDb.insertFileSession(response);
            MFiles outgoingFileSessionDetailsNotCompleted = AppBaseApplication.mDb.getOutgoingFileSessionDetailsNotCompleted(conversationID);
            // do nothing if there is no file session saved.
            if (outgoingFileSessionDetailsNotCompleted == null) {
                Log.DebugFile("no record found in file table for conversationFileSessionID = " + conversationID);
            } else {
                Log.DebugFile("record found in file table for conversationFileSessionID = " + conversationID);
            }
            int value = internalFileSessionID.intValue();
            try {
                sessionFile.getContent().setData(getBytes(filePath));
                Log.DebugApi("Called send API.");
                sessionFile.send(fileListener);
                callback.onSuccess(value);
            } catch (OperationFailedException | InterfaceException e) {
                e.printStackTrace();
                Log.Error("Received Exception");
                // return failure message
                callback.onFailed();
            }
        }
    }

    public void sendRich(SendFileCallback callback) {
        SessionRichCard sessionRichCard = session1.newRichCard();
        if (sessionRichCard != null) {
            JSONObject jsonObject = new JSONObject();
            byte[] objAsBytes = jsonObject.toString().getBytes(StandardCharsets.UTF_8);
            sessionRichCard.getContent().setData(objAsBytes);
            try {
                sessionRichCard.send(richListener);
                callback.onSuccess(0);
            } catch (OperationFailedException | InterfaceException e) {
                e.printStackTrace();
                callback.onFailed();
            }
        }
    }


    private byte[] getBytes(String filePath) {
        byte[] thumbnailBitmapBytes = new byte[]{};
        Bitmap thumbnailBitmap = FileUtils.getBitmapByPath(filePath); // Get it with your approach
        if (thumbnailBitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnailBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            thumbnailBitmapBytes = stream.toByteArray();
        }
        return thumbnailBitmapBytes;
    }

    @Override
    public void queueFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {

    }

    @Override
    public void queueFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {

    }

    @Override
    public void saveFileMessage(int conversationID, int conversationUserID, int fileSessionID, String filePath, byte[] bytes, SaveFileCallback callback) {

    }

    @Override
    public void queueMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

    }

    @Override
    public void queueReplyMessage(int conversationID, int conversationUserID, String newMessage, GetSaveCallback callback) {

    }

    @Override
    public void updateQueuedMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {

    }

    @Override
    public void updateQueuedReplyMessage(int conversationID, int conversationUserID, int msgID, UpdateQueueCallback callback) {

    }

    @Override
    public void updateFailedMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback callback) {

    }

    @Override
    public void updateQueuedFile(int conversationID, int conversationUserID, int fileID, int msgID, UpdateQueueFileCallback callback) {

    }

    @Override
    public void sendQueuedFile(int conversationID, String conversationPhNo, int conversationUserID, String filePath, int fileID, GetFileCallback callback) {

    }

    @Override
    public void updateFailedFile(int conversationID, int conversationUserID, int msgID, UpdateQueueFileCallback callback) {

    }

    @Override
    public void updateInternalSession(String phoneNumber, String sessionID) {

    }

    @Override
    public void updateDeliveredMessage(int conversationID, int conversationUserID, String messageId, UpdateMsgCallback updateMsgCallback) {
        Log.DebugDB("Message :- updating message status userID=" + conversationUserID + " conversationInternalID=" + conversationID + " msgID=" + messageId + " progress=delivered");
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_USER_ID, conversationUserID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_SESSION_ID, conversationID);
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ADDRESS, ConversationSender.OUT.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_STATE, DeliveryStatus.DELIVERED.ordinal());
        contentValues.put(TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_TYPE, ConversationType.TEXT.ordinal());
        String where = TableConversationsContract.ChatEntry.COLUMN_CONVERSATION_MSG_ID + "=?";
        String[] whereArgs = new String[]{"" + messageId};
        int update = AppBaseApplication.context().getContentResolver().update(TableConversationsContract.ChatEntry.CONTENT_URI_CONVERSATION, contentValues, where, whereArgs);

    }

    @Override
    public void resetReply(int conversationID, int userID) {

    }

    @Override
    public void sendComposing(int conversationID, int type, GetChatCallback callback) {

        // delaying the action 
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                // return failure message
                callback.onFailed();

            }
        }, SERVICE_LATENCY_IN_MILLIS);

    }

    @Override
    public void saveMessageReadStatus(int conversationID, int conversationUserID, String msgID, GetChatCallback callback) {

        // delaying the action 
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                // return failure message
                callback.onFailed();

            }
        }, SERVICE_LATENCY_IN_MILLIS);

    }

    @Override
    public void sendMessageReadStatus(String conversationID, int status, int msgID, GetChatCallback callback) {

        // delaying the action 
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                // return failure message
                callback.onFailed();

            }
        }, SERVICE_LATENCY_IN_MILLIS);

    }

    @Override
    public void endIMSession(int conversationID, int conversationUserID, String conversationPhNo, GetChatCallback callback) {

        // delaying the action 
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                session1 = sessionHashMap.get(conversationPhNo);
                Log.DebugApi("session : " + (session1 != null));
                if (session1 != null) {
                    Log.DebugApi("calling session end");
                    sessionHashMap.remove(conversationPhNo);
                    session1.end();
                }
            }
        }, SERVICE_LATENCY_IN_MILLIS);

    }

    @Override
    public void saveNewSession(String phoneNumber, int sessionID) {

    }

    @Override
    public void resetSession(String phoneNumber, int sessionID) {

    }

    @Override
    public void saveEstablishedSession(int sessionID) {

    }

    @Override
    public void deleteMessage(int conversationID, int conversationUserID, DeleteCallback callback) {

    }

    private class SessionMessageListener implements Sender.Listener<SessionMessage> {

        private SessionMessageListener() {
        }

        @Override
        public void onSendResult(SessionMessage sender, boolean success) {
            String message = new String(sender.getContent().getData(), StandardCharsets.UTF_8);
            Log.DebugApi("status of " + message + " is " + success);
            if (success) {
                if (messages.containsKey(message)) {
                    String messageID = messages.remove(message);
                    if (messageID != null) {
                        int[] result = AppBaseApplication.mDb.getUserIDAndSessionIDFromMsgTable(Integer.parseInt(messageID));
                        int conversationUserID = result[0];
                        int conversationSID = result[1];
                        updateDeliveredMessage(conversationSID, conversationUserID, messageID, null);
                    }
                }
            }
        }

    }

    private class SessionFileListener implements Sender.Listener<SessionFile> {
        private SessionFileListener() {
        }

        @Override
        public void onSendResult(SessionFile sender, boolean success) {
            Log.DebugApi("status of file is " + success);
        }
    }

    private class SessionRichListener implements Sender.Listener<SessionRichCard> {
        private final int conversationID;
        private final GetReplyCallback callback;
        private final String messageID;

        private SessionRichListener(int conversationID, GetReplyCallback callback, String messageId) {
            this.conversationID = conversationID;
            this.callback = callback;
            this.messageID = messageId;
        }

        @Override
        public void onSendResult(SessionRichCard sender, boolean success) {
            Log.DebugApi("status of reply is " + success);
            if (success) {
                Log.DebugApi("reply send successfully");
                if (messageID != null) {
                    if (callback != null)
                        callback.onSuccess(String.valueOf(conversationID));
                }
            } else {
                if (callback != null)
                    callback.onFailed();
            }
        }
    }

    private static class SessionListener implements Session.Listener {

        GetChatCallback callback;

        public SessionListener setCallback(GetChatCallback callback) {
            this.callback = callback;
            return this;
        }

        @Override
        public void onSessionState(Session session, Session.State state) {
            Log.DebugApi("Received onSessionState...." + state);
            try {
                Toast.makeText(AppBaseApplication.context(), "" + state, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            switch (state) {
                case Established:
                    String peer = StringUtils.getTelToPhoneNumber(session.getPeer());
                    Log.DebugApi("peer:-" + peer);
                    String conversationID = session.getConversation().getId();
                    Log.DebugApi("conversation ID:-" + conversationID);
                    String contributionId = session.getContributionId();
                    Log.DebugApi("contribution ID:-" + contributionId);
                    callback.onSuccess(contributionId);
                    break;
                case Ended:
                    String peer1 = StringUtils.getTelToPhoneNumber(session.getPeer());
                    Log.DebugApi("Received peer : " + peer1);
                    String userNumber1 = peer1.replace("+", "");
                    callback.onFailed();
                    Log.DebugApi("Looking for user id of " + userNumber1);
                    int userID1 = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber1, "2");
                    Log.DebugApi("User id : " + userID1);
                    break;
                case ConnectFailed:
                    String peer2 = StringUtils.getTelToPhoneNumber(session.getPeer());
                    Log.DebugApi("Received peer : " + peer2);
                    String userNumber2 = peer2.replace("+", "");
                    callback.onFailed();
                    Log.DebugApi("Looking for user id of " + userNumber2);
                    int userID2 = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber2, "2");
                    Log.DebugApi("User id : " + userID2);
                    break;
                case Ringing:
                    String peer3 = StringUtils.getTelToPhoneNumber(session.getPeer());
                    Log.DebugApi("Received peer : " + peer3);
                    String userNumber3 = peer3.replace("+", "");
                    int userID3 = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber3, "0");
                    Log.DebugApi("User id : " + userID3);

            }

        }

        @Override
        public void onSessionEvent(Session session, SessionMessage sessionMessage) {
//            Log.DebugApi("Received onSessionEvent....");
//            Direction direction = sessionMessage.getDirection();
//            if (direction == Direction.Incoming) {
//                String contributionId = session.getContributionId();
//                Log.DebugApi("Received contributionID : " + contributionId);
//                Log.DebugApi("Received peer : " + session.getPeer().replace("+", "").replaceAll("-",""));
//                String message = new String(sessionMessage.getContent().getData(), StandardCharsets.UTF_8);
//                Log.DebugApi("Received message : " + message);
//                String userNumber = session.getPeer().replace("tel:+", "");
//                Log.DebugApi("Looking for user id " + userNumber);
//                int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
//                Log.DebugApi("user id : " + userID);
//                int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
//                Log.DebugApi("internal session id : " + conversationInternalID);
//                int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
//                Log.DebugApi("conversation group user id : " + conversationGroupUserID);
//                // creating new object for saving data to conversation table.
//                ConversationMsg conversationMsg = new ConversationMsg();
//                // setting userID.
//                conversationMsg.setUserID(conversationGroupUserID);
//                // setting conversationID.
//                conversationMsg.setConversationID(conversationInternalID);
//                // setting reply message.
//                conversationMsg.setMessage(message);
//                // setting reply message.
//                conversationMsg.setReplyId("0");
//                // setting conversationID.
//                conversationMsg.setConversationFileID(-1);
//                // setting conversationID.
//                conversationMsg.setConversationFileProgress("0%");
//                // setting isOutGouing value to 0
//                // 0 - isOutGouing
//                // 1 - myself
//                conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
//                // setting status value to 0
//                // 0 - not delivered
//                // 1 - delivered
//                // 2 - composing
//                // 3 - read
//                conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
//                // set time to current time
//                conversationMsg.setTime(System.currentTimeMillis());
//                // 0 - text message
//                // 1 - file message
//                // 2 - info
//                conversationMsg.setConversationType(ConversationType.TEXT.ordinal());
//                // Insert new conversation to "conversation reply" table
//                Uri uri = AppBaseApplication.mDb.insertNewTextMessages(conversationMsg);
//            }
            Log.DebugApi("Received onSessionEvent....");
            Direction direction = sessionMessage.getDirection();
            if (direction == Direction.Incoming) {
                // ==================== Incoming message handling here ==========================
                Log.DebugApi("-----------*session receiving progress*-----------");
                String conversationID = session.getConversation().getId();
                Log.DebugApi("conversation ID:-" + conversationID);
                String contributionId = session.getContributionId();
                Log.DebugApi("Received contributionID : " + contributionId);
                String peer1 = StringUtils.getTelToPhoneNumber(session.getPeer());
                Log.DebugApi("Received peer : " + peer1);
                String txtMessage = new String(sessionMessage.getContent().getData(), StandardCharsets.UTF_8);
                Log.DebugApi("Received message : " + txtMessage);


                String suggested_chip_list = sessionMessage.getBotSuggestion().getJson();
                Log.d2("SPLITTED_JSOn", "Bot suggestion :- " + suggested_chip_list);

                String richReply = null;
                boolean hasSuggestions = false;
                if (!Strings.isNullOrEmpty(suggested_chip_list)) {
                    try {
                        JSONObject entity = new JSONObject(suggested_chip_list);
                        hasSuggestions = true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.DebugMessage("RichCard:- Received scl : " + suggested_chip_list);
                RichCardEntity entity = null;
                if (hasSuggestions) {
//                                        if(!richMessage.endsWith("}}}"))richMessage+="}";
                    try {
                        String richMessage = String.valueOf(Utilities.readString("richmessage.json"));
                        entity = new Gson().fromJson(richMessage, RichCardEntity.class);
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    }
                    if(entity == null){
                        Toast.makeText(AppBaseApplication.context(), "Invalid Data!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Message message = entity.getMessage();
                    if (message != null) {
                        if (message.isGeneralPurpose()) {
                            Content scl_content = new Gson().fromJson(suggested_chip_list, Content.class);
                            List<Suggestion> scl_suggestions = scl_content.getSuggestions();
                            Log.DebugApi((scl_suggestions !=null && !scl_suggestions.isEmpty())?("has global suggestions "+scl_suggestions.size()):"no global suggestions");
                            Content card_content = message.getGeneralPurposeCard().getContent();
                            Log.DebugApi((card_content == null)?"no content":"has content");
                            // No need to combine individual suggestions to global one.
                            if (card_content != null) {
                                if (card_content.getSuggestions() == null || card_content.getSuggestions().size() == 0) {
                                    card_content.setSuggestions((scl_suggestions==null)?new ArrayList<>():scl_suggestions);
                                }
                                else if(card_content.getSuggestions() != null) {
                                    card_content.getSuggestions().clear();
                                    card_content.setSuggestions((scl_suggestions==null)?new ArrayList<>():scl_suggestions);
                                }
                            }else{
                                // to avoid not displaying suggestions if content is not there in JSON
                                Content contentnew = new Content();
                                contentnew.setSuggestions((scl_suggestions==null)?new ArrayList<>():scl_suggestions);
                                message.getGeneralPurposeCard().setContent(contentnew);
                            }
                        }
                    }
                    try {
                        String combinedRichMessage = new Gson().toJson(entity);
                        if (combinedRichMessage != null)
                            richReply = combinedRichMessage;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                String userNumber = peer1.replace("+", "");
                Log.DebugApi("Looking for user id of " + userNumber);
                int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                Log.DebugApi("User id : " + userID);
                int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                Log.DebugApi("Internal session id : " + conversationInternalID);
                int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                Log.DebugApi("Conversation group user id : " + conversationGroupUserID);
                if (conversationGroupUserID != -1) {
                    Log.DebugApi("Inserting new message details to local db");
                    // creating new object for saving data to conversation table.
                    insertMessageToDB(txtMessage, conversationInternalID, conversationGroupUserID);
                    insertRichReplyToDB(richReply, conversationInternalID, conversationGroupUserID);
                    // showing notification in status bar.
                    String action = "android.intent.action.MAIN";
                    // showNotification(conversationInternalID, AppBaseApplication.context(), message, action, userNumber);
                    playSound();
                }
            } else if (direction == Direction.Outgoing) {
                // ==================== Outgoing message handling here ==========================
                Log.DebugApi("-----------*session creating progress*-----------");
                String contributionId = session.getContributionId();
                Log.DebugApi("Received contributionID : " + contributionId);
                String peer1 = StringUtils.getTelToPhoneNumber(session.getPeer());
                Log.DebugApi("Received peer : " + peer1);
                String message = new String(sessionMessage.getContent().getData(), StandardCharsets.UTF_8);
                Log.DebugApi("Received message : " + message);
                String userNumber = peer1.replace("+", "");
                Log.DebugApi("Looking for user id of " + userNumber);
                int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                Log.DebugApi("User id : " + userID);
                int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                Log.DebugApi("Internal session id : " + conversationInternalID);
                int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                Log.DebugApi("Conversation group user id : " + conversationGroupUserID);

            }
        }

        private void playSound() {
            try {
                if (mp != null && mp.isPlaying()) return;
                if (mp == null)
                    mp = MediaPlayer.create(AppBaseApplication.context(), R.raw.notification);
                mp.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSessionEvent(Session session, SessionFile sessionFile) {
            Log.DebugApi("Received onSessionEvent....");
        }

        @Override
        public void onSessionEvent(Session session, SessionRichCard sessionRichCard) {
            Log.DebugApi("RichCard:- Received onSessionEvent...." + sessionRichCard);
            String contributionId = session.getContributionId();
            Log.DebugApi("RichCard:- Received contributionID : " + contributionId);
            String peer1 = StringUtils.getTelToPhoneNumber(session.getPeer());
            Log.DebugApi("RichCard:- Received peer : " + peer1);
            String richMessage = new String(sessionRichCard.getContent().getData(), StandardCharsets.UTF_8);
            String richReply = null;
            String suggestedchiplist = sessionRichCard.getBotSuggestion().getJson();
            Log.d2("SPLITTED_JSOn", "Bot suggestion :- " + suggestedchiplist);

            boolean isSuggestionInside = true;
            if (!Strings.isNullOrEmpty(suggestedchiplist)) {
                try {
                    JSONObject entity = new JSONObject(suggestedchiplist);
                    isSuggestionInside = false;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.DebugMessage("RichCard:- Received message : " + richMessage);
            Log.DebugMessage("RichCard:- Received scl : " + suggestedchiplist);
            RichCardEntity entity = null;
            if (!isSuggestionInside) {
                try {
                    entity = new Gson().fromJson(richMessage, RichCardEntity.class);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
                if (entity == null) {
                    Toast.makeText(AppBaseApplication.context(), "Invalid Data!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Message message = entity.getMessage();
                if (message != null) {
                    if (message.isGeneralPurpose()) {
                        com.ecrio.iota.model.scl.Content suggestion = new Gson().fromJson(suggestedchiplist, com.ecrio.iota.model.scl.Content.class);
                        List<Suggestion> suggestions = suggestion.getSuggestions();
                        Log.DebugApi((suggestions !=null && !suggestions.isEmpty())?("has global suggestions "+suggestions.size()):"no global suggestions");
                        Content content = message.getGeneralPurposeCard().getContent();
                        Log.DebugApi((content == null)?"no content":"has content");
                        // No need to combine individual suggestions to global one.
                        if (content != null) {
                            if (content.getSuggestions() == null || content.getSuggestions().size() == 0) {
                                content.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                            }
                            else if(content.getSuggestions() != null) {
                                content.getSuggestions().clear();
                                content.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                            }
                        }else{
                            // to avoid not displaying suggestions if content is not there in JSON
                            Content contentnew = new Content();
                            contentnew.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                            message.getGeneralPurposeCard().setContent(contentnew);
                        }
                    } else if (message.isCarousal()) {
                        com.ecrio.iota.model.scl.Content suggestion = new Gson().fromJson(suggestedchiplist, com.ecrio.iota.model.scl.Content.class);
                        List<Suggestion> suggestions = suggestion.getSuggestions();
                        Log.DebugApi((suggestions !=null && !suggestions.isEmpty())?("has global suggestions "+suggestions.size()):"no global suggestions");
                        List<Content> content = message.getGeneralPurposeCardCarousel().getContent();
                        Log.DebugApi((content != null && !content.isEmpty()) ? "has content" : "no content");
//                        if (content != null) {
//                            for (Content content_ : content) {
//                                // No need to combine individual suggestions to global one.
//                                if (content_ != null) {
//                                    if (content_.getSuggestions() == null || content_.getSuggestions().size() == 0) {
//                                        content_.setSuggestions((suggestions == null) ? new ArrayList<>() : suggestions);
//                                    } else if (content_.getSuggestions() != null) {
//                                        // content_.getSuggestions().addAll(suggestions);// no need to combine all suggestions
//                                        content_.getSuggestions().clear();
//                                        content_.setSuggestions((suggestions == null) ? new ArrayList<>() : suggestions);
//                                    }
//                                }else{
//                                    // to avoid not displaying suggestions if content is not there in JSON
//                                    content_ = new Content();
//                                    content_.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
//                                }
//                                break;
//                            }
//                        }else{
                            // to avoid not displaying suggestions if content is not there in JSON
                            Content contentNew = new Content();
                            contentNew.setSuggestions((suggestions==null)?new ArrayList<>():suggestions);
                            ArrayList<Content> contents = new ArrayList<>();
                            contents.add(contentNew);
                            message.getGeneralPurposeCardCarousel().setContent(contents);
//                        }
                    }
                }
                try {
                    String combinedRichMessage = new Gson().toJson(entity);
                    if (combinedRichMessage != null)
                        richReply = combinedRichMessage;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                entity = new Gson().fromJson(richMessage, RichCardEntity.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }

            if (entity == null) {
                Toast.makeText(AppBaseApplication.context(), "Invalid Data!", Toast.LENGTH_SHORT).show();
                return;
            }
            boolean hasData = false;
            Log.DebugRich("===PARSING===");
            int richTYPE = -1;
            if (entity == null) {
                Log.DebugRich("rich card object is null");
            } else {
                Message richMsg = entity.getMessage();
                if (richMsg != null) {
                    if (richMsg.isGeneralPurpose()) {
                        richTYPE = RichType.GENERAL.ordinal();
                        GeneralPurposeCard card = richMsg.getGeneralPurposeCard();
                        if (card != null) {
                            Content content = card.getContent();
                            String title = content.getTitle();
                            Log.DebugRich("title is " + title);
                            String description = content.getDescription();
                            Log.DebugRich("description is " + description);
                            List<Suggestion> suggestions = content.getSuggestions();
                            if (suggestions != null && !suggestions.isEmpty()) {
                                for (Suggestion suggestion : suggestions) {
                                    if (suggestion != null) {
                                        if (suggestion.isReply()) {
                                            Reply reply = suggestion.getReply();
                                            Log.DebugRich(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                            hasData = true;
                                        } else if (suggestion.isAction()) {
                                            Action action = suggestion.getAction();
                                            Log.DebugRich(String.format("action data found \"%s\"", action.getDisplayText()));
                                            if (action.hasDialerAction()) {
                                                DialerAction dialerAction = action.getDialerAction();
                                                if (dialerAction.hasDialPhone()) {
                                                    Log.DebugRich("action is dialer");
                                                    DialPhoneNumber dialPhoneNumber = dialerAction.getDialPhoneNumber();
                                                    if (dialPhoneNumber != null) {
                                                        Log.DebugRich("dialing number is " + dialPhoneNumber.getPhoneNumber());
                                                    }
                                                }
                                            } else if (action.hasURLAction()) {
                                                Log.DebugRich("action is URL");
                                                UrlAction urlAction = action.getUrlAction();
                                                if (urlAction != null) {
                                                    Log.DebugRich("URL is " + urlAction.getOpenUrl().getUrl());
                                                }
                                            } else if (action.hasMapAction()) {
                                                MapAction mapAction = action.getMapAction();
                                                if (mapAction.hasShowLocation()) {
                                                    ShowLocation showLocation = mapAction.getShowLocation();
                                                    if (showLocation != null) {
                                                        Log.DebugRich("Location is " + showLocation.getLocation());
                                                        Log.DebugRich("URL is " + showLocation.getFallbackUrl());
                                                    } else {
                                                        Log.DebugRich("showLocation is " + showLocation);
                                                    }
                                                }
                                            } else if (action.hasCalendarAction()) {
                                                Log.DebugRich("action is Calendar");
                                                CalendarAction calendarAction = action.getCalendarAction();
                                                CreateCalendarEvent createCalendarEvent = calendarAction.getCreateCalendarEvent();
                                                if (createCalendarEvent != null) {
                                                    Log.DebugRich("Title is " + createCalendarEvent.getTitle());
                                                    Log.DebugRich("Description is " + calendarAction.getCreateCalendarEvent().getDescription());
                                                }
                                            }
//                                            hasData = true;
                                        }
                                    }
                                }
                            }
                            hasData = true;
                        }
                    } else if (richMsg.isCarousal()) {
                        richTYPE = RichType.CAROUSAL.ordinal();
                        GeneralPurposeCardCarousel cardCarousel = richMsg.getGeneralPurposeCardCarousel();
                        if (cardCarousel != null) {
                            List<Content> content_s = cardCarousel.getContent();
                            if (content_s != null && !content_s.isEmpty()) {
                                for (Content content_ : content_s) {
                                    String title = content_.getTitle();
                                    Log.DebugRich("title is " + title);
                                    String description = content_.getDescription();
                                    Log.DebugRich("description is " + description);
                                    List<Suggestion> suggestion_s = content_.getSuggestions();
                                    if (suggestion_s != null) {
                                        for (Suggestion suggestion_ : suggestion_s) {
                                            if (suggestion_ != null) {
                                                if (suggestion_.isReply()) {
                                                    Reply reply = suggestion_.getReply();
                                                    Log.DebugRich(String.format("reply data found \"%s\"", reply.getDisplayText()));
                                                    hasData = true;
                                                } else if (suggestion_.isAction()) {
                                                    Action action = suggestion_.getAction();
                                                    Log.DebugRich(String.format("action data found \"%s\"", action.getDisplayText()));
                                                    if (action.hasDialerAction()) {
                                                        DialerAction dialerAction = action.getDialerAction();
                                                        if (dialerAction.hasDialPhone()) {
                                                            Log.DebugRich("action is dialer");
                                                            DialPhoneNumber dialPhoneNumber = dialerAction.getDialPhoneNumber();
                                                            if (dialPhoneNumber != null) {
                                                                Log.DebugRich("dialing number is " + dialPhoneNumber.getPhoneNumber());
                                                            }
                                                        }
                                                    } else if (action.hasURLAction()) {
                                                        Log.DebugRich("action is URL");
                                                        Log.DebugRich("URL is " + action.getUrlAction().getOpenUrl().getUrl());
                                                    } else if (action.hasMapAction()) {
                                                        Log.DebugRich("action is Map");
                                                        MapAction mapAction = action.getMapAction();
                                                        if (mapAction.hasShowLocation()) {
                                                            ShowLocation showLocation = mapAction.getShowLocation();
                                                            if (showLocation != null) {
                                                                Log.DebugRich("Location is " + showLocation.getLocation());
                                                                Log.DebugRich("URL is " + showLocation.getFallbackUrl());
                                                            } else {
                                                                Log.DebugRich("showLocation is " + showLocation);
                                                            }
                                                        } else {
                                                            Log.DebugRich("mapAction is " + mapAction);
                                                        }
                                                    } else if (action.hasCalendarAction()) {
                                                        Log.DebugRich("action is Calendar");
                                                        Log.DebugRich("Title is " + action.getCalendarAction().getCreateCalendarEvent().getTitle());
                                                        Log.DebugRich("Description is " + action.getCalendarAction().getCreateCalendarEvent().getDescription());
                                                    }
//                                                    hasData = true;
                                                }
                                            }
                                        }
                                    }
                                    hasData = true;
                                }
                            }
                        }
                    }
                }
            }
            Log.DebugRich("===COMPLETED===" + hasData);

            if (hasData) {
                String userNumber = peer1.replace("+", "");
                Log.DebugApi("RichCard:- Looking for user id " + userNumber);
                int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
                Log.DebugApi("RichCard:- User id : " + userID);
                int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
                Log.DebugApi("RichCard:- Internal session id : " + conversationInternalID);
                int conversationGroupUserID = AppBaseApplication.mDb.getConversationGroupUserIDLatest(conversationInternalID);
                Log.DebugApi("RichCard:- Conversation group user id : " + conversationGroupUserID);

                int RichCardInternalID = AppBaseApplication.mDb.getUpdatedInternalRichCardID(userID, conversationInternalID);
                Log.DebugApi("RichCard:- Conversation rich card id : " + RichCardInternalID);
                // creating new object for saving data to conversation table.
                insertRichMessageToDB(richMessage, conversationInternalID, conversationGroupUserID, RichCardInternalID, richTYPE);
                insertRichReplyToDB(richReply, conversationInternalID, conversationGroupUserID);
                Log.DebugMessage("Debug:Rich:"+richMessage);
                Log.DebugMessage("Debug:SCL:"+richReply);
            }
        }

        private void insertMessageToDB(String message, int conversationInternalID, int conversationGroupUserID) {
            ConversationMsg conversationMsg = new ConversationMsg();
            // userID.
            conversationMsg.setUserID(conversationGroupUserID);
            // conversationInternalID.
            conversationMsg.setConversationID(conversationInternalID);
            // message.
            conversationMsg.setMessage(message);
            // id of message from api.
            conversationMsg.setReplyId("0");
            // conversationFileID.
            conversationMsg.setConversationFileID(-1);
            // file progress.
            conversationMsg.setConversationFileProgress("0%");
            // incoming
            conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
            // status value to delivered
            conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
            // set time to current time
            conversationMsg.setTime(System.currentTimeMillis());
            // TEXT(0) - text message
            // FILE(1) - file message
            // INFO(2) - info
            // IGNORE(3) - ignore
            conversationMsg.setConversationType(ConversationType.TEXT.ordinal());
            // Insert new conversation to "conversation reply" table
            Uri uri = AppBaseApplication.mDb.insertNewTextMessages(conversationMsg);
            Log.DebugDB("URI for the new message " + uri);
        }

        private void insertRichMessageToDB(String message, int conversationInternalID, int conversationGroupUserID, int RichCardInternalID, int richType) {
            ConversationMsg conversationMsg = new ConversationMsg();
            // setting userID.
            conversationMsg.setUserID(conversationGroupUserID);
            // setting conversationInternalID.
            conversationMsg.setConversationID(conversationInternalID);
            // setting message.
            conversationMsg.setMessage(message);
            // setting id of message.
            conversationMsg.setReplyId("0");
            // setting conversationFileID.
            conversationMsg.setConversationFileID(-1);
            // setting file progress.
            conversationMsg.setConversationFileProgress("0%");
            // setting conversationRichCardID
            conversationMsg.setConversationRichCardID(RichCardInternalID);
            conversationMsg.setRichType(richType);
            // setting message as incoming
            conversationMsg.setConversationSender(ConversationSender.IN.ordinal());
            // setting status value to delivered
            conversationMsg.setMsgState(DeliveryStatus.DELIVERED.ordinal());
            // set time to current time
            conversationMsg.setTime(System.currentTimeMillis());
            // TEXT(0) - text message
            // FILE(1) - file message
            // INFO(2) - info
            // IGNORE(3) - ignore
            conversationMsg.setConversationType(ConversationType.RICH.ordinal());
            // Insert new conversation to "conversation reply" table
            Uri uri = AppBaseApplication.mDb.insertNewTextMessages(conversationMsg);
            Log.Debug("URI for the message " + uri);
        }

        private void insertRichReplyToDB(String message, int conversationInternalID, int conversationGroupUserID) {
            if(Strings.isNullOrEmpty(message))return;
            // The message that the user send.
            String Message = String.valueOf(message);
            // unique id of the this user.
            int conversationUserID = conversationGroupUserID;
            // unique conversation id of the chat occurred with the user
            int ConversationID = conversationInternalID;
            // Insert new conversation to "conversation reply" table

            CompositionMsg compositionMsg = new CompositionMsg();
            // setting userID.
            compositionMsg.setUserID(conversationUserID);
            // setting conversationID.
            compositionMsg.setConversationID(ConversationID);
            // setting status.
            compositionMsg.setCompositionState(Integer.parseInt("1"));
            // Insert new conversation to "conversation reply" table
//        YtsApplication.mDb.insertNewCompositionMessage(compositionMsg);

            final ContentResolver cr = AppBaseApplication.context().getContentResolver();

            ContentValues values = new ContentValues();

            values.put(TableReplyContract.CompositionEntry.COLUMN_CONVERSATION_ID, compositionMsg.getConversationID());

            values.put(TableReplyContract.CompositionEntry.COLUMN_STATUS, compositionMsg.getCompositionState());

            values.put(TableReplyContract.CompositionEntry.COLUMN_USER_ID, compositionMsg.getUserID());

            values.put(TableReplyContract.CompositionEntry.COLUMN_MESSAGE, Message);

            Uri insert = cr.insert(TableReplyContract.CompositionEntry.CONTENT_URI_ALL_REPLIES, values);

        }
    }
}