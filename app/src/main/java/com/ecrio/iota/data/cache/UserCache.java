package com.ecrio.iota.data.cache;

import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.model.ContactDetail;
import com.ecrio.iota.utility.GenricFunction;

import java.util.HashMap;

/**
 * Created by user on 2018.
 */

public class UserCache {

    private static HashMap<Integer, String> userCache = new HashMap<>();
    private static HashMap<Integer, String> phoneNumberCache = new HashMap<>();

    public static void clear() {
        userCache.clear();
    }

    public static void resetUser(String userID){

        try {
            if(userCache.containsKey(Integer.parseInt(userID))){
                userCache.remove(Integer.parseInt(userID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String getUserName(int userID) {

        String userNo;

        userNo = userCache.get(userID);

        if (userNo == null) {

            try {

                userNo = AppBaseApplication.mDb.getConversationPhoneNumber(userID);
                ContactDetail botContactInfo = GenricFunction.getBotContactInfo(AppBaseApplication.context(), userNo);
                if (null != botContactInfo.getContactName()) {
                    userNo = botContactInfo.getContactName();
                } else {
                    ContactDetail contactDetail = GenricFunction.getContactInfo(AppBaseApplication.context(), userNo);
                    if (null != contactDetail.getContactName()) {
                        userNo = contactDetail.getContactName();
                    }
                }
            } catch (Exception e) {
                return "Unknown";
            }

            userCache.put(userID, userNo);
        }

        return userNo;
    }

    public static String getPhoneNumber(int userID) {

        String userNo;

        userNo = phoneNumberCache.get(userID);

        if (userNo == null) {

            try {

                userNo = AppBaseApplication.mDb.getConversationPhoneNumber(userID);

            } catch (Exception e) {
                return "Unknown";
            }

            phoneNumberCache.put(userID, userNo);
        }

        return userNo;
    }
}