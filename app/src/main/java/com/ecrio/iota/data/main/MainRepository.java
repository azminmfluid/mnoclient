package com.ecrio.iota.data.main;

import android.support.annotation.NonNull;

import com.ecrio.iota.data.login.IotaRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Mfluid on 5/28/2018.
 */

public class MainRepository implements DataSource {

    private static MainRepository INSTANCE = null;

    private final DataSource mMainRemoteDataSource;

    private final DataSource mMainLocalDataSource;

    // Prevent direct instantiation.
    private MainRepository(@NonNull DataSource mainRemoteDataSource, @NonNull DataSource mainLocalDataSource) {
        mMainRemoteDataSource = checkNotNull(mainRemoteDataSource);
        mMainLocalDataSource = checkNotNull(mainLocalDataSource);
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param mainRemoteDataSource the backend data source
     * @param mainLocalDataSource  the device storage data source
     * @return the {@link IotaRepository} instance
     */
    public static MainRepository getInstance(DataSource mainRemoteDataSource, DataSource mainLocalDataSource) {

        if (INSTANCE == null) {

            INSTANCE = new MainRepository(mainRemoteDataSource, mainLocalDataSource);

        }
        return INSTANCE;
    }

    @Override
    public void getNewChat(@NonNull GetNewChatCallback callback) {
        mMainRemoteDataSource.getNewChat(new GetNewChatCallback() {
            @Override
            public void onNewChat() {

            }

            @Override
            public void onFailed(String s) {

            }
        });
    }
}