package com.ecrio.iota.test;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.RemoteViews;

import com.ecrio.iota.R;
import com.ecrio.iota.base.AppBaseApplication;
import com.ecrio.iota.model.ContactDetail;
import com.ecrio.iota.ui.chat.model.CompositionMsg;
import com.ecrio.iota.utility.GenricFunction;
import com.ecrio.iota.utility.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Random;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d2("Notification_Data","received");
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String[] phoneNumbers = {"1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999", "1010"};
        String userNumber = phoneNumbers[new Random().nextInt(10)].replace("+", "");
        Log.DebugApi("Looking for user id of " + userNumber);
        int userID = AppBaseApplication.mDb.getNewOrUpdatedConversationUserID(userNumber, "1");
        Log.DebugApi("User id : " + userID);
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        String contributionId = String.valueOf(timeInMillis);
        int conversationInternalID = AppBaseApplication.mDb.getUpdatedInternalSessionID(userID, contributionId);
        String action = "android.intent.action.MAIN";
        showNotification(conversationInternalID, AppBaseApplication.context(), userNumber, action, userNumber);

        GenricFunction.exportDatabse("iotachats.db");

        String data = intent.getStringExtra("sms_body");
        data += "}";
        try {
            JSONObject jsonObject = new JSONObject(data);
            CompositionMsg msg = new CompositionMsg();
            msg.setUserID(jsonObject.getInt("user_id"));
            msg.setConversationID(jsonObject.getInt("user_cid"));
            msg.setCompositionState(jsonObject.getInt("user_status"));
            AppBaseApplication.mDb.insertNewCompositionMessage(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showNotification(int conversationInternalID, Context context, String message, String action, String phoneNumber) {

        //intent used to launch MainActivity
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (launchIntent != null) {
            launchIntent.putExtra("PhoneNumber", phoneNumber);
            if (action != null) {
                launchIntent.setAction(action);
            }
        }

        // use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(AppBaseApplication.context(), (int) System.currentTimeMillis(), launchIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // build notification

        // fetch contact details
        ContactDetail contactinfo = GenricFunction.getContactInfo(context, phoneNumber);

        RemoteViews remoteView = new RemoteViews(AppBaseApplication.context().getPackageName(), R.layout.chat_notification_layout);

        remoteView.setTextViewText(R.id.txt_ContactNumber, message);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        remoteView.setTextViewText(R.id.txt_TimeStamp, GenricFunction.getDate(System.currentTimeMillis(), "hh:mm aa"));

        remoteView.setImageViewResource(R.id.imgVolte, R.mipmap.icon_notification);

        Notification n;

        // checking contact details
        if (null != contactinfo) {
            // fetching contact name.
            if (null != contactinfo.getContactName()) {
                //setting contact name.
                remoteView.setTextViewText(R.id.txt_contactName, contactinfo.getContactName());
            } else {
                // setting phone number if no name found.
                remoteView.setTextViewText(R.id.txt_contactName, phoneNumber);
            }
            // fetching profile icon.
            if (null != contactinfo.getPhotoUri()) {
                // setting contact icon.
                remoteView.setImageViewUri(R.id.imgContactImage, Uri.parse(contactinfo.getPhotoUri()));
            } else {
                // setting default icon.
                remoteView.setImageViewResource(R.id.imgContactImage, R.mipmap.icon_user_notify);
            }
        } else {
            // if contact details are empty then,
            // set phone number instead of contact name for display.
            remoteView.setTextViewText(R.id.txt_contactName, phoneNumber);
            // set default profile icon.
            remoteView.setImageViewResource(R.id.imgContactImage, R.mipmap.icon_user_notify);
        }
        n = new Notification.Builder(AppBaseApplication.context())
                .setContentTitle("New message")
                .setContentText(message)
                .setSmallIcon(getNotificationIcon())
                .setContent(remoteView)
                .setSound(soundUri)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pIntent)
                .setAutoCancel(true).build();
        NotificationManager nm = (NotificationManager) AppBaseApplication.context().getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null)
            nm.notify(conversationInternalID, n);
        else
            Log.Error("Notification manager is null.");
    }
    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT);
        return whiteIcon ? R.mipmap.icon_notification : R.mipmap.icon_notification;
    }
}