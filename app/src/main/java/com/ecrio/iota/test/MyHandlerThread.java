package com.ecrio.iota.test;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

/**
 * Created by Mfluid on 7/5/2018.
 */

class MyHandlerThread extends HandlerThread {
    Handler handler;

    public MyHandlerThread(String name) {
        super(name);
    }

    @Override
    protected void onLooperPrepared() {
        handler = new Handler(getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                // process incoming messages here
                // this will run in non-ui/background thread
            }
        };
    }
}