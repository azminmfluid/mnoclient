package com.ecrio.iota.enums;

public enum DeliveryStatus {

    NOT_DELIVERED, DELIVERED, COMPOSING, READ, FAILED, QUEUED

}
