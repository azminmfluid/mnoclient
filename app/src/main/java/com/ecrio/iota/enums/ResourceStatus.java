package com.ecrio.iota.enums;

import com.ecrio.iota.ResourceState;

/**
 * Created by Mfluid on 5/23/2018.
 */

public enum ResourceStatus {

    STATUS_NETWORK(ResourceState.Unavailable, 0),
    STATUS_REGISTRATION(ResourceState.Unavailable, 1),
    STATUS_SUBSCRIPTION(ResourceState.Unavailable, 2),
    STATUS_CAPABILITIES(ResourceState.Unavailable, 3);

    private ResourceState state;
    private final int categoryID;

    ResourceStatus(ResourceState s, int categoryID){

        state = s;

        this.categoryID = categoryID;
    }

    public ResourceStatus setState(ResourceState state) {
        this.state = state;
        return this;
    }

    public boolean available(){
        return state != null && state.equals(ResourceState.Available);
    }

    public boolean unknown(){
        return state != null && state.equals(ResourceState.Unknown);
    }
}