package com.ecrio.iota.enums;

public enum ChatStatus {

    SESSION_UNAVAILABLE, SESSION_AVAILABLE, SESSION_FAILED, NOT_INITIATED
}
