package com.ecrio.iota.enums;

/**
 * Created by Mfluid on 6/1/2018.
 */

public enum RichAction {
    ACTION_NONE, ACTION_LINK, ACTION_CALL
}