package com.ecrio.iota.enums;

public enum ConversationType {

    TEXT, FILE, INFO, RICH, IGNORE, REPLY

}
