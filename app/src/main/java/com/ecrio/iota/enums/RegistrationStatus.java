package com.ecrio.iota.enums;

/**
 * Created by Mfluid on 5/23/2018.
 */

public class RegistrationStatus {
    ResourceStatus status;

    public RegistrationStatus(ResourceStatus status) {
        this.status = status;
    }

    public ResourceStatus getStatus() {
        return status;
    }
}