package com.ecrio.iota.enums;

/**
 * Created by Mfluid on 5/23/2018.
 */

public class NetworkStatus {
    ResourceStatus status;

    public NetworkStatus(ResourceStatus status) {
        this.status = status;
    }

    public ResourceStatus getStatus() {
        return status;
    }
}