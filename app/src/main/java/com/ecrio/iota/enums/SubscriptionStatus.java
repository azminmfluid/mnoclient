package com.ecrio.iota.enums;

/**
 * Created by Mfluid on 5/23/2018.
 */

public class SubscriptionStatus {
    ResourceStatus status;

    public SubscriptionStatus(ResourceStatus status) {
        this.status = status;
    }

    public ResourceStatus getStatus() {
        return status;
    }
}