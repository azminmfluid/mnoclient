package com.ecrio.iota.enums;

public enum ImdnMsgType {

    NONE, DELIVERED, DISPLAYED
}
