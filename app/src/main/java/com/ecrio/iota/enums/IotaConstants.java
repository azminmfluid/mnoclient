package com.ecrio.iota.enums;

/**
 * Created by Mfluid on 5/18/2018.
 */

public class IotaConstants {

    /**
     * Session type constants
     */
    public static final int sessionType_None = 0;
    public static final int sessionType_OneToOne = 1;
    public static final int sessionType_Adhoc = 2;
    public static final int sessionType_Prearranged = 3;
    public static final int sessionType_Chat = 4;

    /**
     * URI type constants
     */

    public static final int uriType_TEL = 0;
    public static final int uriType_SIP = 1;

    /**
     * no of users
     */

    public static final int users_TEL = 0;
}