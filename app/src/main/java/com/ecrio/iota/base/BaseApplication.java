package com.ecrio.iota.base;

import android.app.Application;
import android.content.Context;

/**
 * Created by azmin on 12/1/17.
 */

public class BaseApplication extends Application {

    static Context _context;

    public static synchronized BaseApplication context() {
        return (BaseApplication) _context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _context = getApplicationContext();
    }
}
