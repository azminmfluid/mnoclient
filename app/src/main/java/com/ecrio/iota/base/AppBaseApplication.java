package com.ecrio.iota.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.ecrio.iota.BuildConfig;
import com.ecrio.iota.Configuration;
import com.ecrio.iota.ConfigurationLoader;
import com.ecrio.iota.Feature;
import com.ecrio.iota.InterfaceException;
import com.ecrio.iota.Iota;
import com.ecrio.iota.OperationFailedException;
import com.ecrio.iota.data.dagg.ApplicationComponent;
import com.ecrio.iota.data.dagg.ApplicationModule;
import com.ecrio.iota.data.dagg.DaggerApplicationComponent;
import com.ecrio.iota.data.dagg.DatabaseModule;
import com.ecrio.iota.data.dagg.IotaModule;
import com.ecrio.iota.db.MyDatabase;
import com.ecrio.iota.pref.UserInfo;
import com.ecrio.iota.ui.login.MyApi;
import com.ecrio.iota.utility.Log;

//import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by azmin on 12/1/17.
 */

public class AppBaseApplication extends BaseApplication {


    private static AppBaseApplication mInstance;

    private static Context mContext;

    public static SharedPreferences mPreferences;

    public static MyDatabase mDb;

    public static final String PACKAGE_NAME = AppBaseApplication.class.getPackage().getName();
    private IotaModule mNetComponent;

    private ApplicationComponent component;
    private Iota mIota;
    public static MyApi myApi;

    @Override
    public void onTerminate() {

        super.onTerminate();
        if (BuildConfig.FLAVOR.equals("prod")) {

            try {
                UserInfo info = UserInfo.getInstance();
                info.readCash(mContext);
                if (info.getLogin_status() == 1)
                    mIota.deregister();
//                Log.d2("BaseApp", "called deregister");
            } catch (InterfaceException | OperationFailedException e) {
                Log.d2("BaseApp", "Exception occurred");
                e.printStackTrace();
            }
            mIota.close();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());

        mInstance = this;

        mContext = this.getApplicationContext();

        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        mDb = MyDatabase.getInstance(this);

        mIota = Iota.newInstance(this);

        myApi = new MyApi();


        //needs to run once to generate it
        component = DaggerApplicationComponent.builder()

                .applicationModule(new ApplicationModule(this))

                .iotaModule(new IotaModule(mIota, myApi, mContext))

                .databaseModule(new DatabaseModule(mDb))
                .build();

        UserInfo instance = UserInfo.getInstance();

        instance.readCash(this);

        if (instance.getLogin_status() == 1) {
            myApi.setCredentials(instance.getUser_name(), instance.getUser_password());
            if (BuildConfig.FLAVOR.equals("prod")) {
                updateGrantedPermissions();
                configure(mIota);
                register();
            }
        }

    }

    private void updateGrantedPermissions() {

        // Retrieve required permissions
        List<Iota.Permission> permissions = mIota.getRequiredPermissions();

        for (Iota.Permission p : permissions) {

            if (ContextCompat.checkSelfPermission(this, p.getPermission()) == PackageManager.PERMISSION_GRANTED) {

                // If permission is already granted, update Iota instance immediately
                mIota.permissionUpdate(p.getPermission(), true);

                Log.Debug(Log.TAG_IOTA_LOG, "Permission already granted: %s", p.getPermission());

            }
        }
    }

    private void configure(Iota iota) {

        Configuration cfg;

        try {
            cfg = ConfigurationLoader.fromPreset(AppBaseApplication.context(), "user/local.yaml");
            Log.Debug(Log.TAG_IOTA_LOG, "Loaded default configuration preset");
        } catch (ParseException | IOException e) {
            e.printStackTrace();
            cfg = new Configuration();
            Log.Debug(Log.TAG_IOTA_LOG, String.format("Could not load default configuration preset: %s", e.getMessage()));
        }

        try {

            iota.configure(cfg);
        } catch (InterfaceException | OperationFailedException e) {
            e.printStackTrace();

            Log.Debug(Log.TAG_IOTA_LOG, String.format("Configuration failed: %s", e.getMessage()));

        }
    }

    private void register() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

//                try {

                mIota.register(Feature.CPM_PagerMode, Feature.CPM_Chat);
                Toast.makeText(context(), "Registered", Toast.LENGTH_SHORT).show();
//                } catch (OperationFailedException | InterfaceException e) {
//                    Log.Error(Log.TXT_CHAT + "Received exception in register");
//                    e.printStackTrace();
//                    try {
//                        mIota.deregister();
//                        mIota.close();
//                    } catch (OperationFailedException | InterfaceException e2) {
//                        Log.Error(Log.TXT_CHAT + "Received exception in de-register");
//                        e.printStackTrace();
//                    }
//                }
            }
        }, 1000);
    }

    public void updateSettings(IotaModule module) {
        //needs to run once to generate it
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .iotaModule(module)
                .databaseModule(new DatabaseModule(mDb))
                .build();
//        configure(module.provideIota());
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public static synchronized AppBaseApplication getInstance() {
        return mInstance;
    }

    public static Context getContext() {
        return mContext;
    }

}