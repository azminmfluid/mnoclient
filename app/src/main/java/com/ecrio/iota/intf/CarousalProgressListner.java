package com.ecrio.iota.intf;

import android.view.View;

import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellView;

/**
 * Created by Mfluid on 6/14/2018.
 */

public interface CarousalProgressListner {
    void onProgressClick(View view, CarousalCellView cell);
}