package com.ecrio.iota.intf;

import android.view.View;

import com.ecrio.iota.model.rich.CreateCalendarEvent;
import com.ecrio.iota.ui.chat.item.rc_carousal.list.CarousalCellView;

/**
 * Created by Mfluid on 6/14/2018.
 */

public interface CarousalInterface {
    void doCall(String phoneNumber);
    void doOpenLink(String linkURL);

    void doReply(String data);

    void openFile(String filePath);

    void downloadFile(View view, CarousalCellView cell);

    void doOpenCalendar(CreateCalendarEvent event);
}