package com.ecrio.iota.pref;

import android.content.Context;
import android.content.SharedPreferences;

public class UserInfo {
    private static UserInfo mInstance;
    private int login_status;
    private String user_password;
    private String user_name;
    private String user_address;
    private Integer status = -1;

    SharedPreferences.Editor save_cash;
    SharedPreferences read_cash;
    private String prevUser;

    public static UserInfo getInstance() {
        if (mInstance == null) {
            mInstance = new UserInfo();
        }
        return mInstance;
    }

    public void saveCash(Context context) {
        save_cash = context.getSharedPreferences(Constants.USER_INFO, Context.MODE_PRIVATE).edit();
        save_cash.putString(Constants.USER_NAME, user_name);
        save_cash.putString(Constants.USER_PASSWORD, user_password);
        save_cash.putString(Constants.USER_ADDRESS, user_address);
        save_cash.putInt(Constants.LOGIN_STATUS, status);
        save_cash.putString(Constants.USER_NAME_PREV, prevUser);
        save_cash.apply();
    }

    public void readCash(Context context) {
        read_cash = context.getSharedPreferences(Constants.USER_INFO, Context.MODE_PRIVATE);
        user_name = read_cash.getString(Constants.USER_NAME, "");
        user_password = read_cash.getString(Constants.USER_PASSWORD, "");
        user_address = read_cash.getString(Constants.USER_ADDRESS, "");
        login_status = read_cash.getInt(Constants.LOGIN_STATUS, -1);
        prevUser = read_cash.getString(Constants.USER_NAME, "");

    }

    public void readCashUserInfo(Context context) {
        read_cash = context.getSharedPreferences(Constants.USER_INFO, Context.MODE_PRIVATE);
        user_name = read_cash.getString(Constants.USER_NAME, "NONE");
        user_password = read_cash.getString(Constants.USER_PASSWORD, "NONE");
        user_address = read_cash.getString(Constants.USER_ADDRESS, "NONE");
        login_status = read_cash.getInt(Constants.LOGIN_STATUS, -1);

    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_password() {
        return user_password;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setLogin_status(int login_status) {
        this.login_status = login_status;
    }

    public int getLogin_status() {
        return login_status;
    }

    public void setPassword(String password) {
        user_password = password;
    }

    public void setStatus(int stat) {
        status = stat;
    }

    public String getPrevUser() {
        return prevUser;
    }

    public void setPrevUser(String prevUser) {
        this.prevUser = prevUser;
    }
}